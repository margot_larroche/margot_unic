default_file='/home/margot/Bureau/temp/BWTparams/computeRFs_params_1718_CXRIGHT_TUN2_spksort180523_cluster_stim2_volt1_ceil1.2.txt'
prefix='/home/margot/Bureau/temp/BWTparams/computeRFs_params_1718_CXRIGHT_TUN2_spksort180523_cluster_stim2_volt1_ceil'
find_str="'noise_ceiling_vals' : [1.2],"
ceil_vals=[1.2,1.4,1.6,1.8,2.,2.2,2.4,2.6,2.8,3.,3.2,3.4,3.6,3.8,4.,4.2,4.4,4.6,4.8,5.]


with open(default_file,'r') as f:
    content=f.read()
for ceil in ceil_vals:    
    modif_content=content.replace(find_str,"'noise_ceiling_vals' : ["+str(ceil)+"],")
    with open(prefix+str(ceil)+'.txt','w') as f:
        f.write(modif_content)
        