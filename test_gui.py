import sys
import param
from utils.param_gui_tools import param_dialog, param_list_string


class param_class(param.Parameterized):    
    param1=param.Boolean(default=True, doc='param1 doc')
    param2=param.ListSelector(default=['option1'], objects=['option1','option2','option3'], doc='param2 doc')
    param3=param.ObjectSelector(default='option1', objects=['option1','option2'], doc='param3 doc')

display_order={'TITLE': ['param1','param2','param3']} 
    

try: pm
except NameError:
    pm=param_class()

    
if (len(sys.argv)<2) or (sys.argv[1]!='skip_dialog'):    
    pm,validated=param_dialog(pm,display_order,max_nrows=20)
else:
    validated=True    
if validated:
    print 'PARAMETERS : \n\n'+param_list_string(pm,display_order)+'\n\n'        
