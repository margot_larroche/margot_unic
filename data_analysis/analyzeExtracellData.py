from extracell_funcs import *   
from twophoton_funcs import plotTimeSeries, addStamps
from utils.volterra_kernels import lsq_kernel, plotk1_multifig, plot_kernel1, diag_kernel2, plot_kernel2, restorek2shape, add_temporal_dim, STA_LR_3D
from miscellaneous.convertVideo import playMovieFromMat, reconstruct_compmovs   
from utils.handlebinaryfiles import load_info_table, save_info_table, multiple_pickleload, multiple_pickledump, dict_to_binary
from elphy_utils.elphy_misc_utils import elphy_moviestim_infotable, elphy_vtags, elphy_rawspikes
from utils.param_gui_tools import param_dialog, param_list_string
from utils.rgb_color_list import colors_dict
import param, sys
from os.path import splitext, basename
from os.path import join as join_paths
from user_specific_paths import data_paths, stim_paths, main_path
import numpy as np
from matplotlib.gridspec import GridSpec
from utils.various_tools import list_repeats, cross_correlation, normalize, gaussianLowpass
from scipy.io import savemat, loadmat
from elphy_utils.elphy_reader import ElphyFile
from elphy_utils.elphy_missing_samples import ElphyFileMissingData
#import pandas as pd
#from lscsm.STA import STA_LR

"""
PARAMETERS
"""


class analyzeExtracell_params(param.Parameterized):    
    # STEPS TO EXECUTE:
    load_metadata=param.Boolean(default=True, doc='Reload neuronal data')
    load_resps=param.Boolean(default=True, doc='Reload neuronal data')
    load_stim=param.Boolean(default=True, doc='Reload stimulus')
    to_compute=param.ListSelector(default=['raster'], objects=['history_coeffs','regularized_STA','volt1','volt2diag','volt2','RF_pred','RF_crosspred','timecourse','mean_timecourse','PSTH','raster','repeats','meanrates','stim_with_meanresp','preferred_stim','stim_mean_rates','crosscorr','max_corr','reliability_vs_rate','neuron_similarity','export_to_file','grating_maps','grating_pair_maps'], doc="List of things to plot")
    #split_by=param.List(default=[], doc='Stimulus features along which to split the analysis')
    split_by=param.Dict(default={}, doc='Stimulus features along which to split the analysis')
    
    # DATA PATHS:
    epinfo_file=param.Filename(default=None, allow_None=True, search_paths=data_paths, doc='Episode info file (can contain episode lengths)') 
    python_data_file=param.Filename(default=None, allow_None=True, search_paths=data_paths, doc='Data objects saved into python (pickle) format')
    python_stim_file=param.Filename(default=None, allow_None=True, search_paths=data_paths, doc='Stim objects saved into python (pickle) format')
    elphy_file=param.Filename(default=None, allow_None=True, search_paths=data_paths, doc='Raw elphy file')
    elphy_structure_file=param.Filename(default=None, allow_None=True, search_paths=data_paths, doc='Text file precising how metadata (fileinfo, epinfo) is structured in the provided Elphy file')
    #raw_spikes_file=param.Filename(default=None,allow_None=True,search_paths=data_paths,doc='Raw elphy spikes file')   
    spikesorting_file=param.Filename(default=None, allow_None=True, search_paths=data_paths, doc='Spikesorted neuronal responses file')   
    stimbank_file=param.Filename(default=None, allow_None=True, search_paths=stim_paths, doc='Main stimulus file (if there are several: name of one of them, the others must be in the same folder), from which variants derive')
    stimbank_info_file=param.Filename(default=None, allow_None=True, search_paths=data_paths, doc='List of stimuli and corresponding characteristics in the provided stimulus bank')
    frameorder_file=param.Filename(default=None, allow_None=True, search_paths=stim_paths, doc='Text file describing the frame order variants effectively used')
    save_path=param.Foldername(default=main_path, search_paths=[main_path], doc='Folder to which the reformatted data will be saved')
    #stiminfo_file=param.Filename(default=None,allow_None=True,search_paths=data_paths,doc='Text file describing the order in which the movies (variants) have been shown and their characteristics')    
    # Need to keep vtags temporarily instead of reading them from elphyfile
    #vtag_file=param.Filename(default='2216_CXLEFT_TUN28_Vtags.mat',search_paths=data_paths,doc='Tag vectors of the experiment: screen refreshs and stim onsets')

    # PROTOCOL INFOS:
    protocol=param.ObjectSelector(default='movies', objects=['elphy_noise','movies'], doc="Type of stimulation protocol")
    #ep_dur=param.Number(default=359998, doc='Episode duration, in tick number')
    #sampling_rate=param.Number(default=30000, doc='Sampling rate of neuronal data acquisition (Hz): divide spike times by this number to convert to seconds')
    
    # DATA PREPROCESSING PARAMETERS:
    stim_numtype=param.ObjectSelector(default='float64', objects=['float32','float64'], doc="Number type for variant stimulus array (choose a smaller one to avoid memory problems). Float16 not allowed because badly handled by numpy functions.")
    crop_range=param.Array(default=np.array([]), doc='Spatial area of the stimulus to keep in memory (to prevent memory overflow)')   
    stim_downsplfact=param.Integer(default=1, doc='Downsampling factor for the main stimulus (by which number of pixels along one dimension will be divided)')
    normalize_stim=param.Boolean(default=True, doc="Normalize the stimulus array (map pixel mean to 0 and std to 1)") 
    blank_val=param.Number(default=0., doc='Value to assign to pixels of blank frames')
    discard_wrong_t0=param.Boolean(default=False, doc='Whether to discard anormal t0 (time of stim appearance) values when realigning data according to Vtags')
    bin_width=param.Number(default=None, allow_None=True, doc='Length (in s) of bin to use for transforming spike lists to firing rate vectors. If None: will be set to stim_dton')
    
    # ANALYSIS PARAMETERS:
    eps_tokeep=param.Array(default=np.array([]), doc='Episode numbers to keep for analysis /!\ DO NOT USE, TO BE DEBUGGED')
    errors_to_discard=param.ListSelector(default=[], objects=['wrong_init_blanks','wrong_dton','wrong_nframes','missing_samples'])
    cells_tokeep=param.List(default=[], doc="Units to keep for analysis (if empty: all)")
    unit_quality_tokeep=param.ListSelector(default=['isolated','sorted_multiunit'], objects=['isolated','sorted_multiunit','raw_multiunit'], doc="Types of units to keep for analysis")
    corr_threshold=param.Number(default=-1., doc='Threshold of mean trial-to-[sum of other trials] response correlation under which units are discarded')
    rate_threshold=param.Number(default=0., doc='Firing rate threshold under which units are discarded')
    RF_valfrac=param.Number(default=0., doc='Fraction of the data to reserve for RF validation (computation of prediction performance on a new dataset)')    
    data_rand_seed=param.Integer(default=None, allow_None=True, doc='Seed for data randomization before RF computing')    
    ROI=param.Array(default=np.array([]), doc='Spatial area to which the stimulus will be restricted for RF computing (but whole stim still kept in memory)')
    ROI_downsplfact=param.Integer(default=1, doc='Downsampling factor for the stimulus ROI used for RF computation')
    RF_tau=param.Integer(default=0, doc='Time delay (number of bins) to set between stim and response before RF computing')
    RF_Ntau=param.Integer(default=5, doc='Number of past bins to include into the stimulus for RF computing')
    RF_history_Ntau=param.Integer(default=0, doc='Number of past *response* bins to include into the stimulus for *adaptative* RF computing') 
    RF_history_crosstalks=param.Boolean(default=True, doc='If a response history term is included in the RF, whether or not to include history of other cells')     
    STA_bias=param.Number(default=0.0001, doc='Laplacian regularization parameter for regularized STA')    
    fourier_space=param.ObjectSelector(default='false', objects=['spatiotemporal','spatial','false'], doc="Compute RFs in Fourier space")
    spike_tol=param.Integer(default=6, doc='Time shift threshold (number of bins) until which two spikes are considered as simultaneous (for duplicate spikes detection)')
    ncells_per_fig=param.Integer(default=8, doc='Number of graphs to display per window')
    fig_size=param.NumericTuple(default=(5,10), doc='Figure size')
    onset_dur=param.Number(default=0.1, doc='Length of interval following stimulus onset that will be considered as onset response')
    offset_dur=param.Number(default=0.1, doc='Length of interval following stimulus offset that will be considered as offset response')
    average_repeats=param.Boolean(default=False, doc="Average repeated trials before RF computation")
    crosscorr_bin=param.Number(default=0.01, doc="PSTH bin size for computation of trial-to-trial cross-correlation")
    crosscorr_max=param.Number(default=0.3, doc="Max jitter for computation of trial-to-trial cross-correlation")
    stim_colors=param.Dict(default={}, doc="Colors with which to label stimulus types")

display_order={'1/ TO RUN':['load_metadata','load_resps','load_stim','to_compute','split_by'],
               '2/ DATA PATHS':['epinfo_file','python_data_file','python_stim_file','elphy_file','elphy_structure_file','spikesorting_file','stimbank_file','stimbank_info_file','frameorder_file','save_path'],
               '3/ PROTOCOL INFOS':['protocol'],
               '4/ DATA PREPROCESSING PARAMETERS': ['stim_numtype','crop_range','stim_downsplfact','normalize_stim','blank_val','discard_wrong_t0','bin_width'],
               '5/ ANALYSIS PARAMETERS': ['eps_tokeep','errors_to_discard','cells_tokeep','unit_quality_tokeep','corr_threshold','rate_threshold','RF_valfrac','data_rand_seed','ROI','ROI_downsplfact','RF_tau','RF_Ntau','RF_history_Ntau','RF_history_crosstalks','STA_bias','fourier_space','spike_tol','ncells_per_fig','fig_size','onset_dur','offset_dur','crosscorr_bin','crosscorr_max','stim_colors']}



try: pm
except NameError:
    pm=analyzeExtracell_params()
    #colors='rgbycmk'
#    n_colors=6
#    color_map=plt.cm.get_cmap('hsv',n_colors)
#    colors=[color_map(i) for i in range(n_colors)]
#    np.random.shuffle(colors)
#    with open('/media/margot/DATA/Margot/ownCloud/CODE/Python_code/utils/rgb_color_list.txt','r') as f:
#        colors_dict=eval('{'+f.read()+'}')
    #colors=[[v/255. for v in colors[name]] for name in sorted(colors.keys())]
    colors=[colors_dict[name] for name in ['dark_green','dark_blue','light_blue','light_red','orange','yellow','light_green']]
    
if (len(sys.argv)<2) or (sys.argv[1]!='skip_dialog'):    
    pm,validated=param_dialog(pm,display_order, max_nrows=20)
else:
    validated=True    
if validated:
    print 'PARAMETERS : \n\n'+param_list_string(pm,display_order)+'\n\n'    

    
    """
    RUN
    """    
    for varname in ['train_stim','train_rates','val_stim','val_rates','train_history','val_history','train_pred_rates','val_pred_rates','train_pred_corrs','val_pred_corrs','cross_pred_rates','cross_pred_corrs','RF']:
        if not(vars().has_key(varname)):
            vars()[varname]={}


#A rajouter : sauvegarde variables au fur et a mesure via pickle
######################## LOAD METADATA ##########################

    if pm.load_metadata:
        print "\nLoading metadata\n"
        
        assert (pm.epinfo_file is not None) or np.all([pm.elphy_file is not None, pm.elphy_structure_file is not None, pm.stimbank_info_file is not None])
        
        if pm.epinfo_file is not None:
            ep_info=load_info_table(pm.epinfo_file)
            
        else:
            stimbank_infotable=load_info_table(pm.stimbank_info_file)
            try: # /!\ Passer a ElphyFile simple quand les fichiers auront ete nettoyes
                ef=ElphyFile(pm.elphy_file,read_data=False) 
            except:
                ef=ElphyFileMissingData(pm.elphy_file,read_data=False)
            nEp=ef.n_episodes  
            ep_info=elphy_moviestim_infotable(ef,stimbank_infotable,pm.elphy_structure_file)
            ep_info['n_samples']=np.array([ep.channels[0].nsamp for ep in ef.episodes])
            ep_info['n_missing']=np.array([0 if ep.channels[0].nmissing is None else ep.channels[0].nmissing for ep in ef.episodes])
        
        # nmissing a enlever quand les fichiers seront corriges
        ep_durs=ep_info['n_samples']
        theo_ep_durs=ep_info['n_samples']+ep_info['n_missing']
        recording_errors=(ep_durs!=theo_ep_durs)    
        stim_repeats,_=list_repeats(ep_info['stim_id'])
        repeated_trials=np.array(list(itertools.chain(*stim_repeats.values())))

    

######################## LOAD RESPONSES ##########################    
    
    if pm.load_resps:
        print "\nLoading responses\n"

        assert (pm.python_data_file is not None) or (pm.elphy_file is not None)
               
        if pm.python_data_file is not None:
            objects=multiple_pickleload(pm.python_objects_file)
            spikelist,units,vtags,sampling_rate=[objects.pop(key) for key in ['spikelist','units','vtags','sampling_rate']]
            del objects
                    
        else:
            
            if not(pm.load_metadata):
                try:
                    ef=ElphyFile(pm.elphy_file,read_data=False)
                except:
                    ef=ElphyFileMissingData(pm.elphy_file,read_data=False)
            sampling_rate=1000./ef.episodes[0].dxu

            ################# spikes ######################
            spikelist={}
            units={'raw_multiunit': [], 'sorted_multiunit': [], 'isolated': []}
    
            if pm.elphy_file is not None:
                temp=elphy_rawspikes(ef)
                spikelist.update(temp)
                units['raw_multiunit']=sorted(temp.keys())  
                del temp
            if pm.spikesorting_file is not None:
                if splitext(pm.spikesorting_file)[1]=='.mat':
                    datatype='elphy'
                    temp,nEp=load_elphyspikes_matfmt(pm.spikesorting_file,ep_durs) # /!\  to adapt for variable ep_dur
                    all_units=sorted(temp.keys())
                    units['isolated']=[all_units[i_unit] for i_unit in which_singleunit(all_units)]
                    units['sorted_multiunit']=np.array(list(set(all_units)-set(units['isolated'])))
                elif splitext(pm.spikesorting_file)[1]=='.kwik':
                    datatype='klusta'
                    temp,cluster_groups=loadKlustaSpikes(pm.spikesorting_file)
                    units['isolated']=sorted(cluster_groups['Good'])
                    units['sorted_multiunit']=sorted(cluster_groups['MUA'])
                else:
                    raise ValueError('Spike file should be either .mat (elphy) or .kwik (klusta)') 
                spikelist.update(temp)
                del temp
            assert len(spikelist)>0
            assert max([spkl[-1] for spkl in spikelist.values()])<theo_ep_durs.sum()
                
            ################# Vtags ######################        
            # /!\ A changer : charger les vtags depuis le fichier elphy    
            vtags=elphy_vtags(ef)
            
        # Extract info from Vtags    
        stim_dton,stim_dt_errors,stim_t0,stim_t0_errors=checkVtags(vtags[0])
        if pm.bin_width is None:
            pm.bin_width=stim_dton            
        ep_info['stim_dt_error'],ep_info['stim_t0_error']=stim_dt_errors,stim_t0_errors  
        ep_info['actual_nframes']=np.array([len(vtags[0][iep]) for iep in range(nEp)])
        stim_nframes_errors=(ep_info['actual_nframes']!=ep_info['theo_nframes'])
        ep_errors=(stim_nframes_errors | stim_dt_errors | stim_t0_errors | recording_errors)
        print 'Number of valid eps :', (~ep_errors).sum(), '/', len(ep_errors) 
        
        
        # Extract list of units from spikelist
        n_units=np.sum([len(l) for l in units.values()])
        all_units=sorted(units['isolated']+units['sorted_multiunit'])+units['raw_multiunit']
        is_isolated=np.array([True if str(u) in [str(u) for u in units['isolated']] else False for u in all_units]) # str necessary because of weird interactions between tuples and numpy.int
        is_rawchannel=np.array([False]*(len(units['isolated'])+len(units['sorted_multiunit'])) + [True]*len(units['raw_multiunit']))
        unit_types=np.array(['isolated' if is_isolated[i_unit] else ('raw_multiunit' if is_rawchannel[i_unit] else 'sorted_multiunit') for i_unit in range(n_units)])
        unit_info={'spikesorting': unit_types}        
    	
        # Correct spikelist based on Vtags and compute PSTHs (rates) /!\ to simplify ????
        len_groups,temp=list_repeats(theo_ep_durs)    
        len_groups.update(temp)
        del temp             
        if pm.discard_wrong_t0:
            temp_valid_eps=(~stim_t0_errors)
        else:
            temp_valid_eps=np.ones(nEp).astype(bool)
        rates,spikelist,corrected_vtag0,init_nbins,ep_nbins,ep_cuts=spikelist_to_frate(spikelist.copy(),ep_durs,pm.bin_width*sampling_rate,[np.array(vt)*1./30000*sampling_rate for vt in vtags[0]],valid_eps=temp_valid_eps,unit_order=all_units,theo_lengths=theo_ep_durs)
        theo_stimnbins={l: len(vtags[0][np.where((~ep_errors) & (ep_durs==l))[0][0]]) for l in len_groups.keys()}
        theo_stimnbins=np.array([theo_stimnbins[l] for l in theo_ep_durs])
        theo_fnbins=ep_nbins-init_nbins-theo_stimnbins
        onset_nbins=int(np.ceil(pm.onset_dur*1./pm.bin_width))
        if np.any(init_nbins+onset_nbins+theo_fnbins>=ep_nbins):
            print "\nWarning : some episodes are shorter than provided onset duration\n"
        vtags[0]=[vt*30000./sampling_rate for vt in corrected_vtag0]
        mean_rates=np.nanmean(np.concatenate([rates[iep][init_nbins[iep]:ep_nbins[iep]-theo_fnbins[iep]] for iep in range(nEp)], axis=0), axis=0)*1./pm.bin_width
    

              
######################## LOAD STIM ##########################  
        
    if pm.load_stim:
        print "\nLoading stim\n"
        
        if pm.python_stim_file is not None:
             objects=multiple_pickeload(pm.python_stim_file)
             stim=objects.pop('stim')
             del objects
             
        elif pm.protocol=='elphy_noise':
            # /!\ Probably obsolete
            stim=loadNoiseMat(pm.stimbank_file,np.arange(nEp)).astype(pm.stim_numtype)
            #stim=loadNoiseMat(pm.stimbank_file,pm.eps_tokeep).astype(pm.stim_numtype)
            frameshape=(int(np.sqrt(stim.shape[1])),)*2
            stim=stim.reshape((nEp,-1)+frameshape)
            #stim=stim.reshape((len(pm.eps_tokeep),-1)+frameshape)
            if len(pm.crop_range)>0:
                stim=stim[:,:,pm.crop_range[0][0]:pm.crop_range[0][1],pm.crop_range[1][0]:pm.crop_range[1][1]]
             
        else:     
            
            if pm.stimbank_file[-4:]=='.mat':
                stim=loadmat(pm.stimbank_file)['stim'][[ind for ind in ep_info['stim_num']]]
            else:    
                stim=loadTexStim(ep_info['stim_id'],movie_prefix=pm.stimbank_file,downsample_fact=pm.stim_downsplfact,crop_range=pm.crop_range,num_type=pm.stim_numtype)      
            frameshape=stim[[istim for istim in range(len(stim)) if stim[istim] is not None][0]].shape[1:]         # /!\ /!\ On suppose que tous les stims ont la meme taille spatiale   

            if pm.frameorder_file is not None:
                stim=np.array(reconstruct_compmovs(stim,pm.frameorder_file,movs_tokeep='all')).astype(pm.stim_numtype) # A CHANGER : STIM DE TAILLE VARIABLE
            else:
                stim=[s.astype(pm.stim_numtype) if s is not None else None for s in stim]
            

        print "\nReconstructing effective stim\n" 
        if pm.normalize_stim:
            stim_mean=np.concatenate([s.flatten() for s in stim if s is not None],axis=0).mean()
            stim_std=np.concatenate([s.flatten() for s in stim if s is not None],axis=0).std()
            stim=[(s-stim_mean)*1./stim_std if s is not None else None for s in stim]
        blank_frame=np.zeros((1,1)+frameshape,dtype=pm.stim_numtype)+pm.blank_val
        stim=[reconstructStim(np.concatenate([blank_frame,stim[iep].reshape((1,)+stim[iep].shape),blank_frame],axis=1),[vtags[0][iep]/30000.],np.arange(ep_nbins[iep]+1)*pm.bin_width,num_type=pm.stim_numtype)[0] if stim[iep] is not None else None for iep in range(nEp)] 


######################## PREPARE ANALYSIS ########################## 
     
    if len(pm.ROI)==0:
        try:
            pm.ROI=np.array([[0,frameshape[0]],[0,frameshape[1]]])
        except:
            print '\nWarning : no stim loaded.\n'
            
    # Episodes to keep
    if len(pm.eps_tokeep)==0:        
        valid_eps=np.ones(nEp).astype(bool) 
    else:
        valid_eps=np.zeros(nEp).astype(bool) 
        valid_eps[pm.eps_tokeep]=True
    if 'wrong_init_blanks' in pm.errors_to_discard:
        valid_eps=valid_eps & (~stim_t0_errors)
    if 'wrong_dton' in pm.errors_to_discard:
        valid_eps=valid_eps & (~stim_dt_errors)  
    if 'wrong_nframes' in pm.errors_to_discard:
        valid_eps=valid_eps & (~stim_nframes_errors)          
    if 'missing_samples' in pm.errors_to_discard:
        valid_eps=valid_eps & (ep_durs==theo_ep_durs) 
    eps_tokeep=np.arange(nEp)[valid_eps]    
       
        
    if len(pm.split_by)>0:
        stim_tuples=[tuple([ep_info[attr][iep] for attr in sorted(pm.split_by.keys())]) for iep in range(nEp)]
        valid_tuples=np.where([np.all([ep_info[attr][iep] in pm.split_by[attr] for attr in pm.split_by.keys()]) for iep in range(nEp)])[0]
        stim_categories={st: np.where(np.all(np.array(stim_tuples)==st,axis=1))[0] for st in set([stim_tuples[istim] for istim in valid_tuples])}
    else:
        stim_categories={}   
    if 'all' not in stim_categories.keys(): 
        stim_categories['all'] = np.arange(nEp) if (len(stim_categories)==0) else np.concatenate(stim_categories.values())
    #stim_categories['all']=np.arange(len(eps_tokeep))    
    cat_repeats={cat: [mov_id for mov_id in stim_repeats.keys() if stim_repeats[mov_id][0] in stim_categories[cat]] for cat in stim_categories.keys()}
    ref_ep=stim_categories.values()[0][0]
    #init_nbins,theo_fnbins,ep_nbins = [init_nbins[ref_ep],theo_fnbins[ref_ep],ep_nbins[ref_ep]] # /!\ /!\ A enlever

    # Cells to keep
    is_valid=np.array([u_type in pm.unit_quality_tokeep for u_type in unit_types])
    if len(pm.cells_tokeep)>0:
        is_valid=(is_valid & np.array([(icell in pm.cells_tokeep) for icell in range(n_units)]))
    is_valid=(is_valid & (mean_rates>=pm.rate_threshold))
    cells_tokeep=np.where(is_valid)[0]     
    #cell_titles=[str(icell)+'\n'+str(units[icell])+'\nr'+str(round(mean_rates[icell],2)) for icell in range(n_units)]
    cell_titles=[str(icell)+'\nr'+str(round(mean_rates[icell],2)) for icell in range(n_units)]
    stim_colors={stim_categories.keys()[ikey]: colors_dict[pm.stim_colors[stim_categories.keys()[ikey]]] if pm.stim_colors.has_key(stim_categories.keys()[ikey]) else colors[ikey%len(colors)] for ikey in range(len(stim_categories))}


######################## ANALYSIS ########################## 

    # Max possible crosscorr
    if 'max_corr' in pm.to_compute:
        max_corrs={}
        norm_max_corrs={}
        normrates_nbins=ep_nbins-(init_nbins+onset_nbins+theo_fnbins)
        for cat in stim_categories.keys():           
            if len(cat_repeats[cat])>0:
                eps={mov_id: sorted(list(set(stim_repeats[mov_id]) & set(eps_tokeep))) for mov_id in cat_repeats[cat]}
                min_nmov=min([len(e) for e in eps.values()])
                nbins={mov_id: normrates_nbins[eps[mov_id]] for mov_id in cat_repeats[cat]}
                assert np.all([len(set(n))==1 for n in nbins.values()])
                normalized_rates=[normalize(np.concatenate([rates[iep][init_nbins[iep]+onset_nbins:-theo_fnbins[iep]] for iep in eps[mov_id][:min_nmov]],axis=0),axis=0).reshape(-1,nbins[mov_id][0],n_units) for mov_id in cat_repeats[cat]]
                norm_max_corrs[cat]=max_possible_crosscorr(np.concatenate(normalized_rates,axis=1))
                norm_max_corrs[cat][np.isnan(norm_max_corrs[cat])]=0.
                max_corrs[cat]=max_possible_crosscorr(np.concatenate([np.array([rates[iep] for iep in stim_repeats[mov_id]]) for mov_id in cat_repeats[cat]],axis=1))
                max_corrs[cat][np.isnan(max_corrs[cat])]=0.
        categories=sorted(norm_max_corrs.keys())
        plt.figure()
        #for icell in cells_tokeep:
        for icell in range(n_units):    
            plt.plot([norm_max_corrs[cat][icell] for cat in categories])
        plt.xticks(range(len(norm_max_corrs)),categories) 
        max_corrs_stimmax=np.array([max([norm_max_corrs[cat][icell] for cat in stim_categories.keys()]) for icell in range(n_units)])
        max_corrs_stimmean=np.array([np.mean([norm_max_corrs[cat][icell] for cat in stim_categories.keys()]) for icell in range(n_units)])
            

    # Cross-correlations
    if (np.any([plot_name in pm.to_compute for plot_name in ['crosscorr','reliability_vs_rate','neuron_similarity']]) or (pm.corr_threshold>-1)) and not(vars().has_key('self_cross_corr')) and (len(stim_repeats)>0):
        #repeated_stimids=sorted(stim_repeats.keys())
        repeated_stimids=sorted([sid for sid in stim_repeats.keys() if ep_info['stim_type'][stim_repeats[sid][0]] not in ['grating_pair','bar_pair']])
        n_jitters=int(pm.crosscorr_max/pm.crosscorr_bin)
        cross_corr=np.zeros([len(repeated_stimids),2*n_jitters-1,n_units,n_units])
        crosscorr_nbins=(((ep_nbins-init_nbins-theo_fnbins)*pm.bin_width-pm.onset_dur)/pm.crosscorr_bin).astype(int)
        
        for istim in range(len(repeated_stimids)):
            ep_list=list(set(stim_repeats[repeated_stimids[istim]]) & set(eps_tokeep))
            assert len(set(crosscorr_nbins[ep_list]))==1
            n_ep=len(ep_list)
            PSTHs=np.zeros([n_ep,crosscorr_nbins[ep_list[0]],n_units])
            for icell in range(n_units):
                for iep in range(n_ep):
                        PSTHs[iep,:,icell]=np.histogram(spikelist[all_units[icell]],(np.arange(init_nbins[ep_list[iep]]*pm.bin_width+pm.onset_dur,(ep_nbins[ep_list[iep]]-theo_fnbins[ep_list[iep]])*pm.bin_width,pm.crosscorr_bin)+pm.bin_width*ep_nbins[:ep_list[iep]].sum())*sampling_rate)[0]
            for icell1 in range(n_units):
                for icell2 in (range(n_units) if ('neuron_similarity' in pm.to_compute) else [icell1]): 
                    temp_crosscorr=np.zeros([n_ep,2*n_jitters-1])                   
    #                for ep1 in range(n_ep):
    #                    for ep2 in range(ep1+1,n_ep):
    #                        cross_corr[istim,:,icell]+=cross_correlation(PSTHs[ep1],PSTHs[ep2],n_jitters)*2./(n_ep*(n_ep-1))
                    for iep in range(n_ep):
                        temp_crosscorr[iep]=cross_correlation(PSTHs[iep,:,icell1],PSTHs[:iep,:,icell2].sum(axis=0)+PSTHs[iep+1:,:,icell2].sum(axis=0),n_jitters)
                    cross_corr[istim,:,icell1,icell2]=np.nanmean(temp_crosscorr,axis=0)

        self_cross_corr=cross_corr.diagonal(axis1=2,axis2=3)
        mean_corr=np.nanmean(self_cross_corr[:,n_jitters-1,:],axis=0)            
        if pm.corr_threshold>-1:
            is_valid = (is_valid & np.any(self_cross_corr[:,n_jitters-1,:]>=pm.corr_threshold,axis=0))
            cells_tokeep=np.where(is_valid)[0]
            
    if vars().has_key('self_cross_corr'):        
        cell_titles=[cell_titles[icell]+'\nc'+str(round(mean_corr[icell],2)) for icell in range(n_units)]  
        
    
    if 'crosscorr' in pm.to_compute:   
        repeated_stimids=sorted([sid for sid in stim_repeats.keys() if ep_info['stim_type'][stim_repeats[sid][0]] not in ['grating_pair','bar_pair']])
        repeated_stimtypes=np.array([ep_info['stim_type'][stim_repeats[sid][0]] for sid in repeated_stimids])
        cell_order=cells_tokeep[np.argsort(-mean_corr[cells_tokeep])]
        cell_alphas=np.maximum(mean_corr[cell_order],0) # /!\ A tester
        cell_alphas=(cell_alphas-np.nanmin(cell_alphas))*0.8/(np.nanmax(cell_alphas)-np.nanmin(cell_alphas))+0.2
        #cell_alphas=(cell_alphas-np.nanmin(cell_alphas))*1./(np.nanmax(cell_alphas)-np.nanmin(cell_alphas))
        reduced_cross_corr=np.array([np.nanmean(self_cross_corr[repeated_stimtypes==st],axis=0) for st in sorted(list(set(repeated_stimtypes)))])
        reduced_sem=np.array([np.nanstd(self_cross_corr[repeated_stimtypes==st],axis=0)*2./np.sqrt(np.sum(repeated_stimtypes==st)) for st in sorted(list(set(repeated_stimtypes)))])
        plotby_cellandstim(reduced_cross_corr[:,:,cell_order],val_errs=reduced_sem,cell_labels=[cell_titles[icell] for icell in cell_order],bold_cells=is_isolated[cell_order],stim_labels=sorted(list(set(repeated_stimtypes))),x=np.arange(-n_jitters+1,n_jitters)*pm.crosscorr_bin,x_label='Time (s)',ncells_per_fig=pm.ncells_per_fig,stim_colors=[stim_colors[(st,)] for st in sorted(list(set(repeated_stimtypes)))],cell_colors=[(1-a,)*3 for a in cell_alphas])        
        plt.figure()
        plt.plot(self_cross_corr[:,n_jitters-1,cell_order])
        plt.xticks(range(len(repeated_stimids)),repeated_stimids)
        plt.xlabel('Stim id')
        plt.ylabel('Mean cross-correlation between repeated trials')
        
#    if (('crosscorr' in pm.to_compute) or ('reliability_vs_rate' in pm.to_compute) or (pm.corr_threshold>-1)) and (len(stim_repeats)>0):
#        repeated_stimids=stim_repeats.keys()
#        n_jitters=int(pm.crosscorr_max/pm.crosscorr_bin)
#        cross_corr=np.zeros([len(stim_repeats),2*n_jitters-1,n_units])
#        for icell in range(n_units):
#            for istim in range(len(stim_repeats)):
#                ep_list=stim_repeats[repeated_stimids[istim]]
#                n_ep=len(ep_list)
#                crosscorr_nbins=int(((ep_nbins-init_nbins-theo_fnbins)*pm.bin_width-pm.onset_dur)/pm.crosscorr_bin)
#                PSTHs=np.zeros([n_ep,crosscorr_nbins])
#                temp_crosscorr=np.zeros([n_ep,2*n_jitters-1])
#                for iep in range(n_ep):
#                    PSTHs[iep]=np.histogram(spikelist[units[icell]],(np.arange(init_nbins*pm.bin_width+pm.onset_dur,(ep_nbins-theo_fnbins)*pm.bin_width,pm.crosscorr_bin)+ep_nbins*pm.bin_width*ep_list[iep])*sampling_rate)[0]
##                for ep1 in range(n_ep):
##                    for ep2 in range(ep1+1,n_ep):
##                        cross_corr[istim,:,icell]+=cross_correlation(PSTHs[ep1],PSTHs[ep2],n_jitters)*2./(n_ep*(n_ep-1))
#                for iep in range(n_ep):
#                    temp_crosscorr[iep]=cross_correlation(PSTHs[iep],PSTHs[:iep].sum(axis=0)+PSTHs[iep+1:].sum(axis=0),n_jitters)
#                cross_corr[istim,:,icell]=np.nanmean(temp_crosscorr,axis=0)
#        
#        mean_corr=np.nanmean(cross_corr[:,n_jitters-1,:],axis=0)            
#        if pm.corr_threshold>-1:
#            is_valid=is_valid and np.any(cross_corr[:,n_jitters-1,:]>=pm.corr_threshold,axis=0)
#            cells_tokeep=np.where(is_valid)[0]
#        
#        if 'crosscorr' in pm.to_compute:      
#            count=pm.ncells_per_fig
#            for icell in cells_tokeep:
#                if count>pm.ncells_per_fig:
#                    plt.figure()
#                    count=1
#                plt.subplot(pm.ncells_per_fig,1,count)
#                for istim in range(len(stim_repeats)):
#                    plt.plot(np.arange(-n_jitters+1,n_jitters)*pm.crosscorr_bin,cross_corr[istim,:,icell],colors[istim])
#                    plt.ylabel(units[icell],weight=['normal','bold'][is_isolated[icell]])
#                    plt.axis([(-n_jitters+1)*pm.crosscorr_bin,(n_jitters-1)*pm.crosscorr_bin,-0.05,0.4])
#                if count==1:    
#                    plt.legend(repeated_stimids)  
#                count+=1  
#            plt.figure()  
#            plt.subplot(2,1,1)
#            plt.plot(np.arange(-n_jitters+1,n_jitters)*pm.crosscorr_bin,np.nanmean(cross_corr[:,:,cells_tokeep],axis=2).T) 
#            plt.legend(repeated_stimids)
#            plt.subplot(2,1,2)
#            plt.plot(np.arange(-n_jitters+1,n_jitters)*pm.crosscorr_bin,np.nanmean(cross_corr[:,:,cells_tokeep],axis=0))


    if 'autocorr' in pm.to_compute:
        stim_types=stim_categories.keys()
        n_jitters=int(pm.crosscorr_max/pm.crosscorr_bin)
        auto_corr=np.zeros([len(stim_types),2*n_jitters-1,n_units])
        autocorr_nbins=int(ep_nbins*pm.bin_width/pm.crosscorr_bin)
        
        for istim in range(len(stim_types)):
            ep_list=list(set(stim_categories[stim_types[istim]]) & set(eps_tokeep))
            n_ep=len(ep_list)
            PSTHs=np.zeros([n_ep,crosscorr_nbins,n_units])
            for icell in range(n_units):
                temp_autocorr=np.zeros([n_ep,2*n_jitters-1])
                for iep in range(n_ep):
                    PSTHs[iep,:,icell]=np.histogram(spikelist[all_units[icell]],(np.arange(0,ep_nbins*pm.bin_width,pm.crosscorr_bin)+ep_nbins*pm.bin_width*ep_list[iep])*sampling_rate)[0]
                    temp_autocorr[iep]=cross_correlation(PSTHs[iep,:,icell],PSTHs[iep,:,icell],n_jitters)                   
                auto_corr[istim,:,icell]=np.nanmean(temp_autocorr,axis=0)
                # REPRENDRE ICI
       
    
    if  any([plotname in pm.to_compute for plotname in ['regularized_STA','volt1','volt2diag','volt2']]):
        
        temp_stim=[downsample(downsample(s[:,pm.ROI[0,0]:pm.ROI[0,1],pm.ROI[1,0]:pm.ROI[1,1]],2,pm.ROI_downsplfact),1,pm.ROI_downsplfact) if s is not None else None for s in stim]
        ROIshape=np.array([s for s in temp_stim if s is not None][0].shape[1:])
        for cat in stim_categories.keys():
            train_stim[cat]=np.array([temp_stim[iep] for iep in stim_categories[cat] if iep in eps_tokeep])
            assert np.all([(s is not None) and (s.shape==train_stim[cat][0].shape) for s in train_stim[cat]])
            train_stim[cat]=train_stim[cat].reshape([-1]+list(ROIshape))
            train_rates[cat]=np.concatenate([rates[iep] for iep in stim_categories[cat] if iep in eps_tokeep], axis=0)
            if pm.fourier_space=='spatiotemporal':
                train_stim[cat],train_rates[cat]=add_temporal_dim(train_stim[cat].reshape(train_stim[cat].shape[0],-1),train_rates[cat],pm.RF_Ntau)
                train_stim[cat]=np.abs(stimFourier(train_stim[cat],pm.RF_Ntau,ROIshape))
            elif pm.fourier_space=='spatial':    
                train_stim[cat]=np.abs(stimFourier(train_stim[cat].reshape(train_stim[cat].shape[0],-1),1,ROIshape))
                train_stim[cat],train_rates[cat]=add_temporal_dim(train_stim[cat],train_rates[cat],pm.RF_Ntau)
            elif pm.fourier_space=='false':
                train_stim[cat],train_rates[cat]=add_temporal_dim(train_stim[cat].reshape(train_stim[cat].shape[0],-1),train_rates[cat],pm.RF_Ntau)
            train_stim[cat]=train_stim[cat][:train_stim[cat].shape[0]-pm.RF_tau,:]
            train_rates[cat]=train_rates[cat][pm.RF_tau:,:]
            train_rates[cat]=(train_rates[cat]-train_rates[cat].mean(axis=0))*1./train_rates[cat].std(axis=0)
            rand_order=np.arange(train_rates[cat].shape[0])
            if pm.data_rand_seed is not None:
                np.random.seed(pm.data_rand_seed)
                rand_order=np.random.permutation(rand_order)   
            training_nbins=int(train_rates[cat].shape[0]*(1-pm.RF_valfrac))
            val_rates[cat]=train_rates[cat][rand_order[training_nbins:]]
            val_stim[cat]=train_stim[cat][rand_order[training_nbins:]]
            train_rates[cat]=train_rates[cat][rand_order[:training_nbins]]
            train_stim[cat]=train_stim[cat][rand_order[:training_nbins]]
            if pm.RF_history_Ntau>0:
                train_history[cat]=add_temporal_dim(np.concatenate([np.zeros([pm.RF_history_Ntau,n_units]),train_rates[cat][:-1,:]]), None, pm.RF_history_Ntau)
                val_history[cat]=add_temporal_dim(np.concatenate([np.zeros([pm.RF_history_Ntau,n_units]),val_rates[cat][:-1,:]]), None, pm.RF_history_Ntau)
                    

    for cat in stim_categories.keys():
        
        if ('history_coeffs' in pm.to_compute) & (pm.RF_history_Ntau>0):
            if not(RF.has_key('history_alone')):
                RF['history_alone']={'h0':{},'history':{}}
            if not(pm.RF_history_crosstalks):
                history_coeffs=[lsq_kernel(train_stim[cat],train_rates[cat][:,icell],orders=['0'],history_ntau=pm.RF_history_Ntau) for icell in cells_tokeep]
                history_coeffs={k: np.concatenate([history_coeffs[icell][k] for icell in range(len(history_coeffs))], axis=1) for k in history_coeffs[0].keys()}
                history_coeffs['history']=np.array([np.diag(history_coeffs['history'][tau]) for tau in range(pm.RF_history_Ntau)]).reshape(-1,len(cells_tokeep))
            else:    
                history_coeffs=lsq_kernel(train_stim[cat],train_rates[cat][:,cells_tokeep],orders=['0'],history_ntau=pm.RF_history_Ntau)        
            RF['history_alone']['h0'][cat],RF['history_alone']['history'][cat]=history_coeffs['0'],history_coeffs['history']
            if pm.RF_history_crosstalks:
                plot_kernel1(RF['history_alone']['history'][cat].reshape(-1,1),ntau=pm.RF_history_Ntau,scalemax='bycell',stimshape='square',title=str(cat))
            else:
                plt.figure()
                plt.plot(np.diagonal(RF['history_alone']['history'][cat].reshape(-1,len(cells_tokeep),len(cells_tokeep)),axis1=1,axis2=2))
                plt.title(cat)
            
        
        if 'regularized_STA' in pm.to_compute: 
            
            if not(RF.has_key('regularized_STA')):
                RF['regularized_STA']={'h1':{}}
            RF['regularized_STA']['h1'][cat]=np.array(STA_LR_3D(np.mat(train_stim[cat]),np.mat(train_rates[cat][:,cells_tokeep]),[pm.RF_Ntau]+list(ROIshape),pm.STA_bias))
            plotk1_multifig(RF['regularized_STA']['h1'][cat],ntau=pm.RF_Ntau,ncellsperfig=pm.ncells_per_fig,scalemax='bycell',stimshape=ROIshape,celllabels=[cell_titles[i] for i in cells_tokeep],bold_cells=is_isolated[cells_tokeep],title=str(cat))
    
    
        if 'volt1' in pm.to_compute:
            
            if not(RF.has_key('volt1')):
                RF['volt1']={'h0':{},'h1':{},'history':{}}
            if (pm.RF_history_Ntau>0) and not(pm.RF_history_crosstalks):
                volt1_coeffs=[lsq_kernel(train_stim[cat],train_rates[cat][:,icell],orders=['0','1'],history_ntau=pm.RF_history_Ntau) for icell in cells_tokeep]
                volt1_coeffs={np.concatenate([volt1_coeffs[icell][k] for icell in range(len(volt1_coeffs))], axis=1) for k in volt1_coeffs[0].keys()}
                volt1_coeffs['history']=np.array([np.diag(volt1_coeffs['history'][tau]) for tau in range(pm.RF_history_Ntau)]).reshape(-1,len(cells_tokeep))
            else:    
                #volt1_coeffs=lsq_kernel(train_stim[cat],train_rates[cat][:,cells_tokeep],order0=True,order2=False,history_ntau=pm.RF_history_Ntau)
                volt1_coeffs=lsq_kernel(train_stim[cat],train_rates[cat][:,cells_tokeep],orders=['0','1'],history_ntau=pm.RF_history_Ntau)
            if pm.RF_history_Ntau>0:
                RF['volt1']['h0'][cat],RF['volt1']['h1'][cat],RF['volt1']['history'][cat]=volt1_coeffs['0'],volt1_coeffs['1'],volt1_coeffs['history']
                if pm.RF_history_crosstalks:
                    plot_kernel1(RF['volt1']['history'][cat].reshape(-1,1),ntau=pm.RF_history_Ntau,scalemax='bycell',stimshape='square',title=str(cat))
                else:
                    plt.figure()
                    plt.plot(np.diagonal(RF['volt1']['history'][cat].reshape(-1,len(cells_tokeep),len(cells_tokeep)),axis1=1,axis2=2))
                    plt.title(cat)
            else:
                RF['volt1']['h0'][cat],RF['volt1']['h1'][cat]=volt1_coeffs['0'],volt1_coeffs['1']
            plotk1_multifig(RF['volt1']['h1'][cat],ntau=pm.RF_Ntau,ncellsperfig=pm.ncells_per_fig,scalemax='bycell',stimshape=ROIshape,celllabels=[cell_titles[i] for i in cells_tokeep],bold_cells=is_isolated[cells_tokeep],title=str(cat))
          
        
        if 'volt2diag' in pm.to_compute:
            
            if not(RF.has_key('volt2diag')):
                RF['volt2diag']={'h0':{},'h1':{},'h2diag':{}}
            volt_coeffs=lsq_kernel(train_stim[cat],train_rates[cat][:,cells_tokeep],orders=['0','1','2diag'])    
            RF['volt2diag']['h0'][cat],RF['volt2diag']['h1'][cat],RF['volt2diag']['h2diag'][cat]=[volt_coeffs[k] for k in ['0','1','diag']]
            plotk1_multifig(RF['volt2diag']['h1'][cat],ntau=pm.RF_Ntau,ncellsperfig=pm.ncells_per_fig,scalemax='bycell',stimshape=ROIshape,celllabels=[cell_titles[i] for i in cells_tokeep],bold_cells=is_isolated[cells_tokeep],title=str(cat))
            plotk1_multifig(RF['volt2diag']['h2diag'][cat],ntau=pm.RF_Ntau,ncellsperfig=pm.ncells_per_fig,scalemax='bycell',stimshape=ROIshape,celllabels=[cell_titles[i] for i in cells_tokeep],bold_cells=is_isolated[cells_tokeep],title=str(cat))
          
            
        if 'volt2' in pm.to_compute:
            
            if not(RF.has_key('volt2')):
                RF['volt2']={'h0':{},'h1':{},'h2':{}}
            volt_coeffs=lsq_kernel(train_stim[cat],train_rates[cat][:,cells_tokeep],orders=['0','1','2'])    
            RF['volt2']['h0'][cat],RF['volt2']['h1'][cat],RF['volt2']['h2'][cat]=[volt_coeffs[k] for k in ['0','1','2']]
            eigvals,eigvecs=diag_kernel2(restorek2shape(RF['volt2']['h2'][cat]))
            plot_kernel2(RF['volt2']['h1'][cat],eigvals,eigvecs,ntau=pm.RF_Ntau,k2neig=10,title=str(cat))   
        
       
    if 'RF_pred' in pm.to_compute:
        
        #n_rfs=np.sum([len(v['h1']) for v in RF.values()])
        rf_cats={rf_type: list(set(itertools.chain(*[[c for c in RF[rf_type][key]] for key in RF[rf_type].keys()]))) for rf_type in RF.keys()}
        n_rfs=np.sum([len(rf_cats[rf_type]) for rf_type in RF.keys()])
        x_positions=np.linspace(-0.4,0.4,n_rfs+1)
        x_width=x_positions[1]-x_positions[0]
        plt.figure()
        leg_items=[]
        leg_strings=[]
        ipred=0
        
        for rf_type in RF.keys():
            for varname in ['train_pred_rates','val_pred_rates','train_pred_corrs','val_pred_corrs']:
                vars()[varname][rf_type]={}
            for cat in rf_cats[rf_type]:        
                train_pred_rates[rf_type][cat]=np.zeros(train_rates[cat].shape)
                val_pred_rates[rf_type][cat]=np.zeros(val_rates[cat].shape)
                if RF[rf_type].has_key('h0'):
                    train_pred_rates[rf_type][cat]+=RF[rf_type]['h0'][cat]
                    val_pred_rates[rf_type][cat]+=RF[rf_type]['h0'][cat]
                if RF[rf_type].has_key('h1'):  
                    train_pred_rates[rf_type][cat]+=np.dot(train_stim[cat],RF[rf_type]['h1'][cat])
                    val_pred_rates[rf_type][cat]+=np.dot(val_stim[cat],RF[rf_type]['h1'][cat])
                if RF[rf_type].has_key('h2diag'):
                    train_pred_rates[rf_type][cat]+=np.dot(train_stim[cat]**2,RF[rf_type]['h2diag'][cat])
                    val_pred_rates[rf_type][cat]+=np.dot(val_stim[cat]**2,RF[rf_type]['h2diag'][cat])
                elif RF[rf_type].has_key('h2'):
                    train_pred_rates[rf_type][cat]+=k2resp(restorek2shape(RF[rf_type]['h2'][cat]),train_stim[cat])
                    val_pred_rates[rf_type][cat]+=k2resp(restorek2shape(RF[rf_type]['h2'][cat]),val_stim[cat])
                if RF[rf_type].has_key('history'):
                    train_pred_rates[rf_type][cat]+=np.dot(train_history[cat],RF[rf_type]['history'][cat])
                    val_pred_rates[rf_type][cat]+=np.dot(val_history[cat],RF[rf_type]['history'][cat])
                
                train_pred_corrs[rf_type][cat]=[np.corrcoef(train_rates[cat][:,cells_tokeep[icell]],train_pred_rates[rf_type][cat][:,icell])[0,1] for icell in range(len(cells_tokeep))]
                val_pred_corrs[rf_type][cat]=[np.corrcoef(val_rates[cat][:,cells_tokeep[icell]],val_pred_rates[rf_type][cat][:,icell])[0,1] for icell in range(len(cells_tokeep))]
                leg_items.append(plt.bar(np.arange(len(cells_tokeep))+x_positions[ipred], val_pred_corrs[rf_type][cat], x_width, color=colors[ipred%len(colors)], edgecolor=colors[ipred%len(colors)]))
                plt.bar(np.arange(len(cells_tokeep))+x_positions[ipred], train_pred_corrs[rf_type][cat], x_width, color=colors[ipred%len(colors)], alpha=0.2)           
                leg_strings.append(rf_type+' - '+str(cat))                
                ipred+=1
           
        for icell in range(len(cells_tokeep)):
            plt.axvline(icell,alpha=0.5,color='k')
        plt.gca().set_xticks(np.arange(len(cells_tokeep)))
        plt.gca().set_xticklabels([str(all_units[iunit]) for iunit in cells_tokeep])
        plt.legend(leg_items,leg_strings)
        plt.axis([-1,len(cells_tokeep)]+list(plt.axis()[2:]))
        
        
    if 'RF_crosspred' in pm.to_compute:
        
        n_preds=len(stim_categories)*(len(stim_categories)-1)*len(RF)
        x_positions=np.linspace(-0.4,0.4,n_preds+1)
        x_width=x_positions[1]-x_positions[0]
        plt.figure()
        leg_items=[]
        leg_strings=[]
        ipred=0
        
        for rf_type in RF.keys():
            cross_pred_rates[rf_type]={key:{} for key in stim_categories.keys()}
            cross_pred_corrs[rf_type]={key:{} for key in stim_categories.keys()}
            for cat1 in stim_categories.keys():
                for cat2 in set(stim_categories.keys())-set([cat1]):
                    cross_pred_rates[rf_type][cat1][cat2]=np.dot(train_stim[cat2],RF[rf_type]['h1'][cat1])
                    if RF[rf_type].has_key('h0'):
                        cross_pred_rates[rf_type][cat1][cat2]+=RF[rf_type]['h0'][cat1]
                    if RF[rf_type].has_key('h2diag'):
                        cross_pred_rates[rf_type][cat1][cat2]+=np.dot(train_stim[cat2]**2,RF[rf_type]['h2diag'][cat1])
                    elif RF[rf_type].has_key('h2'):
                        cross_pred_rates[rf_type][cat1][cat2]+=k2resp(restorek2shape(RF[rf_type]['h2'][cat1]),train_stim[cat2])           
                    cross_pred_corrs[rf_type][cat1][cat2]=[np.corrcoef(train_rates[cat2][:,cells_tokeep[icell]],cross_pred_rates[rf_type][cat1][cat2][:,icell])[0,1] for icell in range(len(cells_tokeep))]             
                    leg_items.append(plt.bar(np.arange(len(cells_tokeep))+x_positions[ipred], cross_pred_corrs[rf_type][cat1][cat2], x_width, color=colors[ipred%len(colors)], edgecolor=colors[ipred%len(colors)]))        
                    leg_strings.append(rf_type+' :'+str(cat1)+'->'+str(cat2))
                    ipred+=1
           
        for icell in range(len(cells_tokeep)):
            plt.axvline(icell,alpha=0.5,color='k')
        plt.gca().set_xticks(np.arange(len(cells_tokeep)))
        plt.gca().set_xticklabels([str(all_units[iunit]) for iunit in cells_tokeep])
        plt.legend(leg_items,leg_strings)
        plt.axis([-1,len(cells_tokeep)]+list(plt.axis()[2:]))    
        

    if any([plotname in pm.to_compute for plotname in ['grating_pair_maps','grating_maps']]):

        grat_info={key: ep_info[key][((ep_info['stim_type']=='grating_pair') | (ep_info['stim_type']=='bar_pair')) & valid_eps] for key in ep_info.keys()}
        assert (len(grat_info)>0) and np.all([grat_info.has_key(key) for key in ['ISI','dton1','orientation1','phase1','period1','dton2','orientation2','phase2','period2']])  and np.all(grat_info['theo_nframes']==grat_info['theo_nframes'][0])    
        onset2=grat_info['theo_nframes'][0]-grat_info['dton2'].max()
        ep_info['display_time2']=np.array([[onset2,onset2+ep_info['dton2'][iep]] if ep_info['stim_type'][iep] in ['grating_pair','bar_pair'] else [np.NaN,np.NaN] for iep in range(nEp)])
        ep_info['display_time1']=np.array([onset2-ep_info['ISI']-ep_info['dton1'],onset2-ep_info['ISI']]).T        
    

    if 'grating_pair_maps' in pm.to_compute :
        
        down_sampling=1
        frame_rate=60 #(Hz)
        plot_params=['phase1','orientation1','ISI']
        filter_params={}#{'phase2':[2./3]}
        precision=0.00001
        param_vals=[sorted(list(set(ep_info[plot_params[i]][~np.isnan(ep_info[plot_params[i]])]))) for i in range(len(plot_params))]
        bool_filter=np.all(np.array([np.any((ep_info[p]-np.array(filter_params[p]).reshape(-1,1).repeat(len(ep_info[p]),axis=1))<precision,axis=0) for p in filter_params.keys()]),axis=0) if len(filter_params)>0 else np.ones(len(ep_info.values()[0])).astype(bool)
        fig_grid=GridSpec(*[len(param_vals[i]) for i in range(2)])        
        for icell in list(cells_tokeep)+[cells_tokeep]:
            max_list=[]
            ax_list=[]
            plt.figure()
            for i0 in range(len(param_vals[0])):
                for i1 in range(len(param_vals[1])):
                    ax_list.append(plt.subplot(fig_grid[i0,i1]))
                    if i0==0:
                        plt.title(plot_params[1]+' : '+str(param_vals[1][i1]))
                    if i1==0:
                        plt.ylabel(plot_params[0]+' : '+str(param_vals[0][i0]))
                    leg_objects=[]    
                    for i2 in range(len(param_vals[2])):
                        indices=np.where(((ep_info['stim_type']=='grating_pair') | (ep_info['stim_type']=='bar_pair')) & (ep_info[plot_params[0]]==param_vals[0][i0]) & (ep_info[plot_params[1]]==param_vals[1][i1]) & (ep_info[plot_params[2]]==param_vals[2][i2]) & valid_eps & bool_filter)[0]
                        assert np.all(ep_info['display_time1'][indices]==ep_info['display_time1'][indices][0]) and np.all(ep_info['display_time2'][indices]==ep_info['display_time2'][indices][0])
                        sub_rates=np.array([rates[iep][:,icell] for iep in indices])
                        if not isinstance(icell,int):
                            sub_rates=np.rollaxis(sub_rates,2,1).reshape(-1,sub_rates.shape[1])
                        sub_rates=downsample(sub_rates,axis=1,factor=down_sampling)  
                        max_list.append(np.nanmax(sub_rates.mean(axis=0)))
                        pos=np.array(list(ep_info['display_time1'][indices][0])+list(ep_info['display_time2'][indices][0]))
                        pos=init_nbins[indices[0]]*pm.bin_width+pos*stim_dton
                        pos_col=[colors[i2]]*2+['k']*2
                        pos_style=['-','--']*2
                        for ipos in range(4):
                            plt.axvline(pos[ipos],color=pos_col[ipos],linestyle=pos_style[ipos])  
                            
                        leg_objects.append(plt.errorbar(np.arange(sub_rates.shape[1])*pm.bin_width*down_sampling,sub_rates.mean(axis=0),yerr=2.*sub_rates.std(axis=0)/np.sqrt(sub_rates.shape[0]),color=colors[i2])[0])
#                        for pos in list(ep_info['display_time1'][indices][0])+list(ep_info['display_time2'][indices][0]):
#                            plt.axvline(init_nbins[indices[0]]*pm.bin_width+pos*stim_dton,color=colors[i2])

                    #ax.set_yticks([])
                    if i0==len(param_vals[0])-1:
                        plt.xlabel('Time (s)')
                        #ax.set_xticks(np.arange(1,1.9,0.1))
                    #else:
                        #ax.set_xticks([])
                    if (i0==0) and (i1==0):
                        #plt.legend(leg_objects,param_vals[2],title=plot_params[2])
                        plt.legend(leg_objects,np.array(param_vals[2])*1000./frame_rate,title='ISI (ms)')
                        
            for ax in ax_list :            
                #ax.axis(list(ax.axis()[:2])+[-0.2,max(max_list)+0.2])   
                ax.axis([0.5,2.5,-0.2,max(max_list)+0.2])

                            
    if 'grating_maps' in pm.to_compute :
        # ?? what is this doing ?
        ISI_min=24
        shift=np.round(ep_info['display_time1'][:,0]*stim_dton*1./pm.bin_width).astype(int)
        duration=ep_nbins[ep_info['stim_type']=='grating_pair'][0]-np.nanmax(shift)
        #duration=int(init_nbins(np.round(np.nanmax(ep_info['dton1'])+np.nanmin(ep_info['ISI'])*stim_dton*1./pm.bin_width))
        plot_params=['orientation1','dton1','phase1'] # Ajouter : calcul instant occurrence grat1 et grat2
        param_vals=[sorted(list(set(ep_info[plot_params[i]][~np.isnan(ep_info[plot_params[i]])]))) for i in range(len(plot_params))]
        fig_grid=GridSpec(*[len(param_vals[i]) for i in range(2)])        
        for icell in cells_tokeep:
            plt.figure()
            for i0 in range(len(param_vals[0])):
                for i1 in range(len(param_vals[1])):
                    plt.subplot(fig_grid[i0,i1])
                    if i0==0:
                        plt.title(param_vals[1][i1])
                    if i1==0:
                        plt.ylabel(param_vals[0][i0]) 
                    for i2 in range(len(param_vals[2])):
                        indices=np.where((ep_info['stim_type']=='grating_pair') & (ep_info[plot_params[0]]==param_vals[0][i0]) & (ep_info[plot_params[1]]==param_vals[1][i1]) & (ep_info[plot_params[2]]==param_vals[2][i2]) & (ep_info['ISI']>=ISI_min) & valid_eps)[0]
                        sub_rates=np.array([rates[iep][shift[iep]:duration+shift[iep],icell] for iep in indices])
                        plt.errorbar(np.arange(duration)*pm.bin_width,sub_rates.mean(axis=0),yerr=2.*sub_rates.std(axis=0)/np.sqrt(sub_rates.shape[0]),color=colors[i2])
                        for pos in [0,ep_info['dton1'][indices[0]]]:
                            plt.axvline(init_nbins[indices[0]]*pm.bin_width+pos*stim_dton,color=colors[i2])                        
                            
                    
        
    if 'timecourse' in pm.to_compute:
        
        for cat in stim_categories.keys():
            cat_eps=list(set(stim_categories[cat]) & set(eps_tokeep))
            stamps=list(itertools.chain(*[list(np.array([init_nbins*pm.bin_width,(ep_nbins-theo_fnbins)*pm.bin_width])+ep_nbins*pm.bin_width*i) for i in range(len(cat_eps))]))
            slider,ax=plotTimeSeries(np.array([rates[iep] for iep in cat_eps])[:,:,cells_tokeep].reshape(-1,len(cells_tokeep)),pm.bin_width,ep_nbins*pm.bin_width*4,stamp_lists=[stamps],superimpose=None,norm_fact=0.1,plottype='lines')  
        
        
    if 'raster' in pm.to_compute:
        
        # eps_tokeep not taken into account
        stamps=list(itertools.chain(*[list(np.array([init_nbins*pm.bin_width,vtags[0][i][-1]*1./30000])+ep_nbins*pm.bin_width*i) for i in range(nEp)]))
        plt.figure()
        for i in range(len(cells_tokeep)):
            spikes=np.array(spikelist[all_units[cells_tokeep[i]]])*1./sampling_rate
            plt.plot(spikes,[i]*len(spikes),'|', markersize=10)
        addStamps(stamps)  
        

    if 'repeats' in pm.to_compute:
        
        plot_psths=False      
        try:
            #cell_order=cells_tokeep[np.argsort(-mean_corr[cells_tokeep])]
            cell_order=cells_tokeep[np.argsort(-max_corrs['all'][cells_tokeep])]
        except:
            cell_order=cells_tokeep    
        
#        repeats_infos={}
#        for stim_id in stim_repeats.keys():
#            stim_cat=[cat for cat in stim_categories.keys() if stim_id in stim_categories[cat]]
#            if len(stim_cat)>0:
#                assert len(stim_cat)==1
#                repeats_infos[stim_id]=stim_cat[0]
#        stim_ids=repeats_infos.keys()  
        stim_ids=[sid for sid in stim_repeats.keys() if ep_info['stim_type'][stim_repeats[sid][0]] not in ['grating_pair','bar_pair']]
        n_bins=120
        rep_nums=[len(set(stim_repeats[sid]) & set(eps_tokeep)) for sid in stim_ids]
        fig_grid=GridSpec((2 if plot_psths else 1)*pm.ncells_per_fig,len(stim_ids))
        #fig_grid=GridSpec(len(stim_repeats),(2 if plot_psths else 1)*len(cell_order))
        count=pm.ncells_per_fig
        for icell in cell_order:
            if count==pm.ncells_per_fig:
                fig=plt.figure()
                count=0
            spikes=np.array(spikelist[all_units[icell]])*1./sampling_rate
                        
            for istim in range(len(stim_ids)):
                ax=plt.subplot(fig_grid[(2 if plot_psths else 1)*count,istim])
                #ax=plt.subplot(fig_grid[istim,(2 if plot_psths else 1)*count])
                allspikes=[]
                eps=list(set(stim_repeats[stim_ids[istim]]) & set(eps_tokeep))
                for iep in range(len(eps)):
                    time_pos=ep_nbins[:eps[iep]].sum()*pm.bin_width
                    epspikes=spikes[(spikes>=time_pos) & (spikes<time_pos+ep_nbins[eps[iep]]*pm.bin_width)]-time_pos
                    plt.plot(epspikes,np.ones(len(epspikes))*(iep+1),'k|', markersize=5)
                    allspikes+=list(epspikes)
                if count==0:
                    plt.title('stim '+str(stim_ids[istim]))
                    #plt.ylabel('stim '+str(stim_ids[istim]))
                if istim==0:
#                    if isinstance(units[icell],tuple):   
#                        plt.ylabel(str(icell)+' (e'+str(units[icell][0])+'c'+str(units[icell][1])+')',weight=['normal','bold'][is_isolated[icell]])
#                    else:
#                        plt.ylabel(str(icell)+' ('+str(units[icell])+')',weight=['normal','bold'][is_isolated[icell]])
                    plt.ylabel(cell_titles[icell],weight=['normal','bold'][is_isolated[icell]])
                    #plt.title(cell_titles[icell],weight=['normal','bold'][is_isolated[icell]])
                plt.axis([0,ep_nbins[stim_repeats[stim_ids[istim]][0]]*pm.bin_width,0,len(stim_repeats[stim_ids[istim]])+1])
                ax.set_xticks([])
                ax.set_yticks([])
                
                if plot_psths:           
                    ax=plt.subplot(fig_grid[2*count+1,istim])
                    plt.hist(allspikes,np.linspace(0,ep_nbins[eps[0]]*pm.bin_width,n_bins),color='k')
                    #plt.axis([0,ep_nbins*pm.bin_width,0,20])
                    plt.axis((0,ep_nbins[eps[0]]*pm.bin_width)+plt.axis()[2:])
                    ax.set_yticks([])
                    
#                if count<pm.ncells_per_fig-1:
#                    ax.set_xticks([])                  
            count+=1       
            
            
    if 'duplicate_spikes' in pm.to_compute:
        
        # /!\ eps_tokeep not taken into account
        subspikelist={all_units[i]:spikelist[all_units[i]] for i in cells_tokeep}
        dupl_mat=duplicateSpikes_slowmethod(subspikelist,tolerance=pm.spike_tol,plotfig=True)       
        plt.figure()
        plt.hist(dupl_mat[~np.eye(len(cells_tokeep)).astype(bool)].reshape(-1))
        
        
    if 'meanrates' in pm.to_compute: # /!\ /!\ A adapter pour ep_dur variable
        try:
            alphas=mean_corr.copy()
            alphas[np.isnan(alphas) | (alphas<0)]=0
            alphas=alphas*1./alphas.max()
        except:
            alphas=np.ones(n_units)
            
        curve_types=['solid','dashed','dotted','dashdot']  
        
        onset_nbins=int(np.ceil(pm.onset_dur*1./pm.bin_width))
        offset_nbins=int(np.ceil(pm.offset_dur*1./pm.bin_width))
        
        blank_rates=rates[:,:init_nbins,:].sum(axis=1)*1./(init_nbins*pm.bin_width)
        onset_rates=rates[:,init_nbins:init_nbins+onset_nbins,:].sum(axis=1)*1./(onset_nbins*pm.bin_width)
        stim_rates=rates[:,init_nbins+onset_nbins:-theo_fnbins,:].sum(axis=1)*1./(ep_nbins*pm.bin_width-(init_nbins+onset_nbins+theo_fnbins)*pm.bin_width)
        offset_rates=rates[:,-theo_fnbins:offset_nbins-theo_fnbins,:].sum(axis=1)*1./(offset_nbins*pm.bin_width)  
        
        group_count=0
        for subcells in [np.where(is_isolated & is_valid)[0],np.where(~is_isolated & is_valid)[0]]:
            plt.figure()
            plt.axhline(y=1,color='gray',linewidth=2)
            cat_count=0
            labels=['']*len(stim_categories.keys())
            for cat in stim_categories.keys():
                cell_count=0
                for icell in subcells:
                    curve=np.array([mat[stim_categories[cat],icell].mean() for mat in [blank_rates,onset_rates,stim_rates,offset_rates]])
                    curve_err=np.array([mat[stim_categories[cat],icell].std()*2./np.sqrt(len(stim_categories[cat])) for mat in [blank_rates,onset_rates,stim_rates,offset_rates]])*2./np.sqrt(len(stim_categories[cat]))
                    labels[cat_count]=plt.errorbar(range(4),curve*1./curve[0], yerr=curve_err*1./curve[0],linewidth=2,color=colors[cell_count%len(colors)],linestyle=curve_types[cat_count%len(curve_types)],alpha=alphas[icell])
                    cell_count+=1
                cat_count+=1    
            plt.axis((-0.5,3.5)+plt.axis()[2:])
            plt.xticks([0,1,2,3],['Blank','Onset','Stim','Offset'])
            plt.legend(labels,stim_categories.keys())
            plt.ylabel('Normalized firing rate')
            plt.title(['isolated','multiunit'][group_count])
            group_count+=1
                  
        
    if 'PSTH' in pm.to_compute:
        # /!\ /!\ A adapter pour ep_dur variable
        plt.figure()      
        for i in range(len(cells_tokeep)):
            spikes=(np.array(spikelist[all_units[cells_tokeep[i]]])*1./sampling_rate)%(ep_nbins*pm.bin_width)
            plt.hist(spikes,50,alpha=0.3)
        addStamps([init_nbins*pm.bin_width,ep_nbins*pm.bin_width-theo_fnbins*pm.bin_width])
        plt.xlabel('Time (s)')
        plt.ylabel('Total number of spikes (across all episodes)')    
        
        
    if 'mean_timecourse' in pm.to_compute:
        
        kept_eps=np.array(list((set(np.concatenate(stim_categories.values())) -set(repeated_trials)) & set(eps_tokeep)))
        assert np.all(ep_nbins[kept_eps]==ep_nbins[kept_eps[0]])
        bin_size=0.2
        psth_init_nbins=((init_nbins*pm.bin_width)/bin_size).astype(int)       
        try:
            cell_order=cells_tokeep[np.argsort(-mean_corr[cells_tokeep])]
            cell_alphas=np.maximum(mean_corr[cell_order],0)
            #cell_order=cells_tokeep[np.argsort(-max_corrs['all'][cells_tokeep])]
            #cell_alphas=np.maximum(max_corrs['all'][cell_order],0)            
            cell_alphas=(cell_alphas-np.nanmin(cell_alphas))*0.8/(np.nanmax(cell_alphas)-np.nanmin(cell_alphas))+0.2
            #cell_alphas=(cell_alphas-np.nanmin(cell_alphas))*1./(np.nanmax(cell_alphas)-np.nanmin(cell_alphas))
        except:
            cell_order=cells_tokeep
            cell_alphas=np.linspace(0.2,1,len(cell_order))
        psths,bins=compute_PSTHs([spikelist[all_units[iunit]] for iunit in cell_order],ep_durs=ep_nbins*pm.bin_width*sampling_rate,bin_size=bin_size*sampling_rate)                 
        stim_types=stim_categories.keys()
        mean_init_rates=np.array([psths[iep][:psth_init_nbins[iep],:].mean(axis=0) for iep in (set(range(nEp))-set(repeated_trials)) & set(eps_tokeep)]).mean(axis=0)
        stim_categories_nonrepeated={st: np.array(list( (set(stim_categories[st])-set(repeated_trials)) & set(eps_tokeep) )) for st in stim_types}
        psth_means=np.array([np.array([psths[iep] for iep in stim_categories_nonrepeated[st]]).mean(axis=0) for st in stim_types])-mean_init_rates
        psth_errs=np.array([np.array([psths[iep] for iep in stim_categories_nonrepeated[st]]).std(axis=0)*2./np.sqrt(len(stim_categories_nonrepeated[st])) for st in stim_types])-mean_init_rates
        plotby_cellandstim(psth_means,val_errs=psth_errs,cell_labels=[cell_titles[icell] for icell in cell_order],bold_cells=is_isolated[cell_order],stim_labels=stim_types,x=bins[kept_eps[0]]*1./sampling_rate,x_label='Time (s)',ncells_per_fig=pm.ncells_per_fig,figsize=pm.fig_size,stim_colors=[stim_colors[st] for st in stim_types],cell_colors=[(1-a,)*3 for a in cell_alphas])
        
            
    if 'stim_with_meanresp' in pm.to_compute: # /!\ /!\ A adapter pour ep_dur variable
        
        indicator=rates[:,cells_tokeep].reshape(len(pm.eps_tokeep),-1,len(cells_tokeep))
        indicator=indicator[pm.repeated_eps,:,:].mean(axis=0)
        redstim=stim.reshape((len(pm.eps_tokeep),-1)+frameshape)[pm.repeated_eps[0],:,pm.ROI[0,0]:pm.ROI[0,1],pm.ROI[1,0]:pm.ROI[1,1]]
        playMovieFromMat(redstim,frame_dur=1./60,indicators=indicator)
        
        
    if 'preferred_stim' in pm.to_compute: # /!\ /!\ A adapter pour ep_dur variable
        
        indicator=rates[:,cells_tokeep].reshape(len(pm.eps_tokeep),-1,len(cells_tokeep))
        indicator=indicator[pm.repeated_eps,:,:].mean(axis=0)
        redstim=stim.reshape((len(pm.eps_tokeep),-1)+frameshape)[pm.repeated_eps[0],:,pm.ROI[0,0]:pm.ROI[0,1],pm.ROI[1,0]:pm.ROI[1,1]]
        anims=[]
        for icell in range(len(cells_tokeep)):
            rep_resp_times=detectPeaks(indicator[:,icell],indicator[:,icell].mean()+3*indicator[:,icell].std())
            pref_stims=np.concatenate([redstim[np.max(t-10,0):t,:,:] for t in rep_resp_times],axis=2)
            pref_stims=np.concatenate([pref_stims,np.zeros((2,)+pref_stims.shape[1:])],axis=0)
            anims.append(playMovieFromMat(pref_stims,frame_dur=1./4))
   

    if 'stim_mean_rates' in pm.to_compute: # /!\ /!\ A adapter pour ep_dur variable
        for cat in stim_categories.keys():
            stim_blank_rates=rates[stim_categories[cat],:init_nbins][:,:,cells_tokeep].mean(axis=1)
            stim_norm_rates=rates[stim_categories[cat],init_nbins:-theo_fnbins][:,:,cells_tokeep].mean(axis=1)-stim_blank_rates
            stim_norm_rates=stim_norm_rates*1./stim_norm_rates.mean(axis=0)           
            slider,ax=plotTimeSeries(stim_norm_rates,1,range(10),plottype='colormap',title=cat,xleg=None)
         
            
    if ('reliability_vs_rate' in pm.to_compute) and (len(stim_repeats)>0):
        
        plt.figure()
        plt.plot(mean_rates[is_valid & is_isolated],mean_corr[is_valid & is_isolated],'ro', markersize=12)
        plt.plot(mean_rates[is_valid & ~is_isolated],mean_corr[is_valid & ~is_isolated],'bo', markersize=12)
        plt.legend(['Isolated units','Multiunit'],fontsize=28)   
        plt.xlabel('Mean firing rate (Hz)',fontsize=28)
        plt.ylabel('Mean trial-to-trial correlation',fontsize=28)
        for side in plt.gca().spines.itervalues():
               side.set_linewidth(5)
        plt.tick_params(labelsize=28)
        
        
    if 'neuron_similarity' in pm.to_compute:
        order=np.argsort(-mean_corr)
        corrmax=np.nanmax(mean_corr)*2./3
        plt.figure()
        for istim in range(cross_corr.shape[0]):
            plt.subplot(1,cross_corr.shape[0],istim+1)
            plt.imshow(cross_corr[istim,n_jitters-1,order][:,order],vmin=-corrmax,vmax=corrmax,interpolation='none')
            plt.title('Stim '+str(stim_types[istim]))
            
        
    if 'export_to_file' in pm.to_compute:
        
        print "\nExporting reformatted data to file\n"
                
        assert np.all(init_nbins==init_nbins[0]) # There might be complicated cases where init_nbins differs between episodes...
        assert np.all(theo_fnbins==theo_fnbins[0])
        to_export={'stimulus': [downsample(downsample(s[:,pm.ROI[0,0]:pm.ROI[0,1],pm.ROI[1,0]:pm.ROI[1,1]],2,pm.ROI_downsplfact),1,pm.ROI_downsplfact) if s is not None else np.array([]) for s in stim],
                   'responses': rates,
                   'bin_width': pm.bin_width,
                   'init_blank_nbins': init_nbins[0],
                   'final_blank_nbins': theo_fnbins[0]}
        ep_info['data_splitting']=np.array(['train']*nEp)
        
        for cat in stim_categories.keys():
            merged_repeats=list(itertools.chain(*[list(set(stim_repeats[mov_id]) & set(eps_tokeep)) for mov_id in cat_repeats[cat]]))
            non_repeated=np.array(list((set(stim_categories[cat])-set(merged_repeats)) & set(eps_tokeep)))
            np.random.shuffle(non_repeated)
            n_reg_eps=int(pm.RF_valfrac*len(non_repeated))
            ep_info['data_splitting'][non_repeated[:-n_reg_eps]]='train'
            ep_info['data_splitting'][non_repeated[-n_reg_eps:]]='reg'
            ep_info['data_splitting'][merged_repeats]='val'   
            
        file_name=basename(pm.spikesorting_file).split('.')[0] if pm.spikesorting_file else basename(pm.elphy_file).split('.')[0]
        file_name=join_paths(pm.save_path,file_name)
        save_info_table(ep_info,file_name+'_epinfo.txt')
        save_info_table(unit_info,file_name+'_unitinfo.txt')    
        #savemat(file_name+'.mat',to_export)  # Matlab format
        dict_to_binary(to_export,file_name,binary_ext='.lwb',text_ext='.lwbi') # Custom binary format
        