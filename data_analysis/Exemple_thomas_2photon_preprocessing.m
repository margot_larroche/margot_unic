

%% Load the data
v = load('data - signals')

%% Neuropil correction
x = v.signals - 0.7*v.localneuropil;

%% Estimate a 'drifting' baseline
[nt ncell nep] = size(x);
F0 = zeros(1,ncell,nep);
xm = min(x,[],1);
for i=1:nep, idx = max(i-2,1):min(i+2,nep); F0(:,:,i) = min(xm(:,:,idx),[],3); end

%% Signal DF/F
x = fn_div(fn_subtract(x,F0),F0);

%% Deconvolution
tau = 2;
[~, dxdt, ~] = gradient(x);
dxdt = dxdt / v.dt;
z = dxdt + x/tau;
clear dxdt

%% Low-pass the deconvolved data
sigma = 1;
zf = fn_filt(z,sigma/v.dt,'l',1);

%% Bin temporally 
zb = fn_bin(z,[3 1 1],'same');

