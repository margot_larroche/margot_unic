group_num=2;
baseanalysis = '/media/partage/eqbrice_rawdata/ANALYSIS/';


finfo = fn_getfile('ExperimentInfo.mat','Select file with experiment info');
if isequal(finfo,0), return, end
[savename,hdat,xpar] = fn_loadvar(finfo,'savename','hdat','xpar');
analysisfolder = fileparts(finfo); % do not use the one saved in finfo, as it might have changed
regname = [savename ' - reg'];
disp(analysisfolder)
folder = analysisfolder(length(baseanalysis)+1:end);
regnamefull = fullfile(analysisfolder,regname);
Treg = fn_loadvar([regnamefull '.tptrial']);


% quantities that must be identical: frame rate, number of frame, number of
% conditions
dts = [Treg.dt_sec];
nts = [Treg.nfr];
if isempty(hdat), nconds = []; else nconds = fn_itemlengths({hdat.cond}); end
nums = [dts' nts' nconds'];

% how many groups
[nums iex igroup] = unique(nums,'rows'); %#ok<*NCOMMA>
ngroup = size(nums,1);

if ngroup>1

    % which trials belong to that group
    idx = find(igroup==group_num);
    ntrialk = length(idx);
    % new base name
    namek = [savename '_group' num2str(group_num)];

    % set the needed variables
    xpar = hdat(idx(1)).fullpar;
    hdat = hdat(idx);
    savename = namek;
    regname = [namek ' - reg'];
    
end

DataExtractsignalsScript
