from elphy_utils.elphy_reader import elphy_to_klusta, ElphyFile
from elphy_utils.elphy_misc_utils import elphy_visualize
from utils.param_gui_tools import param_dialog, param_list_string
import os
import param


class convert_params(param.Parameterized):    
    
    filenames=param.List(default=[], doc="List of files to display/convert")
    output_dir=param.Foldername(default='', doc="Path where to create converted files")    
    n_conv_eps=param.Integer(default=None, allow_None=True, doc="Number of converted episodes. Set to none if you want them all, and to 0 to skip the conversion")
    visualized_eps=param.List(default=[], doc="List of episodes to plot data from (to spot dead channels)")
display_order={'PARAMETERS': ['filenames','output_dir','n_conv_eps','visualized_eps']}


try: pm
except NameError:
    pm=convert_params()
    
pm,validated=param_dialog(pm,display_order) 
if validated:
    print 'PARAMETERS : \n\n'+param_list_string(pm,display_order)+'\n\n'        

ep_nums = range(pm.n_conv_eps) if (pm.n_conv_eps!=None) else 'all'
for f in pm.filenames:
    ef=ElphyFile(f)
    
    if len(ep_nums)>0:
        klusta_name=os.path.join(pm.output_dir, os.path.basename(f.split('.')[0])) + ('_'+str(pm.n_conv_eps)+'ep' if pm.n_conv_eps else '') + '.dat'
        print klusta_name
        elphy_to_klusta(ef,klusta_name,ep_nums=ep_nums)
        
    for ep in pm.visualized_eps :
        fig=elphy_visualize(ef,ep_num=ep)
        fig.axes[0].set_title(os.path.basename(f)+' - ep '+str(ep))