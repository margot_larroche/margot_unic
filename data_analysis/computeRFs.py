import numpy as np
import param
from utils.param_gui_tools import param_dialog, param_list_string, retrieve_params, correct_inputs
import sys
import matplotlib.pyplot as plt
from lscsm.checkpointing import restoreLSCSM, loadParams
import os.path
from utils.volterra_kernels import *
from utils.handlebinaryfiles import *
from itertools import chain as join_lists
from utils.various_tools import gaussianLowpass, colornames_to_rgb
from data_analysis.extracell_funcs import barplot, pythondata_to_rfdata, merge_data_repeats
from lscsm_adapt.visualizationMgt import retrieve_LSCSM_params
from utils.simulations import generate_gratingSeq
from utils.theano_rf_models import fit_volterra_theano
from user_specific_paths import rf_paths, data_paths
#if not(vars().has_key('_matlab_engine')):
#    try:
#        import matlab.engine
#        _matlab_engine=matlab.engine.start_matlab()
#    except:
#        _matlab_engine=None
#        print('\nWarning : failed to connect to matlab\n')

"""
PARAMETERS
"""

class computeRF_params(param.Parameterized):
    # DATA PATHS:
    data_file=param.Filename(default=None,allow_None=True,search_paths=data_paths,doc='Name of the .mat file containing the stimulus-response data')
    ep_info_file=param.Filename(default=None,allow_None=True,search_paths=data_paths,doc='Name of the .txt file containing episode info (stimulus identity, etc)')
    unit_info_file=param.Filename(default=None,allow_None=True,search_paths=data_paths,doc='Name of the .txt file containing neuronal unit info (spikesorting quality, etc)')
    stim_categories_file=param.Filename(default=None,allow_None=True,search_paths=rf_paths,doc='Name of the .txt file containing definition of stimulus categories')
    #HSM_files=param.Dict(default={},doc='Parameter files of the HSM models fitted each of these datasets (optional)')
    rf_path=param.Foldername(default=rf_paths[0],search_paths=rf_paths,doc='Path where to save and load RF files from')    
    common_name=param.String(default='',doc='Prefix to add to generated files that mix all provided datasets')
    label=param.String(default='',doc='Suffix to add to generated files')
   
    # STEPS TO EXECUTE:
    load_data=param.Boolean(default=True, doc='Reload data')
    #sep_HSM_data=param.Boolean(default=False, doc='Load data separately for HSM (useful if ROI wasnt the same)')
    model_types=param.ListSelector([], objects=['volt1','volt2diag','volt2','volt2sig','STALR', 'HSM','BWT'], doc="List of RF types to compute/plot")
    time_separable=param.Boolean(default=False, doc="Whether to compute time-separable or time-inseparable RFs")
    add_nonlinearity=param.Boolean(default=False, doc="Whether to estimate the nonlinearity mapping model predictions to real responses and use it for prediction")
    to_compute=param.ListSelector([], objects=['RFs','corr','corr_ceiling','pred_STA','standard_stims_preds'], doc="List of things to compute")
    standard_stims=param.ListSelector([], objects=['gratings','SN'], doc="List of things to compute")
    load_existing_RFs=param.Boolean(default=False, doc='Load already computed RFs instead of re-computing them')       
    to_plot=param.ListSelector([], objects=['RFs','non_linearities','temporal_kernels','corrbarplot_cells_bymodel','corrbarplot_cells_bystim','corrlineplot_cells_bystim','corrbarplot_means','corrlineplot_means','corr_typeVStype','corr_vs_maxcorr','corr_vs_maxcorr2','cross_pred_clouds','cross_pred_hists','cross_pred_mat','talebi_and_baker_2012','pred_and_resps','pred_vs_resps','pred_STA','corr_max','corr_ceiling_fit','corr_ceiling_attempts'], doc="List of RF types to compute")
    to_save=param.ListSelector([], objects=['RFs','plots'], doc="List of things to save")
    
    # RF COMPUTING PARAMETERS
    merge_val_repeats=param.Boolean(default=True, doc="Whether to keep only the average response for repeated trials in validation data") 
    cells_to_keep=param.List([], doc="List of cells to keep for RF computing")
    normalize_stim=param.Boolean(default=False, doc="Normalize stimulus array (map pixel mean to 0 and std to 1)")   
    normalize_resps=param.Boolean(default=False, doc="Normalize respsonse vectors (map mean to 0 and std to 1)") 
    ROI=param.Array(default=np.array([]), doc='Spatial area to which RF computation will be restricted')
    ROI_downsplfact=param.Integer(default=1, doc='Downsampling factor for the stimulus ROI used for RF computation')
    include_constant=param.Boolean(default=True, doc='Include a constant (spontaneous firing rate) while fitting RFs (for RF types that offer this option)')
    fit_mode=param.ObjectSelector(default='linsolve', objects=['linsolve','theano'], doc="RF fitting algorithm")
    epoch_size=param.Integer(default=100, doc='Number of iterations per fitting epoch (for gradient descent algorithms)')
    num_epochs=param.Integer(default=10, doc='Number of fitting epochs (for gradient descent algorithms)')
    reg_frac=param.Number(default=0., doc='Proportion of data points to reserve for RF regularization (early stopping or choice of smoothing parameter)')
    val_frac=param.Number(default=0., doc='Number of data points to reserve for RF validation (computation of prediction performance on a new dataset)')   
    fit_seed=param.Integer(default=None, allow_None=True, doc='Seed for parameter initialization in case of gradient descent fitting (if None: all parameters initialized to 0)')  
    rand_seed=param.Integer(default=0, doc='Seed for data randomization before RF computing, no randomization if -1')    
    RF_tau=param.Integer(default=0, doc='Time delay (number of bins) to set between stim and respsonse before RF computing')
    RF_Ntau=param.Integer(default=5, doc='Number of past bins to include into the stimulus for RF computing')      
    STA_biases=param.Array(default=np.array([100]), doc='Laplacian regularization parameters to try for regularized STA (keep the best one)') 
    volt2eig_nsig=param.Integer(default=5, doc='number of eigenvectors to keep for volt2sig (regularized volt2): temporary solution before implementing a significance measure')  
    #check_HSM_compatibility=param.Boolean(default=True, doc="Match RF computing parameters to the HSM ones")
    normalize_by_corrmax=param.Boolean(default=False, doc="Normalize correlation values by maximum achievable given response noise")
    noise_ceiling_vals=param.List(default=[1.], doc="Values of inverse fraction of data to test for noise ceiling analysis")
    displayed_ceiling_fit=param.ObjectSelector(default='linear',objects=['linear','inverse'], doc='Function to fit to corr data to compute noise ceiling exploration')
    noise_ceiling_grouping=param.Integer(default=1, doc="How many noise ceiling vals to group for fitting")
    remove_blanks=param.Boolean(default=False, doc='Whether to remove long blank periods for correlation computation')
    
    # PLOTTING PARAMETERS
    displayed_data=param.ObjectSelector(default='val',objects=['train','reg','val'],doc="Whether to display results based on training, regularization or validation data")
    displayed_ceiling_val=param.Number(default=1, doc="Value of inverse fraction of data to use for RF display")
    ncells_per_fig=param.Integer(default=10, doc='Number of graphs to display per window')
    n_disp_cells=param.Integer(default=10, doc="Number of cells to display")
    im_type=param.String(default='.svg', doc='Image type (extension) for saved figures')
    display_plots=param.Boolean(default=True, doc='Display computed plots') 
    invert_corrplot=param.Boolean(default=False, doc='If true, plot HSM against other models, otherwise the other way round') 
    smooth_plots=param.Boolean(default=False, doc='Whether to smooth RF plots')
    stim_x_rf=param.Boolean(default=True, doc="When results are separated by stim and rf, if True, factor order will be 1)stim, 2)rf. If False, the other way around.")
    factor2_display=param.ObjectSelector(default='same_fig', objects=['same_fig', 'mean', 'max', 'sep_figs'], doc="How to handle second data factor (rf or stim) plot in same fig, compute mean over this factor, or plot in separate figures")
    single_datapoints=param.Boolean(default=True, doc='Whether to overlay single datapoints on mean plots')
    rf_colors=param.Dict(default={}, doc="Colors with which to label rf types")
    stim_colors=param.Dict(default={}, doc="Colors with which to label stimulus types")
    #order_by_maxcorr=param.Boolean(default=False, doc="When applicable, order cells by maximum possible correlation before displaying")
    channel_depths=param.List(default=[], doc="Channel depth for each unit, for unit ordering purposes")
    order_cells_by=param.ObjectSelector(default='id', objects=['id','depth','corr'], doc='Characteristic to use to order cells for display')
    save_memory=param.Boolean(default=False, doc="Whether to delete unused variables to save memory")
 
  
    
display_order={'1/ DATA PATHS': ['data_file','ep_info_file','unit_info_file','stim_categories_file','rf_path','common_name','label'],
               '2/ STEPS TO EXECUTE': ['load_data', 'model_types', 'time_separable', 'add_nonlinearity', 'to_compute','standard_stims','load_existing_RFs','to_plot','to_save'],
               '3/ RF COMPUTING PARAMETERS': ['merge_val_repeats','cells_to_keep','normalize_stim','normalize_resps','ROI','ROI_downsplfact','include_constant','fit_mode','num_epochs','epoch_size','reg_frac','val_frac','fit_seed','rand_seed','RF_tau','RF_Ntau','STA_biases','volt2eig_nsig','normalize_by_corrmax','noise_ceiling_vals','displayed_ceiling_fit','noise_ceiling_grouping','remove_blanks'],
               '4/ PLOTTING PARAMETERS': ['displayed_data','displayed_ceiling_val','ncells_per_fig','n_disp_cells','im_type','display_plots','invert_corrplot','smooth_plots', 'stim_x_rf', 'factor2_display','single_datapoints','rf_colors','stim_colors','channel_depths','order_cells_by','save_memory'] }

colors='rgbcymk'
linestyles=['-','--','-.',':']
#colors='cymk'


try: pm
except NameError: pm=computeRF_params()
if (len(sys.argv)<2):    
    pm,validated=param_dialog(pm,display_order,max_nrows=19)
else:
    if sys.argv[1]!='previous_params':
        pm=computeRF_params(**correct_inputs(retrieve_params(sys.argv[1],raise_error=True),pm))
    validated=True    
if validated:
    print 'PARAMETERS : \n\n'+param_list_string(pm,display_order)+'\n\n'    
   
    """
    RUN
    """ 
    for var_name in ['stim','resps','rfs','rf_preds','rf_corr','rf_predSTA','datasets','n_cells','cells_to_keep','cell_labels','ROI','lscsm','sp','K','ROIshape','STA_corrs','ceil_inds','global_name','theo_corr_max','NL_funcs','standard_stims']:
        if not(vars().has_key(var_name)):
            vars()[var_name]={}
 
   
    if pm.load_data:
        print 'Loading data'
        stim,resps,ep_info=pythondata_to_rfdata(pm.data_file,ep_info_file=pm.ep_info_file,stim_categories=pm.stim_categories_file,file_type='new')
        n_ep=len(ep_info.values()[0])
        data_types=stim.keys()
        unit_info=load_info_table(pm.unit_info_file) if pm.unit_info_file is not None else None
            
        for dtype in data_types:
            n_cells[dtype]=resps[dtype]['raw_train'][0].shape[1]
            if len(pm.cells_to_keep)==0:
                cells_to_keep[dtype]=np.arange(n_cells[dtype])
            else:
                cells_to_keep[dtype]=np.array(pm.cells_to_keep)
            if unit_info and unit_info.has_key('cell_label'):
                cell_labels[dtype]=unit_info['cell_label']
            else:
                cell_labels[dtype]=range(n_cells[dtype])
            cell_labels[dtype]=np.array([str(l) for l in cell_labels[dtype]])
            if len(pm.ROI)==0:
                ROI[dtype]=np.array([[0,stim[dtype]['raw_train'].shape[1]],[0,stim[dtype]['raw_train'].shape[2]]])
            else:
                ROI[dtype]=np.array(pm.ROI)
  
            if pm.merge_val_repeats:
                stim[dtype]['raw_val'],resps[dtype]['raw_val'],theo_corr_max[dtype]=merge_data_repeats(stim[dtype]['raw_val'],resps[dtype]['raw_val'],ep_info[dtype]['raw_val']['stim_id'])
      
#            if 'HSM' in pm.model_types:
#                print 'Loading HSM results'
##                for varname in ['lscsm','sp','K']:
##                    vars()[varname].update({key: {} for key in set(data_types)-set(vars()[varname].keys())})
#                mp,sp[dtype]=loadParams(pm.HSM_files[dtype])
#                sp[dtype]['stim_ROI']=(np.array(sp[dtype]['stim_ROI']).reshape(2,2) if sp[dtype]['stim_ROI'] is not None else stim[dtype]['full'].shape[1:])
#                lscsm[dtype],K[dtype],_=restoreLSCSM(pm.HSM_files[dtype][:-11],np.zeros((1,mp['n_tau']*np.prod(sp[dtype]['stim_ROI'][:,1]-sp[dtype]['stim_ROI'][:,0]))),np.zeros((1,len(sp[dtype]['cell_nums']))))
#                
#                if pm.check_HSM_compatibility:
#                    # Check that parameters are compatible with the HSM ones, adjust them if necessary
#                    # Stim shape
#                    #assert stim[dtype]['full'].shape[1]==sp[dtype]['stimsize']   
#                    # Randomization seed 
#                    if (pm.rand_seed!=sp[dtype]['stim_seed']) and ((pm.rand_seed>=0) or (sp[dtype]['stim_seed'] is not None)):
#                        if sp[dtype]['stim_seed'] is None:
#                            pm.rand_seed=-1
#                        else:
#                            pm.rand_seed=sp[dtype]['stim_seed']
#                        print 'Warning: setting randomization seed to '+str(pm.rand_seed)+' to match HSM'
#                    # Validation and regularization fraction    
#                    if pm.reg_frac+pm.val_frac!=sp[dtype]['val_frac']:
#                        if pm.reg_frac>sp[dtype]['val_frac']*0.5:
#                            pm.reg_frac=pm.val_frac=sp[dtype]['val_frac']*0.5
#                        else:
#                            pm.val_frac=sp[dtype]['val_frac']-pm.reg_frac
#                        print 'Warning: setting val_frac to '+str(pm.val_frac)+' and reg_frac to '+str(pm.reg_frac)+' to match HSM'
#                    # Tau
#                    if sp[dtype]['tau']!=pm.RF_tau:
#                        pm.RF_tau=sp[dtype]['tau']
#                        print 'Warning: setting tau to '+str(pm.RF_tau)+' to match HSM'    
#                    # N_tau
#                    if lscsm[dtype].n_tau!=pm.RF_Ntau:
#                        pm.RF_Ntau=lscsm[dtype].n_tau
#                        print 'Warning: setting Ntau to '+str(pm.RF_Ntau)+' to match HSM'                
#                    # Normalize stim:
#                    if pm.normalize_stim!=sp[dtype]['norm_stim']:
#                        pm.normalize_stim=sp[dtype]['norm_stim']
#                        print 'Warning: setting normalize_stim to '+str(pm.normalize_stim)+' to match HSM'
#                    # Cells to keep    
#                    if set(pm.cells_to_keep)!=set(sp[dtype]['cell_nums']):
#                        pm.cells_to_keep=list(sp[dtype]['cell_nums'])
#                        print 'Warning: setting cells_to_keep to '+str(pm.cells_to_keep)+' to match HSM'
                  
    if pm.normalize_by_corrmax and pm.merge_val_repeats:
         corr_norm_fact={dtype: theo_corr_max[dtype][cells_to_keep[dtype]] for dtype in theo_corr_max.keys()}
         for dtype in corr_norm_fact.keys():
             corr_norm_fact[dtype][corr_norm_fact[dtype]<10**-10]=np.nan
    else:
         corr_norm_fact={dtype: 1. for dtype in theo_corr_max.keys()}  

    
    if pm.time_separable and (pm.fit_mode!='theano'):
        print "Warning : time separability can't be achieved with the selected fitting mode : choose theano instead"
        pm.time_separable=False                
 
   
    
    if 'RFs' in pm.to_compute:        
        # Reshape data according to RF computation parameters    
    
        for varname in ['ROIshape','ceil_inds']:
             vars()[varname].update({key: {} for key in set(data_types)-set(vars()[varname].keys())})
             
        for dtype in data_types:
            if len(pm.cells_to_keep)==0:
                cells_to_keep[dtype]=np.arange(n_cells[dtype])
            else:
                cells_to_keep[dtype]=np.array(pm.cells_to_keep)

            for dset in ['train','reg','val']:
                stim[dtype][dset],resps[dtype][dset],ROIshape[dtype]=reshape_data_forRF(list(stim[dtype]['raw_'+dset]),[r[:,cells_to_keep[dtype]] for r in resps[dtype]['raw_'+dset]],normalize_stim=pm.normalize_stim,normalize_resp=pm.normalize_resps,tau=pm.RF_tau,n_tau=pm.RF_Ntau,stim_seed=pm.rand_seed,sub_data_fracs=[1],ROI=ROI[dtype],downsample_fact=pm.ROI_downsplfact)
                stim[dtype][dset]=np.array(stim[dtype][dset])
                resps[dtype][dset]=np.array(resps[dtype][dset])
#                [stim[dtype]['train'],stim[dtype]['val'],stim[dtype]['reg']],[resps[dtype]['train'],resps[dtype]['val'],resps[dtype]['reg']],ROIshape[dtype]=reshape_data_forRF([stim[dtype]['raw_train'],stim[dtype]['raw_val'],stim[dtype]['raw_reg']],[resps[dtype]['raw_train'][:,cells_to_keep[dtype]],resps[dtype]['raw_val'][:,cells_to_keep[dtype]],resps[dtype]['raw_reg'][:,cells_to_keep[dtype]]],normalize_stim=pm.normalize_stim,normalize_resp=pm.normalize_resps,tau=pm.RF_tau,n_tau=pm.RF_Ntau,stim_seed=pm.rand_seed,sub_data_fracs=[1],ROI=ROI[dtype],downsample_fact=pm.ROI_downsplfact)            
                if 'HSM' in pm.model_types:
#                    if pm.sep_HSM_data:
#                        stim[dtype]['HSM']={}
#                        resps[dtype]['HSM']={}
#                        [stim[dtype]['HSM']['train'],stim[dtype]['HSM']['val'],stim[dtype]['HSM']['reg']],[resps[dtype]['HSM']['train'],resps[dtype]['HSM']['val'],resps[dtype]['HSM']['reg']],_=reshape_data_forRF([stim[dtype]['full'],stim[dtype]['raw_val'],stim[dtype]['raw_reg']],[resps[dtype]['full'][:,cells_to_keep[dtype]],resps[dtype]['raw_val'][:,cells_to_keep[dtype]],resps[dtype]['raw_reg'][:,cells_to_keep[dtype]]],normalize_stim=sp[dtype]['norm_stim'],normalize_resp=False,tau=sp[dtype]['tau'],n_tau=lscsm[dtype].n_tau,stim_seed=sp[dtype]['stim_seed'],ROI=sp[dtype]['stim_ROI'],sub_data_fracs=[1-sp[dtype]['val_frac'],sp[dtype]['val_frac']])
#                    else:
                    stim[dtype]['HSM']={key: stim[dtype][key] for key in ['train','reg','val']}
                    resps[dtype]['HSM']={key: resps[dtype][key] for key in ['train','reg','val']}

            n_ep=len(stim[dtype]['train'])
            for cv in pm.noise_ceiling_vals:
                np.random.seed(int(np.round(cv*100)))
                ceil_inds[dtype][cv] = np.array(sorted(np.random.choice(n_ep, int(np.round(n_ep*1./cv)), replace=False)))
            if pm.save_memory:
                for key in stim[dtype].keys():
                    if key not in ['train','reg','val','HSM']:
                        del stim[dtype][key]
                for key in resps[dtype].keys():
                    if key not in ['train','reg','val','HSM']:
                        del resps[dtype][key]        
#        ROIshape=np.array(data_dict['stim'].shape[1:])
#        [stim['train']],[resps['train']]=reshape_data_forRF(data_dict['stim'],data_dict['resps'][:,pm.cells_to_keep],tau=pm.RF_tau,n_tau=pm.RF_Ntau)
#        [stim['reg']],[resps['reg']]=reshape_data_forRF(data_dict['reg_stim'],data_dict['reg_resps'][:,pm.cells_to_keep],tau=pm.RF_tau,n_tau=pm.RF_Ntau)
#        [stim['val']],[resps['val']]=reshape_data_forRF(data_dict['val_stim'],data_dict['val_resps'][:,pm.cells_to_keep],tau=pm.RF_tau,n_tau=pm.RF_Ntau)
#        HSM_stim['train'],HSM_stim['val'],HSM_resps['train'],HSM_resps['val']=stim['train'],stim['val'],resps['train'],resps['val']
        
        
        # Compute file names        
        for varname in ['global_name','rfs']:
             vars()[varname].update({key: {} for key in set(data_types)-set(vars()[varname].keys())})
        print 'To load/compute :'     
 
        for dtype in data_types: 
            rfs[dtype].update({rf_type: {} for rf_type in set(pm.model_types)-set(rfs[dtype].keys())})
            file_labels=OrderedDict([
                                    ('','RFs'),
                                    ('nx',ROIshape[dtype][0]),  
                                    ('ny',ROIshape[dtype][1]),
                                    ('tau',pm.RF_tau),
                                    ('ntau',pm.RF_Ntau),
                                    ('randseed',pm.rand_seed),
                                    ('ceil',1),
                                    ('normstim',int(pm.normalize_stim)),
                                    ('normresp',int(pm.normalize_resps)),
                                    ('earlystop',int(pm.fit_mode=='theano')),
                                    ('timesep', int(pm.time_separable)) ])   
            global_name[dtype]=os.path.splitext(os.path.basename(pm.data_file))[0]+'_'+dtype+'_'+'_'.join([key+str(file_labels[key]) for key in file_labels.keys()])                 
   
            for rf_type in set(pm.model_types):
                rfs[dtype][rf_type].update({cv: {} for cv in set(pm.noise_ceiling_vals)-set(rfs[dtype][rf_type].keys())})
                for cv in pm.noise_ceiling_vals:
                    file_labels['timesep']=int(file_labels['timesep'] and (rf_type not in ['HSM','STALR']))
                    file_labels['ceil']=cv
                    file_labels['']=rf_type
                    file_labels['const']=int(pm.include_constant and rf_type not in ['STA_LR','HSM'])
                    if rf_type=='volt2sig':
                        file_labels['nsig']=pm.volt2eig_nsig
                    rfs[dtype][rf_type][cv]['file_name']=os.path.splitext(os.path.basename(pm.data_file))[0]+'_'+dtype+'_'+'_'.join([key+str(file_labels[key]) for key in file_labels.keys()])
                    if rf_type=='HSM':
                       rfs[dtype][rf_type][cv]['file_name']+='_BEST_metaparams' 
                    print rfs[dtype][rf_type][cv]['file_name']
                
        if len(pm.common_name)>0:            
            mixed_name=pm.common_name+global_name[dtype][len(os.path.splitext(os.path.basename(pm.data_files[dtype]))[0]):]
        else:
            mixed_name=global_name[dtype]            


        # Load already computed RFs                    
        for dtype in data_types:            
            for rf_type in set(pm.model_types): 
                for cv in pm.noise_ceiling_vals:      
                    
                    if pm.load_existing_RFs and os.path.isfile(os.path.join(pm.rf_path,rfs[dtype][rf_type][cv]['file_name'])):
                        file_name=os.path.join(pm.rf_path,rfs[dtype][rf_type][cv]['file_name'])
                        print 'Loading '+file_name
                        
                        if rf_type=='HSM':
                            rfs[dtype][rf_type][cv]['mp'],rfs[dtype][rf_type][cv]['sp']=loadParams(file_name)
                            rfs[dtype][rf_type][cv]['sp']['stim_ROI']=(np.array(rfs[dtype][rf_type][cv]['sp']['stim_ROI']).reshape(2,2) if rfs[dtype][rf_type][cv]['sp']['stim_ROI'] is not None else stim[dtype]['raw_train'].shape[1:])
                            rfs[dtype][rf_type][cv]['lscsm'],rfs[dtype][rf_type][cv]['K'],_=restoreLSCSM(file_name[:-11],np.zeros((1,rfs[dtype][rf_type][cv]['mp']['n_tau']*np.prod(rfs[dtype][rf_type][cv]['sp']['stim_ROI'][:,1]-rfs[dtype][rf_type][cv]['sp']['stim_ROI'][:,0]))),np.zeros((1,len(rfs[dtype][rf_type][cv]['sp']['cell_nums']))))
                            
                        else:                            
                            temp=loadVec(file_name)
                            if temp[0]!=len(cells_to_keep[dtype]):
                                assert temp[0]==n_cells[dtype]
                                temp_ncells=n_cells[dtype]
                                temp_cellstokeep=cells_to_keep[dtype]
                            else:
                                temp_ncells=len(cells_to_keep[dtype])
                                temp_cellstokeep=range(temp_ncells)
                            pointer=1
                            if (pm.include_constant) and (rf_type!='STALR'):
                                rfs[dtype][rf_type][cv]['0']=temp[pointer:temp_ncells+pointer].reshape(1,-1)[:,temp_cellstokeep]
                                pointer+=temp_ncells
                            if (rf_type=='BWT'):
                                rfs[dtype][rf_type][cv]['1on']=temp[pointer:temp_ncells*np.prod(ROIshape[dtype])*pm.RF_Ntau+pointer].reshape(-1,temp_ncells)[:,temp_cellstokeep]
                                pointer+=temp_ncells*np.prod(ROIshape[dtype])*pm.RF_Ntau
                                rfs[dtype][rf_type][cv]['1off']=temp[pointer:temp_ncells*np.prod(ROIshape[dtype])*pm.RF_Ntau+pointer].reshape(-1,temp_ncells)[:,temp_cellstokeep]
                                pointer+=temp_ncells*np.prod(ROIshape[dtype])*pm.RF_Ntau
                            else:    
                                rfs[dtype][rf_type][cv]['1']=temp[pointer:temp_ncells*np.prod(ROIshape[dtype])*pm.RF_Ntau+pointer].reshape(-1,temp_ncells)[:,temp_cellstokeep]
                                pointer+=temp_ncells*np.prod(ROIshape[dtype])*pm.RF_Ntau
                            if len(temp)>pointer:
                                if rf_type=='volt2diag':
                                    rfs[dtype][rf_type][cv]['2diag']=temp[pointer:].reshape(-1,temp_ncells)[:,temp_cellstokeep]
                                else:
                                    rfs[dtype][rf_type][cv]['2']=temp[pointer:].reshape(-1,temp_ncells)[:,temp_cellstokeep]
                            del temp 
                    
        
        # Compute RFs
        for varname in ['STA_corrs']:
             vars()[varname].update({key: {} for key in set(data_types)-set(vars()[varname].keys())})
             
        for dtype in data_types:           
            for model in pm.model_types:
                for cv in pm.noise_ceiling_vals:
                    if (len(rfs[dtype][model][cv])==1) or not(pm.load_existing_RFs):
                        print 'Computing '+model+' (ceiling val :'+str(cv)+')'
                        RFlen=np.prod(ROIshape[dtype])*pm.RF_Ntau
    
                        if model=='STALR':
                            
                            n_lapl=len(pm.STA_biases)
                            if n_lapl>1:
                                assert stim[dtype]['reg'].shape[0]>0
                                STA_corrs[dtype]=np.zeros([n_lapl,len(cells_to_keep[dtype])])
                                temp_STAs=np.zeros([n_lapl,pm.RF_Ntau*np.prod(ROIshape[dtype]),len(cells_to_keep[dtype])])
                                for i_lapl in range(n_lapl):
                                    print '   Trial n'+str(i_lapl+1)+'/'+str(n_lapl)
                                    temp_STAs[i_lapl]=STA_LR_3D(stim[dtype]['train'][ceil_inds[dtype][cv]].reshape(-1,RFlen),resps[dtype]['train'][ceil_inds[dtype][cv]].reshape(-1,len(cells_to_keep[dtype])),[pm.RF_Ntau]+list(ROIshape[dtype]),pm.STA_biases[i_lapl])
                                    temp_pred=np.dot(stim[dtype]['reg'].reshape(-1,RFlen),temp_STAs[i_lapl])                    
                                    STA_corrs[dtype][i_lapl]=[np.corrcoef(resps[dtype]['reg'][:,:,icell].flatten(),temp_pred[:,icell].flatten())[0,1] for icell in range(len(cells_to_keep[dtype]))]
                                bests=np.argmax(STA_corrs[dtype],axis=0)
                                rfs[dtype]['STALR'][cv]['1']=np.array([temp_STAs[bests[icell],:,icell] for icell in range(len(cells_to_keep[dtype]))]).T
                                rfs[dtype]['STALR'][cv]['lapl_bias']=pm.STA_biases[bests]
                                del temp_STAs, temp_pred
                                plt.figure(); plt.plot(pm.STA_biases,(STA_corrs[dtype]-STA_corrs[dtype].min(axis=0))*1./(STA_corrs[dtype].max(axis=0)-STA_corrs[dtype].min(axis=0)))
                                plt.legend(range(len(cells_to_keep[dtype])))
                            else:
                                rfs[dtype]['STALR'][cv]['1']=STA_LR_3D(stim[dtype]['train'][ceil_inds[dtype][cv]].reshape(-1,RFlen),resps[dtype]['train'][ceil_inds[dtype][cv]].reshape(-1,len(cells_to_keep[dtype])),[pm.RF_Ntau]+list(ROIshape[dtype]),pm.STA_biases[0])
                                rfs[dtype]['STALR'][cv]['lapl_bias']=pm.STA_biases[0]
            
                
                        elif model in ['volt1','volt2diag','volt2','volt2sig']:
                            
                            name='volt2' if model=='volt2sig' else model
                            orders = (['0'] if pm.include_constant else []) + ['1'] + (['2diag'] if model=='volt2diag' else []) + (['2'] if model in ['volt2','volt2sig'] else [])
                            if pm.fit_mode=='linsolve':
                                rfs[dtype][name][cv].update(lsq_kernel(stim[dtype]['train'][ceil_inds[dtype][cv]].reshape(-1,RFlen),resps[dtype]['train'][ceil_inds[dtype][cv]].reshape(-1,len(cells_to_keep[dtype])),orders=orders))
                            elif pm.fit_mode=='theano':
                                rfs[dtype][name][cv].update(fit_volterra_theano(stim[dtype]['train'][ceil_inds[dtype][cv]].reshape(-1,RFlen),resps[dtype]['train'][ceil_inds[dtype][cv]].reshape(-1,len(cells_to_keep[dtype])),stim[dtype]['reg'].reshape(-1,RFlen),resps[dtype]['reg'].reshape(-1,len(cells_to_keep[dtype])),n_tau=pm.RF_Ntau,orders=orders,numEpochs=pm.num_epochs,epochSize=pm.epoch_size,seed=pm.fit_seed,plot_err=True,time_separable=pm.time_separable)[0])
    
                            if model in ['volt2','volt2sig']:
                                rfs[dtype]['volt2'][cv]['eigw2'],rfs[dtype]['volt2'][cv]['eigv2']=diag_kernel2(restorek2shape(rfs[dtype]['volt2'][cv]['2']))
                        
                            if model=='volt2sig':
                                print 'Computing volt2sig'                            
                                rfs[dtype]['volt2sig'][cv]['0']=rfs[dtype]['volt2'][cv]['0']
                                rfs[dtype]['volt2sig'][cv]['1']=rfs[dtype]['volt2'][cv]['1']
                                rfs[dtype]['volt2sig'][cv]['2']=partial_k2(rfs[dtype]['volt2'][cv]['eigw2'],rfs[dtype]['volt2'][cv]['eigv2'],np.ones(len(cells_to_keep[dtype])).astype(int)*pm.volt2eig_nsig)
                            
                                
                        elif model=='BWT':
    
                            assert np.all(ROIshape[dtype]==9)
                            orders=['0','1on','1off'] if pm.include_constant else ['1on','1off']
                            #stim_BWT=movie_to_BWT(stim[dtype]['train'].reshape([-1]+list(ROIshape[dtype])), matlab_engine=_matlab_engine).reshape(stim[dtype]['train'].shape)
                            stim_BWT=movie_to_BWT(stim[dtype]['train'][ceil_inds[dtype][cv]].reshape(-1,RFlen))#.reshape(stim[dtype]['train'][ceil_inds[dtype][cv]].shape)
    #                            stim_BWT=stim[dtype]['train'][ceil_inds[dtype][cv]].reshape(-1,RFlen)
    #                            movie_to_BWT_inplace(stim_BWT)
    #                            stim_BWT=stim_BWT.reshape(stim[dtype]['train'][ceil_inds[dtype][cv]].shape)
                            if pm.fit_mode=='linsolve':
                                rfs[dtype]['BWT'][cv].update(lsq_kernel(stim_BWT,resps[dtype]['train'][ceil_inds[dtype][cv]].reshape(-1,len(cells_to_keep[dtype])),orders=orders))
                            else: 
                                stim_BWT_reg=movie_to_BWT(stim[dtype]['reg'].reshape(-1,RFlen))#.reshape(stim[dtype]['reg'].shape)
                                rfs[dtype]['BWT'][cv].update(fit_volterra_theano(stim_BWT,resps[dtype]['train'][ceil_inds[dtype][cv]].reshape(-1,len(cells_to_keep[dtype])),stim_BWT_reg,resps[dtype]['reg'].reshape(-1,len(cells_to_keep[dtype])),n_tau=pm.RF_Ntau,orders=orders,numEpochs=pm.num_epochs,epochSize=pm.epoch_size,seed=pm.fit_seed,plot_err=True,time_separable=pm.time_separable)[0])


                        elif model=='HSM':
                            
                            print '/nWarning : HSM has to be computed separately !/n'


                            
    if 'RFs' in pm.to_save:
        print 'Saving RFs'
        for dtype in data_types:
            for rf_type in set(pm.model_types)-set(['HSM','volt2sig']):
                for cv in pm.noise_ceiling_vals:
                    datavec=[len(cells_to_keep[dtype])]
                    for key in ['0','1','1on','1off','2diag','2']:                          
                        if rfs[dtype][rf_type][cv].has_key(key):
                            datavec+=list(rfs[dtype][rf_type][cv][key].flatten())
                    saveMat(np.array(datavec),os.path.join(pm.rf_path,rfs[dtype][rf_type][cv]['file_name']))
        
            
    if 'corr' in pm.to_compute:
        
        rf_list = pm.model_types + ([rf_type+'_NL' for rf_type in pm.model_types if rf_type!='HSM'] if pm.add_nonlinearity else [])
        for varname in ['rf_preds','rf_corr','NL_funcs']:
             vars()[varname].update({key: {} for key in set(data_types)-set(vars()[varname].keys())})
             
        for dtype1 in data_types:
            for varname in ['rf_preds','rf_corr']:
                vars()[varname][dtype1].update({key: {} for key in set(data_types)-set(vars()[varname][dtype1].keys())})  
            NL_funcs[dtype1].update({key: {} for key in set(pm.model_types)-set(NL_funcs[dtype1].keys())})     
            for dtype2 in [dtype1]+list(set(data_types)-set([dtype1])): # data_types but starting with dtype1 (mandatory for NL_funcs computation)                
                for varname in ['rf_preds','rf_corr']:                    
                    vars()[varname][dtype1][dtype2].update({key: {} for key in set(rf_list)-set(vars()[varname][dtype1][dtype2].keys())})                   
                for rf_type in pm.model_types:   
                    for varname in ['rf_preds','rf_corr']:                    
                        vars()[varname][dtype1][dtype2][rf_type].update({cv: {} for cv in set(pm.noise_ceiling_vals)-set(vars()[varname][dtype1][dtype2][rf_type].keys())}) 
                        if pm.add_nonlinearity: 
                            vars()[varname][dtype1][dtype2][rf_type+'_NL'].update({cv: {} for cv in set(pm.noise_ceiling_vals)-set(vars()[varname][dtype1][dtype2][rf_type+'_NL'].keys())})
                    for cv in pm.noise_ceiling_vals:                       
                        print 'Computing '+rf_type+' correlation ('+dtype1+' predicts '+dtype2+', noise ceiling val :'+str(cv)+')'                       
                        RFlen=np.prod(ROIshape[dtype2])*pm.RF_Ntau

                        if rf_type!='HSM':
                            for dset in ['train','reg','val']:
                                if stim[dtype2][dset].shape[0]>0:
                                    valid_times=np.where(np.any(stim[dtype2][dset].reshape(-1,RFlen)!=0,axis=1))[0] if pm.remove_blanks else np.arange(stim[dtype2][dset].reshape(-1,RFlen).shape[0])
                                    if rf_type=='BWT':
#                                        stim_BWT=stim[dtype2][dset].reshape(-1,RFlen)
#                                        movie_to_BWT_inplace(stim_BWT)
#                                        stim_BWT=stim_BWT.reshape(stim[dtype2][dset].shape)
                                        stim_BWT=movie_to_BWT(stim[dtype2][dset].reshape(-1,RFlen))#.reshape(stim[dtype2][dset].shape)
                                        #stim_BWT=movie_to_BWT(stim[dtype2][dset].reshape([-1]+list(ROIshape[dtype2])), matlab_engine=_matlab_engine).reshape(stim[dtype2][dset].shape)
                                        rf_preds[dtype1][dtype2][rf_type][cv][dset]=np.dot(np.maximum(stim_BWT,0),rfs[dtype1][rf_type][cv]['1on'])+np.dot(np.maximum(-stim_BWT,0),rfs[dtype1][rf_type][cv]['1off'])
                                    else:    
                                        rf_preds[dtype1][dtype2][rf_type][cv][dset]=np.dot(stim[dtype2][dset].reshape(-1,RFlen),rfs[dtype1][rf_type][cv]['1'])
                                    if rfs[dtype1][rf_type].has_key('0'):
                                        rf_preds[dtype1][dtype2][rf_type][cv][dset]+=rfs[dtype1][rf_type][cv]['0']
                                    if rf_type=='volt2diag':
                                        rf_preds[dtype1][dtype2][rf_type][cv][dset]+=np.dot(stim[dtype2][dset].reshape(-1,RFlen)**2,rfs[dtype1][rf_type][cv]['2diag'])
                                    elif rf_type=='volt2':     
                                        rf_preds[dtype1][dtype2][rf_type][cv][dset]+=k2resp(restorek2shape(rfs[dtype1][rf_type][cv]['2']),stim[dtype2][dset].reshape(-1,RFlen))
                                    elif rf_type=='volt2sig': 
                                        rf_preds[dtype1][dtype2][rf_type][cv][dset]+=k2resp(rfs[dtype1][rf_type][cv]['2'],stim[dtype2][dset].reshape(-1,RFlen)) 
                                    rf_corr[dtype1][dtype2][rf_type][cv][dset]=np.array([np.corrcoef(resps[dtype2][dset].reshape(-1,len(cells_to_keep[dtype2]))[valid_times,icell].flatten(),rf_preds[dtype1][dtype2][rf_type][cv][dset][valid_times,icell].flatten())[0,1] for icell in range(len(cells_to_keep[dtype1]))])      
                                    if pm.add_nonlinearity:
                                        if (dtype1==dtype2) & (dset=='train'):
                                            NL_funcs[dtype1][rf_type][cv]=[estimate_output_nonlinearity(rf_preds[dtype1][dtype1][rf_type][cv]['train'].reshape(resps[dtype1]['train'].shape)[ceil_inds[dtype1][cv],:,icell].flatten(),resps[dtype1]['train'][ceil_inds[dtype1][cv],:,icell].flatten(),n_intervals=15) for icell in range(len(cells_to_keep[dtype1]))]
                                        rf_preds[dtype1][dtype2][rf_type+'_NL'][cv][dset]=np.array([NL_funcs[dtype1][rf_type][cv][icell](rf_preds[dtype1][dtype2][rf_type][cv][dset][:,icell]) for icell in range(len(cells_to_keep[dtype1]))]).T
                                        rf_corr[dtype1][dtype2][rf_type+'_NL'][cv][dset]=np.array([np.corrcoef(resps[dtype2][dset].reshape(-1,len(cells_to_keep[dtype2]))[valid_times,icell].flatten(),rf_preds[dtype1][dtype2][rf_type+'_NL'][cv][dset][valid_times,icell].flatten())[0,1] for icell in range(len(cells_to_keep[dtype1]))])
                                        if pm.save_memory:
                                            del rf_preds[dtype1][dtype2][rf_type+'_NL'][cv][dset]
                                    if pm.save_memory :
                                        del rf_preds[dtype1][dtype2][rf_type][cv][dset]
                        else:
                            for dset in ['train','reg','val']:
                                if stim[dtype2]['HSM'][dset].shape[0]>0:
                                    rf_preds[dtype1][dtype2][rf_type][cv][dset]=rfs[dtype1][rf_type][cv]['lscsm'].response(stim[dtype2]['HSM'][dset].reshape(-1,RFlen),rfs[dtype1][rf_type][cv]['K'])
                                    rf_corr[dtype1][dtype2][rf_type][cv][dset]=np.array([np.corrcoef(resps[dtype2]['HSM'][dset][:,:,icell].flatten(),rf_preds[dtype1][dtype2][rf_type][cv][dset][:,icell].flatten())[0,1] for icell in range(len(cells_to_keep[dtype1]))])
                                    if pm.save_memory:
                                        del rf_preds[dtype1][dtype2][rf_type][cv][dset]



    if 'corr_ceiling' in pm.to_compute:
        
        fit_list=['linear','inverse','square']
        rf_list = pm.model_types + ([rf_type+'_NL' for rf_type in pm.model_types if rf_type!='HSM'] if pm.add_nonlinearity else [])
        for dtype1 in data_types:                        
            for dtype2 in data_types:
                for rf_type in rf_list:
                    rf_corr[dtype1][dtype2][rf_type][0]={}
                    rf_corr[dtype1][dtype2][rf_type].update({fit: {'intercept':{}, 'slope':{}} for fit in fit_list})
                    for dset in ['train','reg','val']:                        
                        for fit in fit_list:                              
                            rf_corr[dtype1][dtype2][rf_type][fit]['intercept'][dset],rf_corr[dtype1][dtype2][rf_type][fit]['slope'][dset]=noise_ceiling_fit(pm.noise_ceiling_vals,[rf_corr[dtype1][dtype2][rf_type][cv][dset] for cv in pm.noise_ceiling_vals],fit_type=fit,averaging_size=pm.noise_ceiling_grouping)
                            if fit=='inverse':
                                rf_corr[dtype1][dtype2][rf_type][fit]['intercept'][dset]=1./rf_corr[dtype1][dtype2][rf_type][fit]['intercept'][dset]                                                                
                        rf_corr[dtype1][dtype2][rf_type][0][dset]=rf_corr[dtype1][dtype2][rf_type][pm.displayed_ceiling_fit]['intercept'][dset]



    if 'pred_STA' in pm.to_compute:
        
        predSTA_tau=10
        rf_list = pm.model_types + ([rf_type+'_NL' for rf_type in pm.model_types if rf_type!='HSM'] if pm.add_nonlinearity else [])
        for varname in ['rf_predSTA']:
             vars()[varname].update({key: {} for key in set(data_types)-set(vars()[varname].keys())})
             
        for dtype1 in data_types:
            for varname in ['rf_predSTA']:
                vars()[varname][dtype1].update({key: {} for key in set(data_types)-set(vars()[varname][dtype1].keys())})  
            for dtype2 in [dtype1]+list(set(data_types)-set([dtype1])): # data_types but starting with dtype1 (mandatory for NL_funcs computation)                
                for varname in ['rf_predSTA']:                    
                    vars()[varname][dtype1][dtype2].update({key: {} for key in set(rf_list)-set(vars()[varname][dtype1][dtype2].keys())})                   
                for rf_type in rf_list:   
                    for varname in ['rf_predSTA']:                    
                        vars()[varname][dtype1][dtype2][rf_type].update({cv: {} for cv in set(pm.noise_ceiling_vals)-set(vars()[varname][dtype1][dtype2][rf_type].keys())}) 
                    for cv in pm.noise_ceiling_vals:                       
                        print 'Computing '+rf_type+' predSTA ('+dtype1+' predicts '+dtype2+', noise ceiling val :'+str(cv)+')'   
                        
                        for dset in ['train','reg','val']:
                            if stim[dtype2][dset].shape[0]>0:
                                rf_predSTA[dtype1][dtype2][rf_type][cv][dset]=np.zeros([2*predSTA_tau+1,len(cells_to_keep[dtype2])])
                                for icell in range(len(cells_to_keep[dtype2])):
                                    [reshaped_pred],[reshaped_resp],_=reshape_data_forRF(rf_preds[dtype1][dtype2][rf_type][cv][dset][:,icell:icell+1],resps[dtype2][dset].reshape(-1,len(cells_to_keep[dtype2]))[:,icell:icell+1],normalize_stim=False,normalize_resp=False,tau=-predSTA_tau,n_tau=2*predSTA_tau+1,stim_seed=None,sub_data_fracs=[1],ROI=None,downsample_fact=1)
                                    try:
                                        rf_predSTA[dtype1][dtype2][rf_type][cv][dset][:,icell:icell+1]=lsq_kernel(reshaped_pred,reshaped_resp,orders=['0','1'],pure_STA=True)['1']
                                    except:
                                        rf_predSTA[dtype1][dtype2][rf_type][cv][dset][:,icell:icell+1]=np.nan


    # Plot RFs
    if len(pm.to_plot)>0:
        
       figs={}
       if pm.display_plots:
           plt.ion()
       else:
           plt.ioff()
       rfs_to_plot=pm.model_types + ([rf_type+'_NL' for rf_type in pm.model_types] if pm.add_nonlinearity else []) 
       rfs_to_plot=sorted(list( set(rfs_to_plot)- set (['HSM','HSM_NL']) )) + (['HSM'] if 'HSM' in pm.model_types else [])
#       rfs_to_plot=['volt1_NL','BWT_NL','HSM']    
#       print "\nWarning : rfs_to_plot forced to be", rfs_to_plot , "\n"   
       data_types=['natmov', 'natmovRP', 'natmovRF3', 'natmovRF', 'natmovRSP', 'natmovRAP']
       print "\nWarning : data_types forced to be", data_types , "\n"   
       
       # Choose colors (if not defined by user)
       count=0
       rf_colors=colornames_to_rgb(pm.rf_colors)
       for rf_type in rfs_to_plot:
           if not(rf_colors.has_key(rf_type)):
               rf_colors[rf_type]=colors[count%len(colors)]
               count+=1 
       stim_colors=colornames_to_rgb(pm.stim_colors)        
       for dtype in data_types:        
           if not(stim_colors.has_key(dtype)):
               stim_colors[dtype]=colors[count%len(colors)]
               count+=1
               
       # Define cell ordering for plotting purposes        
       if (pm.order_cells_by=='depth') and (len(pm.channel_depths)>0):       
           unit_order={dtype: np.argsort(np.array(pm.channel_depths)[cells_to_keep[dtype]]) for dtype in cells_to_keep.keys()}  
       elif (pm.order_cells_by=='corr') and theo_corr_max.has_key('all') and (len(theo_corr_max['all'])>0):   
           unit_order={dtype: np.argsort(-np.array(theo_corr_max['all'])[cells_to_keep[dtype]]) for dtype in cells_to_keep.keys()}  
       else:
           unit_order={dtype: np.arange(len(cells_to_keep[dtype])) for dtype in data_types}
           
  
    
       if 'RFs' in pm.to_plot:
           print 'Plotting RFs'
           
#           cv=pm.displayed_ceiling_val
#           for dtype in data_types:
#               for rf_type in set(pm.model_types)-set(['HSM']):                  
#                   if rf_type=='volt2':
#                       figs[rfs[dtype][rf_type][cv]['file_name']]=plot_kernel2(rfs[dtype][rf_type][cv]['1'][:,unit_order[dtype][:pm.n_disp_cells]],rfs[dtype][rf_type][cv]['eigw2'][:,unit_order[dtype][:pm.n_disp_cells]],rfs[dtype][rf_type][cv]['eigv2'][:,unit_order[dtype][:pm.n_disp_cells]],ntau=pm.RF_Ntau,k2neig=10,title=rfs[dtype][rf_type][cv]['file_name'],cell_labels=cell_labels[dtype][unit_order[dtype][:pm.n_disp_cells]])
#                   elif rf_type=='volt2diag':
#                       figs[rfs[dtype][rf_type][cv]['file_name']+'_v1']=plotk1_multifig(rfs[dtype][rf_type][cv]['1'][:,unit_order[dtype][:pm.n_disp_cells]],ntau=pm.RF_Ntau,ncellsperfig=pm.ncells_per_fig,scalemax='bycell',stimshape=ROIshape[dtype],celllabels=cell_labels[dtype][unit_order[dtype][:pm.n_disp_cells]],title=rfs[dtype][rf_type][cv]['file_name']+'_v1',smooth=pm.smooth_plots)
#                       figs[rfs[dtype][rf_type][cv]['file_name']+'_v2d']=plotk1_multifig(rfs[dtype][rf_type][cv]['2diag'][:,unit_order[dtype][:pm.n_disp_cells]],ntau=pm.RF_Ntau,ncellsperfig=pm.ncells_per_fig,scalemax='bycell',stimshape=ROIshape[dtype],celllabels=cell_labels[dtype][unit_order[dtype][:pm.n_disp_cells]],title=rfs[dtype][rf_type][cv]['file_name']+'_v2d',smooth=pm.smooth_plots)
#                   elif rf_type=='BWT':
#                       figs[rfs[dtype][rf_type][cv]['file_name']+'_v1+']=plotk1_multifig(rfs[dtype][rf_type][cv]['1on'][:,unit_order[dtype][:pm.n_disp_cells]],ntau=pm.RF_Ntau,ncellsperfig=pm.ncells_per_fig,scalemax='bycell',stimshape=ROIshape[dtype],celllabels=cell_labels[dtype][unit_order[dtype][:pm.n_disp_cells]],title=rfs[dtype][rf_type][cv]['file_name']+'_v1+',smooth=pm.smooth_plots)
#                       figs[rfs[dtype][rf_type][cv]['file_name']+'_v1-']=plotk1_multifig(rfs[dtype][rf_type][cv]['1off'][:,unit_order[dtype][:pm.n_disp_cells]],ntau=pm.RF_Ntau,ncellsperfig=pm.ncells_per_fig,scalemax='bycell',stimshape=ROIshape[dtype],celllabels=cell_labels[dtype][unit_order[dtype][:pm.n_disp_cells]],title=rfs[dtype][rf_type][cv]['file_name']+'_v1-',smooth=pm.smooth_plots)
#                   elif rf_type!='volt2sig':   
#                       figs[rfs[dtype][rf_type][cv]['file_name']]=plotk1_multifig(rfs[dtype][rf_type][cv]['1'][:,unit_order[dtype][:pm.n_disp_cells]],ntau=pm.RF_Ntau,ncellsperfig=pm.ncells_per_fig,scalemax='bycell',stimshape=ROIshape[dtype],celllabels=cell_labels[dtype][unit_order[dtype][:pm.n_disp_cells]],title=rfs[dtype][rf_type][cv]['file_name'],smooth=pm.smooth_plots)

           cv=pm.displayed_ceiling_val
           main_dtype='all' if ('all' in data_types) else data_types[0] # attention je prends un main data type au pif quand pas de all
           for icell in unit_order[main_dtype][:pm.n_disp_cells]: # attention je prends un unit_order au pif plutot que d'en definir un commun, c'est sale
               for rf_type in set(pm.model_types)-set(['HSM']):
                   if rf_type=='volt2': # attention je prends all plutot que de definir un nom commun, c'est sale
                       figs[rfs[main_dtype][rf_type][cv]['file_name']+'_cell'+cell_labels[main_dtype][icell]]=plot_kernel2(np.array([rfs[dtype][rf_type][cv]['1'][:,icell] for dtype in data_types]).T,np.array([rfs[dtype][rf_type][cv]['eigw2'][:,icell] for dtype in data_types]).T,np.array([rfs[dtype][rf_type][cv]['eigv2'][:,icell] for dtype in data_types]).T,ntau=pm.RF_Ntau,k2neig=10,title=rf_type+' '+cell_labels[main_dtype][icell],cell_labels=data_types)
                   elif rf_type=='volt2diag':
                       figs[rfs[main_dtype][rf_type][cv]['file_name']+'_v1'+'_cell'+cell_labels[dtype][icell]]=plotk1_multifig(np.array([rfs[dtype][rf_type][cv]['1'][:,icell] for dtype in data_types]).T,ntau=pm.RF_Ntau,ncellsperfig=len(data_types),scalemax='bycell',stimshape=ROIshape[main_dtype],celllabels=data_types,title=rf_type+'_v1 '+cell_labels[main_dtype][icell],smooth=pm.smooth_plots)
                       figs[rfs[main_dtype][rf_type][cv]['file_name']+'_v2d'+'_cell'+cell_labels[dtype][icell]]=plotk1_multifig(np.array([rfs[dtype][rf_type][cv]['2diag'][:,icell] for dtype in data_types]).T,ntau=pm.RF_Ntau,ncellsperfig=len(data_types),scalemax='bycell',stimshape=ROIshape[main_dtype],celllabels=data_types,title=rf_type+'_v2d '+cell_labels[main_dtype][icell],smooth=pm.smooth_plots)
                   elif rf_type=='BWT':
                       figs[rfs[main_dtype][rf_type][cv]['file_name']+'_v1+'+'_cell'+cell_labels[dtype][icell]]=plotk1_multifig(np.array([rfs[dtype][rf_type][cv]['1on'][:,icell] for dtype in data_types]).T,ntau=pm.RF_Ntau,ncellsperfig=len(data_types),scalemax='bycell',stimshape=ROIshape[main_dtype],celllabels=data_types,title=rf_type+'_v1+ '+cell_labels[main_dtype][icell],smooth=pm.smooth_plots)
                       figs[rfs[main_dtype][rf_type][cv]['file_name']+'_v1-'+'_cell'+cell_labels[dtype][icell]]=plotk1_multifig(np.array([rfs[dtype][rf_type][cv]['1off'][:,icell] for dtype in data_types]).T,ntau=pm.RF_Ntau,ncellsperfig=len(data_types),scalemax='bycell',stimshape=ROIshape[main_dtype],celllabels=data_types,title=rf_type+'_v1- '+cell_labels[main_dtype][icell],smooth=pm.smooth_plots)
                   elif rf_type!='volt2sig':   
                       figs[rfs[main_dtype][rf_type][cv]['file_name']+'_cell'+cell_labels[dtype][icell]]=plotk1_multifig(np.array([rfs[dtype][rf_type][cv]['1'][:,icell] for dtype in data_types]).T,ntau=pm.RF_Ntau,ncellsperfig=len(data_types),scalemax='bycell',stimshape=ROIshape[main_dtype],celllabels=data_types,title=rf_type+' '+cell_labels[main_dtype][icell],smooth=pm.smooth_plots)



       if 'non_linearities' in pm.to_plot:
           print 'Plotting non-linearities'
           
           count=0
           leg_items=[]
           cv=pm.displayed_ceiling_val
           rf_names=list(set(pm.model_types)-set(['HSM']))
           figs[mixed_name+'_nonlinearities']=[]
           for icell in range(len(cells_to_keep[data_types[0]])):
               if icell%pm.ncells_per_fig==0:
                   count+=1
                   figs[mixed_name+'_nonlinearities'].append(plt.figure())
               plt.subplot(1,pm.ncells_per_fig,icell%pm.ncells_per_fig+1)   
               for idtype in range(len(data_types)):
                   for irf in range(len(rf_names)):
                       dtype=data_types[idtype]
                       rf_type=rf_names[irf]
                       leg_items.append(plt.plot(NL_funcs[dtype][rf_type][cv][icell].x,NL_funcs[dtype][rf_type][cv][icell].y,linestyle=linestyles[irf%len(linestyles)],color=stim_colors[dtype], linewidth=2)[0])
               if icell%pm.ncells_per_fig==0:
                   plt.legend(leg_items,[dtype+', '+rf_type for dtype in data_types for rf_type in rf_names])
               plt.title(icell)    
               
               
       if ('temporal_kernels' in pm.to_plot) and (len(lscsm)>0):
           print 'Plotting temporal_kernels'
           
           cv=pm.displayed_ceiling_val
           peak_pos=2
           HSM_params={dtype: retrieve_LSCSM_params(rfs[dtype]['HSM'][cv]['lscsm'],rfs[dtype]['HSM'][cv]['K'])[0] for dtype in data_types}

           figs[mixed_name+'_temporalkernels']=[plt.figure()]           
           leg_items=[]
           for dtype in data_types:
               corrected_kernels=HSM_params[dtype]['lgn_temporal_rf'].copy()
               max_pos=np.argmax(corrected_kernels,axis=0) # On retourne les kernels pour lesquels le pic negatif arrive avant le pic positif
               min_pos=np.argmin(corrected_kernels,axis=0)
               corrected_kernels[:,min_pos<max_pos]*=-1
#               corrected_kernels[:,corrected_kernels[peak_pos,:]<0]*=-1
               leg_items.append(plt.plot(HSM_params[dtype]['precise_rf_time'],np.nanmean(corrected_kernels,axis=1),color=stim_colors[dtype],linewidth=3)[0])
               plt.plot(HSM_params[dtype]['precise_rf_time'],corrected_kernels,color=stim_colors[dtype],linewidth=1,alpha=0.4)
           plt.legend(leg_items,data_types)  
           plt.suptitle(mixed_name+'_temporalkernels')
           
           figs[mixed_name+'_temporalkernels_means']=[plt.figure()]
           leg_items=[]
           for dtype in data_types:
               corrected_kernels=HSM_params[dtype]['lgn_temporal_rf'].copy()
               max_pos=np.argmax(corrected_kernels,axis=0)
               min_pos=np.argmin(corrected_kernels,axis=0)
               corrected_kernels[:,min_pos<max_pos]*=-1
#               corrected_kernels[:,corrected_kernels[peak_pos,:]<0]*=-1
               kernel_mean=np.nanmean(corrected_kernels,axis=1)
               kernel_sem=2./np.sqrt(corrected_kernels.shape[1])*np.nanstd(corrected_kernels,axis=1)
               leg_items.append(plt.plot(HSM_params[dtype]['precise_rf_time'],corrected_kernels.mean(axis=1),color=stim_colors[dtype],linewidth=2)[0])
               plt.fill_between(HSM_params[dtype]['precise_rf_time'],kernel_mean-kernel_sem,kernel_mean+kernel_sem,color=stim_colors[dtype],alpha=0.1)
           plt.legend(leg_items,data_types)  
           plt.suptitle(mixed_name+'_temporalkernels_means')
           
           figs[mixed_name+'_temporalkernels_hists']=[plt.figure()]
           axes=[plt.subplot(2,4,i+1) for i in range(8)]
           count=0
           for dtype in data_types:
               corrected_kernels=HSM_params[dtype]['lgn_temporal_rf'].copy()
               max_pos=np.argmax(corrected_kernels,axis=0)
               min_pos=np.argmin(corrected_kernels,axis=0)
               inverted=np.where(min_pos<max_pos)[0]
               max_pos[inverted],min_pos[inverted]=min_pos[inverted],max_pos[inverted]
               corrected_kernels[:,inverted]*=-1
               axes[0].hist(min_pos,bins=range(len(HSM_params[dtype]['lgn_temporal_rf'])),color=stim_colors[dtype],alpha=0.5)
               axes[4].bar(count,np.mean(min_pos),yerr=np.std(min_pos)*2./np.sqrt(len(min_pos)),color=stim_colors[dtype])
               axes[0].set_title('negative peak position')
               axes[1].hist(max_pos,bins=range(len(HSM_params[dtype]['lgn_temporal_rf'])),color=stim_colors[dtype],alpha=0.5)
               axes[5].bar(count,np.mean(max_pos),yerr=np.std(max_pos)*2./np.sqrt(len(min_pos)),color=stim_colors[dtype])
               axes[1].set_title('positive peak position')
               neg_ampl=np.array([max(-corrected_kernels[min_pos[icell],icell],0) for icell in range(corrected_kernels.shape[1])])
               axes[2].hist(neg_ampl,bins=np.linspace(0,np.max(np.abs(corrected_kernels)),20),color=stim_colors[dtype],alpha=0.5)
               axes[6].bar(count,np.mean(neg_ampl),yerr=np.std(neg_ampl)*2./np.sqrt(len(neg_ampl)),color=stim_colors[dtype])
               axes[2].set_title('negative peak amplitude')
               pos_ampl=np.array([max(corrected_kernels[max_pos[icell],icell],0) for icell in range(corrected_kernels.shape[1])])
               axes[3].hist(pos_ampl,bins=np.linspace(0,np.max(np.abs(corrected_kernels)),20),color=stim_colors[dtype],alpha=0.5)
               axes[7].bar(count,np.mean(pos_ampl),yerr=np.std(pos_ampl)*2./np.sqrt(len(neg_ampl)),color=stim_colors[dtype])
               axes[3].set_title('positive peak amplitude')
               count+=1
           plt.suptitle(mixed_name+'_temporalkernels_hists')     
               
               

       if 'corr_ceiling_fit_old' in pm.to_plot:
           
           print 'Plotting noise ceiling analysis'
           #cells_to_plot=np.array([13,24,40])
           # Fit + extrapolation vs max value
           fit=pm.displayed_ceiling_fit
           for dtype2 in data_types:  
               figs[global_name[dtype2]+'_corrceiling']=plt.figure()
               plt.suptitle(dtype2)
               for irf in range(len(rfs_to_plot)):    
                   plt.subplot(2,len(rfs_to_plot),irf+1)
                   for dtype1 in [dtype2]: #data_types:
                       averaged_ceiling_vals=np.array(pm.noise_ceiling_vals).reshape(-1,pm.noise_ceiling_grouping).mean(axis=1).reshape(-1,1)
                       averaged_corr_vals=np.array([rf_corr[dtype1][dtype2][rf_type][cv][pm.displayed_data] for cv in pm.noise_ceiling_vals]).reshape(-1,pm.noise_ceiling_grouping,len(cells_to_keep[dtype1])).mean(axis=1)
                       if fit=='linear':
                           plt.plot(averaged_ceiling_vals,averaged_corr_vals,marker='o',color=stim_colors[dtype1],linewidth=0.5)
                           fitted_max=rf_corr[dtype1][dtype2][rfs_to_plot[irf]][fit]['intercept'][pm.displayed_data]+rf_corr[dtype1][dtype2][rfs_to_plot[irf]][fit]['slope'][pm.displayed_data]*max(averaged_ceiling_vals)
                           plt.plot([0,max(averaged_ceiling_vals)],np.array([rf_corr[dtype1][dtype2][rfs_to_plot[irf]][0][pm.displayed_data],fitted_max]),color=stim_colors[dtype1])
    #                       plt.plot(1./np.array(pm.noise_ceiling_vals),[rf_corr[dtype1][dtype2][rfs_to_plot[irf]][cv][pm.displayed_data] for cv in pm.noise_ceiling_vals],marker='o',color=stim_colors[dtype1],linewidth=0.5)
    #                       plt.plot(np.array(pm.noise_ceiling_vals),[rf_corr[dtype1][dtype2][rfs_to_plot[irf]][cv][pm.displayed_data] for cv in pm.noise_ceiling_vals],marker='o',color=stim_colors[dtype1],linewidth=0.5)
    #                       plt.plot(np.array(pm.noise_ceiling_vals),[1./rf_corr[dtype1][dtype2][rfs_to_plot[irf]][cv][pm.displayed_data] for cv in pm.noise_ceiling_vals],marker='o',color=stim_colors[dtype1],linewidth=0.5)
                       elif fit=='inverse':                         
                           plt.plot(averaged_ceiling_vals,averaged_corr_vals,marker='o',color=stim_colors[dtype1],linewidth=0.5)
                           fitted_curves=1./(1./rf_corr[dtype1][dtype2][rfs_to_plot[irf]][fit]['intercept'][pm.displayed_data].reshape(1,-1)+rf_corr[dtype1][dtype2][rfs_to_plot[irf]][fit]['slope'][pm.displayed_data].reshape(1,-1)*np.array([0]+list(averaged_ceiling_vals)).reshape(-1,1))
                           plt.plot([0]+list(averaged_ceiling_vals),fitted_curves,color=stim_colors[dtype1])
#                           for icell in unit_order[dtype]:
#                               plt.figure()
#                               plt.plot(averaged_ceiling_vals,averaged_corr_vals[:,icell],marker='o',color=stim_colors[dtype1],linewidth=0.5)
#                               plt.plot([0]+list(averaged_ceiling_vals),fitted_curves[:,icell],color=stim_colors[dtype1])
#                           for icell in unit_order[dtype]:
#                               plt.figure()
#                               plt.plot(averaged_ceiling_vals,1./averaged_corr_vals[:,icell],marker='o',color=stim_colors[dtype1],linewidth=0.5)  
#                               plt.plot([0]+list(averaged_ceiling_vals),1./fitted_curves[:,icell],color=stim_colors[dtype1])
                       elif fit=='polynomial':                           
                           plt.plot(averaged_ceiling_vals,averaged_corr_vals,marker='o',color=stim_colors[dtype1],linewidth=0.5)
                           orders=range(1,3)
                           poly_orders=np.array([np.array([0]+averaged_ceiling_vals)**val for val in orders]).T
                           fitted_curves=rf_corr[dtype1][dtype2][rfs_to_plot[irf]][fit]['intercept'][pm.displayed_data].reshape(1,-1)+np.dot(rf_corr[dtype1][dtype2][rfs_to_plot[irf]][fit]['slope'][pm.displayed_data],poly_orders)
                           plt.plot([0]+list(averaged_ceiling_vals),fitted_curves,color=stim_colors[dtype1])



               plt.subplot(2,len(rfs_to_plot),irf+1+len(rfs_to_plot))
               plt.plot([0,1],[0,1],'k')
               for dtype1 in [dtype2]: #data_types:
                   plt.plot(rf_corr[dtype1][dtype2][rfs_to_plot[irf]][1][pm.displayed_data],rf_corr[dtype1][dtype2][rfs_to_plot[irf]][0][pm.displayed_data].flatten(),'o',color=stim_colors[dtype1])


           for dtype2 in data_types:
               figs[global_name[dtype2]+'_corrceiling_bis']=plt.figure()
               plt.suptitle(dtype2)
               for irf in range(len(rfs_to_plot)):    
                   plt.subplot(1,len(rfs_to_plot),irf+1)
                   for dtype1 in [dtype2]: #data_types:                   
                       plt.plot(1./np.array(pm.noise_ceiling_vals),[rf_corr[dtype1][dtype2][rfs_to_plot[irf]][cv][pm.displayed_data] for cv in pm.noise_ceiling_vals],marker='o',color=stim_colors[dtype1],linewidth=0.5)


           # xy plots
           for irf in range(len(rfs_to_plot)):  
#               std_vals={dtype: np.nanstd(np.array([rf_corr[dtype][dtype][rfs_to_plot[irf]][cv][pm.displayed_data] for cv in pm.noise_ceiling_vals]),axis=0) for dtype in data_types}                         
               xy_vals={dtype: {'std': np.nanstd(np.array([rf_corr[dtype][dtype][rfs_to_plot[irf]][cv][pm.displayed_data] for cv in pm.noise_ceiling_vals]),axis=0),
                                'corr': rf_corr[dtype][dtype][rfs_to_plot[irf]][1.][pm.displayed_data]*1./corr_norm_fact[dtype],
                                'extrap_corr': rf_corr[dtype][dtype][rfs_to_plot[irf]][0.][pm.displayed_data]*1./corr_norm_fact[dtype],
                                'slope': rf_corr[dtype][dtype][rfs_to_plot[irf]]['slope'][pm.displayed_data],
                                'max_corr': theo_corr_max[dtype][cells_to_keep[dtype]]} for dtype in data_types}
               val_names=xy_vals[data_types[0]].keys()
               n_vals=len(val_names)           
               plt.figure()
               for x_ind in range(n_vals):
                   for y_ind in sorted(list(set(range(n_vals))-set([x_ind]))):
                       plt.subplot(n_vals,n_vals,y_ind*n_vals+x_ind+1) 
                       for dtype in data_types:
                           plt.plot(xy_vals[dtype][val_names[x_ind]],xy_vals[dtype][val_names[y_ind]],'o',color=stim_colors[dtype])
                       #if x_ind==0:
                       plt.ylabel(val_names[y_ind])
                       #if y_ind==n_vals-1:  
                       plt.xlabel(val_names[x_ind])

               

       if 'corr_ceiling_fit' in pm.to_plot:
           
           print 'Plotting noise ceiling analysis'

           fits=['linear','inverse','square']
           for dtype in data_types:  
               for irf in range(len(rfs_to_plot)):                   
                   averaged_ceiling_vals=np.array(pm.noise_ceiling_vals).reshape(-1,pm.noise_ceiling_grouping).mean(axis=1).reshape(-1,1)
                   averaged_corr_vals=np.array([rf_corr[dtype1][dtype2][rf_type][cv][pm.displayed_data] for cv in pm.noise_ceiling_vals]).reshape(-1,pm.noise_ceiling_grouping,len(cells_to_keep[dtype1])).mean(axis=1)
                   orders=range(1,3)
                   poly_orders=np.array([np.array([0]+list(averaged_ceiling_vals.flatten()))**val for val in orders]).T
                   fitted_curves={'linear': rf_corr[dtype][dtype][rfs_to_plot[irf]]['linear']['intercept'][pm.displayed_data].reshape(1,-1)+rf_corr[dtype][dtype][rfs_to_plot[irf]]['linear']['slope'][pm.displayed_data].reshape(1,-1)*np.array([0]+list(averaged_ceiling_vals)).reshape(-1,1),
                                  'inverse': 1./(1./rf_corr[dtype][dtype][rfs_to_plot[irf]]['inverse']['intercept'][pm.displayed_data].reshape(1,-1)+rf_corr[dtype][dtype][rfs_to_plot[irf]]['inverse']['slope'][pm.displayed_data].reshape(1,-1)*np.array([0]+list(averaged_ceiling_vals)).reshape(-1,1)),
                                  'square': rf_corr[dtype][dtype][rfs_to_plot[irf]]['square']['intercept'][pm.displayed_data].reshape(1,-1)+np.dot(poly_orders,rf_corr[dtype][dtype][rfs_to_plot[irf]]['square']['slope'][pm.displayed_data])}
                   for icell in range(pm.n_disp_cells):#unit_order[dtype][pm.n_disp_cells]:  
                       if icell%pm.ncells_per_fig==0:
                           figs[global_name[dtype]+'_'+rfs_to_plot[irf]+'_corrceiling_'+str(int(icell/pm.ncells_per_fig)+1)]=plt.figure()
                       plt.suptitle(dtype+'_'+rfs_to_plot[irf]+'_'+str(int(icell/pm.ncells_per_fig)+1))
                       #plt.subplot(2,1,1)
                       plt.subplot(pm.ncells_per_fig,1,icell%pm.ncells_per_fig+1)
                       plt.plot(averaged_ceiling_vals,averaged_corr_vals[:,unit_order[dtype][icell]],marker='o',color='k',linewidth=0.5)
                       leg_items=[]
                       for fit in fits:
                           leg_items.append(plt.plot([0]+list(averaged_ceiling_vals),fitted_curves[fit][:,unit_order[dtype][icell]])[0])
                       plt.ylabel('unit '+str(unit_order[dtype][icell])+'\ncorrelation')
                       if icell%pm.ncells_per_fig==pm.ncells_per_fig-1:
                           plt.xlabel('1/data_size')
                       if icell%pm.ncells_per_fig==0:   
                           plt.legend(leg_items,fits) 
#                       plt.subplot(2,1,2)
#                       plt.plot(averaged_ceiling_vals,1./averaged_corr_vals[:,unit_order[dtype][icell]],marker='o',color='k',linewidth=0.5)
#                       leg_items=[]
#                       for fit in fits:
#                           leg_items.append(plt.plot([0]+list(averaged_ceiling_vals),1./fitted_curves[fit][:,unit_order[dtype][icell]])[0])
#                       plt.legend(leg_items,fits) 
                           
                           
       if 'corr_ceiling_attempts' in pm.to_plot:
           
           print 'Plotting noise ceiling attempts'

           fits=['linear','inverse','square']
           ceiling_range=np.arange(1,5.2,0.2)
           multipliers=np.arange(6)+1
           assert np.all([np.all([np.round(val*mult,1) in pm.noise_ceiling_vals for val in ceiling_range]) for mult in multipliers])
           for dtype in data_types:  
               for irf in range(len(rfs_to_plot)):
                   intercepts={}
                   for fit in fits: 
                       intercepts[fit]=np.zeros([len(multipliers),n_cells[dtype]])
                       for imult in range(len(multipliers)):
                           intercepts[fit][imult,:],_=noise_ceiling_fit(np.round(ceiling_range*multipliers[imult],1),[rf_corr[dtype][dtype][rfs_to_plot[irf]][cv][pm.displayed_data] for cv in np.round(ceiling_range*multipliers[imult],1)],fit_type=fit,averaging_size=pm.noise_ceiling_grouping)
                       if fit=='inverse':
                            intercepts[fit]=1./intercepts[fit]             
                   for icell in range(pm.n_disp_cells):#unit_order[dtype][pm.n_disp_cells]:  
                       if icell%pm.ncells_per_fig==0:
                           figs[global_name[dtype]+'_'+rfs_to_plot[irf]+'_corrceilingattempts_'+str(int(icell/pm.ncells_per_fig)+1)]=plt.figure()
                       plt.suptitle(dtype+'_'+rfs_to_plot[irf]+'_'+str(int(icell/pm.ncells_per_fig)+1))
                       #plt.subplot(2,1,1)
                       plt.subplot(pm.ncells_per_fig,1,icell%pm.ncells_per_fig+1)
                       leg_items=[]
                       for ifit in range(len(fits)):
                           leg_items.append(plt.plot(multipliers,intercepts[fits[ifit]][:,unit_order[dtype][icell]],color=colors[ifit],marker='+')[0])
                       plt.axhline(rf_corr[dtype][dtype][rfs_to_plot[irf]][1][pm.displayed_data][unit_order[dtype][icell]],color='k',linestyle='--')   
                       plt.ylabel('unit '+str(unit_order[dtype][icell])+'\nextrapolated\ncorrelation')
                       if icell%pm.ncells_per_fig==pm.ncells_per_fig-1:
                           plt.xlabel('1/maximum_used_data_size')
                       if icell%pm.ncells_per_fig==0:   
                           plt.legend(leg_items,fits)                            



       if 'corr_ceiling_attempts_old' in pm.to_plot:
           
           print 'Plotting noise ceiling attempts'

           fits=['linear','inverse','square']
           n_attempts=20
           sizes=np.ceil(np.linspace(0,len(pm.noise_ceiling_vals),n_attempts+1)[1:]).astype(int)
           for dtype in data_types:  
               for irf in range(len(rfs_to_plot)):
                   intercepts={}
                   for fit in fits: 
                       intercepts[fit]=np.zeros([n_attempts,n_cells[dtype]])
                       for isize in range(n_attempts):
                           intercepts[fit][isize,:],_=noise_ceiling_fit(np.array(pm.noise_ceiling_vals)[-sizes[isize]:],[rf_corr[dtype][dtype][rfs_to_plot[irf]][cv][pm.displayed_data] for cv in np.array(pm.noise_ceiling_vals)[-sizes[isize]:]],fit_type=fit,averaging_size=pm.noise_ceiling_grouping)
                       if fit=='inverse':
                            intercepts[fit]=1./intercepts[fit]
                   last_ceilvals=[pm.noise_ceiling_vals[-val] for val in sizes]             
                   for icell in range(pm.n_disp_cells):#unit_order[dtype][pm.n_disp_cells]:  
                       if icell%pm.ncells_per_fig==0:
                           figs[global_name[dtype]+'_'+rfs_to_plot[irf]+'_corrceilingattempts_'+str(int(icell/pm.ncells_per_fig)+1)]=plt.figure()
                       plt.suptitle(dtype+'_'+rfs_to_plot[irf]+'_'+str(int(icell/pm.ncells_per_fig)+1))
                       #plt.subplot(2,1,1)
                       plt.subplot(pm.ncells_per_fig,1,icell%pm.ncells_per_fig+1)
                       leg_items=[]
                       for ifit in range(len(fits)):
                           leg_items.append(plt.plot(last_ceilvals,intercepts[fits[ifit]][:,unit_order[dtype][icell]],color=colors[ifit],marker='+')[0])
                       plt.axhline(rf_corr[dtype][dtype][rfs_to_plot[irf]][1][pm.displayed_data][unit_order[dtype][icell]],color='k',linestyle='--')   
                       plt.ylabel('unit '+str(unit_order[dtype][icell])+'\nextrapolated\ncorrelation')
                       if icell%pm.ncells_per_fig==pm.ncells_per_fig-1:
                           plt.xlabel('smallest 1/data_size')
                       if icell%pm.ncells_per_fig==0:   
                           plt.legend(leg_items,fits)      

                                       
       if 'corrbarplot_cells_bymodel' in pm.to_plot:
           print 'Plotting corr bar plot (cell by cell) by model'
           
           cv=pm.displayed_ceiling_val
           n_rfs=len(rfs_to_plot)
           x_positions=np.linspace(-0.4,0.4,n_rfs+1)
           x_width=x_positions[1]-x_positions[0]
           x_positions+=0.5*x_width
           for dtype in data_types:
               figs[global_name[dtype]+'_corrbarplotcells']=[plt.figure()]
               leg_items=[]
               for icell in range(len(cells_to_keep[dtype])):
                   plt.axvline(icell,alpha=0.5,color='k')
               if not(pm.normalize_by_corrmax) and len(theo_corr_max)>0:
                   plt.bar(np.arange(len(cells_to_keep[dtype])), theo_corr_max[dtype][cells_to_keep[dtype][unit_order[dtype]]], 0.8, edgecolor='k', color='white',linewidth=1.5)    
               for i_rf in range(n_rfs):
                   leg_items.append(plt.bar(np.arange(len(cells_to_keep[dtype]))+x_positions[i_rf], (rf_corr[dtype][dtype][rfs_to_plot[i_rf]][cv][pm.displayed_data]*1./corr_norm_fact[dtype])[unit_order[dtype]], x_width, color=rf_colors[rfs_to_plot[i_rf]],linewidth=0))#,edgecolor=colors[i_rf%len(colors)])
                   plt.bar(np.arange(len(cells_to_keep[dtype]))+x_positions[i_rf], (rf_corr[dtype][dtype][rfs_to_plot[i_rf]][cv]['train']*1./corr_norm_fact[dtype])[unit_order[dtype]], x_width, color=rf_colors[rfs_to_plot[i_rf]], alpha=0.2, linewidth=0)
               plt.axis([-1,len(cells_to_keep[dtype])]+list(plt.axis()[2:]))     
               plt.legend(leg_items,rfs_to_plot)    
               plt.gca().set_xticks(range(len(unit_order[dtype])))
               plt.gca().set_xticklabels(cell_labels[dtype][unit_order[dtype]])
               plt.suptitle(global_name[dtype]+'_corrbarplotcells')
               
               
       if 'corrbarplot_cells_bystim' in pm.to_plot:
           print 'Plotting corr bar plot (cell by cell) by stim'
           
           cv=pm.displayed_ceiling_val
           n_stims=len(data_types)
           x_positions=np.linspace(-0.4,0.4,n_stims+1)
           x_width=x_positions[1]-x_positions[0]
           x_positions+=0.5*x_width
           mixed_cellstokeep=cells_to_keep[data_types[0]]
           mixed_celllabels=cell_labels[data_types[0]]
           mixed_unitorder=unit_order[data_types[0]]
           for rf_type in rfs_to_plot:    
               figs[mixed_name+'_corrbarplotcells_'+rf_type]=[plt.figure()]
               leg_items=[]
               for icell in range(len(mixed_cellstokeep)):
                   plt.axvline(icell,alpha=0.5,color='k')
               for i_stim in range(n_stims):                   
                   if not(pm.normalize_by_corrmax) and (len(theo_corr_max)>0):
                       plt.bar(np.arange(len(mixed_cellstokeep))+x_positions[i_stim], theo_corr_max[data_types[i_stim]][mixed_cellstokeep[mixed_unitorder]], x_width, edgecolor=stim_colors[data_types[i_stim]], color='white',linewidth=1.5)
                   leg_items.append(plt.bar(np.arange(len(mixed_cellstokeep))+x_positions[i_stim], (rf_corr[data_types[i_stim]][data_types[i_stim]][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[data_types[i_stim]])[mixed_unitorder], x_width, color=stim_colors[data_types[i_stim]],linewidth=0))#,edgecolor=colors[i_rf%len(colors)])
                   plt.bar(np.arange(len(mixed_cellstokeep))+x_positions[i_stim], (rf_corr[data_types[i_stim]][data_types[i_stim]][rf_type][cv]['train']*1./corr_norm_fact[dtype])[mixed_unitorder], x_width, color=stim_colors[data_types[i_stim]], alpha=0.2, linewidth=0)
               plt.axis([-1,len(mixed_cellstokeep)]+list(plt.axis()[2:]))     
               plt.legend(leg_items,data_types)    
               plt.gca().set_xticks(range(len(mixed_unitorder)))
               plt.gca().set_xticklabels(mixed_celllabels[mixed_unitorder])
               plt.suptitle(mixed_name+'_corrbarplotcells_'+rf_type)        


       if 'corrlineplot_cells_bystim' in pm.to_plot:
           print 'Plotting corr line plot (cell by cell) by stim'
           
           cv=pm.displayed_ceiling_val
           n_stims=len(data_types)
           mixed_cellstokeep=cells_to_keep[data_types[0]]
           x_vals=np.array(pm.channel_depths)[mixed_cellstokeep] if len(pm.channel_depths)>0 else range(len(mixed_cellstokeep))
           mixed_celllabels=cell_labels[data_types[0]]
           mixed_unitorder=unit_order[data_types[0]]
           for rf_type in rfs_to_plot: 
               figs[mixed_name+'_corrlineplotcells_'+rf_type]=[plt.figure()]
               leg_items=[]
               for i_stim in range(n_stims):                   
                   if len(theo_corr_max)>0:
                       plt.plot(x_vals[mixed_unitorder], theo_corr_max[data_types[i_stim]][mixed_cellstokeep[mixed_unitorder]], color=stim_colors[data_types[i_stim]], linestyle='--', linewidth=1.5)
                   leg_items.append(plt.plot(x_vals[mixed_unitorder], (rf_corr[data_types[i_stim]][data_types[i_stim]][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[data_types[i_stim]])[mixed_unitorder], color=stim_colors[data_types[i_stim]], linewidth=1.5)[0])#,edgecolor=colors[i_rf%len(colors)])
               plt.legend(leg_items,data_types)    
               plt.suptitle(mixed_name+'_corrlineplotcells_'+rf_type)        


           
       if 'corrbarplot_means' in pm.to_plot:
           print 'Plotting corr bar plot (means)'
           
           cv=pm.displayed_ceiling_val 
           fig_title=mixed_name+'_corrbarplotmeans'
           if pm.single_datapoints:
               single_color='gray'
           else:
               single_color=None
           if not(pm.normalize_by_corrmax) and (len(theo_corr_max)>0) and (pm.displayed_data=='val'):
               ref_corr=np.array([np.nanmean(theo_corr_max[dtype][cells_to_keep[dtype]]) for dtype in data_types])
           else:
               ref_corr=None
           if pm.stim_x_rf:
               factor1,factor2=data_types,rfs_to_plot
               f2_colors=[rf_colors[rft] for rft in rfs_to_plot]
               corr_mat=np.array([[rf_corr[dtype][dtype][rft][cv][pm.displayed_data]*1./corr_norm_fact[dtype] for rft in rfs_to_plot] for dtype in data_types])
               ref_corr=[ref_corr,None]
           else: 
               factor1,factor2=rfs_to_plot,data_types
               f2_colors=[stim_colors[dtype] for dtype in data_types]
               corr_mat=np.array([[rf_corr[dtype][dtype][rft][pm.displayed_data]*1./corr_norm_fact[dtype] for dtype in data_types] for rft in rfs_to_plot])
               ref_corr=[None,ref_corr]
           if pm.factor2_display=='sep_fig':
               figs[fig_title]=[barplot(corr_mat[:,i2],reference_values=ref_corr[:1],labels=[factor1,None,'Performance (correlation) averaged over units'],single_vals_color=single_color,title=fig_title+'_'+factor2[i2]) for i2 in range(len(factor2))]
           elif pm.factor2_display=='same_fig':
               figs[fig_title]=[barplot(corr_mat,reference_values=ref_corr,labels=[factor1,factor2,'Performance (correlation) averaged over units'],factor2_colors=f2_colors,single_vals_color=single_color,title=fig_title)]
           elif pm.factor2_display=='mean':
               figs[fig_title]=[barplot(np.nanmean(corr_mat,axis=1),reference_values=ref_corr[:1],labels=[factor1,None,'Performance (correlation) averaged over units'],single_vals_color=single_color,title=fig_title)]
           elif pm.factor2_display=='max':
               figs[fig_title]=[barplot(np.nanmean(corr_mat,axis=1),reference_values=ref_corr[:1],labels=[factor1,None,'Performance (correlation) averaged over units'],single_vals_color=single_color,title=fig_title)]
 
    
       if 'corrlineplot_means' in pm.to_plot:
           print 'Plotting corr line plot (means)'

           cv=pm.displayed_ceiling_val             
           fig_title=mixed_name+'_corrlineplotmeans'
           if pm.single_datapoints:
               single_alpha=0.5
           else:
               single_color=None
           if pm.stim_x_rf:
               factor1,factor2=data_types,rfs_to_plot
               f2_colors=[rf_colors[rft] for rft in rfs_to_plot]
               corr_mat=np.array([[rf_corr[dtype][dtype][rft][cv][pm.displayed_data]*1./corr_norm_fact[dtype] for rft in rfs_to_plot] for dtype in data_types])
               ref_corr=[ref_corr,None]
           else: 
               factor1,factor2=rfs_to_plot,data_types
               f2_colors=[stim_colors[dtype] for dtype in data_types]
               corr_mat=np.array([[rf_corr[dtype][dtype][rft][pm.displayed_data]*1./corr_norm_fact[dtype] for dtype in data_types] for rft in rfs_to_plot])
               ref_corr=[None,ref_corr]
           if pm.factor2_display=='sep_fig':
               figs[fig_title]=[lineplot(corr_mat[:,i2],labels=[factor1,None,'Performance (correlation) averaged over units'],single_vals_alpha=single_alpha,title=fig_title+'_'+factor2[i2]) for i2 in range(len(factor2))]
           elif pm.factor2_display=='same_fig':
               figs[fig_title]=[lineplot(corr_mat,labels=[factor1,factor2,'Performance (correlation) averaged over units'],factor2_colors=f2_colors,single_vals_alpha=single_alpha,title=fig_title)]
           elif pm.factor2_display=='mean':
               figs[fig_title]=[lineplot(np.nanmean(corr_mat,axis=1),labels=[factor1,None,'Performance (correlation) averaged over units'],single_vals_alpha=single_alpha,title=fig_title)]
           elif pm.factor2_display=='max':
               figs[fig_title]=[lineplot(np.nanmean(corr_mat,axis=1),labels=[factor1,None,'Performance (correlation) averaged over units'],single_vals_alpha=single_alpha,title=fig_title)]
      
    
          
       if 'corr_typeVStype' in pm.to_plot:
           
           cv=pm.displayed_ceiling_val 
           x_model=[name for name in ['HSM','volt1','STALR','volt2diag','volt2','volt2sig'] if name in rfs_to_plot][0]
           for dtype in data_types:
               figs[global_name[dtype]+'_corrxy']=[plt.figure()]
               leg_items=[]
               count=0
               corr_min=min([min(rf_corr[dtype][dtype][rft][cv][pm.displayed_data]*1./corr_norm_fact[dtype]) for rft in rfs_to_plot])
               corr_max=max([max(rf_corr[dtype][dtype][rft][cv][pm.displayed_data]*1./corr_norm_fact[dtype]) for rft in rfs_to_plot])    
               plt.plot([corr_min-0.05,corr_max+0.05],[corr_min-0.05,corr_max+0.05],'k',linewidth=5)
               for rf_type in set(rfs_to_plot)-set([x_model]):
                   if pm.invert_corrplot:
                       li=plt.plot(rf_corr[dtype][dtype][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[dtype],rf_corr[dtype][dtype][x_model][cv][pm.displayed_data]*1./corr_norm_fact[dtype],color=rf_colors[rf_type],marker='o',markersize=10,linestyle='')[0]
                   else:    
                       li=plt.plot(rf_corr[dtype][dtype][x_model][cv][pm.displayed_data]*1./corr_norm_fact[dtype],rf_corr[dtype][dtype][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[dtype],color=rf_colors[rf_type],marker='o',markersize=10,linestyle='')[0]
                   leg_items.append(li)
                   count+=1
               plt.legend(leg_items,list(set(rfs_to_plot)-set([x_model])),loc='upper left').get_frame().set_linewidth(3)
               if pm.invert_corrplot:
                   plt.xlabel("Other models' performance (correlation)",fontsize=20)
                   plt.ylabel(x_model+' performance (correlation)',fontsize=20)  
               else:    
                   plt.xlabel(x_model+' performance (correlation)',fontsize=20)
                   plt.ylabel("Other models' performance (correlation)",fontsize=20)          
               plt.axis([corr_min-0.05,corr_max+0.05,corr_min-0.05,corr_max+0.05])
               plt.gca().set_aspect('equal')
               for side in plt.gca().spines.itervalues():
                   side.set_linewidth(3)
               plt.tick_params(labelsize=20)
               plt.suptitle(global_name[dtype]+'_corrxy')
               
               
       if 'corr_vs_maxcorr' in pm.to_plot:
           
           cv=pm.displayed_ceiling_val 
           assert np.all([len(theo_corr_max[dtype])>0 for dtype in data_types])
           for dtype in data_types:
               figs[global_name[dtype]+'_corrvsmax']=[plt.figure()]
               leg_items=[]
               count=0
               corr_min=np.nanmin([np.nanmin(rf_corr[dtype][dtype][rft][cv][pm.displayed_data]*1./corr_norm_fact[dtype]) for rft in rfs_to_plot]+list(theo_corr_max[dtype][cells_to_keep[dtype]]))
               corr_max=np.nanmax([np.nanmax(rf_corr[dtype][dtype][rft][cv][pm.displayed_data]*1./corr_norm_fact[dtype]) for rft in rfs_to_plot]+list(theo_corr_max[dtype][cells_to_keep[dtype]]))    
               plt.plot([corr_min-0.05,corr_max+0.05],[corr_min-0.05,corr_max+0.05],'k',linewidth=5)
               for rf_type in rfs_to_plot:
                   li=plt.plot(theo_corr_max[dtype][cells_to_keep[dtype]],rf_corr[dtype][dtype][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[dtype],color=rf_colors[rf_type],marker='o',markersize=10,linestyle='')[0]
                   leg_items.append(li)
                   count+=1
               plt.legend(leg_items,rfs_to_plot,loc='upper left').get_frame().set_linewidth(3)   
               plt.xlabel('Maximum possible correlation (given noise in PSTH)',fontsize=20)
               plt.ylabel('Model performance (correlation)',fontsize=20)          
               plt.axis([corr_min-0.05,corr_max+0.05,corr_min-0.05,corr_max+0.05])
               plt.gca().set_aspect('equal')
               for side in plt.gca().spines.itervalues():
                   side.set_linewidth(3)
               plt.tick_params(labelsize=20)
               plt.suptitle(global_name[dtype]+'_corrvsmax')  
               
               
       if 'corr_vs_maxcorr2' in pm.to_plot:
           
           cv=pm.displayed_ceiling_val 
           assert np.all([len(theo_corr_max[dtype])>0 for dtype in data_types])
           for rf_type in rfs_to_plot:
               figs[mixed_name+'_corrvsmax_'+rf_type]=[plt.figure()]
               leg_items=[]
               count=0
               corr_min=np.nanmin([np.nanmin(list(rf_corr[dtype][dtype][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[dtype])+list(theo_corr_max[dtype][cells_to_keep[dtype]])) for dtype in data_types])
               corr_max=np.nanmax([np.nanmax(list(rf_corr[dtype][dtype][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[dtype])+list(theo_corr_max[dtype][cells_to_keep[dtype]])) for dtype in data_types])    
               plt.plot([corr_min-0.05,corr_max+0.05],[corr_min-0.05,corr_max+0.05],'k',linewidth=5)
               for dtype in data_types:
                   li=plt.plot(theo_corr_max[dtype][cells_to_keep[dtype]],rf_corr[dtype][dtype][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[dtype],color=stim_colors[dtype],marker='o',markersize=10,linestyle='')[0]
                   leg_items.append(li)
                   count+=1
               plt.legend(leg_items,data_types,loc='upper left').get_frame().set_linewidth(3)   
               plt.xlabel('Maximum possible correlation (given noise in PSTH)',fontsize=20)
               plt.ylabel('Model performance (correlation)',fontsize=20)          
               plt.axis([corr_min-0.05,corr_max+0.05,corr_min-0.05,corr_max+0.05])
               plt.gca().set_aspect('equal')
               for side in plt.gca().spines.itervalues():
                   side.set_linewidth(3)
               plt.tick_params(labelsize=20)
               plt.suptitle(mixed_name+'_corrvsmax_'+rf_type)                  
               
               
       if 'cross_pred_clouds' in pm.to_plot:

           disp_cells=cells_to_keep[data_types[0]][unit_order[data_types[0]]][:pm.n_disp_cells]
           cv=pm.displayed_ceiling_val 
           corr_max=min(np.nanmax([np.nanmax((rf_corr[dtype1][dtype2][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[dtype2])[disp_cells]) for dtype1 in data_types for dtype2 in data_types for rf_type in rfs_to_plot]),1)
           plt.figure()
           count1=0
           for dtype1 in data_types:
               count2=0
               for dtype2 in data_types:
                   ax=plt.subplot(len(data_types),len(data_types),count1*len(data_types)+count2+1)
                   if dtype1!=dtype2:                       
                       curves=[]
                       countR=0
                       plt.plot([-1,1],[-1,1],'k',linewidth=3)
                       for rf_type in rfs_to_plot:
                           curves.append(plt.plot((rf_corr[dtype2][dtype2][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[dtype2])[disp_cells],(rf_corr[dtype1][dtype2][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[dtype2])[disp_cells],color=rf_colors[rf_type],marker='o',linestyle='',markersize=5)[0])
                           countR+=1
                       if (count2==0) | ((count1==0) & (count2==1)):    
                           plt.ylabel(dtype1,fontsize=10)
                       if (count1==(len(data_types)-1)) | ((count1==(len(data_types)-2)) & (count2==(len(data_types)-1))):    
                           plt.xlabel(dtype2,fontsize=10)
                       #plt.legend(curves,rfs_to_plot)
                       plt.axis([-0.1,corr_max+0.1,-0.1,corr_max+0.1])
                   else:
                       ax.set_facecolor((0.9,0.9,0.9))
                   ax.set_xticks([])
                   ax.set_yticks([])
                   plt.gca().set_aspect('equal')
                   count2+=1
               count1+=1  


       if 'cross_pred_hists' in pm.to_plot:
           
           disp_cells=cells_to_keep[data_types[0]][unit_order[data_types[0]]][:pm.n_disp_cells]
           cv=pm.displayed_ceiling_val 
           max_diff=np.nanmax([np.nanmax(np.abs((rf_corr[dtype1][dtype2][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[dtype2])[disp_cells]-(rf_corr[dtype2][dtype2][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[dtype2])[disp_cells])) for dtype1 in data_types for dtype2 in data_types for rf_type in rfs_to_plot])
           bins=np.arange(-max_diff,max_diff+0.025,0.025)
           for rf_type in rfs_to_plot:
               plt.figure()
               plt.suptitle(rf_type)
               count1=0
               for dtype1 in data_types:
                   count2=0
                   for dtype2 in data_types:
                       ax=plt.subplot(len(data_types),len(data_types),count1*len(data_types)+count2+1)
                       if dtype1!=dtype2:  
                           values=(rf_corr[dtype1][dtype2][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[dtype2])[disp_cells]-(rf_corr[dtype2][dtype2][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[dtype2])[disp_cells]             
                           plt.hist(values[values<0],bins,color='b')
                           plt.hist(values[values>0],bins,color='r')
                           plt.hist(values[values==0],bins,color='k')
                           if (count2==0) | ((count1==0) & (count2==1)):    
                               plt.ylabel(dtype1,fontsize=10)
                           if (count1==(len(data_types)-1)) | ((count1==(len(data_types)-2)) & (count2==(len(data_types)-1))):    
                               plt.xlabel(dtype2,fontsize=10)
                       else:
                           ax.set_facecolor((0.9,0.9,0.9))
                       ax.set_xticks([])
                       ax.set_yticks([])
                       count2+=1
                   count1+=1
                   
                   
       if 'cross_pred_mat' in pm.to_plot:

           disp_cells=cells_to_keep[data_types[0]][unit_order[data_types[0]]][:pm.n_disp_cells]
           cv=pm.displayed_ceiling_val 
           for rf_type in rfs_to_plot:
               diff_mat=np.zeros([len(data_types),len(data_types)])
               prop_mat=np.zeros([len(data_types),len(data_types)])
               for i1 in range(len(data_types)):
                   count2=0
                   for i2 in range(len(data_types)):
                       #values=rf_corr[data_types[i1]][data_types[i2]][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[data_types[i2]]-rf_corr[data_types[i2]][data_types[i2]][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[data_types[i2]]
                       diff_vec=(rf_corr[data_types[i1]][data_types[i2]][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[data_types[i2]])[disp_cells]-(rf_corr[data_types[i2]][data_types[i2]][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[data_types[i2]])[disp_cells]               
                       diff_mat[i1,i2]=np.nanmedian(diff_vec)  
                       prop_mat[i1,i2]=np.nansum(diff_vec>0)*100./np.sum(~np.isnan(diff_vec))
               diff_max=np.nanmax(np.abs(diff_mat))
               plt.figure()
               plt.suptitle(rf_type)
               plt.imshow(-diff_mat,vmin=-diff_max,vmax=diff_max,cmap='RdBu')
               for (j,i),label in np.ndenumerate(np.round(diff_mat,2)):
                   if i!=j:
                       plt.gca().text(i,j,label,ha='center',va='top')
               for (j,i),label in np.ndenumerate(np.round(prop_mat).astype(int)):
                   if i!=j:
                       plt.gca().text(i,j,str(label)+'%',ha='center',va='bottom')    
               plt.gca().set_xticks(range(len(data_types)))
               plt.gca().set_xticklabels(data_types)
               plt.gca().set_yticks(range(len(data_types)))
               plt.gca().set_yticklabels(data_types)
                 
                


       if 'talebi_and_baker_2012' in pm.to_plot:
           
           cv=pm.displayed_ceiling_val 
           corr_max=np.nanmax([np.nanmax(rf_corr[dtype1][dtype2][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[dtype]) for dtype1 in data_types for dtype2 in data_types for rf_type in rfs_to_plot])
           plt.figure()
           count1=0
           for dtype1 in data_types:
               count2=0
               for dtype2 in data_types:
                   ax=plt.subplot(len(data_types),len(data_types),count1*len(data_types)+count2+1)
                   if dtype1!=dtype2:                       
                       curves=[]
                       countR=0
                       plt.plot([-1,1],[-1,1],'k',linewidth=3)
                       for rf_type in rfs_to_plot:
                           curves.append(plt.plot(rf_corr[dtype1][dtype1][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[dtype2],rf_corr[dtype1][dtype2][rf_type][cv][pm.displayed_data]*1./corr_norm_fact[dtype2],color=rf_colors[rf_type],marker='o',linestyle='',markersize=5)[0])
                           countR+=1
                       if (count2==0) | ((count1==0) & (count2==1)):    
                           plt.ylabel(dtype1,fontsize=10)
                       if (count1==(len(data_types)-1)) | ((count1==(len(data_types)-2)) & (count2==(len(data_types)-1))):    
                           plt.xlabel(dtype2,fontsize=10)
                       #plt.legend(curves,rfs_to_plot)
                       plt.axis([-0.1,corr_max+0.1,-0.1,corr_max+0.1])
                   else:
                       ax.set_facecolor((0.9,0.9,0.9))
                   ax.set_xticks([])
                   ax.set_yticks([])
                   plt.gca().set_aspect('equal')
                   count2+=1
               count1+=1     

               
       if 'pred_and_resps' in pm.to_plot:  
           print 'Plotting predictions over true response'
           
           cv=pm.displayed_ceiling_val 
           for rf_type in rfs_to_plot:
               for dtype2 in data_types:
                   count=1
                   for icell in range(min(len(cells_to_keep[dtype]),pm.n_disp_cells)):
                       if icell%pm.ncells_per_fig==0:
                           figs[global_name[dtype2]+'_'+rf_type+'_predandresp_'+str(count)]=[plt.figure()]
                           plt.suptitle(global_name[dtype2]+'_'+rf_type+'_predandresp'+str(count))
                           count+=1
                       plt.subplot(pm.ncells_per_fig,1,icell%pm.ncells_per_fig+1)
                       #plt.plot(gaussianLowpass(resps[dtype][pm.displayed_data][:,icell],3),'k')
                       plt.plot(resps[dtype2][pm.displayed_data][:,:,unit_order[dtype2][icell]].flatten(),'k',linewidth=2) 
                       leg_items=[]
                       for dtype1 in data_types:
                           leg_items.append(plt.plot(rf_preds[dtype1][dtype2][rf_type][cv][pm.displayed_data][:,unit_order[dtype2][icell]],color=stim_colors[dtype1])[0])
                       if icell%pm.ncells_per_fig==0:        
                           plt.legend(leg_items,data_types) 
                       if len(theo_corr_max[dtype2]>0):    
                           plt.ylabel(str(unit_order[dtype2][icell])+'\n'+str(np.round(theo_corr_max[dtype2][cells_to_keep[dtype2][unit_order[dtype2][icell]]],2)))
                       else:
                           plt.ylabel(unit_order[dtype2][icell])
                       plt.xticks([])
                       plt.yticks([])


       if 'pred_STA' in pm.to_plot:
           print 'Plotting prediction STAs'
           
           cv=pm.displayed_ceiling_val 
           for rf_type in rfs_to_plot:
               for dtype2 in data_types:
                   count=1
                   for icell in range(min(len(cells_to_keep[dtype2]),pm.n_disp_cells)):#range(len(cells_to_keep[dtype])):
                       if icell%pm.ncells_per_fig==0:
                           figs[global_name[dtype2]+'_'+rf_type+'_predSTA_'+str(count)]=[plt.figure()]
                           plt.suptitle(global_name[dtype2]+'_'+rf_type+'_predSTA'+str(count))
                           count+=1
                       plt.subplot(pm.ncells_per_fig,1,icell%pm.ncells_per_fig+1)
                       leg_items=[]
                       for dtype1 in data_types:
                           vals=rf_predSTA[dtype1][dtype2][rf_type][cv][pm.displayed_data][::-1,unit_order[dtype2][icell]]
                           leg_items.append(plt.plot(range(-predSTA_tau,predSTA_tau+1),vals*1./vals.sum(),color=stim_colors[dtype1])[0])
                       if icell%pm.ncells_per_fig==0:        
                           plt.legend(leg_items,data_types) 
                       if len(theo_corr_max[dtype2]>0):    
                           plt.ylabel(str(unit_order[dtype2][icell])+'\n'+str(np.round(theo_corr_max[dtype2][cells_to_keep[dtype2][unit_order[dtype2][icell]]],2)))
                       else:
                           plt.ylabel(unit_order[dtype2][icell])

#                   figs[global_name[dtype2]+'_'+rf_type+'_predSTA_mean']=[plt.figure()]
#                   plt.suptitle(global_name[dtype2]+'_'+rf_type+'_predSTA_mean')
#                   leg_items=[]
#                   for dtype1 in data_types:
#                       leg_items.append(plt.plot(range(-predSTA_tau,predSTA_tau+1),np.nanmean(rf_predSTA[dtype1][dtype2][rf_type][cv][pm.displayed_data][::-1,unit_order[dtype2][:pm.n_disp_cells]],axis=1),color=stim_colors[dtype1])[0])    
#                   plt.legend(leg_items,data_types) 
#                   plt.ylabel('mean over cells')        


#       if 'pred_and_resps' in pm.to_plot:  
#           print 'Plotting predictions over true response'
#           for dtype in data_types:
#               count=1
#               for icell in range(len(cells_to_keep[dtype])):
#                   if icell%pm.ncells_per_fig==0:
#                       figs[global_name[dtype]+'_predandresp_'+str(count)]=[plt.figure()]
#                       plt.suptitle(global_name[dtype]+'_predandresp'+str(count))
#                       count+=1
#                   plt.subplot(pm.ncells_per_fig,1,icell%pm.ncells_per_fig+1)
#                   #plt.plot(gaussianLowpass(resps[dtype][pm.displayed_data][:,icell],3),'k')
#                   plt.plot(resps[dtype][pm.displayed_data][:,icell],'k',linewidth=2) 
#                   leg_items=[]
#                   for rf_type in rfs_to_plot:
#                       leg_items.append(plt.plot(rf_preds[dtype][dtype][rf_type][pm.displayed_data][:,icell],color=rf_colors[rf_type])[0])
#                   if icell%pm.ncells_per_fig==0:        
#                       plt.legend(leg_items,rfs_to_plot) 
#                   if len(theo_corr_max[dtype]>0):    
#                       plt.ylabel(str(icell)+'\ncmax='+str(np.round(theo_corr_max[dtype][cells_to_keep[dtype][icell]],2)))
#                   else:
#                       plt.ylabel(icell)
#                   plt.xticks([])
#                   plt.yticks([])                           
       
        
       if 'pred_vs_resps' in pm.to_plot:  
           print 'Plotting predictions vs true response'
           
           cv=pm.displayed_ceiling_val 
           n_digits=4
           
           for dtype in data_types: 
               figs[global_name[dtype]+'_predvsresp']=[]
               count=1
               for icell in range(len(cells_to_keep[dtype])):
                   quantified_resps=np.array(sorted(list(set(np.round(resps[dtype][pm.displayed_data][:,:,icell].flatten()*10**n_digits).astype(int)))))
                   if icell%pm.ncells_per_fig==0:
                       figs[global_name[dtype]+'_predvsresp'].append(plt.figure())
                       plt.suptitle(global_name[dtype]+'_predvsresp'+str(count))
                       count+=1
                   plt.subplot(1,pm.ncells_per_fig,icell%pm.ncells_per_fig+1)    
                   plot_min=min([resps[dtype][pm.displayed_data][:,:,icell].min()]+[rf_preds[dtype][dtype][rfs_to_plot[i_rf]][cv][pm.displayed_data][:,icell].min() for i_rf in range(n_rfs)])
                   plot_max=max([resps[dtype][pm.displayed_data][:,:,icell].max()]+[rf_preds[dtype][dtype][rfs_to_plot[i_rf]][cv][pm.displayed_data][:,icell].max() for i_rf in range(n_rfs)])
                   plt.plot([plot_min,plot_max],[plot_min,plot_max],'k',linewidth=2)
                   leg_items=[]
                   for rf_type in rfs_to_plot:
                       leg_items.append(plt.plot(resps[dtype][pm.displayed_data][:,:,icell].flatten(),rf_preds[dtype][dtype][rf_type][cv][pm.displayed_data][:,icell],'o',color=rf_colors[rf_type],alpha=0.1,mew=0)[0])                         
                       quantified_preds=[rf_preds[dtype][dtype][rf_type][cv][pm.displayed_data][np.round(resps[dtype][pm.displayed_data][:,:,icell].flatten()*10**n_digits).astype(int)==r,icell] for r in quantified_resps]
                       plt.errorbar(quantified_resps*10**-n_digits, [vals.mean() for vals in quantified_preds], yerr=[vals.std()*1./np.sqrt(len(vals)) for vals in quantified_preds],color=rf_colors[rf_type],linewidth=2)
                       pred_stds=[np.mean(np.round(resps[dtype][pm.displayed_data][:,:,icell].flatten()*10**n_digits).astype(int)==r) for r in quantified_resps]
                   plt.legend(leg_items,[rf_type+' (' +str(np.round(rf_corr[dtype][dtype][rf_type][cv][pm.displayed_data][icell],2))+')' for rf_type in rfs_to_plot]) 
                   if len(theo_corr_max[dtype]>0):    
                       plt.title(str(icell)+', cmax='+str(np.round(theo_corr_max[dtype][cells_to_keep[dtype][icell]],2)))
                   #plt.xlabel('Recorded response') 
                   #plt.ylabel('Predicted response')
                   
                   
       if 'corr_max' in pm.to_plot:
           if 'all' in data_types:
               x_stim='all'
           else:
               x_stim=data_types[0]
           y_stims=sorted(list(set(data_types)-set([x_stim])))
           figs[mixed_name+'_corrmax']=[plt.figure()]
           plt.plot([-0.05,1],[-0.05,1],'k',linewidth=3)
           leg_items=[]
           for dtype in y_stims:
               leg_items.append(plt.plot(theo_corr_max[x_stim],theo_corr_max[dtype],marker='o',linestyle='',color=stim_colors[dtype])[0])
           plt.legend(leg_items,y_stims)   
           plt.xlabel(x_stim+' stim max corr',fontsize=20)
           plt.ylabel('Other stims max corr',fontsize=20)
           plt.gca().set_aspect('equal')   
           for side in plt.gca().spines.itervalues():
                   side.set_linewidth(3)
           plt.tick_params(labelsize=20)
           plt.axis([-0.05,1,-0.05,1])
           plt.suptitle(global_name[dtype]+'_corrvsmax')  
        
        
       if 'plots' in pm.to_save:
           print 'Saving plots'
           for figname in figs.keys():              
               for ifig in range(len(figs[figname])):
                   fullname=os.path.join(pm.rf_path,figname)
                   if len(figs[figname])>1:
                       fullname+='_'+str(ifig+1)
                   figs[figname][ifig].savefig(fullname+pm.im_type)
            
            
      #'corr_barplot','corr_typeVStype','corr_sortedlines'  
        
