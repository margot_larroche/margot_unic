from scipy.io import loadmat
import numpy as np
from data_analysis.extracell_funcs import pythondata_to_rfdata
from miscellaneous.convertVideo import playMovieFromMat
import matplotlib.pyplot as plt

old_file='/media/margot/DATA/Margot/SYNCED_FILES/DATA/extracell_data/python_data/1718_CXRIGHT_TUN2_natmov.mat'
new_file='/media/margot/DATA/Margot/SYNCED_FILES/DATA/extracell_data/python_data/1718_CXRIGHT_TUN2_spksort180523.binary'
stim_list={'name':['natmov'],'fraction':[1],'selection_seed':[0],'stimuli':['natmov']}
dtype='float32'

old=loadmat(old_file)
new_stim,new_resps,_=pythondata_to_rfdata(new_file,ep_info_file=new_file.split('.')[0]+'_epinfo.txt',stim_categories=stim_list)
old_ROI=[1,13,0,12]
new_ROI=[0,12,0,12]
n_pix=old_ROI[1]-old_ROI[0]

n_bins=new_resps['natmov']['raw_train'].shape[1]
n_units=old['resps'].shape[1]
old_r=np.concatenate([old['resps'],old['reg_resps']],axis=0).reshape(-1,n_bins,n_units).astype(dtype)
new_r=np.concatenate([new_resps['natmov']['raw_train'],new_resps['natmov']['raw_reg']],axis=0)[:,:,:n_units].astype(dtype)
n_ep=new_r.shape[0]
old_s=np.concatenate([old['stim'],old['reg_stim']],axis=0)[:,old_ROI[0]:old_ROI[1],old_ROI[2]:old_ROI[3]].reshape(n_ep,n_bins,n_pix,n_pix).astype(dtype)
new_s=np.concatenate([new_stim['natmov']['raw_train'],new_stim['natmov']['raw_reg']],axis=0)[:,:,new_ROI[0]:new_ROI[1],new_ROI[2]:new_ROI[3]].astype(dtype)


positions=np.zeros(n_ep,dtype=int)
for iep in range(n_ep):
    positions[iep]=np.where(np.all(old_r.reshape(-1,n_bins*n_units)==new_r[iep].flatten(),axis=1))[0]
    
print 'Matching resps ?'
print np.all(old_r[positions].flatten()==new_r.flatten())
print 'Matching stims ?'
print np.all(old_s[positions].flatten()==new_s.flatten())   

#print np.where(~np.all(old_s[positions].reshape(n_ep,-1)==new_s.reshape(n_ep,-1),axis=1))

#playMovieFromMat(np.concatenate([old_s[positions],new_s],axis=3).reshape(-1,n_pix,n_pix*2))
#o=old_s[positions][0,100]
#n=new_s[0,100]
#playMovieFromMat((old_s[positions]-new_s).reshape(-1,n_pix,n_pix))

plt.hist((old_s[positions]-new_s).flatten())