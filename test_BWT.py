# Imports
import matplotlib.pyplot as plt
from data_analysis.extracell_funcs_NEW import loadTexStim
import glob
import numpy as np
from miscellaneous.convertVideo import playMovieFromMat
import matlab.engine
from utils.volterra_kernels import lsq_kernel, movie_to_BWT
from utils.handlebinaryfiles import saveMat


# In[3]:

# Parameters
stim_size=9
stim_dir='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/mixedstims_tex_20pix_std60_090218'
n_stim=100
power_law=1
stim_fracs={'train':0.7,'reg':0.1,'val':0.2}
colors='rgbycm'
line_width=2
numEpochs=5
epochSize=100



# In[ ]:

# Load stimulus
stim_list=sorted(glob.glob(stim_dir+'/*.tex'))[:n_stim]
stim=loadTexStim(stim_list,crop_range=np.array([[0,stim_size],[0,stim_size]]))
stim=np.concatenate(stim,axis=0)
stim=(stim-stim.mean())*1./stim.std()
print stim.shape
playMovieFromMat(stim)
stim=stim.reshape(stim.shape[0],-1)


# In[ ]:

# Compute stim BWT
if not(vars().has_key('matlab_engine')):
    matlab_engine=matlab.engine.start_matlab()
stim_BWT=movie_to_BWT(stim.reshape(-1,stim_size,stim_size), matlab_engine=matlab_engine)
playMovieFromMat(stim_BWT)
stim_BWT=stim_BWT.reshape(-1,stim_size**2)


transform=lsq_kernel(stim,stim_BWT,order0=False,order2=False)[0]
saveMat(transform,'BWT_filter_9pix')
reconstructed_BWT=np.dot(stim,transform)
c=np.corrcoef(reconstructed_BWT.flatten(),stim_BWT.flatten())[0,1]
print c
plt.figure();
plt.hist((reconstructed_BWT-stim_BWT).flatten())
#playMovieFromMat(np.concatenate([stim_BWT.reshape(-1,stim_size,stim_size),reconstructed_BWT.reshape(-1,stim_size,stim_size)],axis=2))

plt.figure();
for ifilter in range(stim_size**2):
    plot_max=np.abs(transform[:,ifilter]).max()
    plt.subplot(stim_size,stim_size,ifilter+1)
    plt.imshow(transform[:,ifilter].reshape(stim_size,stim_size), vmin=-plot_max, vmax=plot_max)