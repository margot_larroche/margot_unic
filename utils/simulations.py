import numpy as np
from numpy.random import rand, seed, normal
from miscellaneous.convertVideo import loadTexMovie, downsample
from utils.volterra_kernels import lsq_kernel, plot_kernel1
from lscsm.checkpointing import restoreLSCSM
from lscsm_adapt.loadandsave import loadParams
from lscsm.STA import STA_LR
import matplotlib.pyplot as plt

    
def generate_visualnoise(stimsize=10, nbins=1000, stimtype='Dnoise', seedval=0, freqs=[1./3,1./3]):
    seed(seedval)
    
    if stimtype=='Snoise':
        stim=np.zeros((nbins,stimsize))
        pixpos=np.floor(rand(nbins)*stimsize).astype(int)
        pixval=np.floor(rand(nbins)*2)
        pixval[pixval==0]=-1.
        for i in range(nbins): stim[i,pixpos[i]]=pixval[i]
            
#    elif stimtype=='Dnoise':
#        stim=rand(nbins,stimsize)*3
#        stim[stim<1]=-1
#        stim[(stim>=1) & (stim<2)]=0
#        stim[stim>=2]=1
        
    elif stimtype=='Dnoise':
        stim_map=rand(nbins,stimsize)
        stim=np.zeros(stim_map.shape)
        stim[stim_map<freqs[0]]=-1
        stim[stim_map>1-freqs[1]]=1

    elif stimtype=='Gnoise':
        stim=normal(0,1,(nbins,stimsize))
        
    else:
        raise Exception("Stim type "+stimtype+" unknown")
        
    return stim
    


def generate_grating(sidesize=10,period=10,ori=0,phase_ratio=0,bar_length=10,functype='sin'):
    # Ori in degrees
    # Note : stim std if sin with max 1 : 0.7071
    stim=np.zeros((sidesize,sidesize))
    rad_ori=ori*np.pi/180
    phase=phase_ratio*period
    
    for i in range(sidesize):
        for j in range(sidesize):
            rotated_i=np.cos(rad_ori)*(i-0.5*sidesize)+np.sin(rad_ori)*(j-0.5*sidesize)
            rotated_j=-np.sin(rad_ori)*(i-0.5*sidesize)+np.cos(rad_ori)*(j-0.5*sidesize)
            if functype=='sin':
                stim[i,j]=np.sin((rotated_j+phase)*(2*np.pi)/period)
            elif functype=='crenel':
                if (rotated_j+phase)%period<0.5*period:
                    stim[i,j]=1
                else:
                    stim[i,j]=-1
            elif functype== 'bar':
                if (rotated_j>=-0.25*period+phase) and (rotated_j<0.25*period+phase) and (np.abs(rotated_i)<0.5*bar_length):
                    stim[i,j]=1
                else:
                    stim[i,j]=0           
    return stim.reshape(-1)


#def generate_grating(sidesize=10,period=1,ori=0,phase_ratio=0,functype='sin'):
#    # Ori in degrees
#    stim=np.zeros((sidesize,sidesize))
#    rad_ori=ori*np.pi/180
#    phase=phase_ratio*period
#    for i in sidesize-1-np.arange(sidesize):
#        for j in range(sidesize):
#            rotated_j=-np.sin(rad_ori)*(i-0.5*sidesize)+np.cos(rad_ori)*j
#            if functype=='sin':
#                stim[int(i),j]=np.sin((rotated_j+phase)*(2*np.pi)/period)
#            elif functype=='crenel':
#                if (rotated_j+phase)%period<0.5*period:
#                    stim[int(i),j]=1
#                else:
#                    stim[int(i),j]=-1       
#    return stim.reshape(-1)/0.7071 # Normalize std to 1 (for functype='sin')

 
def circular_mask(stim_shape,inner_radius,outer_radius,center='middle'):
    assert len(stim_shape)==2
    assert outer_radius>=inner_radius
    if center=='middle':
        center=0.5*(np.array(stim_shape)-1)
    i_mat=np.arange(stim_shape[0]).reshape(-1,1).repeat(stim_shape[1],axis=1)
    j_mat=np.arange(stim_shape[1]).reshape(1,-1).repeat(stim_shape[0],axis=0)
    radius_mat=np.sqrt((i_mat-center[0])**2+(j_mat-center[1])**2)
    mask=1-np.maximum(np.minimum((radius_mat-inner_radius)*1./max(outer_radius-inner_radius,10**-12),np.ones(stim_shape)),np.zeros(stim_shape))
    return mask
    
    
    

def generate_gratingSeq(periods=[1],oris=[0],phase_ratios=[0],factors=[1],bar_length=10,sidesize=10,functype='sin'):
    #nstims=len(periods)*len(oris)*len(phases)
    #paramvals=np.zeros((nstims,3))
    #stim=np.zeros((nstims,sidesize**2))
    stim=np.zeros((len(periods),len(oris),len(phase_ratios),len(factors),sidesize**2))
    #i=0
    for iperiod in range(len(periods)):
        for iori in range(len(oris)):
            for iphase in range(len(phase_ratios)):
                for ifact in range(len(factors)):
                    #paramvals[i,:]=[periods[iperiod],oris[iori],phases[iphase]]
                    #stim[i,:]=generate_grating(sidesize=sidesize,period=periods[iperiod],ori=oris[iori],phase=phases[iphase])
                    stim[iperiod,iori,iphase,ifact,:]=generate_grating(sidesize=sidesize,period=periods[iperiod],ori=oris[iori],phase_ratio=phase_ratios[iphase],bar_length=bar_length,functype=functype)*factors[ifact]
                    #i+=1                
    #return stim, paramvals
    return stim            


def load_natimages_stim(frame_shape,equalize=False,stim_file='/DATA/Margot/tex_stimuli/van_hateren/vanhateren_imc500_ds2_tex50.tex'):
    frame_shape=np.array(frame_shape)    
    full_stim_mat=loadTexMovie(stim_file)
    full_shape=full_stim_mat.shape[1:]
    assert np.all(full_shape>=frame_shape)
    margins=np.array([np.floor((full_shape-frame_shape)*0.5),np.ceil((full_shape-frame_shape)*0.5)]).astype(int).T
    stim=full_stim_mat[:,margins[0,0]:full_shape[0]-margins[0,1],margins[1,0]:full_shape[1]-margins[1,1]].astype(float)
    stim=stim.reshape(stim.shape[0],-1)
    if equalize:
        stim=(stim-stim.mean(axis=1).reshape(-1,1))*1./stim.std(axis=1).reshape(-1,1)
    return stim


def onoffSubunit(npixside,updw_margin=0,side_margin=0,sep_width=0,phase=0,transpose=False):
    rect_width=np.int((npixside-2*side_margin-sep_width)/2)
    sep_width=npixside-2*side_margin-2*rect_width
    assert np.all(np.array([side_margin,sep_width,rect_width])>=0)
    h1=np.zeros((npixside,npixside))
    h1[updw_margin:npixside-updw_margin,side_margin:side_margin+rect_width]=1
    h1[updw_margin:npixside-updw_margin,side_margin+rect_width+sep_width:npixside-side_margin]=-1
    h1=np.concatenate((h1[:,phase:],h1[:,:phase]),axis=1)
    if transpose:
        h1=h1.T
    return h1.reshape(-1,1)


def powerlaw_rect(signal,exp):
    s=signal.copy()
    s[s<0]=0
    return s**exp
    

#def LNLmodel_resp(stim,lin_subunit,nl_subunits,pointNL_exp=1,weights=None,pwl2lin=False):
#    if weights is None:
#        weights=np.ones((nl_subunits.shape[1]+1,1))
#    nl_resp=powerlaw_rect(np.dot(stim,nl_subunits),pointNL_exp)
#    lin_resp=np.dot(stim,lin_subunit)
#    if pwl2lin:
#       lin_resp=powerlaw_rect(lin_resp,pointNL_exp)-powerlaw_rect(-lin_resp,pointNL_exp)
#    return np.dot(nl_resp,weights[1:]).reshape(-1,1)+weights[0]*lin_resp 


def HSM_resps(stim,model_name,n_cells,randomK_seed=None):
    mp,sp=loadParams(model_name+'_metaparams')
    lscsm,K,_=restoreLSCSM(model_name,np.zeros((1,sp['stimsize']**2)),np.zeros((1,n_cells))) 
    assert (np.sqrt(stim.shape[1])==sp['stimsize']) | ((np.sqrt(stim.shape[1])<sp['stimsize']) & (sp['stimsize']%np.sqrt(stim.shape[1])==0))
    if np.sqrt(stim.shape[1])<sp['stimsize']:
        n_rep=int(sp['stimsize']/np.sqrt(stim.shape[1]))
        stim=stim.reshape(-1,np.sqrt(stim.shape[1]),np.sqrt(stim.shape[1])).repeat(n_rep,axis=1).repeat(n_rep,axis=2)
        stim=stim.reshape(stim.shape[0],-1)
    if randomK_seed is not None:
        K = lscsm.create_random_parametrization(randomK_seed)
    return lscsm.response(stim,K)    
    
    
def LNLmodel_resp(stim,subunits,subunits_exp=1,output_exp=1,subunits_thresh=0,output_thresh=0,subunits_weights=1):
    assert np.array(stim).shape[1]==np.array(subunits).shape[0]
    subunits_resps=powerlaw_rect(np.dot(np.array(stim),np.array(subunits))-np.array([subunits_thresh]).reshape(1,-1),np.array([subunits_exp]).reshape(1,-1))
    output_resp=(subunits_resps*np.array([subunits_weights]).reshape(1,-1)).sum(axis=1).reshape(-1,1)
    return powerlaw_rect(output_resp-np.array([output_thresh]).reshape(1,-1), np.array([output_exp]).reshape(1,-1))    


#if __name__ == '__main__':
#    stim_size=16
#    n_bins=10000
#    seed_val=0
##    base_subunit1=onoffSubunit(stim_size,updw_margin=2,side_margin=2)
##    base_subunit2=onoffSubunit(stim_size,updw_margin=2,side_margin=2,phase=2)
##    subunits=np.array([base_subunit1.flatten(),base_subunit2.flatten()]).T
##    sub_exp=1
##    out_exp=1
##    sub_thresh=[-10,10]
##    out_thresh=0
#    equal_ampl=None
#    match_to='natural images 2'
#    HSM_name='/DATA/Margot/ownCloud/MargotUNIC/Data/LSCSM_data/FittingResults/2916_CXRIGHT_TUN15_croppedstim_standardparams'
#    datapath='/DATA/Margot/ownCloud/MargotUNIC/Data/Extracellular_elphy_data'
#    HSM_ncells=42
#        
#    from lscsm_adapt.loadandsave import restoreLSCSM_mgt
#    lscsm,K,suppl_params,training_inputs,training_set,validation_inputs,validation_set=restoreLSCSM_mgt(HSM_name,resetK=False,data_path=datapath)
#    
#    stims={'sparse noise': generate_visualnoise(stimsize=stim_size**2, nbins=n_bins, stimtype='Snoise', seedval=seed_val),
#           'dense noise': generate_visualnoise(stimsize=stim_size**2, nbins=n_bins, stimtype='Dnoise', seedval=seed_val),
#           'gaussian noise': generate_visualnoise(stimsize=stim_size**2, nbins=n_bins, stimtype='Gnoise', seedval=seed_val),
#           'natural images 1': load_natimages_stim([stim_size,stim_size])[:n_bins],
#           'natural images 2': training_inputs[np.any(training_inputs!=0,axis=1)][:n_bins] }
#
#    stim_names=stims.keys()        
#    if equal_ampl is not None:
#        stims={key: (stims[key]-stims[key].mean(axis=1).reshape(-1,1))*1./stims[key].std(axis=1).reshape(-1,1)*equal_ampl for key in stim_names}             
#    elif match_to is not None:
#        stims={key: (stims[key]-stims[key].mean(axis=1).reshape(-1,1))*stims[match_to].std(axis=1).reshape(-1,1)/stims[key].std(axis=1).reshape(-1,1)+stims[match_to].mean(axis=1).reshape(-1,1) for key in stim_names}
#
#    resps={}
#    STA={}
#    volt1={}   
#        
#           
#    for name in stim_names:
#        #resps[name]=LNLmodel_resp(stims[name],subunits,subunits_exp=1,output_exp=sub_exp,subunits_thresh=sub_thresh,output_thresh=out_thresh)
#        #redstim=stims[name].reshape(-1,stim_size,stim_size)[:,10:30,20:40].reshape(n_bins,-1)
#        #redstim=downsample(downsample(stims[name].reshape(-1,stim_size,stim_size)[:,0:32,15:47],2,2),1,2).reshape(n_bins,-1)
#        redstim=stims[name]-stims[name].mean(axis=0)
#        redstim=redstim*1./redstim.std()
#        #redstim=stims[name]
#        resps[name]=HSM_resps(stims[name],HSM_name,HSM_ncells,randomK_seed=None)
#        volt1[name]=lsq_kernel(redstim,resps[name],order0=True,order2=False)[1]
#        #RFs[name]=lsq_kernel(redstim,(resps[name]-resps[name].mean())*1./resps[name].std(),order0=False,order2=False)[0]
#        STA[name]=np.array(STA_LR(np.mat(redstim),np.mat((resps[name]-resps[name].mean())*1./resps[name].std()),1000))
#    for icell in range(STA.values()[0].shape[1]):    
#        plot_kernel1(np.array([volt1[key][:,icell]-volt1[key][:,icell].mean() for key in stim_names]).squeeze().T,celllabels=stim_names,title='Volt1 - cell n'+str(icell+1),scalemax='bycell')    
#        plot_kernel1(np.array([STA[key][:,icell]-STA[key][:,icell].mean() for key in stim_names]).squeeze().T,celllabels=stim_names,title='STA - cell n'+str(icell+1),scalemax='bycell') 
#    
##    cellnum_toplot=4    
##    plt.figure()
##    for name in stim_names:
##        plt.plot((resps[name][:,cellnum_toplot]-resps[name][:,cellnum_toplot].mean())*1./resps[name][:,cellnum_toplot].std())
##    plt.legend(stim_names)           
