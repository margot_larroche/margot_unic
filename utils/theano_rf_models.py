import param
from theano import tensor as T
import theano
from lscsm.TheanoVisionModel import TheanoVisionModel
from lscsm.fitting import fitLSCSM
import numpy as np
import matplotlib.pyplot as plt
   
         
class volterra_rf(TheanoVisionModel):
      
    orders = param.ListSelector(default=['0','1'], objects=['0','1','1on','1off','2diag','2'], doc="""Orders of the Volterra decomposition to use""")
    output_func = param.ObjectSelector(default='Linear', objects=['Linear','Exp','Sigmoid','SoftSign','Square','ExpExp','ExpSquare','LogisticLoss','Rectification','SquaredRectif','Pow3Rectif'],doc="""Output non-linearity""")
    time_separable = param.Boolean(default=False, doc="""If True, separate temporal and spatial kernels will be computed""")
     
    def construct_free_params(self):
        
        self.kernel_size=self.n_tau*self.size**2 
        temp_bound=100
        if (len(set(['1','1on','1off','2diag']) & set(self.orders))==0) or (self.n_tau<=1):
            self.time_separable=False
        
        if '0' in self.orders :
            self.k0 = self.add_free_param("k0",(1,self.num_neurons),(-temp_bound,temp_bound)) 
        
        if self.time_separable:
            self.k1_tau = self.add_free_param("k1_tau",(self.n_tau,self.num_neurons),(-temp_bound,temp_bound))
        
        if '1' in self.orders :    
            if self.time_separable:
                self.k1_x = self.add_free_param("k1_x",(self.size**2,self.num_neurons),(-temp_bound,temp_bound))
            else:
                self.k1 = self.add_free_param("k1",(self.kernel_size,self.num_neurons),(-temp_bound,temp_bound))
                
        if '1on' in self.orders :    
            if self.time_separable:
                self.k1on_x = self.add_free_param("k1on_x",(self.size**2,self.num_neurons),(-temp_bound,temp_bound))
            else:
                self.k1on = self.add_free_param("k1on",(self.kernel_size,self.num_neurons),(-temp_bound,temp_bound))
                
        if '1off' in self.orders :    
            if self.time_separable:
                self.k1off_x = self.add_free_param("k1off_x",(self.size**2,self.num_neurons),(-temp_bound,temp_bound))
            else:
                self.k1off = self.add_free_param("k1off",(self.kernel_size,self.num_neurons),(-temp_bound,temp_bound))        
                
        if '2diag' in self.orders :
            if self.time_separable:
                self.k2diag_x = self.add_free_param("k2diag",(self.size**2,self.num_neurons),(-temp_bound,temp_bound)) 
            else:    
                self.k2diag = self.add_free_param("k2diag",(self.kernel_size,self.num_neurons),(-temp_bound,temp_bound))  
            
        if '2' in self.orders :
            self.k2 = self.add_free_param("k2",(self.kernel_size*(self.kernel_size+1)/2,self.num_neurons),(-temp_bound,temp_bound))
        

    def construct_model(self):   
        
        self.updates=None        
        
        if '0' in self.orders :
            self.model_output=self.k0
        else:
            self.model_output=T.zeros(self.num_neurons)
            
        if '1' in self.orders :
            if self.time_separable:
                self.k1=T.reshape( T.repeat(T.reshape(self.k1_tau,[self.n_tau,1,self.num_neurons]),self.size**2,axis=1) * T.repeat(T.reshape(self.k1_x,[1,self.size**2,self.num_neurons]),self.n_tau,axis=0), [self.kernel_size, self.num_neurons])   
                #outer_product = lambda icell, k_time, k_space : T.flatten(T.outer(k_time[:,icell],k_space[:,icell]))
                #self.k1=theano.scan(outer_product , sequences= T.arange(self.num_neurons), non_sequences=[self.k1_tau,self.k1_x])[0].T
#                t_vals = theano.shared(np.repeat([np.arange(0,self.n_tau,dtype=int)],self.size**2,axis=0).T.flatten())
#                x_vals = theano.shared(np.repeat([np.arange(0,self.size**2,dtype=int)],self.n_tau,axis=0).flatten())
#                prod_func = lambda icell, k_t, k_x, tt, xx : k_t[tt,icell]*k_x[xx,icell]
#                self.k1 = theano.scan(prod_func , sequences= T.arange(self.num_neurons), non_sequences=[self.k1_tau,self.k1_x,t_vals,x_vals])[0].T
#                t_vals = theano.shared(np.repeat([np.arange(0,self.n_tau,dtype=int)],self.size**2,axis=0).T.flatten())
#                x_vals = theano.shared(np.repeat([np.arange(0,self.size**2,dtype=int)],self.n_tau,axis=0).flatten())
#                resp_func = lambda icell, k_t, k_x, tt, xx : T.dot(self.X,k_t[tt,icell]*k_x[xx,icell])
#                resp, _ = theano.scan(resp_func , sequences= T.arange(self.num_neurons), non_sequences=[self.k1_tau,self.k1_x,t_vals,x_vals])
#                self.model_output+=resp.T
#            else:  
#                self.model_output+=T.dot(self.X,self.k1)
            self.model_output+=T.dot(self.X,self.k1)

        if '1on' in self.orders :
            if self.time_separable:
                self.k1on=T.reshape( T.repeat(T.reshape(self.k1_tau,self.n_tau,1,self.num_neurons),self.size**2,axis=1) * T.repeat(T.reshape(self.k1on_x,1,self.size**2,self.num_neurons),self.n_tau,axis=0), self.kernel_size, self.num_neurons)   
            self.model_output+=T.dot(T.maximum(self.X,0),self.k1on) 
            
        if '1off' in self.orders :
            if self.time_separable:
                self.k1off=T.reshape( T.repeat(T.reshape(self.k1_tau,self.n_tau,1,self.num_neurons),self.size**2,axis=1) * T.repeat(T.reshape(self.k1off_x,1,self.size**2,self.num_neurons),self.n_tau,axis=0), self.kernel_size, self.num_neurons)   
            self.model_output+=T.dot(T.maximum(-self.X,0),self.k1off)    
            
        if '2diag' in self.orders :
            if self.time_separable:
                self.k2diag=T.reshape( T.repeat(T.reshape(self.k1_tau,self.n_tau,1,self.num_neurons),self.size**2,axis=1) * T.repeat(T.reshape(self.k2diag_x,1,self.size**2,self.num_neurons),self.n_tau,axis=0), self.kernel_size, self.num_neurons)
            self.model_output+=T.dot(self.X**2,self.k2diag)
            
        if '2' in self.orders :
            outer_product = lambda t, stim : T.outer(stim[t],stim[t])[np.triu_indices(self.kernel_size)]
            X_order2,_=theano.scan(outer_product , sequences= T.arange(self.X.shape[0]), non_sequences=[self.X])
            self.model_output+=T.dot(X_order2,self.k2)
            
        self.model_output=self.construct_of(self.model_output,self.output_func)
        
        return self.model_output
        


def fit_volterra_theano(train_stim,train_resps,reg_stim,reg_resps,**kwargs):  
    
    colors='rgbcmyk'
    linewidth=1
    params = {'n_tau': 1,
              'time_separable': False,
              'orders': ['0','1'],
              'output_func': 'Linear',
              'error_function': 'MSE', #'LogLikelyhood',
              'seed': 0,
              'numEpochs': 10,
              'epochSize': 100,
              'early_stop': True,
              'monitored': 'corr',
              'cell_by_cell': True,
              'plot_err': False,
              'retry_if_exc': 5}
    params.update(kwargs)  
    suppl_params={key: params.pop(key) for key in ['seed','numEpochs','epochSize','early_stop','monitored','plot_err','cell_by_cell','retry_if_exc']}            
     
    if suppl_params['cell_by_cell']:
        vrf = volterra_rf(train_stim,train_resps[:,:1],**params)
        cell_list=[[icell] for icell in range(train_resps.shape[1])]  
    else:      
        vrf = volterra_rf(train_stim,train_resps,**params)
        cell_list=[range(train_resps.shape[1])]
    init_rf_coeffs = vrf.create_random_parametrization(suppl_params['seed']) if (suppl_params['seed'] is not None) else np.zeros(vrf.free_param_count) 
    kernels_dict=[[]]*len(cell_list)
    terr=np.zeros([suppl_params['numEpochs']+1,len(cell_list)])
    verr=np.zeros([suppl_params['numEpochs']+1,len(cell_list)])
    tcorr=np.zeros([suppl_params['numEpochs']+1,len(cell_list)])
    vcorr=np.zeros([suppl_params['numEpochs']+1,len(cell_list)])
    
    for icell in range(len(cell_list)):
        exception='not_done_yet'
        count=0
        while (len(exception)>0) and count<=suppl_params['retry_if_exc']:
            print('\nCell '+str(icell)+', attempt '+str(count))
            rf_coeffs,terr[:,icell],verr[:,icell],tcorr[:,icell],vcorr[:,icell],exception=fitLSCSM(vrf,init_rf_coeffs,train_stim,train_resps[:,cell_list[icell]],reg_stim,reg_resps[:,cell_list[icell]],fit_params=suppl_params,compCorr=True,return_best=suppl_params['early_stop'],monitored=suppl_params['monitored'])
            count+=1
        kernels_dict[icell]={key: vrf.__dict__['k'+key].eval({vrf.K: rf_coeffs}) for key in params['orders']}
    kernels_dict={key: np.concatenate([kernels_dict[icell][key] for icell in range(len(cell_list))], axis=1) for key in params['orders']}    
    
    if suppl_params['plot_err']:
        time=np.arange(suppl_params['numEpochs']+1)*suppl_params['epochSize'] 
        plt.figure()
        for icell in range(len(cell_list)):                  
            plt.plot(time,terr[:,icell],colors[icell%len(colors)]+'--',linewidth=linewidth)
            plt.plot(time,verr[:,icell],colors[icell%len(colors)],linewidth=linewidth)
        plt.twinx()
        for icell in range(len(cell_list)):      
            plt.plot(time,tcorr[:,icell],colors[icell%len(colors)]+'--',linewidth=linewidth*2)
            plt.plot(time,vcorr[:,icell],colors[icell%len(colors)],linewidth=linewidth*2)
       
    return kernels_dict,terr,verr,tcorr,vcorr    





#def fit_volterra_theano(train_stim,train_resps,reg_stim,reg_resps,**kwargs):  
#    
#    params = {'n_tau': 1,
#              'time_separable': False,
#              'orders': ['0','1'],
#              'output_func': 'Linear',
#              'error_function': 'MSE',
#              'seed': 0,
#              'numEpochs': 10,
#              'epochSize': 100,
#              'early_stop': True}
#    params.update(kwargs)  
#    suppl_params={key: params.pop(key) for key in ['seed','numEpochs','epochSize','early_stop']}            
#                    
#    vrf = volterra_rf(train_stim,train_resps,**params)
#    init_rf_coeffs = vrf.create_random_parametrization(params['seed'])    
#    rf_coeffs,terr,verr,tcorr,vcorr=fitLSCSM(vrf,init_rf_coeffs,train_stim,train_resps,reg_stim,reg_resps,fit_params=suppl_params,compCorr=True,return_best=suppl_params['early_stop'])   
#    
#    to_return=[]
#    if '0' in params['orders']:
#        to_return.append(-vrf.getParam(rf_coeffs,'threshold'))
#    if '1' in params['orders']:
#        if params['time_separable'] and (params['n_tau']>1):
#            to_return.append(vrf.k1.eval({'K':rf_coeffs}))
#        else:    
#            to_return.append(vrf.getParam(rf_coeffs,'k1'))  
#    if 'onoff' in params['orders']:
#        if params['time_separable'] and (params['n_tau']>1):
#            to_return.append(vrf.k1on.eval({'K':rf_coeffs}))
#            to_return.append(vrf.k1off.eval({'K':rf_coeffs}))
#        else:    
#            to_return.append(vrf.getParam(rf_coeffs,'k1on'))
#            to_return.append(vrf.getParam(rf_coeffs,'k1off')) 
#    if '2diag' in params['orders']:
#        to_return.append(vrf.getParam(rf_coeffs,'k2diag'))    
#    if '2' in params['orders']:
#        to_return.append(vrf.getParam(rf_coeffs,'k2')) 
#    
#    return to_return+[terr,verr,tcorr,vcorr]    