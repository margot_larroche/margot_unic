import os
import glob
from utils.handlebinaryfiles import loadElphyBinary, saveMat

to_do=[]

data_files_path=''
trunc_files_path=''
klusta_dir_path=''
prm_template=''
prb_file=''
strong_thresh_vals=range(0.5,5.5,0.5)
weak_thresh_vals=range(2,6.25,0.25)
ext='.dat'
n_trunc_ep=5
n_elec=32
n_ep=942


if 'trunc_files' in to_do:
    data_files=sorted(glob.glob(os.path.join(data_files_path,'*'+ext)))
    
    for filename in data_files:
        data=loadElphyBinary(filename,ep_start=int(n_ep/2),ep_stop=int(n_ep/2)+n_trunc_ep,n_elec=n_elec,n_ep=n_ep)
        saveMat(data,os.path.join(trunc_files_path,os.path.basename(filename))+'_'+str(n_trunc_ep),fmt='h',endian='<')


if 'klusta_dir' in to_do:
    with open(prm_template,'r') as f:
        
    data_files=sorted(glob.glob(os.path.join(data_files_path,'*'+ext)))
    for filename in data_files:
        for wv in weak_thresh_vals:
            for sv un strong_thresh_vals:
                klusta_dir_name=os.path.join(klusta_dir_path,os.path.basename(filename)+'_s'+str(sv*100)+'_w'+str(wv*100))
                os.mkdir(klusta_dir_name)
                os.copy(filename,os.path.join(klusta_dir_name,os.path.basename(filename)))
                