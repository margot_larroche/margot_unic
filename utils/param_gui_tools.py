from warnings import warn
import param, os, sys
import numpy as np
try:
    import Tkinter as tk
    from tkFileDialog import askopenfilename, askdirectory
except:
    warn('Tkinter import failed: only non-GUI tools will be available')
try:
    from Pmw import Balloon, ScrolledFrame
except:
    Balloon='failed_import'
    ScrolledFrame='failed_import'
    warn('Pmw import failed: there will be neither tooltips nor scrollbars')
    
ScrolledFrame='failed_import'  # /!\ temporary because scrolledframe doesn't work for now, to be fixed !!!!!!!!

__TYPESDICT__={
param.Array: np.ndarray,
param.Boolean: bool,
param.Callable: 'function',
param.Dict: dict,
param.Filename: str,
param.Foldername: str,
param.HookList: list,
param.Infinity: float,
param.Integer: int,
param.List: list,
param.ListSelector: list,
param.Magnitude: float,
param.Number: float,
param.NumericTuple: tuple,
param.Path: str,
param.String: str,
param.Tuple: tuple}


def read_and_close_dialog(widget_dict,val_dict,dialog):
    for key in widget_dict.keys():
        if widget_dict[key].widgetName=='listbox':
            if widget_dict[key].config()['selectmode'][-1] in ['multiple','extended']:
                val_dict[key]=[widget_dict[key].get(index) for index in widget_dict[key].curselection()]
            else:    
                val_dict[key]=widget_dict[key].get(widget_dict[key].curselection())
        else:    
            val_dict[key]=widget_dict[key].var.get()  
    dialog.destroy()
    
    
def reset_dialog(widget_dict,newvals_dict):
    for key in newvals_dict.keys():
        pval=newvals_dict[key]
        if widget_dict[key].widgetName=='listbox':
            if type(pval)==str:
                pval=[pval]
            for oldsel in widget_dict[key].curselection():   
                widget_dict[key].select_clear(oldsel)    
            for sel in pval:
                widget_dict[key].select_set(widget_dict[key].get(0,'end').index(sel))    
        elif type(pval)==bool: 
            widget_dict[key].var.set(pval)
        else: 
            widget_dict[key].var.set(str(pval))  


def retrieve_params(pfile_name,raise_error=False):
    try:
        with open(pfile_name,'r') as f:
            p=eval('{'+f.read()+'}')
    except:
        t,v,tb=sys.exc_info()  
        print 'Invalid/missing parameter file : '+pfile_name
        p=None
        if raise_error:
            raise t,v,tb
        else:
            print tb
    return p
        
    
def reset_from_pfile(widget_dict,pfile_obj):
    filename=askopenfilename(initialdir=os.path.dirname(pfile_obj.fullpath))
    pdict=retrieve_params(filename)
    
    if pdict is not None:
        reset_dialog(widget_dict,pdict)
        pfile_obj.fullpath=filename
        pfile_obj.set(os.path.basename(filename))
    

def param_dialog(default_params,param_order=None,max_nrows=25,tooltip=True):     
    """
    Takes an instance of a param.Parameterized -derived  class as input.
    Displays a Tkinter GUI to modify its parameters.
    Returns the updated objet and a boolean that is True if user pressed OK, False if he aborted the process by closing the GUI
    The GUI's first widget allows to load parameter values previously stored in a text file (see param_list_string function for appropriate formatting).
    
    Optionals:
    - param_order: a dict with keys being parameter categories, and values being lists of parameter names to display under the category (respecting list order)
    - max_nrows: maximum number of parameters to display in the same column. When reached, a new column is created
    - tooltip: whether to a diplay tooltip ("infobulles") containing a parameter's documentation string (param_object.doc, defined by the user) when hovering the cursor over its widget
    """

    param_dict=default_params.params()
    param_names=param_dict.keys()
    if param_order is None:
        param_order={'PARAMETERS':param_names} 
    param_infos={}
    for key in param_names:
        param_infos[key]={'ptype':type(param_dict[key]),
                          'doc': param_dict[key].doc,
                          'val':eval('default_params.'+key),
                          'vtype':__TYPESDICT__[type(param_dict[key])] if type(param_dict[key]) in __TYPESDICT__.keys() else type(param_dict[key].default)}
    window=tk.Tk()
    if ScrolledFrame!='failed_import':
        sf=ScrolledFrame(window)
        sf.pack()
        container=sf.interior()
    else:
        container=window
    widgets={}
    inputs={}
    row=1
    col=0

    paramfile=tk.StringVar(value='')
    paramfile.fullpath=''
    tk.Label(container,text='Parameter file :').grid(row=0,column=0)
    tk.Label(container,textvariable=paramfile).grid(row=0,column=1)
    tk.Button(container,text='Browse',command=lambda:reset_from_pfile(widgets,paramfile)).grid(row=0,column=2)
    tooltip_widget=Balloon() if tooltip & (Balloon!='failed_import') else None
               
    for category in sorted(param_order.keys()):
        tk.Label(container,text='').grid(row=row,column=col+1)
        tk.Label(container,text='######  '+category+'  ######').grid(row=row+1,column=col+1)
        row+=2
        
        for pname in param_order[category]:
            if row>max_nrows:
                col+=3
                row=1
            ptype=param_infos[pname]['ptype']
            pdoc=param_infos[pname]['doc']
            pval=param_infos[pname]['val']
            if type(pval)==np.ndarray:    
                pval=pval.tolist()
            
            if ptype==param.Boolean:         
                boolvar=tk.BooleanVar(value=pval)
                widgets[pname]=tk.Checkbutton(container,text=pname,variable=boolvar) 
                widgets[pname].var=boolvar
            else:
                tk.Label(container,text=pname).grid(row=row,column=col) 
                if ptype in [param.ObjectSelector, param.ClassSelector, param.FileSelector]:   
                    choices=param_dict[pname].objects
                    widgets[pname]=tk.Listbox(container,selectmode='browse',exportselection=0,height=len(choices))
                    widgets[pname].insert('end',*choices)
                    widgets[pname].select_set(choices.index(pval))      
                elif ptype in [param.ListSelector, param.MultiFileSelector]:
                    choices=param_dict[pname].objects
                    widgets[pname]=tk.Listbox(container,selectmode='multiple',exportselection=0,height=len(choices))
                    widgets[pname].insert('end',*choices)
                    for i in range(len(pval)):
                        widgets[pname].select_set(choices.index(pval[i]))            
                else:
                    v=tk.StringVar()
                    widgets[pname]=tk.Entry(container,textvariable=v)
                    widgets[pname].var=v
                    if ptype in [param.Integer,param.Number]:
                        width=10
                    else:    
                        width=50
                        if ptype in [param.Path, param.Foldername, param.Filename]:
                            search_path=param_dict[pname].search_paths[0] if len(param_dict[pname].search_paths)>0 else ''
                        if ptype in [param.Path, param.Foldername]:
                            tk.Button(container,text='Browse',command=lambda p=pname:widgets[p].var.set(askdirectory(initialdir=search_path))).grid(row=row,column=2)
                        elif ptype==param.Filename:
                            tk.Button(container,text='Browse',command=lambda p=pname:widgets[p].var.set(askopenfilename(initialdir=search_path))).grid(row=row,column=2)
                    widgets[pname].configure(width=width)
                    widgets[pname].var.set(str(pval))
            
            if tooltip_widget:
                tooltip_widget.bind(widgets[pname],pdoc)
            widgets[pname].grid(row=row,column=col+1)
            row+=1  
    
    tk.Label(container,text='').grid(row=row,column=col+1)    
    tk.Button(window,text='OK',command=lambda:read_and_close_dialog(widgets,inputs,container)).grid(row=row+1,column=col+1)    
    window.mainloop()
    
    if len(inputs)>0:
        for key in inputs.keys():
            if bool(param_dict[key].allow_None) & (inputs[key] in ['None','none','']):
                inputs[key]=None
            elif type(inputs[key])!=param_infos[key]['vtype']:
                if param_infos[key]['vtype']==np.ndarray:
                    inputs[key]=np.array(eval(str(inputs[key])))
                else:
                    inputs[key]=param_infos[key]['vtype'](eval(str(inputs[key])))
        return default_params.__class__(**inputs), True
    else:
        return default_params, False
   

def correct_inputs(input_dict,param_obj):
    """
    To be applied to a param dictionnary "input_dict" that would be loaded directly from text file and used to configure a parameter object "param_obj" without transiting via the GUI.
    In this case, some input types must indeed be corrected.
    """
    corr_dict=input_dict.copy()
    param_dict=param_obj.params()
    for key in corr_dict.keys():
        vtype=__TYPESDICT__[type(param_dict[key])] if type(param_dict[key]) in __TYPESDICT__.keys() else type(eval('param_obj.'+key))
        #vtype=type(eval('param_obj.'+key))
        if bool(param_dict[key].allow_None) & (input_dict[key] in ['None','none','',None]):
            input_dict[key]=None
        elif type(corr_dict[key])!=vtype:
            if vtype==np.ndarray:
                corr_dict[key]=np.array(eval(str(corr_dict[key])))
            else:
                corr_dict[key]=vtype(eval(str(corr_dict[key])))
    return corr_dict            
                

 
def param_list_string(param_object,param_order=None,sep=',\n'):
    """
    Takes an instance of a param.Parameterized -derived  class as input.
    Returns a string in which its parameters have been pretty-printed (with categories and order optionally defined by param_order).
    The latter can be printed to a text file: this allows to reload the corresponding parameter set in the parameter GUI.
    """
    str_to_print='\n'
    if param_order is None:
        param_order={'PARAMETERS':param_object.params().keys()} 
        
    for category in sorted(param_order.keys()):
        str_to_print+='### '+category+' ###\n'
        for pname in param_order[category]:
            pval=eval('param_object.'+pname)
            if type(pval)==str:
                printed_p="'"+pval+"'"
            elif type(pval)==np.ndarray:
                printed_p=str(pval.tolist())    
            else:
                printed_p=str(pval)
            str_to_print+="'"+pname+"' : "+printed_p+sep
        str_to_print+='\n\n'
    return str_to_print[:-2-len(sep)]       