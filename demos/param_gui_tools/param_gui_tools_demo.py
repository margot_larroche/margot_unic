import param
from param_gui_tools import param_dialog, param_list_string

# Define a class derived from param.Parameterized: contains the list of parameters with their specifications,
# and will allow to create instances with specific values for these parameters, that can be used to provide a parametrization to a script
class example_parameterized_class(param.Parameterized):
    boolean                 = param.Boolean(default=True, doc="A simple boolean parameter")
    string                  = param.String(default="coucou",doc="A simple string parameter")
    integer_number          = param.Integer(default=50000,bounds=(-200,100000), doc="A bounded integer parameter")
    float_number            = param.Number(default=1.5, doc="An unbounded float parameter")  
    float_tuple             = param.NumericTuple(default=(1.0,2.0), doc="A tuple of floats")
    select_string           = param.ObjectSelector(default="option1", objects=["option1","option2","option3"], doc="You have to choose one option, the result will be a string")
    int_list                = param.ListSelector(default=[3,5], objects=[1,3,5,7,9], doc="You can choose as many options as you want, the result will be a list of integers")
    file_name               = param.Filename(default=None, search_paths=['dir1/dir1a','dir1/dir1b'], doc="A filename with its own path, or to be found in dir1/dir1a or dir1/dir1b (if not found, an error will be raised)")

# Define the order and categories in which paramters will be displayed in the GUI (optional) 
example_param_order={'Category name 1':['boolean', 'string', 'integer_number', 'float_number'],
                     'Category name 2':['float_tuple', 'select_string', 'int_list', 'file_name']}   

# Create an instance which will be initialized to default values (one can also provide alternative values as keyword arguments)
example_class_instance=example_parameterized_class()
# Print its parameter values
print(param_list_string(example_class_instance, example_param_order)) 

# Open the GUI with this instance: all parameter names are displayed with appropriate widgets for updating corresponding values.
# Parameter documentation strings are displayed in tooltips ("infobulles")
# Modify parameter values and then press OK.
# The function will return an instance with the new values, and a boolean (True if you pressed OK, False if you closed the window otherwise).
# The script is paused as long as the window remains open.
modified_class_instance, pressed_ok=param_dialog(example_class_instance, example_param_order)

# Check parameter values
print(param_list_string(modified_class_instance, example_param_order))

# Copy paste the printed output to a text file to save this parametrization for later.

# Suppose you rerun the script in a new kernel: you can reload the saved parametrization by loading your text file
modified_class_instance, pressed_ok=param_dialog(example_class_instance, example_param_order)


# Use the parametrized object to provide parameter values to your script
print modified_class_instance.int_list