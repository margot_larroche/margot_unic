import numpy as np

 
def reconstructStim(stim_mat,Vtags,bin_width,ep_dur,time_unit=0.001):
    nEp=len(Vtags)
    ep_nbins=int(np.round(ep_dur*1./bin_width))
    ep_bins=np.linspace(0,ep_dur,ep_nbins+1)
    corr_bin_width=ep_bins[1]
    new_mat=np.zeros((nEp,ep_nbins)+stim_mat.shape[2:])
    for iep in range(nEp):
        current_frame=0
        theo_dton=np.median(np.diff(Vtags[iep]))
        tags=np.array(list(time_unit*Vtags[iep])+[time_unit*Vtags[iep][-1]+theo_dton]) 
        for ibin in range(ep_nbins):
            frame_changes=tags[(tags>=ep_bins[ibin])&(tags<ep_bins[ibin+1])]
            proportions=np.diff(np.array([ep_bins[ibin]]+list(frame_changes)+[ep_bins[ibin+1]]))*1./corr_bin_width
            #if len(proportions)>2:
            print iep, ibin, len(proportions)
            new_mat[iep,ibin]=(stim_mat[iep,range(current_frame,current_frame+len(proportions))]*proportions.reshape((-1,)+(1,)*len(stim_mat.shape[2:]))).sum(axis=0)
            current_frame+=len(proportions)-1
    return new_mat            

ext_stim,ext_rates=add_temporal_dim(stim,rates,ntau)