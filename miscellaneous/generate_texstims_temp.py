from utils.simulations import generate_visualnoise
from miscellaneous.convertVideo import initializeTex, appendToBinary
import numpy as np
from warnings import warn

mov_nsquares=[20,20]
inset_pos=np.array([[9,11],[9,11]])
inset_nsquares=list(inset_pos[:,1]-inset_pos[:,0])
npix_per_square=6
mov_nbins=600
n_movs=300
noise_type='Dnoise'
freqs=[1./6, 1./6]
inset_freqs=[1./3,1./3]
mov_name='/media/margot/DATA/Margot/tex_stimuli/noise/dense_noise_fr120_sq6_std60_center033_surr017/densenoise_fr120_sq6_std60_center033_surr017'
n_zeros=4
frame_mean=127.5
frame_std=60.
ext='.tex'
starting_num=0

for imov in np.arange(n_movs)+starting_num:
    mov=generate_visualnoise(stimsize=np.prod(mov_nsquares), nbins=mov_nbins, stimtype=noise_type, seedval=imov, freqs=freqs).reshape([-1]+mov_nsquares)
    if np.all(inset_nsquares>0):
        inset=generate_visualnoise(stimsize=np.prod(inset_nsquares), nbins=mov_nbins, stimtype=noise_type, seedval=imov, freqs=inset_freqs).reshape([-1]+inset_nsquares)
        mov[:,inset_pos[0,0]:inset_pos[0,1],inset_pos[1,0]:inset_pos[1,1]]=inset    
    mov=mov.repeat(npix_per_square,axis=1).repeat(npix_per_square,axis=2).reshape([-1,np.prod(mov_nsquares)*npix_per_square**2])
    mov=(mov-mov.mean())*frame_std*1./mov.std()+frame_mean
    n_satur=np.sum(mov<0)+np.sum(mov>255)
    if n_satur>0:
        warn('Warning: '+str(n_satur)+' over '+str(np.prod(mov.shape))+' pixels saturate')
        mov[mov<0]=0
        mov[mov>255]=255
    with initializeTex(mov_name+'_'+str(imov+1).zfill(n_zeros)+ext,np.array(mov_nsquares)*npix_per_square,fmt='B',close=False) as f:
        for iframe in range(mov_nbins):
            appendToBinary(f,mov[iframe],fmt='B')