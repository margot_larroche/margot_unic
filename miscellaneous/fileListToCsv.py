import os
import time
import numpy as np
from sys import argv

def fileListToCsv(directory, savename='list_of_files.csv', sep=','):
    #os.chdir(directory)
    #files = filter(os.path.isfile, os.listdir(directory))
    if directory[-1]!='/':
        directory+='/'
    files = [fname for fname in os.listdir(directory) if os.path.isfile(directory+fname)]
    files.sort(key=lambda x: os.path.getctime(directory+x))
    ctimes= np.array([os.path.getctime(directory+fname) for fname in files]) 
    mtimes= np.array([os.path.getmtime(directory+fname) for fname in files])
    dates= [time.ctime(t)[:-8] for t in ctimes]
    _,_,_,cumtimes=ctime_to_HourMinSec(ctimes-ctimes[0])
    _,_,_,durations=ctime_to_HourMinSec(mtimes-ctimes)
    sizes= np.array([os.path.getsize(directory+fname) for fname in files]) *1./1000000
    debits= np.round(sizes*60./(mtimes-ctimes),2)
    sizes=np.round(sizes,2)
    with open(savename,'w') as f:
        f.write(sep.join(['File','Date','Cumulated time','Duration','Size (Mo)','Mo per min','Sampling rate','Eye','Stimulus','Stim parameters','Other params/info','Nb of stimuli','Nb of repetitions','Nb of episodes','Spike sorting'])+'\n')
        for ifile in range(len(files)):
            f.write(sep.join([files[ifile],dates[ifile],cumtimes[ifile],durations[ifile],str(sizes[ifile]),str(debits[ifile])])+'\n')
    return files    
    
    
def ctime_to_HourMinSec(ctimes):
    seconds=np.round(np.array(ctimes)).astype(int)
    hours=np.floor(seconds*1./3600).astype(int)
    seconds-=3600*hours
    minutes=np.floor(seconds*1./60).astype(int)
    seconds-=60*minutes
    times=[':'.join([str(hours[i]).zfill(2),str(minutes[i]).zfill(2),str(seconds[i]).zfill(2)]) for i in range(len(ctimes))]
    return hours, minutes, seconds, times
    
if __name__=='__main__':
    if (len(argv)==1) or (argv[1]=='help'):
	print 'Syntax : python fileListToCsv directory_name [output_name separator]'
    else:
        fileListToCsv(*argv[1:])
        if len(argv)>2:
	    print 'Created '+argv[2]
        else:
	    print 'Created list_of_files.csv'	
