import numpy as np
import glob
from utils.handlebinaryfiles import *
from miscellaneous.convertVideo import playMovieFromMat, compressHistTail, initializeTex, downsample, loadFrame, saveMatToTex, loadTexMovie, rescale_with_saturation
import matplotlib.pyplot as plt
from scipy.io import loadmat, savemat
import os
from utils.fourier_utils import randomize_phases
from shutil import copy


colors='rgbycmk'
to_do=['randomize_frames']
#to_do=['frame_infos','crop_frames','merge_to_texmovs']
im_dir='/DATA/Margot/tex_stimuli/van_hateren/vanhateren_imc_images'
framesinfo_file='/DATA/Margot/tex_stimuli/van_hateren/frames_infos_test0304.mat'
framesexamples_file='/DATA/Margot/tex_stimuli/van_hateren/frames_examples_test0304.mat'
newim_prefix='/DATA/Margot/tex_stimuli/van_hateren/vanhateren_imc120_images/vanhateren_imc120_'
#im_shape=np.array([1920,2560])
im_shape=np.array([1024,1536])
downsample_fact=1
righttail_pos=92.5
lefttail_pos=2
std_threshold=50
red_std_margin=30
redstd_threshold=50
normmin_threshold=-2.965
normmax_threshold=2.965
tail_max_percent=0.2
offset=0
#cuts=np.array([[0,300,0,300],[0,300,300,600],[0,300,600,900],[0,300,900,1200],[300,600,0,300],[300,600,300,600],[300,600,600,900],[300,600,900,1200],[600,900,0,300],[600,900,300,600],[600,900,600,900],[600,900,900,1200]])
cuts='most_possible'
frame_shape=np.array([120,120])
#cuts=np.array([[0,500,0,500],[0,500,500,1000],[0,500,1000,1500],[500,1000,0,500],[500,1000,500,1000],[500,1000,1000,1500]])
#ext='.imc'
ext='.imc'
im_fmt='H'
#im_fmt='image' #'tex_movie'
im_endian='>'    
im_margin=2
n_examples=50

if cuts=='most_possible':
    max_ncuts=np.prod((im_shape/(frame_shape*downsample_fact)).astype(int))
else:
    max_ncuts=len(cuts)    

# RAJOUTER DISTINCTION STD SPATIALE/TEMPORELLE
if 'frame_infos' in to_do:
        
    print 'Computing frames infos from '+im_dir    
    n_histbins=100
    hist_max=5  
    std_centile=2.5    
    images=sorted(glob.glob(os.path.join(im_dir,'*'+ext)))
    n_im=len(images)
    
    frame_examples=np.zeros([n_examples,np.prod(frame_shape)])
    example_inds=np.array([np.random.permutation(n_im)[:n_examples],np.random.permutation(max_ncuts)[:n_examples]]).T
    frame_info={}
    frame_info['frame_hists']=np.zeros([n_im,max_ncuts,n_histbins+2],dtype='uint32')
    frame_info['frame_stds']=np.zeros([n_im,max_ncuts])
    frame_info['frame_means']=np.zeros([n_im,max_ncuts])
    frame_info['frame_redstds']=np.zeros([n_im,max_ncuts])
    frame_info['frame_normmins']=np.zeros([n_im,max_ncuts])
    frame_info['frame_normmaxs']=np.zeros([n_im,max_ncuts])
    frame_info['almost_blank']=np.zeros([n_im,max_ncuts],dtype=bool)
    frame_info['hist_bins']=np.linspace(-hist_max,hist_max,n_histbins+1).flatten()
    frame_info['corrected_frame_hists']=np.zeros([n_im,max_ncuts,n_histbins+2],dtype='uint32')
    frame_info['corrected_frame_normmins']=np.zeros([n_im,max_ncuts])
    frame_info['corrected_frame_normmaxs']=np.zeros([n_im,max_ncuts])

    for i_im in range(n_im):
        print os.path.basename(images[i_im+offset])
        if im_fmt=='image':
            im=loadFrame(images[i_im+offset])
        elif im_fmt=='tex_movie':
            im=np.rollaxis(loadTexMovie(images[i_im+offset]),0,2)    
        else:    
            im=loadVec(images[i_im+offset],fmt=im_fmt,endian=im_endian).reshape(im_shape)
        im=im[im_margin:im.shape[0]-im_margin,im_margin:im.shape[1]-im_margin]
        if downsample_fact>1:
            im=downsample(downsample(im,1,downsample_fact),0,downsample_fact)
        if cuts=='most_possible':
            true_cuts=[[i*frame_shape[0],(i+1)*frame_shape[0],j*frame_shape[1],(j+1)*frame_shape[1]] for i in range(int(im.shape[0]/frame_shape[0])) for j in range(int(im.shape[1]/frame_shape[1]))]
        else:
            true_cuts=cuts
        frames=np.array([im[cut[0]:cut[1],cut[2]:cut[3]] for cut in true_cuts])
        frame_shape=np.array(frames.shape[1:3])
        frames=frames.reshape(len(true_cuts),-1)
        center=np.zeros(frame_shape,dtype=bool)
        center[red_std_margin:frame_shape[0]-red_std_margin,red_std_margin:frame_shape[1]-red_std_margin]=True
        inf_centiles=np.percentile(frames,std_centile,axis=1).reshape(-1,1)
        sup_centiles=np.percentile(frames,100-std_centile,axis=1).reshape(-1,1)
        frame_info['almost_blank'][i_im]=(sup_centiles<=inf_centiles).flatten()
        frame_info['frame_stds'][i_im]=[frames[ifr,(frames[ifr]>inf_centiles[ifr]) & (frames[ifr]<sup_centiles[ifr])].std() for ifr in range(frames.shape[0])]
        frame_info['frame_redstds'][i_im]=[frames[ifr,(frames[ifr]>inf_centiles[ifr]) & (frames[ifr]<sup_centiles[ifr]) & center.flatten()].std() for ifr in range(frames.shape[0])]
        frame_info['frame_means'][i_im]=[frames[ifr,(frames[ifr]>inf_centiles[ifr]) & (frames[ifr]<sup_centiles[ifr])].mean() for ifr in range(frames.shape[0])]
        frames=(frames-frame_info['frame_means'][i_im].reshape(-1,1))*1./frame_info['frame_stds'][i_im].reshape(-1,1)
        frame_info['frame_hists'][i_im]=np.array([[(fr<-hist_max).sum()]+list(np.histogram(fr,frame_info['hist_bins'])[0])+[(fr>hist_max).sum()] for fr in frames]).astype('uint32')        
        frame_info['frame_normmins'][i_im]=frames.min(axis=1)
        frame_info['frame_normmaxs'][i_im]=frames.max(axis=1)
        
        if i_im in example_inds[:,0]:
            ex_pos=np.where(example_inds[:,0]==i_im)[0]
            frame_examples[ex_pos]=frames[example_inds[ex_pos,1]]
            
#        frames=(frames-inf_centiles)*2./(sup_centiles-inf_centiles)-1 
#        frame_stds[i_im]=[frames[ifr,(frames[ifr]>inf_centiles[ifr]) & (frames[ifr]<sup_centiles[ifr])].std() for ifr in range(frames.shape[0])]
#        frames=(frames-frames.mean(axis=1).reshape(-1,1))*1./frames.std(axis=1).reshape(-1,1)
#        frame_hists[i_im]=np.array([[fr[fr<-hist_max].sum()]+list(np.histogram(fr,hist_bins)[0])+[fr[fr>hist_max].sum()] for fr in frames]).astype('uint32')
#        inf_centiles=np.percentile(frames,std_centile,axis=1).reshape(-1,1)
#        sup_centiles=np.percentile(frames,100-std_centile,axis=1).reshape(-1,1)         
#        frames=(frames-inf_centiles)*2./(sup_centiles-inf_centiles)-1 
#        frame_stds[i_im]=[frames[ifr,(frames[ifr]>inf_centiles[ifr]) & (frames[ifr]<sup_centiles[ifr])].std() for ifr in range(frames.shape[0])]

        right_tails=np.array([np.percentile(fr,righttail_pos) for fr in frames]) 
        left_tails=np.array([np.percentile(fr,lefttail_pos) for fr in frames])
        frames=np.array([compressHistTail(frames[ifr],right_tails[ifr]+tail_max_percent*(right_tails[ifr]-left_tails[ifr]),right_tails[ifr]) for ifr in range(len(left_tails))])   
        frames=-1*np.array([compressHistTail(-frames[ifr],-left_tails[ifr]+tail_max_percent*(right_tails[ifr]-left_tails[ifr]),-left_tails[ifr]) for ifr in range(len(left_tails))])
        frames=(frames-frames.mean(axis=1).reshape(-1,1))*1./frames.std(axis=1).reshape(-1,1)
        frame_info['corrected_frame_hists'][i_im]=np.array([[(fr<-hist_max).sum()]+list(np.histogram(fr,frame_info['hist_bins'])[0])+[(fr>hist_max).sum()] for fr in frames]).astype('uint32')        
        frame_info['corrected_frame_normmins'][i_im]=frames.min(axis=1)
        frame_info['corrected_frame_normmaxs'][i_im]=frames.max(axis=1)
        frame_info['corrected_parameters']={'righttail_pos':righttail_pos,'lefttail_pos':lefttail_pos, 'tail_max_percent':tail_max_percent}

    savemat(framesinfo_file,frame_info)
    savemat(framesexamples_file,{'frames':frame_examples, 'indices': example_inds})
   
   
if 'plot_infos' in to_do:
    plt.figure()
    plt.hist(frame_info['frame_stds'].flatten(),1000)
    plt.title('STD values')
    plt.figure()
    plt.hist(frame_info['frame_redstds'].flatten(),1000)
    plt.title('STD values on center area')
    
    extreme_hists=(frame_info['frame_hists'].reshape(n_im*max_ncuts,-1)[:,0]+frame_info['frame_hists'].reshape(n_im*max_ncuts,-1)[:,-1])>0 
    plt.figure()
    plt.plot([-hist_max]+list(0.5*(frame_info['hist_bins'][:-1]+frame_info['hist_bins'][1:]))+[hist_max],frame_info['frame_hists'].reshape(n_im*max_ncuts,-1)[extreme_hists][:30].T)
    plt.plot([-hist_max]+list(0.5*(frame_info['hist_bins'][:-1]+frame_info['hist_bins'][1:]))+[hist_max],frame_info['frame_hists'].reshape(n_im*max_ncuts,-1)[extreme_hists].mean(axis=0),'k',linewidth=3)
    plt.figure()
    plt.plot([-hist_max]+list(0.5*(frame_info['hist_bins'][:-1]+frame_info['hist_bins'][1:]))+[hist_max],frame_info['frame_hists'].reshape(n_im*max_ncuts,-1)[~extreme_hists][:30].T)
    plt.plot([-hist_max]+list(0.5*(frame_info['hist_bins'][:-1]+frame_info['hist_bins'][1:]))+[hist_max],frame_info['frame_hists'].reshape(n_im*max_ncuts,-1)[~extreme_hists].mean(axis=0),'k',linewidth=3)
        
    
if 'predicted_statistics' in to_do:  
    frame_info=loadmat(framesinfo_file)
    n_im,n_cuts=frame_info['frame_means'].shape
    new_mins=np.zeros([n_im,n_cuts])
    new_maxs=np.zeros([n_im,n_cuts])
    frame_info['hist_bins']=frame_info['hist_bins'].flatten()
    frame_hist_bins=np.array([np.nan]+list(frame_info['hist_bins'])+[np.nan])
    new_centers=np.zeros([n_im,n_cuts,len(frame_info['hist_bins'])+1])
    new_histbins=np.zeros([n_im,n_cuts,len(frame_info['hist_bins'])+2])
        
    for i_im in range(n_im):
        for i_frame in range(n_cuts):
            frame_hist_bins[0]=min(frame_info['frame_normmins'][i_im][i_frame],frame_hist_bins[1])
            frame_hist_bins[-1]=max(frame_info['frame_normmaxs'][i_im][i_frame],frame_hist_bins[-2])
            frame_bin_centers=0.5*(frame_hist_bins[:-1]+frame_hist_bins[1:])
            left_tail=frame_bin_centers[np.cumsum(frame_info['frame_hists'][i_im,i_frame])*100./frame_info['frame_hists'][i_im,i_frame].sum()>=lefttail_pos][0]
            right_tail=frame_bin_centers[np.cumsum(frame_info['frame_hists'][i_im,i_frame])*100./frame_info['frame_hists'][i_im,i_frame].sum()>=righttail_pos][0]
            left_thresh=left_tail-tail_max_percent*(right_tail-left_tail)
            right_thresh=right_tail+tail_max_percent*(right_tail-left_tail)
            new_centers[i_im,i_frame]=compressHistTail(-1*compressHistTail(-frame_bin_centers,-left_thresh,-left_tail),right_thresh,right_tail)
            new_histbins[i_im,i_frame]=compressHistTail(-1*compressHistTail(-frame_hist_bins,-left_thresh,-left_tail),right_thresh,right_tail)
            temp_mean=(new_centers[i_im,i_frame].reshape(1,-1)*frame_info['frame_hists'][i_im,i_frame]).sum()*1./frame_info['frame_hists'][i_im,i_frame].sum()
            temp_std=np.sqrt((((new_centers[i_im,i_frame]-temp_mean)**2)*frame_info['frame_hists'][i_im,i_frame]).sum()*1./(frame_info['frame_hists'][i_im,i_frame].sum()-1))
            new_centers[i_im,i_frame]=(new_centers[i_im,i_frame]-temp_mean)*1./temp_std
            new_mins[i_im,i_frame]=new_histbins[i_im,i_frame][:-1][frame_info['frame_hists'][i_im,i_frame]>0][0]
            new_maxs[i_im,i_frame]=new_histbins[i_im,i_frame][1:][frame_info['frame_hists'][i_im,i_frame]>0][-1]
   
    valid_frames=(frame_info['almost_blank']==False) & (frame_info['frame_stds']>=std_threshold) & (frame_info['frame_redstds']>=redstd_threshold) & (new_mins>=normmin_threshold) & (new_maxs<=normmax_threshold)      
    stim_extremums=[new_mins[valid_frames].min(),new_maxs[valid_frames].max()]
    expected_mean=stim_extremums[0]*255./(stim_extremums[0]-stim_extremums[1])
    expected_std=255./(stim_extremums[1]-stim_extremums[0])
    plt.figure()
    plt.hist(new_mins.flatten(),200)
    plt.title('Minimum values after normalization')
    plt.figure()
    plt.hist(new_maxs.flatten(),200)
    plt.title('Maximum values after normalization')
    #rand_order=np.random.permutation(n_im*n_cuts)
    examples=loadmat(framesexamples_file)
    ex_right_tails=np.array([np.percentile(fr,righttail_pos) for fr in examples['frames']]) 
    ex_left_tails=np.array([np.percentile(fr,lefttail_pos) for fr in examples['frames']])
    new_exframes=np.array([compressHistTail(examples['frames'][ifr],ex_right_tails[ifr]+tail_max_percent*(ex_right_tails[ifr]-ex_left_tails[ifr]),ex_right_tails[ifr]) for ifr in range(len(ex_left_tails))])   
    new_exframes=-1*np.array([compressHistTail(-new_exframes[ifr],-ex_left_tails[ifr]+tail_max_percent*(ex_right_tails[ifr]-ex_left_tails[ifr]),-ex_left_tails[ifr]) for ifr in range(len(ex_left_tails))])
    new_exframes=(new_exframes-new_exframes.mean(axis=1).reshape(-1,1))*1./new_exframes.std(axis=1).reshape(-1,1)  
    n_examples=examples['indices'].shape[0]
    n_plots=25
    n_figs=int(np.ceil(n_examples*1./n_plots))
    for ifig in range(n_figs):
        plt.figure()    
        for ifr in range(min(n_plots,n_examples-ifig*n_plots)):
            #frame_hist_bins[0]=min(frame_info['frame_normmins'].flatten()[rand_order[ifr+n_plots*ifig]],frame_hist_bins[1])
            #frame_hist_bins[-1]=max(frame_info['frame_normmaxs'].flatten()[rand_order[ifr+n_plots*ifig]],frame_hist_bins[-2])
            fr_inds=examples['indices'][ifr+n_plots*ifig]
            frame_hist_bins[0]=min(frame_info['frame_normmins'][fr_inds[0],fr_inds[1]],frame_hist_bins[1])
            frame_hist_bins[-1]=max(frame_info['frame_normmaxs'][fr_inds[0],fr_inds[1]],frame_hist_bins[-2])
            frame_bin_centers=0.5*(frame_hist_bins[:-1]+frame_hist_bins[1:])
            #frame_new_centers=new_centers.reshape(n_im*n_cuts,-1)[rand_order[ifr+n_plots*ifig]]
            #hist_vals=frame_info['frame_hists'].reshape(n_im*n_cuts,-1)[rand_order[ifr+n_plots*ifig]]
            frame_new_centers=new_centers[fr_inds[0],fr_inds[1]]
            hist_vals=frame_info['frame_hists'][fr_inds[0],fr_inds[1]]
            new_frame=[]
            for ibin in range(len(frame_new_centers)):
                new_frame+=[frame_new_centers[ibin]]*hist_vals[ibin]
            corr_hist,corr_bins=np.histogram(new_frame,frame_hist_bins)
            plt.subplot(5,5,ifr+1)
            plt.plot(0.5*(corr_bins[:-1]+corr_bins[1:]),corr_hist,colors[ifr%len(colors)],linewidth=2)
            plt.plot(frame_bin_centers,hist_vals,colors[ifr%len(colors)]+'--',linewidth=2) 
            plt.axis('off')
    #        plt.plot(new_centers.reshape(n_im*n_cuts,-1)[rand_order[ifr]],frame_info['frame_hists'].reshape(n_im*n_cuts,-1)[rand_order[ifr]],colors[ifr%len(colors)])#*1./np.diff(new_histbins.reshape(n_im*n_cuts,-1)[ifr]))
    #        plt.plot(frame_bin_centers,frame_info['frame_hists'].reshape(n_im*n_cuts,-1)[rand_order[ifr]],colors[ifr%len(colors)]+'--') 
        plt.figure()
        for ifr in range(min(n_plots,n_examples-ifig*n_plots)):  
            plt.subplot(5,5,ifr+1)
            plt.imshow(new_exframes[ifr+n_plots*ifig].reshape(frame_shape),vmin=stim_extremums[0],vmax=stim_extremums[1],interpolation='none',cmap='gray')
            plt.axis('off')
    
    print 'Nframes : '+str(valid_frames.sum())+'/'+str(valid_frames.size)
    print 'Expected mean : '+str(expected_mean)
    print 'Expected std : '+str(expected_std)       
    
    
if 'real_statistics' in to_do:   
    frame_info=loadmat(framesinfo_file)
    frame_hist_bins=np.array([np.nan]+list(frame_info['hist_bins'].flatten())+[np.nan])
    corr_hist_bins=frame_hist_bins.copy()
    valid_frames=(frame_info['almost_blank']==False) & (frame_info['frame_stds']>=std_threshold) & (frame_info['frame_redstds']>=redstd_threshold) & (frame_info['corrected_frame_normmins']>=normmin_threshold) & (frame_info['corrected_frame_normmaxs']<=normmax_threshold)      
    stim_extremums=[frame_info['corrected_frame_normmins'][valid_frames].min(),frame_info['corrected_frame_normmaxs'][valid_frames].max()]
    expected_mean=stim_extremums[0]*255./(stim_extremums[0]-stim_extremums[1])
    expected_std=255./(stim_extremums[1]-stim_extremums[0])
    plt.figure()
    plt.hist(frame_info['corrected_frame_normmins'].flatten(),200)
    plt.title('Minimum values after normalization')
    plt.figure()
    plt.hist(frame_info['corrected_frame_normmaxs'].flatten(),200)
    plt.title('Maximum values after normalization')
    #rand_order=np.random.permutation(n_im*n_cuts)
    examples=loadmat(framesexamples_file)
    ex_right_tails=np.array([np.percentile(fr,righttail_pos) for fr in examples['frames']]) 
    ex_left_tails=np.array([np.percentile(fr,lefttail_pos) for fr in examples['frames']])
    new_exframes=np.array([compressHistTail(examples['frames'][ifr],ex_right_tails[ifr]+tail_max_percent*(ex_right_tails[ifr]-ex_left_tails[ifr]),ex_right_tails[ifr]) for ifr in range(len(ex_left_tails))])   
    new_exframes=-1*np.array([compressHistTail(-new_exframes[ifr],-ex_left_tails[ifr]+tail_max_percent*(ex_right_tails[ifr]-ex_left_tails[ifr]),-ex_left_tails[ifr]) for ifr in range(len(ex_left_tails))])
    new_exframes=(new_exframes-new_exframes.mean(axis=1).reshape(-1,1))*1./new_exframes.std(axis=1).reshape(-1,1)  
    n_examples=examples['indices'].shape[0]
    n_plots=25
    n_figs=int(np.ceil(n_examples*1./n_plots))
    hist_labels=(frame_info['hist_bins'].flatten()[:-1]+frame_info['hist_bins'].flatten()[1:])*0.5
    for ifig in range(n_figs):
        plt.figure()    
        for ifr in range(min(n_plots,n_examples-ifig*n_plots)):
            fr_inds=examples['indices'][ifr+n_plots*ifig]
            frame_hist_bins[0]=min(frame_info['frame_normmins'][fr_inds[0],fr_inds[1]],frame_hist_bins[1])
            frame_hist_bins[-1]=max(frame_info['frame_normmaxs'][fr_inds[0],fr_inds[1]],frame_hist_bins[-2])
            frame_bin_centers=0.5*(frame_hist_bins[:-1]+frame_hist_bins[1:])
            corr_hist_bins[0]=min(frame_info['corrected_frame_normmins'][fr_inds[0],fr_inds[1]],frame_hist_bins[1])
            corr_hist_bins[-1]=max(frame_info['corrected_frame_normmaxs'][fr_inds[0],fr_inds[1]],frame_hist_bins[-2])
            corr_bin_centers=0.5*(corr_hist_bins[:-1]+corr_hist_bins[1:])
            plt.subplot(5,5,ifr+1)
            plt.plot(frame_bin_centers,frame_info['corrected_frame_hists'][fr_inds[0],fr_inds[1]],colors[ifr%len(colors)],linewidth=2)
            plt.plot(corr_bin_centers,frame_info['frame_hists'][fr_inds[0],fr_inds[1]],colors[ifr%len(colors)]+'--',linewidth=2) 
            plt.axis('off')
        plt.figure()
        for ifr in range(min(n_plots,n_examples-ifig*n_plots)):  
            plt.subplot(5,5,ifr+1)
            plt.imshow(new_exframes[ifr+n_plots*ifig].reshape(frame_shape),vmin=stim_extremums[0],vmax=stim_extremums[1],interpolation='none',cmap='gray')
            plt.axis('off')
    
    print 'Nframes : '+str(valid_frames.sum())+'/'+str(valid_frames.size)
    print 'Expected mean : '+str(expected_mean)
    print 'Expected std : '+str(expected_std)        


if 'crop_frames' in to_do:
    
    nzeros=6     
    frame_info=loadmat(framesinfo_file)
    images=sorted(glob.glob(os.path.join(im_dir,'*'+ext)))
    n_im=len(images)
    frames_info=loadmat(framesinfo_file)
    assert frames_info['frame_means'].shape[0]==n_im
    valid_frames=(frames_info['almost_blank']==False) & (frames_info['frame_stds']>=std_threshold) & (frames_info['frame_redstds']>=redstd_threshold) & (frame_info['corrected_frame_normmins']>=normmin_threshold) & (frame_info['corrected_frame_normmaxs']<=normmax_threshold)
    stim_extremums=[frame_info['corrected_frame_normmins'][valid_frames].min(),frame_info['corrected_frame_normmaxs'][valid_frames].max()]    
    frame_count=0
    
    for i_im in range(n_im):
        if im_fmt=='image':
            im=loadFrame(images[i_im+offset])
        elif im_fmt=='tex_movie':
            im=np.rollaxis(loadTexMovie(images[i_im+offset]),0,2)    
        else:    
            im=loadVec(images[i_im+offset],fmt=im_fmt,endian=im_endian).reshape(im_shape)
        im=im[im_margin:im.shape[0]-im_margin,im_margin:im.shape[1]-im_margin]
        
        if np.any(valid_frames[i_im]):
            if cuts=='most_possible':
                true_cuts=np.array([[i*frame_shape[0],(i+1)*frame_shape[0],j*frame_shape[1],(j+1)*frame_shape[1]] for i in range(int(im.shape[0]/frame_shape[0])) for j in range(int(im.shape[1]/frame_shape[1]))])
            else:
                true_cuts=cuts
            frames=np.array([im[cut[0]:cut[1],cut[2]:cut[3]] for cut in true_cuts[valid_frames[i_im]]])       
            frame_shape=np.array(frames.shape[1:])
            frames=frames.reshape(frames.shape[0],-1)
            right_tails=np.array([np.percentile(fr,righttail_pos) for fr in frames]) 
            left_tails=np.array([np.percentile(fr,lefttail_pos) for fr in frames])
            frames=np.array([compressHistTail(frames[ifr],right_tails[ifr]+tail_max_percent*(right_tails[ifr]-left_tails[ifr]),right_tails[ifr]) for ifr in range(len(left_tails))])   
            frames=-1*np.array([compressHistTail(-frames[ifr],-left_tails[ifr]+tail_max_percent*(right_tails[ifr]-left_tails[ifr]),-left_tails[ifr]) for ifr in range(len(left_tails))])
            frames=(frames-frames.mean(axis=1).reshape(-1,1))*1./frames.std(axis=1).reshape(-1,1)
            frames=(frames-stim_extremums[0])*255./(stim_extremums[1]-stim_extremums[0])
            frames=np.round(frames).astype(int)
            assert np.all((frames>=0) & (frames<=255))       
            for iframe in range(len(frames)):
                frame_count+=1
                saveMat(frames[iframe],newim_prefix+str(frame_count).zfill(nzeros),fmt='B')
                
                
if 'randomize_phases' in to_do:

    bin_im_dir='/media/margot/DATA/Margot/tex_stimuli/van_hateren/vanhateren_imc120_std60_images/'
    rand_im_dir='/media/margot/DATA/Margot/tex_stimuli/van_hateren/vanhateren_imc120_std60_randphase_images/'
    nzeros=6
    ext=''
    bin_im_shape=np.array([120,120])
    theo_mean=127.5
    theo_std=60
    
    images=sorted(glob.glob(os.path.join(bin_im_dir,'*'+ext)))
    n_im=len(images)
    saturated_pixels=np.zeros(n_im)
    true_mean=np.zeros(n_im)
    true_std=np.zeros(n_im)
    true_rand_mean=np.zeros(n_im)
    true_rand_std=np.zeros(n_im)
    for i_im in range(n_im):
        print images[i_im]
        im=loadVec(images[i_im],fmt='B').reshape(bin_im_shape)
        rand_im=randomize_phases(im)
        rand_im=(rand_im-rand_im.mean())*theo_std*1./rand_im.std()+theo_mean
        saturated_pixels[i_im]=((rand_im<0) | (rand_im>255)).sum()
        rand_im[rand_im<0]=0
        rand_im[rand_im>255]=255
        true_mean[i_im]=im.mean()
        true_std[i_im]=im.std()
        true_rand_mean[i_im]=rand_im.mean()
        true_rand_std[i_im]=rand_im.std()
        saveMat(rand_im,rand_im_dir+images[i_im][len(bin_im_dir):-nzeros-len(ext)]+'randphase_'+images[i_im][-nzeros-len(ext):],fmt='B')

#    movies=sorted(glob.glob(os.path.join(bin_im_dir,'*'+ext)))
#    n_movs=len(movies)
#    for i_mov in range(n_movs):
#        print movies[i_mov]
#        mov=loadTexMovie(movies[i_mov])
#        n_im=mov.shape[0]
#        saturated_pixels=np.zeros(n_im)
#        true_mean=np.zeros(n_im)
#        true_std=np.zeros(n_im)
#        true_rand_mean=np.zeros(n_im)
#        true_rand_std=np.zeros(n_im)
#        for i_im in range(n_im):
#            print i_im    
#            true_mean[i_im]=mov[i_im].mean()
#            true_std[i_im]=mov[i_im].std()
#            mov[i_im]=randomize_phases(mov[i_im])
#            mov[i_im]=(mov[i_im]-mov[i_im].mean())*theo_std*1./mov[i_im].std()+theo_mean
#            saturated_pixels[i_im]=((mov[i_im]<0) | (mov[i_im]>255)).sum()
#            mov[i_im][mov[i_im]<0]=0
#            mov[i_im][mov[i_im]>255]=255
#            true_rand_mean[i_im]=mov[i_im].mean()
#            true_rand_std[i_im]=mov[i_im].std()
#        saveMatToTex(mov,rand_im_dir+movies[i_mov][len(bin_im_dir):-nzeros-len(ext)]+'randphase_'+movies[i_mov][-nzeros-len(ext):])    


if 'randomize_pixels' in to_do:

    bin_im_dir='/DATA/Margot/tex_stimuli/van_hateren/vanhateren_imc120_images/'
    rand_im_dir='/DATA/Margot/tex_stimuli/van_hateren/vanhateren_imc120_randpix_images/'
    nzeros=6
    ext=''
    
    images=sorted(glob.glob(os.path.join(bin_im_dir,'*'+ext)))
    n_im=len(images)
    for i_im in range(n_im):
        print images[i_im]
        im=loadVec(images[i_im],fmt='B')
        saveMat(np.random.permutation(im),rand_im_dir+images[i_im][len(bin_im_dir):-nzeros-len(ext)]+'randpix'+images[i_im][-nzeros-len(ext):],fmt='B')              
    

if 'merge_to_texmovs' in to_do:

    bin_im_dir='/media/margot/DATA/Margot/tex_stimuli/van_hateren/vanhateren_imc120_std60_images'
    tex_prefix='/media/margot/DATA/Margot/tex_stimuli/van_hateren/vanhateren_imc120_std60_tex600/vanhateren_imc120_std60_tex600_'
    randomization_file='/DATA/Margot/tex_stimuli/van_hateren/vanhateren_imc120_tex600/randomization_order.txt'
    nzeros=3
    ext=''
    bin_im_shape=np.array([120,120])
    mov_nframes=600
    new_rand=False
    
    images=sorted(glob.glob(os.path.join(bin_im_dir,'*'+ext)))
    n_im=len(images)
    n_movs=int(n_im/mov_nframes)
    
    if new_rand:
        randomization=np.random.permutation(np.arange(n_movs*mov_nframes)).reshape(n_movs,-1)    
        with open(randomization_file,'w') as f:
            for line in range(n_movs):
                f.write(' '.join([str(num) for num in randomization[line]])+'\n')
                
    else:            
        randomization=np.zeros((n_movs,mov_nframes)).astype(int)
        with open(randomization_file,'r') as f:
            lines=f.read().split('\n')
        for i_line in range(n_movs):
            randomization[i_line]=np.array(lines[i_line].split(' ')).astype(int)
            
    initializeTex('temptex',bin_im_shape,fmt='B',close=True)
    for imov in range(n_movs):
        mov_name=tex_prefix+str(imov+1).zfill(nzeros)+'.tex'
        os.system('cat temptex '+' '.join([images[randomization[imov,i_im]] for i_im in range(mov_nframes)])+' >'+mov_name)  
        
        
if 'randomize_frames' in to_do:
    
    bin_mov_dir='/media/margot/DATA/Margot/tex_stimuli/natural_movies/tenseconds_tex_120_std60_subselected/'
    rand_mov_dir='/media/margot/DATA/Margot/tex_stimuli/natural_movies/temp_randframes/'
    randomization_file='/media/margot/DATA/Margot/tex_stimuli/natural_movies/temp_randframes/randomization_order.txt'
    new_rand=True
    nzeros=3
    ext='.tex'
 
    movies=sorted(glob.glob(os.path.join(bin_mov_dir,'*'+ext)))
    n_mov=len(movies)
    for i_mov in range(n_mov):
        print movies[i_mov]
        mov=loadTexMovie(movies[i_mov])
        
        if i_mov==0:
            mov_nframes=mov.shape[0]
            if new_rand:
                randomization=np.array([np.random.permutation(mov_nframes) for i in range(n_mov)]) 
                with open(randomization_file,'w') as f:
                    for line in range(n_mov):
                        f.write(' '.join([str(num) for num in randomization[line]])+'\n')                
            else:            
                randomization=np.zeros((n_movs,mov_nframes)).astype(int)
                with open(randomization_file,'r') as f:
                    lines=f.read().split('\n')
                for i_line in range(n_movs):
                    randomization[i_line]=np.array(lines[i_line].split(' ')).astype(int)
     
        saveMatToTex(mov[randomization[i_mov]],rand_mov_dir+movies[i_mov][len(bin_mov_dir):-nzeros-len(ext)]+'randframe_'+movies[i_mov][-nzeros-len(ext):])
        
  
if 'sort_by_pixvar' in to_do:
    
    bin_mov_dir='/media/margot/DATA/Margot/tex_stimuli/natural_movies/tenseconds_tex_120_std60_all'
    sorted_mov_dir='/media/margot/DATA/Margot/tex_stimuli/natural_movies/tenseconds_tex_120_std60_all_sortedbypixvar'
    #old_randomization_file='/DATA/Margot/tex_stimuli/natural_movies/tenseconds_tex_120/file_order.txt'
    new_randomization_file='/media/margot/DATA/Margot/tex_stimuli/natural_movies/tenseconds_tex_120_std60_all_sortedbypixvar/pixvar_sorting_order.txt'
    nzeros=4
    ext='.tex'
    final_nmov=100000
 
    movies=sorted(glob.glob(os.path.join(bin_mov_dir,'*'+ext)))
    n_mov=len(movies)
    mean_pix_std=np.zeros(n_mov)
    luminance_std=np.zeros(n_mov)
    for i_mov in range(n_mov):
        mov=loadTexMovie(movies[i_mov])
        mean_pix_std[i_mov]=np.nanmean(np.nanstd(mov,axis=0))
        luminance_std[i_mov]=np.nanstd(np.nanmean(mov.reshape(mov.shape[0],-1),axis=1))
     
    ratio=mean_pix_std*1./luminance_std
    try:
        plt.figure()
        plt.hist(ratio,100)
    except:
        pass
    new_order=np.argsort(-ratio)
    for i_mov in range(min(n_mov,final_nmov)):
        copy(movies[new_order[i_mov]],sorted_mov_dir+movies[new_order[i_mov]][len(bin_mov_dir):-nzeros-len(ext)]+str(i_mov+1).zfill(nzeros)+ext)
#    with open(old_randomization_file,'r') as f:
#        lines=f.read().split('\n')
    with open(new_randomization_file,'w') as f:  
#        f.write('\n'.join([lines[i] for i in new_order[:min(n_mov,final_nmov)]]))
        f.write('\n'.join([movies[new_order[i_mov]][len(bin_mov_dir)+1:] for i_mov in new_order[:min(n_mov,final_nmov)]]))
        
        
if 'downsample' in to_do:

    bin_im_dir='/media/margot/DATA/Margot/tex_stimuli/van_hateren/vanhateren_imc120_images/'
    down_im_dir='/media/margot/DATA/Margot/tex_stimuli/van_hateren/vanhateren_imc120_pix6/'
    nzeros=6
    down_pix_size=6
    ext=''
    
    assert np.all(frame_shape%down_pix_size==0)
    new_size=(frame_shape/down_pix_size).astype(int)
    images=sorted(glob.glob(os.path.join(bin_im_dir,'*'+ext)))
    n_im=len(images)
    for i_im in range(n_im):
        print images[i_im]
        im=loadVec(images[i_im],fmt='B')
        im_mean=im.reshape([new_size[0],down_pix_size,new_size[1],down_pix_size]).mean(axis=3).mean(axis=1)
        im_mean=np.round(im_mean).astype(int)
        saveMat(im_mean.repeat(down_pix_size,axis=0).repeat(down_pix_size,axis=1),down_im_dir+images[i_im][len(bin_im_dir):-nzeros-len(ext)]+'pix'+str(down_pix_size)+'_'+images[i_im][-nzeros-len(ext):],fmt='B')        
 


if 'change_std' in to_do:
#    bin_im_dir='/media/margot/DATA/Margot/tex_stimuli/van_hateren/vanhateren_imc120_images/'
#    new_im_dir='/media/margot/DATA/Margot/tex_stimuli/van_hateren/vanhateren_imc120_std60_images/'
    bin_im_dir='/media/margot/DATA/Margot/tex_stimuli/natural_movies/tenseconds_tex_120'
    new_im_dir='/media/margot/DATA/Margot/tex_stimuli/natural_movies/tenseconds_tex_120_std60_all'
    nzeros=4
    new_std=60
    new_mean=127.5
    #ext=''
    ext='.tex'
    
    images=sorted(glob.glob(os.path.join(bin_im_dir,'*'+ext)))
    n_im=len(images)
    for i_im in range(n_im):
        print images[i_im]
        #im=loadVec(images[i_im],fmt='B')
        #saveMat(rescale_with_saturation(im,[0,255],new_mean,new_std),new_im_dir+images[i_im][len(bin_im_dir):-nzeros-len(ext)]+'std'+str(new_std)+'_'+images[i_im][-nzeros-len(ext):],fmt='B')   
        mov=loadTexMovie(images[i_im])
        saveMatToTex(rescale_with_saturation(mov,[0,255],new_mean,new_std),new_im_dir+images[i_im][len(bin_im_dir):-nzeros-len(ext)]+'std'+str(new_std)+'_'+images[i_im][-nzeros-len(ext):],fmt='B')

#from miscellaneous.convertVideo import appendToBinary
#im_dir='/DATA/Margot/natural_movies/vanhateren_imc500_images/'
#mov_name='/DATA/Margot/natural_movies/vanhateren_imc500_ds2_tex50.tex'
#ext=''
#im_shape=np.array([500,500])
#crop_range=(np.array([[200,300],[200,300]]))
#downsample_fact=2
#final_shape=np.array([50,50])
#im_fmt='B'
#im_endian='<'
#
#images=sorted(glob.glob(im_dir+'*'+ext))
#n_im=len(images)
#mov_nframes=n_im
#n_movs=1
#randomization=np.random.permutation(np.arange(mov_nframes))
#        
#with initializeTex(mov_name,final_shape,fmt='B',close=False) as f:
#    for i_im in randomization:
#        im=loadVec(images[i_im],fmt=im_fmt,endian=im_endian).reshape(im_shape)[crop_range[0,0]:crop_range[0,1],crop_range[1,0]:crop_range[1,1]]
#        im=downsample(downsample(im,1,downsample_fact),0,downsample_fact)
#        appendToBinary(f,im,fmt=im_fmt)
        