import glob
from PIL import Image
from struct import pack, unpack
import numpy as np
from shutil import copyfile
import os
import matplotlib.pyplot as plt
from scipy.interpolate import interp2d
from utils.handlebinaryfiles import saveMat, loadVec
from utils.various_tools import downsample
import itertools
import matplotlib.animation as animation
from traceback import format_exc
from collections import OrderedDict


texheader_length=27 # Used by several functions


def loadFrame(filename):
    return np.asarray(Image.open(filename).convert('L'))
    
    
def loadTexFrame(fileobj,framenum,frameshape,fmt,close=True):
    framelen=np.prod(frameshape)
    readrange=np.array([0,framelen])+texheader_length+framelen*framenum
    return np.reshape(loadVec(fileobj, fmt=fmt, start=readrange[0], stop=readrange[1], close=close), frameshape)
        

def loadTexMovie(filename):
    texinfo=readTexHeader(filename)
    return loadVec(filename,fmt=texinfo['format'],start=texheader_length,size_unit='byte').reshape([texinfo['num_frames']]+texinfo['frame_shape'])     


def loadSubTexMovie(filename,frames_tokeep='all',crop_range=None):
    crop_range=np.array(crop_range)
    texinfo=readTexHeader(filename)
    if frames_tokeep=='all':
        frames_tokeep=range(texinfo['num_frames'])
    if np.all(crop_range==None):
        crop_range=np.array([[0,texinfo['frame_shape'][0]],[0,texinfo['frame_shape'][1]]])
    movmat=np.zeros([len(frames_tokeep)]+list(crop_range[:,1].T-crop_range[:,0].T))
    with open(filename,'rb') as f:
        for iframe in range(len(frames_tokeep)):
            movmat[iframe,:,:]=loadTexFrame(f,frames_tokeep[iframe],texinfo['frame_shape'],texinfo['format'],close=False)[crop_range[0,0]:crop_range[0,1],crop_range[1,0]:crop_range[1,1]]
    return movmat
           
        
def playMovieFromTex(filename,frame_dur=1./60,title=''):
    texinfo=readTexHeader(filename)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.title(title)
    im = ax.imshow(loadTexFrame(filename,0,texinfo['frame_shape'],texinfo['format']),cmap='gray',interpolation='none',vmin=0,vmax=255)

    def update_frame(iframe):
        frame=loadTexFrame(filename,iframe,texinfo['frame_shape'],texinfo['format']).astype(float)
        im.set_data(frame)
        return im

    ani = animation.FuncAnimation(fig,update_frame,texinfo['num_frames'],interval=frame_dur*1000)
    return ani    

      
def playMovieFromMat(mat,frame_dur=1./60,indicators=None,vmin=None,vmax=None):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    if vmin is None:
        vmin=0
    else:
        vmin=(vmin-mat.min())/(mat.max()-mat.min())
    if vmax is None:
        vmax=1
    else:
        vmax=(vmax-mat.min())/(mat.max()-mat.min())    
    mat=(mat[:]-mat.min())/(mat.max()-mat.min())


    if indicators is not None:
        margin=int(np.round(mat.shape[1]*0.2))
        indicators=(indicators[:]-indicators.min())/(indicators.max()-indicators.min())
        indicators=indicators.repeat(int(mat.shape[2]/indicators.shape[1])).reshape(indicators.shape[0],-1)
        frame=np.zeros((mat.shape[1]+margin,mat.shape[2]))+0.5
        frame[-margin:,:indicators.shape[1]]=indicators[0,:]
        frame[:-margin,:]=mat[0,:,:]
    else:
        frame=mat[0,:,:]
                
    im = ax.imshow(frame,cmap='gray',interpolation='none',vmin=vmin,vmax=vmax)

    def update_frame(iframe):
        if indicators is not None:
            frame[-margin:,:indicators.shape[1]]=indicators[iframe,:]
        frame[:mat.shape[1],:]=mat[iframe,:,:]
        im.set_data(frame)
        return im

    ani = animation.FuncAnimation(fig,update_frame,mat.shape[0],interval=frame_dur*1000,repeat_delay=2)
    return fig, ani        


def addRepeats(fileprefix,nrep,repeated_frames,blank_duration,meanval=128,imagetype='.jpg'):
    imagefiles=sorted(glob.glob(fileprefix+'*'+imagetype))
    im=loadFrame(imagefiles[0])
    blank=np.zeros(im.shape)+meanval
    lastframe=len(imagefiles)-1
    rep_duration=len(repeated_frames)
    for irep in range(nrep):
        framenum=lastframe+irep*(blank_duration+rep_duration)+1
        for iblank in range(blank_duration):
            #print 'blank:', iblank, 'im:', framenum+iblank
            saveToImfile(blank,fileprefix+str(framenum+iblank).zfill(5))
        for iframe in range(rep_duration):
            copyfile(imagefiles[repeated_frames[iframe]],fileprefix+str(framenum+blank_duration+iframe).zfill(5)+imagetype)
            #print 'frame:',repeated_frames[iframe], imagefiles[repeated_frames[iframe]], 'im:', framenum+blank_duration+iframe        


def normalizeFrame(framemat,moviemean,meanval=None,stdfactor=None,bounds=[0,255]):
    if meanval is None:
        meanval=moviemean
    normframe=framemat-moviemean
    if stdfactor is not None:
        normframe=normframe*stdfactor
    normframe=np.round(normframe+meanval)
    normframe[normframe<bounds[0]]=bounds[0]
    normframe[normframe>bounds[1]]=bounds[1]          
    return normframe.astype(int)
    
    
def resizeFrame(framemat,croprange=None,newresol=None):
    if croprange is not None:
        framemat=framemat[croprange[0,0]:croprange[0,1],croprange[1,0]:croprange[1,1]]
    if isinstance(newresol,int):
        framemat=downsample(downsample(framemat,1,newresol),0,newresol)
    elif newresol is not None:    
        framemat=np.round(interp2d(np.linspace(0,1,framemat.shape[1]),np.linspace(0,1,framemat.shape[0]),framemat).__call__(np.linspace(0,1,newresol[1]),np.linspace(0,1,newresol[0]))).astype(int)    
    return framemat


def saveToImfile(framemat,filename,imagetype='.jpg'):
    Image.fromarray(framemat.astype(float)).convert('RGB').save(filename+imagetype)   
    

def appendToBinary(filehandle,framemat,fmt='B'):
    filehandle.write(pack('<'+str(framemat.size)+fmt,*np.reshape(np.array(framemat),-1)))
        

def processMovie(fileprefix,savename,croprange=None,newresol=None,toignore=np.array([[0,0],[0,0]]),meanval=None,stdval=None,normMode='factor',framerange='all',inputtype='.jpg',outputtype='.jpg',histfile=None):
    imagefiles,fileinfo,framerange=listFrames(fileprefix,inputtype,framerange)

    if (meanval is not None) | (stdval is not None):    
        moviemean=computeMean(fileprefix,croprange=croprange,toignore=toignore,framerange=framerange,imagetype=imagetype)
        print 'moviemean:', moviemean
    if normMode=='std':
        moviestd=computeStd(moviemean,fileprefix,croprange=croprange,toignore=toignore,framerange=framerange)
        print 'moviestd:', moviestd
        stdfactor=stdval*1./moviestd
    elif normMode=='factor':
        stdfactor=stdval
    elif normMode is None:
        stdfactor=None
    if histfile is not None:
        offset,stretchfactor=histStretchParams(histfile)    
        
    for ifile in range(framerange[0],framerange[1]):
        if inputtype=='.tex':
            im=loadTexFrame(fileprefix+inputtype,ifile,fileinfo['frame_shape'],fileinfo['format'])
        else:    
            im=loadFrame(imagefiles[ifile])
        im=resizeFrame(im,croprange=croprange,newresol=newresol)
        if (ifile==0) & (outputtype=='.tex'):
             f=initializeTex(savename+'.tex',im.shape,close=False)  
        if meanval is not None:    
            im=normalizeFrame(im,moviemean,meanval=meanval,stdfactor=stdfactor)
        if histfile is not None:
            im=normalizeFrame(im,offset,meanval=0,stdfactor=stretchfactor)
        
        if outputtype=='.tex':
            appendToBinary(f,im)
        else:    
            saveToImfile(im,savename+str(ifile).zfill(5),imagetype=outputtype)      

    if outputtype=='.tex':
        f.close()         


def processMoviePiecewise(fileprefix,savename,listofCuts=[],croprange=None,toignore=np.array([[0,0],[0,0]]),meanval=128,stdval=None,normMode='factor',imagetype='.jpg'):
    imagefiles=sorted(glob.glob(fileprefix+'*'+imagetype))
    segments=[0]+list(listofCuts[:])+[len(imagefiles)]
    for iseg in range(len(segments)-1):
        print segments[iseg],'-',segments[iseg+1]-1
        processMovie(fileprefix,savename,croprange=croprange,toignore=toignore,meanval=meanval,stdval=stdval,normMode=normMode,framerange=segments[iseg:iseg+2],imagetype=imagetype)


        

def computeFrameParams(fileprefix,croprange=None,framerange='all',imagetype='.jpg'):
    imagefiles=sorted(glob.glob(fileprefix+'*'+imagetype))
    if framerange=='all':
        framerange=[0,len(imagefiles)]
    else:
        framerange=[max(framerange[0],0),min(framerange[1],len(imagefiles))]
        
    means=np.zeros(framerange[1]-framerange[0]) 
    stds=np.zeros(framerange[1]-framerange[0])
    count=0
    for ifile in range(framerange[0],framerange[1]):
        im=loadFrame(imagefiles[ifile])   
        means[count]=im.mean()
        stds[count]=im.std()
        count+=1        
    return means, stds    
    
 
def detectJumps(framemeans,framestds,meanthresh,stdthresh):
    meander=np.diff(framemeans)
    meander2=np.diff(meander)
    meanjumps=(np.abs(meander2[:-1])>=meanthresh) & (np.abs(meander2[1:])>=meanthresh) & ((meander2[:-1]*meander2[1:])<0) & (np.abs(meander[1:-1])>=meanthresh)
    stdder=np.diff(framestds)
    stdder2=np.diff(stdder)
    stdjumps=(np.abs(stdder2[:-1])>=stdthresh) & (np.abs(stdder2[1:])>=stdthresh) & ((stdder2[:-1]*stdder2[1:])<0) & (np.abs(stdder[1:-1])>=stdthresh)
    return np.where(meanjumps|stdjumps)[0]+2

               
def removeItems(vector,toremove):
    shortlist=list(vector[:])
    for item in toremove:
        while item in shortlist:
            shortlist.remove(item)
    shortlist.sort()
    return shortlist       
    

def renameFiles(fileprefix,nbzeros=5,imagetype='.jpg'):  
    imagefiles=sorted(glob.glob(fileprefix+'*'+imagetype))
    for ifile in range(len(imagefiles)):
        filenum=imagefiles[ifile][len(fileprefix):-len(imagetype)]
        os.rename(imagefiles[ifile],fileprefix+str(filenum).zfill(nbzeros)+imagetype)
 

def listFrames(fileprefix,imagetype,framerange):
    if imagetype=='.tex':
        fileinfo=readTexHeader(fileprefix+imagetype) 
        imagefiles=[]
    else:    
        imagefiles=sorted(glob.glob(fileprefix+'*'+imagetype))
        fileinfo={'num_frames': len(imagefiles)}
        
    if fileinfo['num_frames']==0:
        print 'Error:', fileprefix+'*'+imagetype,  'no such files or directory'    
       
    if framerange=='all':
        framerange=[0,fileinfo['num_frames']]
    else:
        framerange=[max(framerange[0],0), min(framerange[1],fileinfo['num_frames'])]
    
    return imagefiles, fileinfo, framerange    

   
def computeMean(fileprefix,croprange=None,toignore=np.array([[0,0],[0,0]]),framerange='all',imagetype='.jpg',bypixel=False):    
    imagefiles,fileinfo,framerange=listFrames(fileprefix,imagetype,framerange)    
    aggr_mean=0;
   
    for ifile in range(framerange[0],framerange[1]):
        if imagetype=='.tex':
            im=loadTexFrame(fileprefix+imagetype,ifile,fileinfo['frame_shape'],fileinfo['format'])
        else:    
            im=loadFrame(imagefiles[ifile])
            
        if ifile==framerange[0]:
            if croprange is None:
                croprange=np.array([[0,im.shape[0]],[0,im.shape[1]]])
            mask=np.zeros(im.shape).astype(bool)
            mask[croprange[0,0]:croprange[0,1],croprange[1,0]:croprange[1,1]]=True
            mask[toignore[0,0]:toignore[0,1],toignore[1,0]:toignore[1,1]]=False
            if bypixel:
                aggr_mean=np.zeros([croprange[0,1]-croprange[0,0],croprange[1,1]-croprange[1,0]])
        if bypixel:
            aggr_mean+=im[croprange[0,0]:croprange[0,1],croprange[1,0]:croprange[1,1]]
        else:    
            aggr_mean+=im[mask].mean()
            
    return aggr_mean*1./(framerange[1]-framerange[0])      
    
    
def computeStd(moviemean,fileprefix,croprange=None,toignore=np.array([[0,0],[0,0]]),framerange='all',imagetype='.jpg',bypixel=False):
    imagefiles,fileinfo,framerange=listFrames(fileprefix,imagetype,framerange)    
    aggr_std=0;
   
    for ifile in range(framerange[0],framerange[1]):
        if imagetype=='.tex':
            im=loadTexFrame(fileprefix+imagetype,ifile,fileinfo['frame_shape'],fileinfo['format'])
        else:    
            im=loadFrame(imagefiles[ifile])
            
        if ifile==framerange[0]:
            if croprange is None:
                croprange=np.array([[0,im.shape[0]],[0,im.shape[1]]])
            mask=np.zeros(im.shape).astype(bool)
            mask[croprange[0,0]:croprange[0,1],croprange[1,0]:croprange[1,1]]=True
            mask[toignore[0,0]:toignore[0,1],toignore[1,0]:toignore[1,1]]=False
            if bypixel:
                assert len(moviemean)>=len(im)
                aggr_std=np.zeros([croprange[0,1]-croprange[0,0],croprange[1,1]-croprange[1,0]])
        if bypixel:        
            aggr_std+=(im[croprange[0,0]:croprange[0,1],croprange[1,0]:croprange[1,1]]-moviemean)**2
        else:                   
            aggr_std+=((im[mask]-moviemean)**2).mean()
            
    return np.sqrt(aggr_std*1./(framerange[1]-framerange[0]))       


def initializeTex(filename,frameshape,fmt='B',close=True):
    f=open(filename,'wb')
    try:
        f.write(pack('<1B',13))
        f.write('Elphy Texture')
        #f.write(pack('<3i',27,*frameshape))
        f.write(pack('<3i',27,frameshape[1],frameshape[0]))
               
        if fmt=='B': # unsigned int, 1 byte
            f.write(pack('<1B',0))
        elif fmt=='f': # signed float, 4 bytes    
            f.write(pack('<1B',5))
        else:
            raise ValueError('Type not supported')
        if close:
            f.close()
            f=None
        return f
    except:
        f.close()
        raise
    

def saveMatToTex(mat,file_name,fmt='B'):
    with initializeTex(file_name,mat.shape[1:],fmt=fmt,close=False) as f:
        appendToBinary(f,np.round(mat).astype(int),fmt=fmt)
                
          
def readTexHeader(filename, close=True):
    f=open(filename,'rb')
    try:
        header=f.read(texheader_length)
            
        fileinfo={}
        frameshape=unpack('<2i',header[18:26])
        fileinfo['frame_shape']=[frameshape[1],frameshape[0]]
        
        format_code=unpack('<1B',header[26])[0]
        if format_code==0:
            fileinfo['format']='B'
            fileinfo['itemsize']=1
        elif format_code==5:   
            fileinfo['format']='f'
            fileinfo['itemsize']=4
        else:
            fileinfo['format']='unknown'
            fileinfo['itemsize']=np.nan
            
        fileinfo['num_frames']=(os.path.getsize(filename)-texheader_length)/(fileinfo['itemsize']*np.prod(fileinfo['frame_shape']))  
        
        if close:
            f.close()
            return fileinfo
        else:
            return fileinfo, f    
            
    except:
        f.close()
        raise    
        
     

def convertToTex(fileprefix,savename,framerange='all',imagetype='.jpg',fmt='B'):    
    imagefiles=sorted(glob.glob(fileprefix+'*'+imagetype)) 
    
    if len(imagefiles)==0:
        print 'Error:', fileprefix+'*'+imagetype,  'no such files or directory'        
    if framerange=='all':
        framerange=[0,len(imagefiles)]  
        
    fmtsize={'B':1,'f':4}    
    im=loadFrame(imagefiles[0])
    filesize=im.size*(framerange[1]-framerange[0])*fmtsize[fmt]
    print "File size:",filesize*10**-6,'Mo' 
    
    with initializeTex(savename,im.shape,fmt=fmt,close=False) as f:      
        for ifile in range(max(framerange[0],0),min(framerange[1],len(imagefiles))):
            im=loadFrame(imagefiles[ifile])
            appendToBinary(f,im,fmt=fmt) 
            #plt.figure(); plt.imshow(im)
            #print im.shape
            
            
def addMask(fileprefix,savename,maskpos,maskval=128,framerange='all',imagetype='.jpg'):  
    imagefiles=sorted(glob.glob(fileprefix+'*'+imagetype))

    if len(imagefiles)==0:
        print 'Error:', fileprefix+'*'+imagetype,  'no such files or directory'        
    if framerange=='all':
        framerange=[0,len(imagefiles)] 

    for ifile in range(max(framerange[0],0),min(framerange[1],len(imagefiles))):
        im=loadFrame(imagefiles[ifile]).copy()
        im[maskpos[0,0]:maskpos[0,1],maskpos[1,0]:maskpos[1,1]]=maskval
        saveToImfile(im,savename+str(ifile).zfill(5))
 
       
def createRandomization(fileprefix,savename,seedval=0,framerange='all',imagetype='.jpg'):
    imagefiles=sorted(glob.glob(fileprefix+'*'+imagetype))

    if len(imagefiles)==0:
        print 'Error:', fileprefix+'*'+imagetype,  'no such files or directory'        
    if framerange=='all':
        framerange=[0,len(imagefiles)] 
        
    defaultrange=range(max(framerange[0],0),min(framerange[1],len(imagefiles)))
    np.random.seed(seedval)
    randomrange=np.random.permutation(defaultrange)

    for ifile in range(len(defaultrange)):            
        copyfile(imagefiles[randomrange[ifile]],savename+str(defaultrange[ifile]).zfill(5)+imagetype)       
        
    return randomrange


def computeHist(fileprefix,croprange=None,toignore=np.array([[0,0],[0,0]]),framerange='all',imagetype='.jpg',figname=None,savename=None,display=True):
    imagefiles,fileinfo,framerange=listFrames(fileprefix,imagetype,framerange)    
    hist_bounds=np.arange(257)-0.5;
    temphist=np.zeros(256)

    for ifile in range(framerange[0],framerange[1]):
        if imagetype=='.tex':
            im=loadTexFrame(fileprefix+imagetype,ifile,fileinfo['frame_shape'],fileinfo['format'])
        else:    
            im=loadFrame(imagefiles[ifile])
            
        if ifile==framerange[0]:
            if croprange is None:
                croprange=np.array([[0,im.shape[0]],[0,im.shape[1]]])
            mask=np.zeros(im.shape).astype(bool)
            mask[croprange[0,0]:croprange[0,1],croprange[1,0]:croprange[1,1]]=True
            mask[toignore[0,0]:toignore[0,1],toignore[1,0]:toignore[1,1]]=False
        temphist+=np.histogram(im[mask],hist_bounds)[0]
    
    if savename is not None:
        saveMat(temphist,savename,fmt='f')
        
    if figname is not None:
        if not(display):
            plt.ioff()
        fig=plt.figure()
        plt.bar(hist_bounds[:-1],temphist*1./temphist.sum(),1)
        plt.axis([-0.5,255.5,plt.axis()[2],plt.axis()[3]])
        fig.savefig(figname)
        plt.close(fig)
        if not(display):
            plt.ion()
        
    return temphist  
    
    
def computeStatMaps(fileprefix,savename,croprange=None,framerange='all',imagetype='.jpg',scale=None,display=True):        
    meanmap=computeMean(fileprefix,croprange=croprange,framerange=framerange,imagetype=imagetype,bypixel=True)
    saveMat(meanmap,savename+'_meanmap')
    stdmap=computeStd(meanmap,fileprefix,croprange=croprange,framerange=framerange,imagetype='.jpg',bypixel=True)
    saveMat(stdmap,savename+'_stdmap')
    if scale is None:
        scale=[meanmap.min(),meanmap.max(),stdmap.min(),stdmap.max()]
    if not(display):
        plt.ioff()    
    fig=plt.figure();
    plt.imshow(meanmap,vmin=scale[0],vmax=scale[1],interpolation='none')
    plt.axis('off')
    fig.savefig(savename+'_meanmap.png') 
    plt.close(fig)       
    fig=plt.figure();
    plt.imshow(stdmap,vmin=scale[2],vmax=scale[3],interpolation='none')
    plt.axis('off')
    fig.savefig(savename+'_stdmap.png')
    plt.close(fig)
    if not(display):
        plt.ion()
    return meanmap, stdmap
    
 
def computeCropRange(center,finalshape,orishape):
    center=np.array(center)
    finalshape=np.array(finalshape)
    croprange=np.array([np.floor(center-finalshape/2).astype(int),np.floor(center+finalshape/2).astype(int)+np.mod(finalshape,2)]).T
    for i in range(2):
        if croprange[i,0]<0:
            croprange[i,:]-=croprange[i,0]
        if croprange[i,1]>orishape[i]:     
            croprange[i,:]+=orishape[i]-croprange[i,1]
    return croprange     
    
    
def cropRangeList(centerfile,outname,finalshape,moviepath):
    imshape=None
    with open(centerfile,'r') as cf:
        with open(outname,'a') as of:
            for line in cf:
                
                if line.isspace():
                    of.write('\n')
                    
                elif np.all([(char.isdigit() | char.isspace()) for char in line]):
                    centerpos=np.array(line.split(' ')).astype(int)
                    croprange=computeCropRange(centerpos,finalshape,imshape).reshape(-1)
                    of.write(' '.join([str(num) for num in croprange])+'\n')
                    
                else:
                    of.write(line)
                    imagefiles=sorted(glob.glob(moviepath+line[:-1]+'/'+line[:-1]+'*.jpg'))
                    imshape=loadFrame(imagefiles[0]).shape
                        
  
def cropMovies(croprangefile,allmoviespath,savepath,savemode='tex'):
    with open(croprangefile,'r') as f:
        movname=''
        for line in f:
            if not(line.isspace()):
                if np.all([(char.isdigit() | char.isspace()) for char in line]):
                    count+=1
                    croprange=np.array(line[:-1].split(' ')).astype(int).reshape(2,2)
                    print movname, count
                    if savemode=='tex':
                        processMovie(allmoviespath+movname+'/'+movname,savepath+movname+'_'+str(count)+'.tex',croprange=croprange,savemode='tex')
                    elif savemode=='image':
                        imagepath=savepath+movname+'_'+str(count)+'/'
                        os.mkdir(imagepath)
                        processMovie(allmoviespath+movname+'/'+movname,imagepath+movname+'_'+str(count)+'_',croprange=croprange,savemode='image')
                else:
                    movname=line[:-1]
                    print movname
                    count=0

                
def renameMovie(oldname,newname,moviespath,addsep=''):
    os.rename(moviespath+oldname,moviespath+newname)
    prefix=moviespath+newname+'/'+oldname
    for movfile in sorted(glob.glob(prefix+'*')):
        os.rename(movfile,moviespath+newname+'/'+newname+addsep+movfile[len(prefix):])
        
        
def genericTexName(tex_prefix,genericname,summaryfilename,nbzeros=3,ext='.tex',offset=0):
    texfiles=sorted(glob.glob(tex_prefix+'*'))
    with open(summaryfilename,'w') as f:
        for ifile in range(len(texfiles)):
            texname=texfiles[ifile].split('/')[-1]
            newname=genericname+'_'+str(offset+ifile+1).zfill(nbzeros)+ext
            f.write(texname+' '+newname+'\n')
            os.rename(texfiles[ifile],os.path.dirname(tex_prefix)+'/'+newname)
            
            
def plotstd(filenum,imshape=[1080,1920]):
    stdmapfiles=sorted(glob.glob(statspath+'*_stdmap'))
    stdmap=loadVec(stdmapfiles[filenum]).reshape(imshape)
    print stdmapfiles[filenum]
    fig=plt.figure()
    pid=plt.imshow(stdmap)
    cid = fig.canvas.mpl_connect('button_press_event',onclick)            
        

def onclick(event):
    print int(round(event.ydata)), int(round(event.xdata))                


def histStretchParams(histfile):
    hist=loadVec(histfile,fmt='f')
    nonzero=np.where(hist>0)[0]
    return nonzero.min(), 255./(nonzero.max()-nonzero.min())
    
    
def randomizeMovies(movie_dir,rand_prefix,chunk_size=1,across_movies=False,seed_val=0,summaryfilename='randomization',n_zeros=3):
    texfiles=sorted(glob.glob(movie_dir+'*.tex'))
    n_movies=len(texfiles)
    movie_handles=[[]]*n_movies
    frameshapes=[[]]*n_movies
    n_frames=np.zeros(n_movies)
    for ifile in range(n_movies):
        finfo,movie_handles[ifile]=readTexHeader(texfiles[ifile], close=False)
        frameshapes[ifile]=finfo['frame_shape']
        n_frames[ifile]=finfo['num_frames'] 
    assert np.all([frameshapes[i]==frameshapes[0] for i in range(len(frameshapes))])
    assert np.all(n_frames==n_frames[0])
    assert n_frames[0]%chunk_size==0
    n_chunks=int(n_frames[0]/chunk_size)
    
    np.random.seed(seed_val)
    frameorder=np.array([np.random.permutation(range(n_chunks)) for i in range(n_movies)]).T
    allorders=np.concatenate([np.arange(n_movies).reshape(1,-1,1).repeat(n_chunks,0),frameorder.reshape(frameorder.shape+(1,))],axis=2)
    if across_movies:
        allorders=np.rollaxis(allorders.reshape(n_movies,n_chunks,2),1)
        for imovie in range(n_movies):
            allorders[:,imovie,:]=allorders[np.random.permutation(range(n_chunks)),imovie,:]
    
    with open(rand_prefix+'_'+summaryfilename+'_order.txt','w') as f:   
        for ichunk in range(n_chunks):
            for iframe in range(chunk_size):
                for imovie in range(n_movies):
                    f.write(str(allorders[ichunk,imovie,0])+','+str(allorders[ichunk,imovie,1]*chunk_size+iframe)+' ')
                f.write('\n') 
                
    with open(rand_prefix+'_'+summaryfilename+'_movielist.txt','w') as f:
        for movie_name in texfiles:
            f.write(movie_name+'\n')             
    
    chunk_length=chunk_size*np.prod(frameshapes[0])            
    for imovie in range(n_movies):
        with initializeTex(rand_prefix+'_'+str(imovie+1).zfill(n_zeros)+'.tex',frameshapes[0],close=False) as f:
            for ichunk in range(n_chunks):
                chunk=loadTexFrame(movie_handles[allorders[ichunk,imovie,0]],allorders[ichunk,imovie,1],chunk_length,finfo['format'],close=False)             
                appendToBinary(f,chunk)
                
    for filehandle in movie_handles:
        filehandle.close()            
                
    return allorders 
    
    
def loadMovieList(filename,lookfor='STIM: ',convert_to=int):
    with open(filename,'r') as f:
        lines=f.read().split('\n')
    movielist=[]
    for l in lines:
        if len(l)>0:
            movielist.append(convert_to(l[l.index(lookfor)+len(lookfor):]))
    return movielist 
    
# /!\   MOVED TO HANDLEBINARYFILES : LOAD_INFO_TABLE, SAVE_INFO_TABLE
#def loadStimInfo(filename,sep=' '):
#    with open(filename,'r') as f:
#        lines=f.read().split('\n')
#    keys=lines[0].split(sep)
#    types=lines[1].split(sep)
#    stim_infos=OrderedDict()
#    for ikey in range(len(keys)):
#        stim_infos[keys[ikey]]=np.array([l.split(sep)[ikey] for l in lines[2:] if len(l)>0]).astype(types[ikey])
#    return stim_infos  
#
#
#def saveStimInfo(stim_info_dict,file_name='stim_info.txt'):
#    with open(file_name,'w') as f:          
#        f.write(' '.join(stim_info_dict.keys())+'\n' )
#        f.write(' '.join([str(type(stim_info_dict[key][0])).split("'")[1].split('numpy.')[-1] for key in stim_info_dict.keys()])+'\n' )
#        for i_stim in range(len(stim_info_dict.values()[0])):
#            f.write(' '.join([str(stim_info_dict[key][i_stim]) for key in stim_info_dict.keys()])+'\n')  


def mov_identifiers(moviefile,chunk_size=[5,5],excentr=[0,0],frames_tokeep='all'):
    texinfo=readTexHeader(moviefile)
    upleft_corner=np.round(np.array(texinfo['frame_shape'])*0.5-np.array(chunk_size)*0.5+np.array(excentr)).astype(int)
    crop_range=np.array([upleft_corner,upleft_corner+np.array(chunk_size)]).T
    return loadSubTexMovie(moviefile,crop_range=crop_range,frames_tokeep=frames_tokeep)


def correlate_frames(unknown_ids,known_ids,corr_thresh=0.99,diff_thresh=1.1):
    match_list=[]
    for iuframe in range(unknown_ids.shape[0]):
        match_list.append([])
        for imov in range(len(known_ids)):           
            for ikframe in range(known_ids[imov].shape[0]):
                corr=np.corrcoef(unknown_ids[iuframe].reshape(-1),known_ids[imov][ikframe].reshape(-1))[0,1]
                max_diff=np.max(np.abs(unknown_ids[iuframe]-known_ids[imov][ikframe]))
                if (max_diff==0) or ((corr>=corr_thresh) and (max_diff<=diff_thresh)):
                    match_list[iuframe].append(([imov,ikframe],corr,max_diff))
    return match_list                
        
        
def match_movieframes(composite_mov,original_movs,chunk_size=[5,5],corr_thresh=0.99,diff_thresh=1.1,max_iter=200,seedval=0):
    texinfo=readTexHeader(composite_mov)
    frame_corresp=np.zeros([texinfo['num_frames'],2])-1   
    max_excentr=((np.array(texinfo['frame_shape'])-np.array(chunk_size))*0.5/np.array(chunk_size)).astype(int)
    np.random.seed(seedval)
    excentr_vals=np.round(np.random.normal(size=(max_iter*2,2))*max_excentr*0.5).astype(int)
    excentr_vals=excentr_vals[(excentr_vals[:,0]<=max_excentr[0]) & (excentr_vals[:,1]<=max_excentr[1])]
    excentr_vals=np.array(list(set([tuple(v) for v in excentr_vals])))
    excentr_vals=excentr_vals[:max_iter]*np.array(chunk_size)
    
    it_num=0
    while (np.any(frame_corresp==-1)) and (it_num<max_iter):
        print '\niteration number '+str(it_num)+' : excentricity='+str(excentr_vals[it_num])
        
        if it_num>0:
            unresolved=np.where(np.any(frame_corresp==-1,axis=1))[0]
            print 'remaining frames : ', unresolved
            to_explore={}
            remaining_tuples=list(itertools.chain(*[match_list[framenum] for framenum in unresolved]))
            for item in set([tuple(tup[0]) for tup in remaining_tuples]):
                if to_explore.has_key(item[0]):
                    if item[1] not in to_explore[item[0]]:
                        to_explore[item[0]].append(item[1])
                else:
                    to_explore[item[0]]=[item[1]]
            kept_movs=sorted(to_explore.keys())            
        else:
            unresolved=range(texinfo['num_frames'])
            to_explore={movnum: 'all' for movnum in range(len(original_movs))}
            kept_movs=range(len(original_movs))

        composite_ids=mov_identifiers(composite_mov,chunk_size=chunk_size,excentr=excentr_vals[it_num],frames_tokeep=unresolved)
        original_ids=[mov_identifiers(original_movs[key],chunk_size=chunk_size,excentr=excentr_vals[it_num],frames_tokeep=to_explore[key]) for key in kept_movs]          
        #movie_ids=mov_identifiers([composite_mov]+original_movs,chunk_size=chunk_size,excentr=excentr_vals[it_num])
        #match_list=correlate_frames(movie_ids[0],movie_ids[1:],corr_thresh=corr_thresh,diff_thresh=diff_thresh)
        match_list=correlate_frames(composite_ids,original_ids,corr_thresh=corr_thresh,diff_thresh=diff_thresh)
        if it_num>0:
            match_list=restore_match_indices(match_list,to_explore,unresolved,texinfo['num_frames'])
            
        for framenum in range(texinfo['num_frames']):
            if len(match_list[framenum])==1:
                frame_corresp[framenum]=match_list[framenum][0][0]
            elif len(match_list[framenum])==0:
                print 'Warning: 0 correspondence found for frame '+str(framenum)
        it_num+=1
        
    if it_num==max_iter and (np.any(frame_corresp==-1)):
        unresolved=np.where(np.any(frame_corresp==-1,axis=1))[0]
        print '\nWarning: max number of iterations reached, some unsolved frames remain: ', unresolved
        remains_dict={framenum: match_list[framenum] for framenum in unresolved}
        #remains_dict={unresolved[-1][iframe]: match_list[iframe] for iframe in range(len(unresolved[-1]))}
    else:
        print '\nMatching complete!'
        remains_dict={}
    return frame_corresp.astype(int), remains_dict 
 
 
def restore_match_indices(match_list,to_explore,unresolved,init_nframes):
    kept_movs=sorted(to_explore.keys())
    restored_list=[]
    count=0
    for framenum in range(init_nframes):
        if framenum in unresolved:
            restored_list.append(match_list[count])
            for ival in range(len(match_list[count])):
                restored_movnum=kept_movs[match_list[count][ival][0][0]]
                restored_list[-1][ival][0][0]=restored_movnum
                restored_list[-1][ival][0][1]=to_explore[restored_movnum][match_list[count][ival][0][1]]
            count+=1
        else:
            restored_list.append('already solved')
    return restored_list        
   
    
def match_moviedirs(movie_dir,mixed=False):
    movielist_files=sorted(glob.glob(os.path.join(movie_dir,'*movielist.txt')))
    assert len(movielist_files)==1
    original_movies=sorted(loadMovieList(movielist_files[0],lookfor='',convert_to=str))
    composite_movies=sorted(glob.glob(os.path.join(movie_dir,'*.tex')))
    if ~mixed:
        assert len(original_movies)==len(composite_movies)
    matches=[]  
    
    try:
        with open(os.path.join(movie_dir,'randomization_order.txt'),'w') as f:
            for imov in range(len(composite_movies)):
                print '\n'+composite_movies[imov]
                if mixed:
                    matches.append(match_movieframes(composite_movies[imov],original_movies)[0])
                else:
                    matches.append(match_movieframes(composite_movies[imov],[original_movies[imov]])[0]+np.array([imov,0]))
                f.write(' '.join([str(matches[-1][iframe,0])+','+str(matches[-1][iframe,1]) for iframe in range(matches[-1].shape[0])])+'\n')
    except:
        print format_exc()
    finally:
        return matches 


def reconstruct_compmovs(original_movs,order_file,movs_tokeep='all'):
    comp_movs=[]
    lines=open(order_file,'r').readlines()
    if movs_tokeep=='all':
        movs_tokeep=range(len(lines))
    for imov in movs_tokeep:
        frame_order=np.array([eval('['+item+']') for item in lines[imov].split(' ')])
        comp_movs.append(original_movs[list(frame_order.T)])
    return comp_movs    
        

def reconstruct_saccadeMov(src_movfile,dest_movfile,saccade_file,final_shape,blank_val=0,refreshes_perframe=1,keep_movfps=True):
    final_shape=np.array(final_shape)
    texinfo=readTexHeader(src_movfile)
    with open(saccade_file,'r') as f:
        sacc_pos=np.array([eval('['+item+']') for item in f.readlines()])+0.5*np.array(texinfo['frame_shape'])
    assert sacc_pos.shape[0]>=texinfo['num_frames']*refreshes_perframe
    crop_ranges=np.rollaxis(np.array([sacc_pos-final_shape*0.5,sacc_pos+final_shape*0.5]),0,3)
    
    with open(src_movfile,'rb') as f_src:
        with initializeTex(dest_movfile,final_shape,fmt=texinfo['format'],close=False) as f_dest:    
            for iframe in range(texinfo['num_frames']):
                original_frame=loadTexFrame(f_src,iframe,texinfo['frame_shape'],texinfo['format'],close=False)
                if keep_movfps:
                    new_frames=np.zeros([refreshes_perframe]+list(final_shape))
                for iref in range(refreshes_perframe):
                    sacc_num=iframe*refreshes_perframe+iref
                    extensions=np.ceil(np.array([-np.minimum(crop_ranges[sacc_num,:,0],0),np.maximum(crop_ranges[sacc_num,:,1]-np.array(texinfo['frame_shape']),0)]).T).astype(int)
                    if np.any(extensions>0):
                        print "Warning: crop range out of bounds for frame "+str(iframe)+", padding with blank value ("+str(blank_val)+")"
                    frame=np.zeros(np.array(texinfo['frame_shape'])+extensions.sum(axis=1))+blank_val  
                    frame[extensions[0,0]:texinfo['frame_shape'][0]+extensions[0,0],extensions[1,0]:texinfo['frame_shape'][1]+extensions[1,0]]=original_frame
                    interp_func=interp2d(np.arange(frame.shape[1])-extensions[1,0],np.arange(frame.shape[0])-extensions[0,0],frame)
                    frame=interp_func(np.arange(final_shape[1])+crop_ranges[sacc_num,1,0],np.arange(final_shape[0])+crop_ranges[sacc_num,0,0])
                    if keep_movfps:
                        new_frames[iref]=frame
                    else:    
                        appendToBinary(f_dest,np.round(frame).astype(int),fmt=texinfo['format'])
                if keep_movfps:
                    appendToBinary(f_dest,np.round(new_frames.mean(axis=0)).astype(int),fmt=texinfo['format'])
                    
                    
def add_saccades_to_movie(movie_array,saccade_array,final_shape,nsaccades_per_frame=np.inf,degree_to_pixel=24,blank_val=0):
    """
    movie_array : initial_nframes x initial_npix1 x initial_npix2, the original movie
    saccade_array : n_saccades * 2 (position1,position2), successive saccade positions as given by elphy (they will be converted into python array coordinates, but one has to provide the magnification factor degree_to_pixel)
    final_shape : 2 (final_npix1,final_npix2), frame shape of the returned movie
    degree_to_pixel : magnification factor to convert elphy coordinates to python array coordinations
    blank_val : value with which to pad generated frames if they exceed image borders
    n_saccade_per_frame : how many saccades you do on each frame before the next frame is displayed. If inf: all saccades are done on the first frame (following frames are discarded)
    """
    if len(movie_array.shape)==2:
        movie_array=movie_array.reshape((1,)+movie_array.shape)
    n_saccades=saccade_array.shape[0]
    final_shape=np.array(final_shape)
    saccade_array=saccade_elphy2python(saccade_array,degree_to_pixel=degree_to_pixel)+0.5*np.array(movie_array.shape[1:])
    crop_ranges=np.rollaxis(np.array([saccade_array-final_shape*0.5,saccade_array+final_shape*0.5]),0,3)
    extensions=np.array([np.ceil(np.array([-np.minimum(crop_ranges[i_sacc,:,0],0),np.maximum(crop_ranges[i_sacc,:,1]-np.array(movie_array.shape[1:]),0)]).T).astype(int) for i_sacc in range(n_saccades)]).max(axis=0)
    if np.any(extensions>0):
        print "Warning : crop range out of bounds for some frames, padding with blank value ("+str(blank_val)+")"
    saccade_movie=np.zeros([n_saccades]+list(final_shape))
    
    frame_num=0
    for i_sacc in range(n_saccades): 
        if i_sacc%nsaccades_per_frame==0:            
            base_frame=np.zeros(np.array(movie_array.shape[1:])+extensions.sum(axis=1))+blank_val  
            base_frame[extensions[0,0]:movie_array.shape[1]+extensions[0,0],extensions[1,0]:movie_array.shape[2]+extensions[1,0]]=movie_array[frame_num]
            interp_func=interp2d(np.arange(base_frame.shape[1])-extensions[1,0],np.arange(base_frame.shape[0])-extensions[0,0],base_frame)  
            frame_num+=1
        saccade_movie[i_sacc]=interp_func(np.arange(final_shape[1])+crop_ranges[i_sacc,1,0],np.arange(final_shape[0])+crop_ranges[i_sacc,0,0])
       
    return saccade_movie               
   
        
def saccade_elphy2python(saccade_array,degree_to_pixel):
    # (Field of view displacement on image) = -1 x (image displacement on screen)
    #new_sacc=-saccade_array 
    new_sacc=saccade_array # In fact no, the right sign for NBR is this one... Who knows why ...
    # Screen: x goes from left to right, y goes from down to up
    # Image: x goes from up to down, y goes from left to right
    new_sacc=np.array([-new_sacc[:,1],new_sacc[:,0]]).T  
    # From degrees to number of image pixels
    new_sacc=degree_to_pixel*new_sacc
    return new_sacc                         
    
    
def compressHistTail(values,max_val,inflexion_pos):
    assert max_val>=inflexion_pos
    exp_ampl=max_val-inflexion_pos
    newvals=values.copy()
    if exp_ampl==0:
        modvals=max_val
    else:    
        modvals=exp_ampl*( 1 - np.exp( (inflexion_pos-newvals[newvals>inflexion_pos])*1./exp_ampl ) ) + inflexion_pos
    newvals[newvals>inflexion_pos]=modvals
    return newvals    


def rescale_with_saturation(values,limits,target_mean=None,target_std=None):
    if target_mean is None:
        target_mean=values.mean()
    if target_std is None:
        target_std=values.std()
    new_values=np.array(values).copy()
    past_values=np.array([0])
    int_values=np.array([1])
    while np.any(past_values!=int_values):
        new_values=(new_values-new_values.mean())*target_std*1./new_values.std()+target_mean
        new_values[new_values<limits[0]]=limits[0]
        new_values[new_values>limits[1]]=limits[1]
        past_values=int_values.copy()
        int_values=np.round(new_values).astype(int)
    return int_values


def loadtexmovie_simple(filename):
    # Returns an array of integers (time x nx x ny)
    with open(filename,'rb') as f:
        header=f.read(27)
        binary=f.read()
    frame_shape=unpack('<2i',header[18:26])[::-1]    
    return np.array(unpack('<' + str(len(binary)) + 'B', binary)).reshape((-1,)+frame_shape)