import os
import datetime
import numpy as np
from sys import argv
from collections import OrderedDict
import glob
try:
    from elphy_utils.elphy_reader import ElphyFile
    elphy_reader_import=True
except:
    elphy_reader_import=False
    print "\nWarning : no elphy_reader\n"    


def elphy_database(elphy_directory, output_directory='', file_suffix='_filelist.csv', sep=';', ext='.DAT'):
    for path, dirs, files in os.walk(elphy_directory):
        matching_files = [f for f in files if f[len(f)-len(ext):]==ext]
        if len(matching_files)>0:
            print '\n\n'+path+'\n'
            fileListToCsv(path, savename=os.path.join(output_directory,os.path.basename(path)+file_suffix), sep=sep, ext=ext)


def fileListToCsv(directory, savename='list_of_files.csv', sep=';', ext='.DAT'):
    
    labels_list=['Valid','Manip','Cortex','Penetration ID','File','Date','Cumulated time','Duration','Size (Mo)','Sampling rate (kHz)', 'N episodes', 'N truncated episodes',
               'Max truncation (ms)','Electrode','Eye','Other experimental params/remarks','Stimulus type','Stimulus subtype','Stim size','Stim mask',
               'Stim number of div (noise)','Stim orientation','Stim contrast','Stim dton (ms)','Screen rate (Hz)','N stimuli',
               'N repetitions','Other stim params','Spike sorting','Analysis summary']
    values_dict=OrderedDict(zip(labels_list,['']*len(labels_list))) 
    file_dicts=[]
         
    if directory[-1]!='/':
        directory+='/'
    files = [os.path.basename(fname) for fname in glob.glob(directory+'*'+ext)]    
    sizes = np.array([os.path.getsize(directory+fname) for fname in files]) *1./1000000
    sizes = np.round(sizes,2)  
    dates = np.array([None]*len(files))        

    for ifile in range(len(files)):
        
        print files[ifile]
        file_dicts.append(values_dict.copy())
        splitted_name=files[ifile].split('.')[0].split('_')
        file_dicts[ifile]['Manip']=splitted_name[0]
        file_dicts[ifile]['Cortex']='left' if 'CXLEFT' in files[ifile] else ('right' if 'CXRIGHT' in files[ifile] else '')
        file_dicts[ifile]['Penetration ID']=str(1) if len(splitted_name)==3 else splitted_name[2]
        file_dicts[ifile]['File']=splitted_name[-1]        
        file_dicts[ifile]['Size (Mo)']=str(sizes[ifile])
        
        try:
            ef=ElphyFile(directory+files[ifile])
            n_ep=len(ef.episodes)
            dxu=np.array([[ch.dxu for ch in ep.channels] for ep in ef.episodes]) # episodes x channels
            file_dicts[ifile]['Sampling rate (kHz)']=str(int(np.round(1./dxu[0,0])))
            file_dicts[ifile]['N episodes']=str(n_ep)
            if n_ep>0:
                dates[ifile]=ef.episodes[0].date
                file_dicts[ifile]['Date']=str(dates[ifile])[:19]
                last_ep_dur=ef.episodes[n_ep-1].channels[0].dxu*ef.episodes[n_ep-1].channels[0].nsamp/1000.
                file_dicts[ifile]['Duration']=str(ef.episodes[n_ep-1].date-ef.episodes[0].date+datetime.timedelta(0,last_ep_dur)).split('.')[0]
#                nmissing=np.array([[ch.nmissing for ch in ep.channels] for ep in ef.episodes])*dxu # episodes x channels
#                n_channels=nmissing.shape[1]
#                valid_channels=range(n_channels-3) if n_channels in [35,67] else range(n_channels)
#                file_dicts[ifile]['N truncated episodes']=str(np.any(nmissing[:,valid_channels]>0,axis=1).sum())
#                file_dicts[ifile]['Max truncation (ms)']=str(np.round(nmissing[:,valid_channels].max(),1))
            # To add : other info I will manage to extract from the ElphyFile object
        except:
            print 'Warning : reading of the Elphy file failed' 
        
    date_order=np.arange(len(files))
    valid_dates=np.array([d!=None for d in dates])
    date_order=list(date_order[valid_dates][np.argsort(dates[valid_dates])])+list(date_order[~valid_dates])
    with open(savename,'w') as f:
        f.write(sep.join(labels_list)+'\n')
        for ifile in date_order:
            try: file_dicts[ifile]['Cumulated time']=str(dates[ifile]-dates[date_order[0]]).split('.')[0]
            except: pass
            f.write(sep.join(file_dicts[ifile].values())+'\n')
            
    return files    
    
    
def ctime_to_HourMinSec(ctimes):
    
    seconds=np.round(np.array(ctimes)).astype(int)
    hours=np.floor(seconds*1./3600).astype(int)
    seconds-=3600*hours
    minutes=np.floor(seconds*1./60).astype(int)
    seconds-=60*minutes
    times=[':'.join([str(hours[i]).zfill(2),str(minutes[i]).zfill(2),str(seconds[i]).zfill(2)]) for i in range(len(ctimes))]
    
    return hours, minutes, seconds, times
    
    
    
if __name__=='__main__':
    
    if (len(argv)==1) or (argv[1]=='help'):
	print 'Syntax : python fileListToCsv directory_name [output_name separator]'
    else:
        fileListToCsv(*argv[1:])
        if len(argv)>2:
	    print 'Created '+argv[2]
        else:
	    print 'Created list_of_files.csv'	
