import numpy as np
import glob
import os
import shutil

#parent_dir='/DATA/Margot/natural_movies/vanhateren_imc_images'
#new_dir=''
parent_dir='/DATA/Margot/tex_stimuli/mcgill'
new_dir='/home/margot/Bureau/all'

child_dirs=sorted(glob.glob(os.path.join(parent_dir,'*')))
for dir_name in child_dirs:
    for file_name in sorted(glob.glob(os.path.join(dir_name,'*'))):
        #os.rename(file_name,os.path.join(new_dir,os.path.basename(dir_name)+'_'+os.path.basename(file_name)))
        shutil.copy(file_name,os.path.join(new_dir,os.path.basename(dir_name)+'_'+os.path.basename(file_name)))