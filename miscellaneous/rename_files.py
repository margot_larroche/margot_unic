import os
import glob

old_prefix='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/grating_pairs/grating_pairs_144_stdmax_20180225_renamed/grating_pairs_stdmax_'
new_prefix='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/grating_pairs/grating_pairs_144_stdmax_20180225_renamed/grating_pairs_stdmax_'
ext='.tex'
shift=0
n_zeros=4
renumber=False

files=sorted(glob.glob(old_prefix+'*'+ext))
for ifile in range(len(files)):
    suffix=files[ifile][len(old_prefix):len(files[ifile])-len(ext)]
    if n_zeros is None:
        nz=len(suffix)
    else:
        nz=n_zeros
    if renumber:
        suffix=str(ifile+1).zfill(nz)
    elif (shift!=0) or (n_zeros!=None):
        suffix=str(int(suffix)+shift).zfill(nz)
    os.rename(files[ifile],new_prefix+suffix+ext)
