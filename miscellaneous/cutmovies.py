import os
import glob
from miscellaneous.convertVideo import *

old_movdir='/DATA/Margot/tex_stimuli/motion_clouds/MC_6ori_5FT_4FS_4orisp'
new_movdir='/DATA/Margot/tex_stimuli/motion_clouds/MC_6ori_5FT_4FS_4orisp_saturated'
to_remove=[0,360]
sat_thresh=40.

mov_names=sorted(glob.glob(old_movdir+'/*'))
for mov_name in mov_names:
    mov=loadTexMovie(mov_name)
    if sat_thresh!=None:
        mov[mov>sat_thresh]=sat_thresh
    saveMatToTex(mov[to_remove[0]:to_remove[1]],os.path.join(new_movdir,os.path.basename(mov_name)))
    

def playAllMovs(mov_dir,frame_dur=1./60.,n_movs='all'):    
    mov_names=sorted(glob.glob(os.path.join(mov_dir,'*')))
    if n_movs=='all':
        n_movs=len(mov_names)
    mov_info=readTexHeader(mov_names[0])    
    allmovs=np.zeros([n_movs,mov_info['num_frames'],mov_info['frame_shape'][0],mov_info['frame_shape'][1]])
    for imov in range(n_movs):
        allmovs[imov]=loadTexMovie(mov_names[imov])        
    return playMovieFromMat(allmovs.reshape(np.prod(allmovs.shape[:2]),allmovs.shape[2],allmovs.shape[3]),frame_dur)