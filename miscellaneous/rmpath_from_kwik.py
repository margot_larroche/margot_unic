import os
import sys
import glob
import h5py

spksort_folders=os.listdir(sys.argv[1])

for folder in spksort_folders:
    print folder
    kwik_files=glob.glob(os.path.join(sys.argv[1],folder,'*.kwik'))
    for kfile in kwik_files:
        print kfile
        with h5py.File(os.path.join(sys.argv[1],folder,kfile),'r+') as hf:
            hf['application_data/spikedetekt'].attrs['prb_file']=os.path.basename(hf['application_data/spikedetekt'].attrs['prb_file'][0])
            hf['recordings/0/raw'].attrs['dat_path']=os.path.basename(hf['recordings/0/raw'].attrs['dat_path'][0])
            hf['application_data/spikedetekt'].attrs['raw_data_files']=os.path.basename(hf['application_data/spikedetekt'].attrs['raw_data_files'][0])