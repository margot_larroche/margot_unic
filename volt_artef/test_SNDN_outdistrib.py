from testArtefVolt import *
from lscsm_adapt.simulations import generate_stim
import numpy as np
import matplotlib.pyplot as plt
from lscsm_adapt.volterra_kernels import diagterms, lsq_kernel, restorek2shape, k2resp

npixside=5
nbins=50000
seed=0
colors=['r','g','b','y','c','m']
#explist=[0.5,1,1.5,2,3,4]
#explist=[0.1,0.5,1,2,4]
explist=[0.1,0.5,1,2,3]
short_alist=[0,0.25,0.5,0.75,1]

SN=np.concatenate([np.zeros((1,npixside**2)),generate_stim(stimsize=npixside**2, nbins=nbins, stimtype='Snoise',seedval=seed)], axis=0)
DN=np.concatenate([np.zeros((1,npixside**2)),generate_stim(stimsize=npixside**2, nbins=nbins, stimtype='Dnoise',seedval=seed)], axis=0)

#lin_subunit=onoffSubunit(npixside,updw_margin=1,side_margin=2,sep_width=0,phase=0,transpose=False)
lin_subunit=onoffSubunit(npixside,updw_margin=1,side_margin=1,sep_width=0,phase=0,transpose=False)
#nl_subunits=np.concatenate([lin_subunit,-lin_subunit,abs(lin_subunit)],axis=1)
nl_subunits=np.concatenate([lin_subunit,-lin_subunit],axis=1)

resp_SN=np.dot(SN,lin_subunit)
resp_DN=np.dot(DN,lin_subunit)
#DN=DN*resp_SN.std()/resp_DN.std()
#corrDN=DN[(np.abs(resp_DN)==1).squeeze(),:]
#corrSN=SN[:corrDN.shape[0],:]
corrDN=DN[(np.abs(resp_DN)<=1).squeeze(),:]
corrSN=SN[:corrDN.shape[0],:]
#corrSN=SN
#corrDN=DN
#corrDN=DN[(abs(resp_DN)>=2).squeeze(),:]
#corrDN=np.concatenate([corrDN,SN[:SN.shape[0]-corrDN.shape[0],:]])
#corrSN=SN
#corrDN=DN[(np.abs(resp_DN)>=3).squeeze(),:]
#corrSN=SN[:corrDN.shape[0],:]



#plt.figure()
#plt.hist(resp_DN)
#plt.hist(resp_SN)

modelParams={
'lin_subunit': lin_subunit,
'nl_subunits': nl_subunits,
'nl_weights': [1,1],
'pwl2lin': False,
'exp': 1 }

#factors=arange(0.340,0.355,0.005)
#factors=arange(0.320,0.345,0.005)
#factors=arange(0.3,0.41,0.01)
#factors=arange(0.1,0.8,0.1)
#factors=[0.5,1,1.5]
#for a in range(0,1,0.1):
for a in [0.4]:    
    respSNcorr=modelResp(corrSN,lin_subunit,nl_subunits,a,1,nl_weights=None,pwl2lin=False)
    respDNcorr=modelResp(corrDN,lin_subunit,nl_subunits,a,1,nl_weights=None,pwl2lin=False)
    k1SNcorr,k2SNcorr=lsq_kernel(corrSN,respSNcorr,order0=False,order2='diag')
    k1DNcorr,k2DNcorr=lsq_kernel(corrDN,respDNcorr,order0=False,order2='full')
    vrespSNcorr=np.dot(corrSN,k1SNcorr)+np.dot(corrSN**2,k2SNcorr)
    vrespDNcorr=np.dot(corrDN,k1DNcorr)+k2resp(restorek2shape(k2DNcorr),corrDN)
    respsimilSNcorr=np.corrcoef(respSNcorr.squeeze(),vrespSNcorr.squeeze())
    respsimilDNcorr=np.corrcoef(respDNcorr.squeeze(),vrespDNcorr.squeeze())
    print 'a:', a, 'SN:', respsimilSNcorr, 'DN:', respsimilDNcorr
SI,SIstar,lids=plotSI([corrSN,corrDN],modelParams,alist=np.arange(0,1.01,0.1),col='b',intercept=False,order2='diag',center_resp=False)

#count=1
#plt.figure()
#for factor in factors:
#    #corrDN=DN*factor
#    plt.subplot(1,len(factors)+1,count)
#    lids=[[]]*len(explist)
#    for i in range(len(explist)):
#        modelParams['exp']=explist[i]
#        SI,SIstar,lids[i]=plotSI([corrSN,corrDN],modelParams,alist=np.arange(0,1.01,0.1),col=colors[i],intercept=True,order2='full',center_resp=False)
#    plt.title('DN x '+str(factor))   
#    plt.xlabel('SI SN')
#    if count==1:
#        plt.ylabel('SI DN')
#    count+=1
#plt.subplot(1,len(factors)+1,len(factors)+1)
#plt.legend(lids,[str(exp) for exp in explist],title='Exponent',loc='center')
#plt.axis('off') 

#plt.figure()
#for i in range(len(explist)):
#    modelParams['exp']=explist[i]
#    SI,SIstar,lids[i]=plotSI([corrSN,corrDN],modelParams,alist=np.arange(0,1.01,0.1),col=colors[i],intercept=True,order2='full',center_resp=False)

#modelParams['exp']=2    
#onoff_SN,onoff_DN,volt_SN,volt_DN=SNDNrange([corrSN,corrDN],modelParams,alist=short_alist,intercept=True,order2='full',rescale=1,substract_r0=0)
##plotAllRFs(onoff_SN[1],onoff_DN[1],onoff_SN[2],onoff_DN[2],volt_SN[3],volt_DN[3],volt_SN[4],volt_DN[4],rescale=1,scalebycell=False,alist=short_alist)
#plotAllRFs(onoff_SN[1],onoff_DN[1],onoff_SN[2],onoff_DN[2],volt_SN[3],volt_DN[3],diagterms(volt_SN[4]),diagterms(volt_DN[4]),rescale=1,scalebycell=False,alist=short_alist)
#    