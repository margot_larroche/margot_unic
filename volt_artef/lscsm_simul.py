testdatapath='/DATA/Margot/ownCloud/MargotUNIC/VoltArtef_data/'
from lscsm.checkpointing import loadParams
import numpy as np
from testArtefVolt import *
from lscsm.LSCSM import LSCSM
from lscsm_adapt.visualizationMgt import plotLSCSM
import matplotlib.pyplot as plt
from test_orientation_selectivity import *


def setLSCSMparam(lscsm,K,param_name,new_value):
    newK=np.array(K[:])
    if isinstance(param_name,str):
        param_name=[param_name]
        new_value=[new_value]
    for iparam in range(len(param_name)):    
        (i,t) = lscsm.free_params[param_name[iparam]]
        newK[i:i+np.prod(t)]=np.array([new_value[iparam]]).reshape(-1)
    return list(newK)
        

#fileprefix='blank'
fileprefix='blank'
nOut=1000
#nCellsPerPlot=25
nPix=225
nbins=30000
outthresh=False
LGNthresh=False
powerlaw=1
minLGNsize=4
SN=np.concatenate((generate_stim(stimsize=nPix, nbins=nbins, stimtype='Snoise'),np.zeros((1,nPix))),axis=0)
DN=np.concatenate((generate_stim(stimsize=nPix, nbins=nbins, stimtype='Dnoise'),np.zeros((1,nPix))),axis=0)

meta_params, suppl_params = loadParams(resultspath+fileprefix+'_metaparams')
lscsmParams={}
#meta_params['lgn_size_bounds']=(minLGNsize,meta_params['lgn_size_bounds'][1])
lscsm=LSCSM(np.zeros((2,nPix)),np.zeros((2,nOut)),**meta_params)
K=lscsm.create_random_parametrization(0)
if not(outthresh):
    K=setLSCSMparam(lscsm,K,['output_layer_threshold'],[-1000])
if powerlaw==2:
    meta_params['v1of']='SquareRect'
    lscsm=LSCSM(np.zeros((2,nPix)),np.zeros((2,nOut)),**meta_params)
elif powerlaw==3:
    meta_params['v1of']='Pow3Rect'
    lscsm=LSCSM(np.zeros((2,nPix)),np.zeros((2,nOut)),**meta_params)
elif powerlaw=='fullsq':
    meta_params['v1of']='Square'
    lscsm=LSCSM(np.zeros((2,nPix)),np.zeros((2,nOut)),**meta_params)    
if LGNthresh:
    meta_params['LGN_treshold']=True
    meta_params['lgnof']=meta_params['v1of']
    lscsm=LSCSM(np.zeros((2,nPix)),np.zeros((2,nOut)),**meta_params)
    tempK=lscsm.create_random_parametrization(0)
    i,t=lscsm.free_params['lgn_threshold']
    K=K[:i]+tempK[i:i+t]+K[i:]


overlaps={}
kon={}
koff={}
SI={}
SIstar={}
k0={}
k1={}
k2diag={}
resps={}
stims={'SN':SN, 'DN':DN}
center_resp={'SN':False, 'DN': True}

##K=K0
r0=lscsm.response(np.zeros((1,nPix)),K)
##onoffSN,onoffDN,voltSN,voltDN=plotRFrange([SN,DN],lscsmParams,alist=None,intercept=True,rescale=0.8,substract_r0=r0)
##plotLSCSM(lscsm,K)
#
for stimtype in stims.keys():
    resps[stimtype]=lscsm.response(stims[stimtype],K)-r0   
    overlaps[stimtype],kon[stimtype],koff[stimtype]=ONOFFrange(stims[stimtype],resps[stimtype],center_resp=center_resp[stimtype])
    SI[stimtype],SIstar[stimtype],k0[stimtype],k1[stimtype],k2diag[stimtype]=SIrange(stims[stimtype],resps[stimtype],intercept=True,order2='diag',center_resp=False)

func=lambda s:lscsm.response(s,K)
period_vals=np.arange(4,30)
ori_vals=arange(0,180,2)
phaserat_vals=arange(0,1,0.1)
oriresps=rectify_negresps(compute_ori_resp(func,period_vals,ori_vals,phaserat_vals,stimSide=15))
circvar_mean=compute_circvar(oriresps,period_vals,ori_vals,mode='mean')
circvar_F1=compute_circvar(oriresps,period_vals,ori_vals,mode='F1')
circvar_max=compute_circvar(oriresps,period_vals,ori_vals,mode='max')
sortorder=np.argsort(circvar_mean)
subsel=sortorder[:300]
#
#
#sortorder=np.argsort(SI['SN'])
#
#
##nplots=np.int(np.ceil(nOut*1./nCellsPerPlot))
##
##for iPlot in range(nplots):
##    subset=sortorder[iPlot*nCellsPerPlot:np.min([(iPlot+1)*nCellsPerPlot,nOut])]
##    fig=plotAllRFs(kon['SN'][:,subset],kon['DN'][:,subset],koff['SN'][:,subset],koff['DN'][:,subset],k1['SN'][:,subset],k1['DN'][:,subset],k2diag['SN'][:,subset],k2diag['DN'][:,subset])
##    fig.savefig(testdatapath+'kernels_0_k0_onoffDNcentered_lscsm4-'+str(nOut)+'cells_sharpthreshhid_'+str(iPlot+1)+'.png')
##    plt.close(fig)
##subset=sortorder[list(arange(0,nOut,50))+[nOut-1]]
#subset=range(5)
#fig=plotAllRFs(kon['SN'][:,subset],kon['DN'][:,subset],koff['SN'][:,subset],koff['DN'][:,subset],k1['SN'][:,subset],k1['DN'][:,subset],k2diag['SN'][:,subset],k2diag['DN'][:,subset],scalebycell=True,rescale=1)
##fig.savefig(testdatapath+'kernels_0_k0_onoffDNcentered_lscsm4-'+str(nOut)+'cells_sharpthreshhid.png')
#   
fig=plt.figure()
plt.plot(overlaps['SN'],overlaps['DN'],'b.')
plt.plot(overlaps['SN'][subsel],overlaps['DN'][subsel],'c.')
xlabel('ON-OFF overlap, SN')
ylabel('ON-OFF overlap, DN')
#gca().set_aspect('equal')
#fig.savefig(testdatapath+'OnOffOverlapDN=f(SN)_0_nolinpwl_onoffDNcentered_lscsm4-'+str(nOut)+'cells_sharpthreshhid.png')
#
#
plt.figure()
plt.plot(SI['SN'],SI['DN'],'b.')
plt.plot(SIstar['SN'],SIstar['DN'],'r.')
plt.legend(['SI','SI*'],'lower right')
plt.plot(SI['SN'][subsel],SI['DN'][subsel],'c.')
plt.plot(SIstar['SN'][subsel],SIstar['DN'][subsel],'y.')
plt.plot([0,1],[0,1],'k')
xlabel('SN')
ylabel('DN')
gca().set_aspect('equal')
#fig.savefig(testdatapath+'OnOffOverlapDN=f(SN)_0_nolinpwl_onoffDNcentered_lscsm4-'+str(nOut)+'cells_sharpthreshhid.png')
#
#
#plotLSCSM(lscsm,K,DN,subSet=arange(20))
#figname='/DATA/Margot/ownCloud/MargotUNIC/VoltArtef_data/OrientationSelectivity_lscsm4_100cells_sharpthreshhid'
#func=lambda s:lscsm.response(s,K)
##period_vals=np.arange(4,30)
##ori_vals=arange(0,180,2)
##phaserat_vals=arange(0,1,0.1)
#period_vals=np.arange(4,30,2)
#ori_vals=arange(0,180,20)
#phaserat_vals=arange(0,1,0.1)
#oriresps=compute_ori_resp(func,period_vals,ori_vals,phaserat_vals,stimSide=15)
#resps_pos,resps_neg=split_by_respsign(oriresps)
#plot_ori_select(resps_pos,period_vals,ori_vals,phaserat_vals,subset=arange(43,resps_pos.shape[3]),mode='mean',figname=figname)
##plot_ori_select(resps_pos,period_vals,ori_vals,phaserat_vals,mode='mean')
##plot_ori_select(resps_pos,period_vals,ori_vals,phaserat_vals,mode='F1')
#circvar_mean=compute_circvar(resps_pos,period_vals,ori_vals,mode='mean')
#circvar_F1=compute_circvar(resps_pos,period_vals,ori_vals,mode='F1')
#pref_mean,resps_bestphase_mean, best_indices_mean=compute_ori_pref(resps_pos,period_vals,ori_vals,mode='mean')
#pref_F1,resps_bestphase_F1, best_indices_F1=compute_ori_pref(resps_pos,period_vals,ori_vals,mode='F1')
#modratio=compute_modulation_ratio(resps_pos)
