# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
#from sklearn.linear_model import LinearRegression
from lscsm_adapt.computeReceptiveFields import computeSI, computeSIstar
from lscsm_adapt.volterra_kernels import lsq_kernel, diagterms
from lscsm_adapt.simulations import generate_stim
from lscsm.visualization import computeCorr, clean_plot

    
#def solveVolt2(stim,a,offdiag=False,exp=1):
#    Nstim=size(stim,0)
#    assert Nstim>=4
#    #R=powerlaw(stim[:,1]-stim[:,0],exp) + (1-a)*powerlaw(stim[:,0]-stim[:,1],exp)
#    R=(1-a)*(powerlaw(stim[:,1]-stim[:,0],exp) + powerlaw(stim[:,0]-stim[:,1],exp)) + a*(stim[:,1]-stim[:,0])
#    #R=(1-a)*(powerlaw(stim[:,1]-stim[:,0],exp)) + a*(stim[:,1]-stim[:,0])
#    #R=modelResp(stim,[[1],[-1]],[[1,-1],[-1,1]],a,exp)
#    if offdiag:    
#        X=np.zeros((Nstim,5))
#        X[:,4]=stim[:,0]*stim[:,1]
#    else:
#        X=np.zeros((Nstim,4))
#    X[:,:2]=stim
#    X[:,2:4]=stim**2
#    
#    #predictor = LinearRegression(copy_X=False, fit_intercept=False) 
#    #predictor.fit(X,R.reshape(-1,1))
#    #return predictor.coef_                             
#    return np.dot(np.linalg.inv(np.dot(X.T,X)),np.dot(X.T,R))
#
#
#
#def respK1(k1,stim):
#    return np.dot(stim,k1.T)
#    
#def respK2diag(k2diag,stim):
#    return np.dot(stim**2,k2diag.T) 
#
#    
#def computeSIold(kernels,stim):
#    SI=(kernels[:2]**2).sum()*1./(kernels[:4]**2).sum()
#    r_k1=respK1(kernels[:2],stim)
#    r_k2d=respK2diag(kernels[2:4],stim)
#    SIstar=np.sum(r_k1**2)*1./(np.sum(r_k1**2)+np.sum(r_k2d**2))
#    return SI,SIstar
#    
#def plotSIold(stims,alist=np.arange(0,1.01,0.05),col='r',offdiag=[False,False],exp=1):
#    SI=[np.zeros((len(alist),1)),np.zeros((len(alist),1))]
#    SIstar=[np.zeros((len(alist),1)),np.zeros((len(alist),1))]
#    for istim in range(2):
#        for ival in range(len(alist)):
#            sol=solveVolt2(stims[istim],alist[ival],offdiag=offdiag[istim],exp=exp)
#            SI[istim][ival],SIstar[istim][ival]=computeSIold(sol,stims[istim])
#            
#    plt.plot([0,1],[0,1],'k')
#    plt.plot(SI[0],SI[1],col)
#    plt.plot(SIstar[0],SIstar[1],col+'--') 
#    return SI, SIstar   
    
def powerlaw(signal,exp):
    s=signal.copy()
    s[s<0]=0
    return s**exp  
    
    
def modelResp(stim,lin_subunit,nl_subunits,a,exp,nl_weights=None,pwl2lin=False):
    if nl_weights==None:
        nl_weights=np.ones((nl_subunits.shape[1],1))
    nl_resp=powerlaw(np.dot(stim,nl_subunits),exp)
    lin_resp=np.dot(stim,lin_subunit)
    if pwl2lin:
       lin_resp=powerlaw(lin_resp,exp)-powerlaw(-lin_resp,exp)
#    return (1-a)*nl_resp.sum(axis=1).reshape(-1,1)+a*lin_resp 
    return (1-a)*np.dot(nl_resp,nl_weights).reshape(-1,1)+a*lin_resp   
    

#def plotSI(stims,lin_subunit,nl_subunits,alist=np.arange(0,1.01,0.05),exp=1,pwl2lin=False,col='r',nl_weights=None,intercept=False,order2='diag',center_resp=False):
def plotSI(stims,modelParams,alist=np.arange(0,1.01,0.05),col='r',intercept=False,order2='diag',center_resp=False):
# modelParams: lin_subunit, nl_subunits, exp, nl_weights, pwl2lin
    assert len(stims)==2
    SI=[[],[]]
    SIstar=[[],[]]
    
    for istim in range(2): 
        resps=np.zeros((stims[istim].shape[0],len(alist)))
        for ival in range(len(alist)):
            resps[:,ival]=modelResp(stims[istim],modelParams['lin_subunit'],modelParams['nl_subunits'],alist[ival],modelParams['exp'],nl_weights=modelParams['nl_weights'],pwl2lin=modelParams['pwl2lin']).T    
        SI[istim],SIstar[istim], _ , _, _ =SIrange(stims[istim],resps,order2=order2,center_resp=center_resp)

    plt.plot([0,1],[0,1],'k',linewidth=3)
    lid,=plt.plot(SI[0],SI[1],col,linewidth=3)
    plt.plot(SIstar[0],SIstar[1],col+'--',linewidth=3)
    plt.gca().set_aspect('equal')
    return SI, SIstar, lid 
    

def plotOverlap(stims,modelParams,col='r',center_resp=[False,True]):
    assert len(stims)==2
    ovl=[[],[]]
    
    for istim in range(2):
        if modelParams.has_key('lscsm'):  
            resps=modelParams['lscsm'].response(stims[istim],modelParams['K'])
        else:
            resps=np.zeros((stims[istim].shape[0],len(alist)))
            for ival in range(len(alist)):
               resps[:,ival]=modelResp(stims[istim],modelParams['lin_subunit'],modelParams['nl_subunits'],alist[ival],modelParams['exp'],nl_weights=modelParams['nl_weights'],pwl2lin=modelParams['pwl2lin']).T            
        ovl[istim], _ , _ =ONOFFrange(stims[istim],resps,center_resp=center_resp[istim])
        
    plt.plot([-1,1],[-1,1],'k',linewidth=2)
    lid,=plt.plot(ovl[0],ovl[1],col,linewidth=2)
    plt.gca().set_aspect('equal')
    return ovl, lid          
        

def SIrange(stim,resps,intercept=False,order2='diag',center_resp=False):
    assert order2 in ['diag','full']
    if center_resp:
        resps=resps-resps.mean(axis=0)
    if intercept:
        k0,k1,k2=lsq_kernel(stim,resps,order0=True,order2=order2)
    else:    
        k1,k2=lsq_kernel(stim,resps,order0=False,order2=order2)
        k0=np.zeros((1,resps.shape[1]))
    if order2=='full':
        k2diag=diagterms(k2)
    else:
        k2diag=k2
    return computeSI(k1,k2diag), computeSIstar(k1,k2,stim), k0, k1, k2
    
    
def ONOFFrange(stim,resps,center_resp=False):
    if center_resp:
        resps=resps-resps.mean(axis=0)
    kON,kOFF=computeONOFF(stim,resps)
    return computeCorr(kON,kOFF), kON, kOFF
  

def computeONOFF(stim,resp):
    assert len(stim.shape)==2 & len(resp.shape)==2
    stimON=powerlaw(stim,1)
    stimOFF=powerlaw(-stim,1)
    return np.dot(stimON.T,resp)*1./np.sum(stimON,axis=0).reshape(-1,1), np.dot(stimOFF.T,resp)*1./np.sum(stimOFF,axis=0).reshape(-1,1)
    
    
def onoffSubunit(npixside,updw_margin=0,side_margin=0,sep_width=0,phase=0,transpose=False):
#    offset=np.int(np.round(npixside*1./7))
#    rect_width=np.int((npixside-3*offset)*1./2)
#    sep_width=npixside-2*rect_width-2*offset
    rect_width=np.int((npixside-2*side_margin-sep_width)/2)
    sep_width=npixside-2*side_margin-2*rect_width
    assert np.all(np.array([side_margin,sep_width,rect_width])>=0)
    h1=np.zeros((npixside,npixside))
    h1[updw_margin:npixside-updw_margin,side_margin:side_margin+rect_width]=1
    h1[updw_margin:npixside-updw_margin,side_margin+rect_width+sep_width:npixside-side_margin]=-1
    h1=np.concatenate((h1[:,phase:],h1[:,:phase]),axis=1)
    if transpose:
        h1=h1.T
    #plt.pcolor(h1)
    return h1.reshape(-1,1)
        

def SNDNrange(stims,modelParams,alist=None,intercept=False,order2='diag',rescale=1,substract_r0=0,centeronoff=[False,True]):
    resps=[[],[]]
    for istim in range(2):
        if modelParams.has_key('lscsm'):  
            resps[istim]=modelParams['lscsm'].response(stims[istim],modelParams['K'])-substract_r0
        else:
            resps[istim]=np.zeros((stims[istim].shape[0],len(alist)))
            for ival in range(len(alist)):
               resps[istim][:,ival]=modelResp(stims[istim],modelParams['lin_subunit'],modelParams['nl_subunits'],alist[ival],modelParams['exp'],nl_weights=modelParams['nl_weights'],pwl2lin=modelParams['pwl2lin']).T-substract_r0
    
    overlapsSN,konSN,koffSN=ONOFFrange(stims[0],resps[0],center_resp=centeronoff[0])
    overlapsDN,konDN,koffDN=ONOFFrange(stims[1],resps[1],center_resp=centeronoff[1])
    siSN,sistarSN,k0SN,k1SN,k2SN=SIrange(stims[0],resps[0],intercept=intercept,order2=order2,center_resp=False)
    siDN,sistarDN,k0DN,k1DN,k2DN=SIrange(stims[1],resps[1],intercept=intercept,order2=order2,center_resp=False)  
    
    return [overlapsSN,konSN,koffSN], [overlapsDN,konDN,koffDN], [siSN,sistarSN,k0SN,k1SN,k2SN], [siDN,sistarDN,k0DN,k1DN,k2DN]


def plotAllRFs(konSN,konDN,koffSN,koffDN,k1SN,k1DN,k2diagSN,k2diagDN,rescale=1,scalebycell=False,alist=None,RFshape='square'):    
    npix,nplots=konSN.shape
    if RFshape=='square':
        RFshape=[np.int(np.sqrt(npix)),np.int(np.sqrt(npix))]
    if scalebycell:
        RFmax=rescale*np.max(np.abs(np.concatenate([konSN,konDN,koffSN,koffDN,k1SN,k1DN,k2diagSN,k2diagDN],axis=0)),axis=0)
    else:       
        RFmax=np.ones(nplots)*rescale*np.max(np.abs(np.concatenate([konSN,konDN,koffSN,koffDN,k1SN,k1DN,k2diagSN,k2diagDN],axis=0)))
    #fig=plt.figure(figsize=(3*nplots,24));   
    fig=plt.figure();
    
    for i in range(nplots):
        ax=plt.subplot(8,nplots,i+1)
        plt.pcolor(k1SN[:,i].reshape(RFshape),vmin=-RFmax[i],vmax=RFmax[i])
        clean_plot(ax)
        if i==0: plt.ylabel('k1 SN')
        if alist!=None: plt.title('a='+str(alist[i]))
        
        ax=plt.subplot(8,nplots,nplots+i+1)
        plt.pcolor(k1DN[:,i].reshape(RFshape),vmin=-RFmax[i],vmax=RFmax[i])
        clean_plot(ax)
        if i==0: plt.ylabel('k1 DN')
            
        ax=plt.subplot(8,nplots,2*nplots+i+1)
        plt.pcolor(k2diagSN[:,i].reshape(RFshape),vmin=-RFmax[i],vmax=RFmax[i])
        clean_plot(ax)
        if i==0: plt.ylabel('k2diag SN')
    
        ax=plt.subplot(8,nplots,3*nplots+i+1)
        plt.pcolor(k2diagDN[:,i].reshape(RFshape),vmin=-RFmax[i],vmax=RFmax[i])
        clean_plot(ax)
        if i==0: plt.ylabel('k2diag DN')
    
        ax=plt.subplot(8,nplots,4*nplots+i+1)
        plt.pcolor(konSN[:,i].reshape(RFshape),vmin=-RFmax[i],vmax=RFmax[i])
        clean_plot(ax)
        if i==0: plt.ylabel('kON SN')    
    
        ax=plt.subplot(8,nplots,5*nplots+i+1)
        plt.pcolor(konDN[:,i].reshape(RFshape),vmin=-RFmax[i],vmax=RFmax[i])
        clean_plot(ax)
        if i==0: plt.ylabel('kON DN')
    
        ax=plt.subplot(8,nplots,6*nplots+i+1)
        plt.pcolor(koffSN[:,i].reshape(RFshape),vmin=-RFmax[i],vmax=RFmax[i])
        clean_plot(ax)
        if i==0: plt.ylabel('kOFF SN')   
        
        ax=plt.subplot(8,nplots,7*nplots+i+1)
        plt.pcolor(koffDN[:,i].reshape(RFshape),vmin=-RFmax[i],vmax=RFmax[i])
        clean_plot(ax)
        if i==0: plt.ylabel('kOFF DN')

    return fig            

#def analytic_sol(a):
#    return np.array([[1-2*a,1,1,1-2*a,0,0,2*(1-2*a),2]]).T, np.array([[-a,a,1-a,1-a]]).T, np.array([[-a,a,0.6*(1-a),0.6*(1-a),a-1]]).T      
#            
#def r2pix(stim,a,exp):
#    return a*(max(stim[1]-stim[0],0)**exp-max(stim[0]-stim[1],0)**exp) + (1-a)*abs(stim[0]-stim[1])**exp

if __name__ == '__main__':
    npixside=4
#    lin_subunit=np.array([[1],[-1]])
#    nl_subunits=np.array([[-2,1],[1,-1]])
    #lin_subunit=np.array([[-1],[1],[0]])
    #nl_subunits=np.array([[-1,1],[1,-1],[0,0]])
    lin_subunit=onoffSubunit(npixside,updw_margin=1,side_margin=1,sep_width=0,phase=0,transpose=False)
    nl_subunits=np.concatenate([lin_subunit,-lin_subunit],axis=1)
    nbins=10000
    nzeros=1
    SN=np.concatenate([generate_stim(stimsize=len(lin_subunit), nbins=nbins, stimtype='Snoise'),np.zeros((nzeros,len(lin_subunit)))],axis=0)
    DN=np.concatenate([generate_stim(stimsize=len(lin_subunit), nbins=nbins, stimtype='Dnoise'),np.zeros((nzeros,len(lin_subunit)))],axis=0)
    #DN=DN[np.abs(np.dot(DN,lin_subunit)).squeeze()<=1,:]    
    #stims={'SN':SN, 'DN':DN}
    modelParams={}
    modelParams['nl_subunits']=nl_subunits
    modelParams['lin_subunit']=lin_subunit
    modelParams['nl_weights']=np.array([1,1]) 
    modelParams['exp']=1
    modelParams['pwl2lin']=False 
    alist=np.arange(0,1.01,0.2)
    #center_resp={'SN':False, 'DN': True}
    center_resp=[False,False]
    
#    onoffSN,onoffDN,voltSN,voltDN=SNDNrange([SN,DN],modelParams,alist=alist,intercept=True,order2='diag',rescale=1,substract_r0=0,centeronoff=center_resp)
##    #[overlapsSN,konSN,koffSN], [overlapsDN,konDN,koffDN], [siSN,sistarSN,k0SN,k1SN,k2SN], [siDN,sistarDN,k0DN,k1DN,k2DN]
#    plotAllRFs(onoffSN[1],onoffDN[1],onoffSN[2],onoffDN[2],voltSN[3],voltDN[3],voltSN[4],voltDN[4],rescale=1,scalebycell=False,alist=alist,RFshape=[2,1]) 
#    plt.figure()
#    plt.plot(onoffSN[0],onoffDN[0],'r')
#    plt.plot(voltSN[0],voltDN[0],'b')


#    SI={}; SIstar={}; k0={}; k1={}; k2diag={}; k2={}; overlap={}; kon={}; koff={};
#    center_resp={'SN': False, 'DN': True}
#    stims={'SN':SN,'DN':DN}
#    resps={'SN': np.zeros((stims['SN'].shape[0],len(alist))) , 'DN': np.zeros((stims['DN'].shape[0],len(alist)))}
#    for stimtype in ['SN','DN']:
#        i=0
#        for a in alist:
#            resps[stimtype][:,i]=modelResp(stims[stimtype],lin_subunit,nl_subunits,a,expval,nl_weights=None,pwl2lin=False).squeeze()
#            i+=1
#        SI[stimtype],SIstar[stimtype],k0[stimtype],k1[stimtype],k2diag[stimtype]=SIrange(stims[stimtype],resps[stimtype],intercept=True,order2='diag',center_resp=False)
#        #SI[stimtype],SIstar[stimtype],k0[stimtype],k1[stimtype],k2[stimtype]=SIrange(stims[stimtype],resps[stimtype],intercept=True,order2='full',center_resp=False)            
#        overlap[stimtype], kon[stimtype], koff[stimtype] = ONOFFrange(stims[stimtype],resps[stimtype],center_resp=center_resp[stimtype])
#        #overlap[stimtype], kon[stimtype], koff[stimtype] = ONOFFrange(stims[stimtype],resps[stimtype],center_resp=False)
#        #k2diag[stimtype]=diagterms(k2[stimtype])
#    plotAllRFs(kon['SN'],kon['DN'],koff['SN'],koff['DN'],k1['SN'],k1['DN'],k2diag['SN'],k2diag['DN'],rescale=1,scalebycell=True,alist=alist) 


##SN=np.array([[0,1],[1,0],[0,-1],[-1,0]])
##DN=np.array([[1,0],[0,1],[-1,0],[0,-1],[1,1],[-1,-1],[1,-1],[-1,1]])
##SN=np.array([[0,0],[0,1],[1,0],[0,-1],[-1,0]])
##DN=np.array([[0,0],[0,1],[1,0],[0,-1],[-1,0],[1,1],[-1,-1],[1,-1],[-1,1]]) 
#lin_subunit=np.array([[-1],[1]])
#nl_subunits=np.array([[-1,1],[1,-1]])
#nbins=100000
#npixside=6
#updw_margin=1
#side_margin=2
#sep_width=0
#modelParams={}
##SN=generate_stim(stimsize=25, nbins=1000, stimtype='Snoise')
##DN=generate_stim(stimsize=25, nbins=1000, stimtype='Dnoise')    
#SN=np.concatenate((generate_stim(stimsize=npixside**2, nbins=nbins, stimtype='Snoise'),np.zeros((1,npixside**2))),axis=0)
#DN=np.concatenate((generate_stim(stimsize=npixside**2, nbins=nbins, stimtype='Dnoise'),np.zeros((1,npixside**2))),axis=0)
##SN=generate_stim(stimsize=npixside**2, nbins=nbins, stimtype='Snoise')
##DN=generate_stim(stimsize=npixside**2, nbins=nbins, stimtype='Dnoise')
##SN00=np.concatenate((generate_stim(stimsize=npixside**2, nbins=nbins, stimtype='Snoise'),np.zeros((nbins,npixside**2))),axis=0)
##DN00=np.concatenate((generate_stim(stimsize=npixside**2, nbins=nbins, stimtype='Dnoise'),np.zeros((nbins,npixside**2))),axis=0)
#modelParams['lin_subunit']=onoffSubunit(npixside,updw_margin=updw_margin,side_margin=side_margin,sep_width=sep_width,phase=0)
##nl_subunits=np.concatenate([onoffSubunit(npixside,margin=margin,sep_width=sep_width,phase=i) for i in range(npixside)],axis=1)
##nl_subunits=np.concatenate([lin_subunit,-lin_subunit,onoffSubunit(npixside,updw_margin=updw_margin,side_margin=side_margin,sep_width=sep_width,phase=0,transpose=True),-onoffSubunit(npixside,updw_margin=updw_margin,side_margin=side_margin,sep_width=sep_width,phase=0,transpose=True)],axis=1) 
##nl_weights=array([1,1,0,0])  
##nl_subunits=np.concatenate([onoffSubunit(npixside,updw_margin=updw_margin,side_margin=side_margin,sep_width=sep_width,phase=0,transpose=False),-onoffSubunit(npixside,updw_margin=updw_margin,side_margin=side_margin,sep_width=sep_width,phase=0,transpose=False),onoffSubunit(npixside,updw_margin=updw_margin,side_margin=side_margin,sep_width=sep_width,phase=1,transpose=False),-onoffSubunit(npixside,updw_margin=updw_margin,side_margin=side_margin,sep_width=sep_width,phase=1,transpose=False),onoffSubunit(npixside,updw_margin=updw_margin,side_margin=side_margin,sep_width=sep_width,phase=-1,transpose=False),-onoffSubunit(npixside,updw_margin=updw_margin,side_margin=side_margin,sep_width=sep_width,phase=-1,transpose=False)],axis=1) 
##nl_weights=np.array([1,1,1,1,1,1])
#modelParams['nl_subunits']=np.concatenate([modelParams['lin_subunit'],-modelParams['lin_subunit']],axis=1)
#clean_nl_subunits=nl_subunits.copy()
#clean_nl_subunits[abs(clean_nl_subunits)<0.0007]=0
#clean_nl_subunits[clean_nl_subunits>0]=nl_subunits.max()
#clean_nl_subunits[clean_nl_subunits<0]=nl_subunits.min()
##clean_nl_subunits=clean_nl_subunits*1./clean_nl_subunits.max()
#clean_lin_subunit=clean_nl_subunits[:,0].reshape(-1,1)

#modelParams['nl_subunits']=nl_subunits
#modelParams['lin_subunit']=lin_subunit
#modelParams['nl_subunits']=clean_nl_subunits
#modelParams['lin_subunit']=clean_lin_subunit
#modelParams['lin_subunit']=onoffSubunit(npixside,updw_margin=1,side_margin=1)
#modelParams['nl_subunits']=np.concatenate((modelParams['lin_subunit'],np.abs(modelParams['lin_subunit'])),axis=1)
#modelParams['nl_weights']=np.array([1,1]) 
#modelParams['exp']=1
#modelParams['pwl2lin']=False 
#alist=np.arange(0,1.01,0.2)
#nl_weights=np.ones(2)
#alist=np.arange(0,1.01,0.05)
#expval=1
#resp=modelResp(SN,modelParams['lin_subunit'],modelParams['nl_subunits'],0,1,nl_weights=None,pwl2lin=False)
#resp01=modelResp(SN,modelParams['lin_subunit']*0.1,modelParams['nl_subunits']*0.1,0,1,nl_weights=None,pwl2lin=False)
#print all(resp==resp01*10)
#k0,k1,k2diag=lsq_kernel(SN,resp,order0=True,order2='diag')
#k001,k101,k2diag01=lsq_kernel(SN,resp01,order0=True,order2='diag')
#kon,koff=computeONOFF(SN,resp)
#kon01,koff01=computeONOFF(SN,resp)
#print all(k0==k001*10), all(k1==k101*10), all(k2diag==k2diag01*10)
#print all(k0==k001), all(k1==k101), all(k2diag==k2diag01)
##    
    colors=['r','g','b','y','c','m']
    explist=[0.5,1,1.5,2,3,4]

#onoffSN,onoffDN,voltSN,voltDN=plotRFrange([SN,DN],modelParams,alist=alist,rescale=0.5,intercept=True)
#modelParams['lin_subunit']=modelParams['lin_subunit']*0.1
#modelParams['nl_subunits']=modelParams['nl_subunits']*0.1
#onoffSN01,onoffDN01,voltSN01,voltDN01=plotRFrange([SN,DN],modelParams,alist=alist,rescale=1,intercept=True)


#plt.figure()
#lids=[]
#for i in range(len(explist)):
#    _,_,lid=plotSI([SN,DN],lin_subunit,nl_subunits,col=colors[i],exp=explist[i],pwl2lin=False,intercept=True,order2='full')
#    lids.append(lid)
#plt.legend(lids,[str(exp) for exp in explist],title='Exponent',loc='upper left')    
#xlabel('SI SN')
#ylabel('SI DN')


#plt.figure()
#lids=[]
#for i in range(len(explist)):
#    _,lid=plotOverlap([SN,DN],lin_subunit,nl_subunits,col=colors[i],exp=explist[i],pwl2lin=False,nl_weights=nl_weights)
#    lids.append(lid)
#plt.legend(lids,[str(exp) for exp in explist],title='Exponent',loc='upper left')    
#xlabel('ON-OFF overlap SN')
#ylabel('ON-OFF overlap DN')
 
#plotSI([SN*2,DN*2])
#solSN=solveVolt2(SN,1)
#solDN=solveVolt2(DN,0.5)
#solSNmult=solveVolt2(2*SN,1)  
#solDNmult=solveVolt2(2*DN,1) 
 
#resp=modelResp(DN,lin_subunit,nl_subunits,0.5,1)
#on,off=computeONOFF(DN,resp)
 
 


#
 
#
#    
#plt.figure()
#non0sub=(nl_weights!=0)
#toplot=np.concatenate((modelParams['lin_subunit'],modelParams['nl_subunits'][:,non0sub]),axis=1)
#rfmax=np.abs(toplot).max()
#nplots=toplot.shape[1]
#ax=subplot(2,nplots,1)
#plt.plot([-1,1],[-1,1],'k',linewidth=3)
#axis([-1,1,-1,1])
#clean_plot(ax)
#for i in arange(1,nplots):
#    ax=plt.subplot(2,nplots,i+1)
#    x=arange(-1,1,0.01)
#    y=powerlaw(x,expval)*nl_weights[non0sub][i-1]
#    plt.plot(x,y,'k',linewidth=3)
#    axis([-1,1,-1,1])
#    clean_plot(ax)
#for j in range(nplots):
#    ax=plt.subplot(2,nplots,nplots+j+1)
#    plt.pcolor(toplot[:,j].reshape(npixside,npixside),vmin=-rfmax,vmax=rfmax)
#    clean_plot(ax)    


#plt.figure()
#plt.plot(overlapsSN,overlapsDN,'r',linewidth=2)
#plt.plot([-1,1],[-1,1],'k',linewidth=2)
#gca().set_aspect('equal')
#expval=2
#alist=np.arange(0,1.01,0.2)
#stims={'SN':SN, 'DN':DN}  
#lin_subunit=onoffSubunit(npixside,updw_margin=1,side_margin=2,sep_width=0,phase=0,transpose=False)
#nl_subunits=np.concatenate([lin_subunit,-lin_subunit],axis=1)
#SI={}; SIstar={}; k0={}; k1={}; k2diag={}; k2={}; overlap={}; kon={}; koff={};
#resps={'SN': np.zeros((stims['SN'].shape[0],len(alist))) , 'DN': np.zeros((stims['DN'].shape[0],len(alist)))}
#center_resp={'SN': False, 'DN': True}
##center_resp={'SN': False, 'DN': False}
#  
#for stimtype in ['SN','DN']:
#    i=0
#    for a in alist:
#        resps[stimtype][:,i]=modelResp(stims[stimtype],lin_subunit,nl_subunits,a,expval,nl_weights=None,pwl2lin=False).squeeze()
#        i+=1
#    SI[stimtype],SIstar[stimtype],k0[stimtype],k1[stimtype],k2diag[stimtype]=SIrange(stims[stimtype],resps[stimtype],intercept=True,order2='diag',center_resp=False)
#    #SI[stimtype],SIstar[stimtype],k0[stimtype],k1[stimtype],k2[stimtype]=SIrange(stims[stimtype],resps[stimtype],intercept=True,order2='full',center_resp=False)            
#    overlap[stimtype], kon[stimtype], koff[stimtype] = ONOFFrange(stims[stimtype],resps[stimtype],center_resp=center_resp[stimtype])
#    #overlap[stimtype], kon[stimtype], koff[stimtype] = ONOFFrange(stims[stimtype],resps[stimtype],center_resp=False)
#    #k2diag[stimtype]=diagterms(k2[stimtype])
#plotAllRFs(kon['SN'],kon['DN'],koff['SN'],koff['DN'],k1['SN'],k1['DN'],k2diag['SN'],k2diag['DN'],rescale=1,scalebycell=False,alist=alist) 

#modelParams={'lin_subunit': lin_subunit, 'nl_subunits': nl_subunits, 'nl_weights': None, 'pwl2lin':False}

    lids=[]
    i=0
    figure()
    for exp in [0.5,1,2,3,4]:
        modelParams['exp']=exp
        #plotOverlap([SN,DN],modelParams,col=colors[i])
        _,_,lid=plotSI([SN,DN],modelParams,alist=np.arange(0,1.01,0.05),col=colors[i],intercept=True,order2='full',center_resp=False)
        i+=1
        lids.append(lid)
    plt.legend(lids,[str(exp) for exp in explist],title='Exponent',loc='upper left')    
    xlabel('SI SN')
    ylabel('SI DN')
