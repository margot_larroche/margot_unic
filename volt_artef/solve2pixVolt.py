import numpy as np
from testArtefVolt import powerlaw
from sympy import Symbol, Matrix

a=Symbol('a',positive=True)
mu=Symbol('mu',positive=True,nonzero=True)
expval=1

SN=np.array([[0,0],[0,1],[1,0],[0,-1],[-1,0]])
DN=np.array(list(SN)+[[1,1],[-1,-1],[1,-1],[-1,1]])
subDN=DN[:-2,:]
subDNbis=DN[range(5)+[7,8],:]

lin_subunit=np.array([[1],[-1]])
#lin_subunit=np.array([[1],[-2]])
nl_subunits=np.array([[1,-2],[-1,1]])
#nl_subunits=np.array([[1,-1,1],[-1,1,1]])
#nl_subunits=np.array([[1,-1,1,-1],[-1,1,1,-1]])
#nl_subunits=np.array([[1,-1],[1,-1]])

lin_resp=lambda stim, linsub: np.dot(stim,linsub)
nl_resps=lambda stim, nlsubs, exp, weights=np.array([1]): np.array(weights).squeeze()*powerlaw(np.dot(stim,nlsubs),exp)
tot_resp=lambda stim, linsub, nlsubs, exp, aval, weights=np.array([1]): aval*lin_resp(stim,linsub) + (1-aval)*nl_resps(stim,nlsubs,exp,weights).sum(axis=1).reshape(-1,1)
volt_coeffs=lambda stim: np.concatenate([np.ones((stim.shape[0],1)), stim,stim**2, stim[:,0:1]*stim[:,1:2]],axis=1)
onoff_coeffs=lambda stim: np.concatenate([np.ones((stim.shape[0],1)), powerlaw(stim,1), powerlaw(-stim,1)],axis=1)
lin_sol=lambda A, B: np.dot(np.array(Matrix(np.dot(A.T,A)).inv()), np.dot(A.T,B))
#lin_sol=lambda A, B: np.dot(np.linalg.inv(np.dot(A.T,A)), np.dot(A.T,B))
sta_approx=lambda A,B: np.dot(np.diag(1./np.diag(np.dot(A.T,A))), np.dot(A.T,B))
volt_sol=lambda stim, linsub, nlsubs, exp, aval, voltorders: lin_sol( volt_coeffs(stim)[:,voltorders], tot_resp(stim,linsub,nlsubs,exp,aval) )
onoff_sol=lambda stim, linsub, nlsubs, exp, aval, onofforders: lin_sol( onoff_coeffs(stim)[:,onofforders], tot_resp(stim,linsub,nlsubs,exp,aval) )
onoff_approx=lambda stim, linsub, nlsubs, exp, aval, onofforders: sta_approx( onoff_coeffs(stim)[:,onofforders], tot_resp(stim,linsub,nlsubs,exp,aval) )

testSN=volt_sol(SN, lin_subunit, nl_subunits, expval, a, [1,2,3,4])
testDN=volt_sol(DN, lin_subunit, nl_subunits, expval, a, [1,2,3,4])
testDNFull=volt_sol(DN, lin_subunit, nl_subunits, expval, a, [1,2,3,4,5])
testSub=volt_sol(subDN, lin_subunit, nl_subunits, expval, a, [1,2,3,4])
testSubFull=volt_sol(subDN, lin_subunit, nl_subunits, expval, a, [1,2,3,4,5])
onoffSN=onoff_sol(SN, lin_subunit, nl_subunits, expval, a, [0,1,2,3,4])
onoffDN=onoff_sol(DN, lin_subunit, nl_subunits, expval, a, [0,1,2,3,4])
onoffSNap=onoff_approx(SN, lin_subunit, nl_subunits, expval, a, [0,1,2,3,4])
onoffDNap=onoff_approx(DN, lin_subunit, nl_subunits, expval, a, [0,1,2,3,4])

testSN2=volt_sol(2*SN, lin_subunit, nl_subunits, expval, a, [1,2,3,4])
testDN2=volt_sol(2*DN, lin_subunit, nl_subunits, expval, a, [1,2,3,4])
testDNFull2=volt_sol(2*DN, lin_subunit, nl_subunits, expval, a, [1,2,3,4,5])
testSub2=volt_sol(2*subDN, lin_subunit, nl_subunits, expval, a, [1,2,3,4])
testSubFull2=volt_sol(2*subDN, lin_subunit, nl_subunits, expval, a, [1,2,3,4,5])

lresp=lin_resp(DN,lin_subunit)
nlresp=nl_resps(DN,nl_subunits,expval)
totresp=tot_resp(DN,lin_subunit,nl_subunits,expval,a)
vcoeffs=volt_coeffs(DN)


b=Symbol('b')
c=Symbol('c')
d=Symbol('d')
e=Symbol('e')
f=Symbol('f')
g=Symbol('g')
h=Symbol('h')
i=Symbol('i')
j=Symbol('j')
#b=Symbol('b',positive=True)
#c=Symbol('c',positive=True)
#d=Symbol('d',positive=True)
#e=Symbol('e',positive=True)
#f=Symbol('f',positive=True)
#g=Symbol('g',positive=True)
#h=Symbol('h',positive=True)
#i=Symbol('i',positive=True)
#j=Symbol('j',positive=True)
symbresp=np.array([[b,c,d,e,f,g,h,i,j]]).T

onoffSNapSymb=sta_approx(onoff_coeffs(SN), symbresp[:5])
onoffDNapSymb=sta_approx(onoff_coeffs(DN), symbresp)