import numpy as np
import glob
import os
from struct import pack, unpack
from sys import argv



texheader_length=27



def appendToBinary(filehandle,framemat,fmt='B'):
    filehandle.write(pack('<'+str(framemat.size)+fmt,*np.reshape(np.array(framemat),-1)))


def readTexHeader(filename, close=True):
    f=open(filename,'rb')
    try:
        header=f.read(texheader_length)
            
        fileinfo={}
        frameshape=unpack('<2i',header[18:26])
        fileinfo['frame_shape']=[frameshape[1],frameshape[0]]
        
        format_code=unpack('<1B',header[26])[0]
        if format_code==0:
            fileinfo['format']='B'
            fileinfo['itemsize']=1
        elif format_code==5:   
            fileinfo['format']='f'
            fileinfo['itemsize']=4
        else:
            fileinfo['format']='unknown'
            fileinfo['itemsize']=np.nan
            
        fileinfo['num_frames']=(os.path.getsize(filename)-texheader_length)/(fileinfo['itemsize']*np.prod(fileinfo['frame_shape']))  
        
        if close:
            f.close()
            return fileinfo
        else:
            return fileinfo, f    
            
    except:
        f.close()
        raise


def initializeTex(filename,frameshape,fmt='B',close=True):
    f=open(filename,'wb')
    try:
        f.write(pack('<1B',13))
        f.write('Elphy Texture')
        #f.write(pack('<3i',27,*frameshape))
        f.write(pack('<3i',27,frameshape[1],frameshape[0]))
               
        if fmt=='B': # unsigned int, 1 byte
            f.write(pack('<1B',0))
        elif fmt=='f': # signed float, 4 bytes    
            f.write(pack('<1B',5))
        else:
            raise ValueError('Type not supported')
        if close:
            f.close()
            f=None
        return f
    except:
        f.close()
        raise



def loadVec(fileobj,fmt='d',start=0,stop='end',size_unit='item',close=True,endian='<',keep_fmt=False):
    """
    Load an array/matrix stored in binary format
    """
        
    try:
        f=open(fileobj,'rb')
    except TypeError:
        f=fileobj
        
    itemsize=len(pack(endian+'1'+fmt,1.))
    if size_unit=='item':
        assert os.path.getsize(f.name)%itemsize==0
        mult_factor=itemsize
    else:
        assert size_unit=='byte'
        mult_factor=1
    if stop=='end':
        stop=os.path.getsize(f.name)/mult_factor    
        
    f.seek(start*mult_factor)
    binary=f.read((stop-start)*mult_factor)
    if close:
        f.close()    
    
    if keep_fmt:
        dtype=fmt
    else:
        dtype=float
        
    return np.array(unpack(endian+str(len(binary)/itemsize)+fmt,binary),dtype=dtype)
    

def circular_mask(stim_shape,inner_radius,outer_radius,center='middle'):
    assert len(stim_shape)==2
    assert outer_radius>=inner_radius
    if center=='middle':
        center=0.5*(np.array(stim_shape)-1)
    i_mat=np.arange(stim_shape[0]).reshape(-1,1).repeat(stim_shape[1],axis=1)
    j_mat=np.arange(stim_shape[1]).reshape(1,-1).repeat(stim_shape[0],axis=0)
    radius_mat=np.sqrt((i_mat-center[0])**2+(j_mat-center[1])**2)
    mask=1-np.maximum(np.minimum((radius_mat-inner_radius)*1./max(outer_radius-inner_radius,10**-12),np.ones(stim_shape)),np.zeros(stim_shape))
    return mask
    
    
def loadTexMovie(filename):
    texinfo=readTexHeader(filename)
    return loadVec(filename,fmt=texinfo['format'],start=texheader_length,size_unit='byte').reshape([texinfo['num_frames']]+texinfo['frame_shape'])    
    
  
def saveMatToTex(mat,file_name,fmt='B'):
    with initializeTex(file_name,mat.shape[1:],fmt=fmt,close=False) as f:
        appendToBinary(f,np.round(mat).astype(int),fmt=fmt)
        
        
        
argv+=['/home/margot/Bureau/add_mask/mixedstims_tex_20pix_std60_090218']
if (len(argv)==1) or (argv[1]=='help'):
	print 'Syntax : python add_mask.py directory_name [inner_radius outer_radius background]'
else:
    dir_name=os.path.abspath(argv[1])
    inner_radius = argv[2] if (len(argv)>2) else 2./3
    outer_radius = argv[3] if (len(argv)>3) else 1
    background = argv[4] if (len(argv)>4) else 127.5
    print 'inner radius :', inner_radius, 'outer radius :', outer_radius
    files=sorted(glob.glob(dir_name+'/*.tex'))
    os.makedirs(dir_name+'_withmask')
    for ifile in range(len(files)):
        print files[ifile]
        mat=loadTexMovie(files[ifile])
        stim_shape=mat.shape[1:]
        mask=circular_mask(stim_shape,inner_radius*min(stim_shape)/2.,outer_radius*min(stim_shape)/2.).reshape((1,)+stim_shape).repeat(mat.shape[0],axis=0)
        saveMatToTex(mat*mask+background*(1-mask),dir_name+'_withmask/'+os.path.basename(files[ifile]))