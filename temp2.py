from data_analysis.extracell_funcs_NEW import matdata_to_rfdata

mat_file='/home/margot/Bureau/temp_data/1718_CXRIGHT_TUN2_spksort180523.mat'
epinfo_file='/home/margot/Bureau/temp_data/test_epinfo.txt'
unitinfo_file='/home/margot/Bureau/temp_data/1718_CXRIGHT_TUN2_spksort180523_unitinfo.txt'
stimcat_file='/home/margot/Bureau/temp_data/test_stim_groups'


stim,resps,ep_info=matdata_to_rfdata(mat_file,ep_info_file=epinfo_file,stim_categories_file=stimcat_file,file_type='new')