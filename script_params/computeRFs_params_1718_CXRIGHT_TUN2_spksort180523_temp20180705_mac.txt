### 1/ DATA PATHS ###
'data_files' : {'natmov': '/Users/margot/ownCloud/MargotUNIC/DATA/extracell_data/python_data/1718_CXRIGHT_TUN2_natmov.mat',
                'natmovRF': '/Users/margot/ownCloud/MargotUNIC/DATA/extracell_data/python_data/1718_CXRIGHT_TUN2_natmov_randframe.mat',
                'natmovRF3': '/Users/margot/ownCloud/MargotUNIC/DATA/extracell_data/python_data/1718_CXRIGHT_TUN2_natmov_randframe3.mat',
                'natmovRP': '/Users/margot/ownCloud/MargotUNIC/DATA/extracell_data/python_data/1718_CXRIGHT_TUN2_natmov_randphase.mat',
                'natmovRAP': '/Users/margot/ownCloud/MargotUNIC/DATA/extracell_data/python_data/1718_CXRIGHT_TUN2_natmov_randallpix.mat',
                'natmovRSP': '/Users/margot/ownCloud/MargotUNIC/DATA/extracell_data/python_data/1718_CXRIGHT_TUN2_natmov_randspatpix.mat'},
                
'rf_path' : '/Users/margot/ownCloud/MargotUNIC/ANALYSIS/RF_analysis/1718_CXRIGHT_TUN2_spksort180523_temp',


### 2/ STEPS TO EXECUTE ###
'load_data' : True,
'model_types' : ['volt1','BWT','HSM'],
'add_nonlinearity' : True,
'to_compute' : ['RFs','corr','corr_ceiling'],
'load_existing_RFs' : True,
'to_plot' : ['cross_pred_clouds','cross_pred_mat','corrbarplot_means'],
'to_save' : ['plots'],


### 3/ RF COMPUTING PARAMETERS ###
'normalize_stim' : False,
'normalize_resps' : False,
'ROI' : [[2,11],[2,11]],
'include_constant' : True,
'fit_mode' : 'theano',
'rand_seed' : -1,
'RF_tau' : 0,
'RF_Ntau' : 10,
'noise_ceiling_vals' : [1., 1.2, 1.4, 1.6, 1.8, 2., 2.2, 2.4, 2.6, 2.8, 3., 3.2, 3.4, 3.6, 3.8, 4., 4.2, 4.4, 4.6, 4.8, 5.],


### 4/ PLOTTING PARAMETERS ###
'displayed_ceiling_val': 0.0,
'ncells_per_fig' : 49,
'n_disp_cells': 49,
'rf_colors' : {'HSM': 'cyan', 'volt2diag_NL': 'pink', 'BWT_NL':'purple', 'volt1_NL':'kaki'},
'stim_colors' : {'all':'black', 'natmov':'dark_green', 'natmovRF3':'light_green', 'natmovRF':'light_red', 'natmovRAP':'dark_blue', 'natmovRSP':'orange', 'natmovRP':'yellow'},
'save_memory': True,
'single_datapoints': False