### 1/ TO RUN ###
'load_resps' : True,
'load_bstim' : True,
'recompute_stim' : True,
'store_bstim' : False,
'save_to_mat' : False,
'to_plot' : [],


### 2/ DATA PATHS ###
'spikes_file' : '/DATA/Margot/ownCloud/MargotUNIC/Data/Extracellular_elphy_data/2916_CXRIGHT_TUN16_spksortYP.kwik',
'basestim_file' : '/DATA/Margot/natural_movies/tenseconds_tex_800_stretched_c2ds4/naturalmovie800_001.tex',
'frameorder_file' : '/DATA/Margot/natural_movies/composite_movies/tenseconds_tex_800_stretched_Rand1_mixed_randomization_order.txt',
'movorder_file' : '/DATA/Margot/ownCloud/MargotUNIC/Data/Extracellular_elphy_data/2916_CXRIGHT_TUN16_stimlist.txt',
'vtag_file' : '/DATA/Margot/ownCloud/MargotUNIC/Data/Extracellular_elphy_data/2916_CXRIGHT_TUN16_Vtags.mat',
'save_path' : '/DATA/Margot/ownCloud/MargotUNIC/Data/Extracellular_elphy_data/Sharing_format',


### 3/ PROTOCOL INFOS ###
'protocol' : 'natural_movies',
'ep_dur' : 12,
'ep_iblank' : 1,
'ep_fblank' : 1,
'sampling_rate' : 30000,
'repeated_eps' : [],


### 4/ STIMULUS PREPROCESSING PARAMETERS ###
'bstim_numtype' : 'uint8',
'stim_numtype' : 'float32',
'crop_range' : [],
'bstim_downsplfact' : 1,
'normalize_stim' : True,
'blank_val' : 0.0,


### 5/ DATA PROCESSING/PLOTTING PARAMETERS ###
'eps_tokeep' : [],
'cells_tokeep' : 'isolated',
'ROI' : [],
'ROI_downsplfact' : 1,
'RF_tau' : 0,
'RF_Ntau' : 5,
'fourier_space' : 'false',
'spike_tol' : 6,
'ncells_per_fig' : 8