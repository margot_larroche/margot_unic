### 1/ TO RUN ###
'load_metadata' : True,
'load_resps' : True,
'load_stim' : True,
'to_plot' : [],
'split_by' : {'stim_type': ['densenoise', 'vanhateren', 'vanhateren_randphase', 'natmov', 'natmov_randframe'], 'pixel_size':[1,6]},


### 2/ DATA PATHS ###
'epinfo_file' : None,
'python_data_file' : None,
'python_stim_file' : None,
'elphy_file' : '/media/margot/backup_mgt/DATA/raw_data/MANIP_2017/MANIP_2017_28/2817_CXLEFT/2817_CXLEFT_TUN21.DAT',
'elphy_structure_file': 'elphy_fileinfo_structure_2817.txt',
'spikesorting_file' : '2817_CXLEFT_TUN21.kwik',
'stimbank_file' : '2817_CXLEFT_TUN21_stim_ds3.mat',
'stimbank_info_file': 'stimbankinfo_mixedtypes_120_sd60_withgratings.txt',
'frameorder_file' : None,


### 3/ PROTOCOL INFOS ###
'protocol' : 'movies',


### 4/ DATA PREPROCESSING PARAMETERS ###
'stim_numtype' : 'float32',
'crop_range' : [],
'stim_downsplfact' : 1,
'normalize_stim' : True,
'blank_val' : 0.0,
'discard_wrong_t0': False,
'bin_width': None,


### 5/ ANALYSIS PARAMETERS ###
'eps_tokeep' : [],
'errors_to_discard': ['wrong_init_blanks','wrong_dton','missing_samples'],
'cells_tokeep' : [ 2,  3,  5,  6,  7,  9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 24, 27, 30],
'unit_quality_tokeep': ['isolated','sorted_multiunit'],
'corr_threshold' : -1.0,
'rate_threshold' : 0.0,
'RF_valfrac' : 0.2,
'data_rand_seed' : None,
'ROI' : [[10, 34], [10, 34]],
'ROI_downsplfact' : 2,
'RF_tau' : 2,
'RF_Ntau' : 4,
'STA_bias' : 8000.0,
'fourier_space' : 'false',
'spike_tol' : 6,
'ncells_per_fig' : 31,
'onset_dur' : 0.8,
'offset_dur' : 0.5,
'crosscorr_bin' : 0.0166666666667,
'crosscorr_max' : 0.6