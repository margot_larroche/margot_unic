### 1/ TO RUN ###
'load_metadata' : True,
'load_resps' : True,
'load_stim' : False,
'to_plot' : [],
'split_by' : {},


### 2/ DATA PATHS ###
'epinfo_file' : None,
'python_data_file' : None,
'python_stim_file' : None,
'elphy_file' : 'MANIP_2018/MANIP_2018_09/0918_CXLEFT_2/0918_CXLEFT_2_TUN4.DAT',
'elphy_structure_file': 'elphy_fileinfo_structure_2018.txt',
'spikesorting_file' : None,
'stimbank_file' : None,
'stimbank_info_file': 'stimbankinfo_mixedtypes_20_sd60_withgratings.txt',
'frameorder_file' : None,
'save_path' : 'SYNCED_FILES/DATA/extracell_data/python_data',


### 3/ PROTOCOL INFOS ###
'protocol' : 'movies',


### 4/ DATA PREPROCESSING PARAMETERS ###
'stim_numtype' : 'float32',
'crop_range' : [],
'stim_downsplfact' : 1,
'normalize_stim' : True,
'discard_wrong_t0' : True,
'blank_val' : 0.0,


### 5/ ANALYSIS PARAMETERS ###
'eps_tokeep' : list(set(range(822))-set([40,68,95,99,101,113,118,123,138,149,178,182,195,218,223,229,230,284,342,377,421,439,475,477,508,517,721,745,792,799])),
'errors_to_discard': ['wrong_init_blanks','wrong_dton','missing_samples'],
'cells_tokeep' : [],
'unit_quality_tokeep': ['isolated','sorted_multiunit','raw_multiunit'],
'corr_threshold' : -1.0,
'rate_threshold' : 0.0,
'RF_valfrac' : 0.2,
'data_rand_seed' : None,
'ROI' : [],
'ROI_downsplfact' : 1,
'RF_tau' : 0,
'RF_Ntau' : 9,
'STA_bias' : 10000,
'fourier_space' : 'false',
'spike_tol' : 6,
'ncells_per_fig' : 16,
'onset_dur' : 0.8,
'offset_dur' : 0.5,
'crosscorr_bin' : 0.0166666666667,
'crosscorr_max' : 0.6,
'stim_colors': {}
