### 1/ DATA PATHS ###
'data_files' : {'DN': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/2817_CXLEFT_TUN21_DN.mat',
                'natmovRF': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/2817_CXLEFT_TUN21_natmovRF.mat',
                'vanhaterenRP': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/2817_CXLEFT_TUN21_vanhaterenRP.mat',
                'vanhateren6': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/2817_CXLEFT_TUN21_vanhateren6.mat',
                'vanhateren': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/2817_CXLEFT_TUN21_vanhateren.mat',
                'natmov': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/2817_CXLEFT_TUN21_natmov.mat',
                'all': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/2817_CXLEFT_TUN21_allstim.mat'},
                
'HSM_files' : {'DN': '/media/margot/DATA/Margot/ownCloud/ANALYSIS/HSM_analysis/HSM_exploration_files/2817_CXLEFT_TUN21/2817_CXLEFT_TUN21_allstim_only20valid_sizeexpl/2817_CXLEFT_TUN21_DN_20validcells_refitfromallstim_BEST_metaparams', 
               'natmovRF': '/media/margot/DATA/Margot/ownCloud/ANALYSIS/HSM_analysis/HSM_exploration_files/2817_CXLEFT_TUN21/2817_CXLEFT_TUN21_allstim_only20valid_sizeexpl/2817_CXLEFT_TUN21_allstim_20validcells_sizeexpl_97_BEST_metaparams',
               'vanhaterenRP': '/media/margot/DATA/Margot/ownCloud/ANALYSIS/HSM_analysis/HSM_exploration_files/2817_CXLEFT_TUN21/2817_CXLEFT_TUN21_allstim_only20valid_sizeexpl/2817_CXLEFT_TUN21_allstim_20validcells_sizeexpl_97_BEST_metaparams',
               'vanhateren6': '/media/margot/DATA/Margot/ownCloud/ANALYSIS/HSM_analysis/HSM_exploration_files/2817_CXLEFT_TUN21/2817_CXLEFT_TUN21_allstim_only20valid_sizeexpl/2817_CXLEFT_TUN21_vanhateren6_20validcells_refitfromallstim_BEST_metaparams',
               'vanhateren': '/media/margot/DATA/Margot/ownCloud/ANALYSIS/HSM_analysis/HSM_exploration_files/2817_CXLEFT_TUN21/2817_CXLEFT_TUN21_allstim_only20valid_sizeexpl/2817_CXLEFT_TUN21_allstim_20validcells_sizeexpl_97_BEST_metaparams',
               'natmov': '/media/margot/DATA/Margot/ownCloud/ANALYSIS/HSM_analysis/HSM_exploration_files/2817_CXLEFT_TUN21/2817_CXLEFT_TUN21_allstim_only20valid_sizeexpl/2817_CXLEFT_TUN21_natmov_20validcells_refitfromallstim_BEST_metaparams',
               'all': '/media/margot/DATA/Margot/ownCloud/ANALYSIS/HSM_analysis/HSM_exploration_files/2817_CXLEFT_TUN21/2817_CXLEFT_TUN21_allstim_only20valid_sizeexpl/2817_CXLEFT_TUN21_allstim_20validcells_sizeexpl_97_BEST_metaparams'},

'rf_path' : '/media/margot/DATA/Margot/ownCloud/ANALYSIS/RF_analysis/2817_CXLEFT_TUN21/20validcells',
'common_name' : '20validcells',
'label' : '',


### 2/ STEPS TO EXECUTE ###
'load_data' : True,
'model_types' : [],
'add_nonlinearity' : True,
'to_compute' : [],
'load_existing_RFs' : False,
'to_plot' : [],
'to_save' : [],


### 3/ RF COMPUTING PARAMETERS ###
'cells_to_keep' : [2, 3, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 24, 27, 30],
'normalize_stim' : False,
'normalize_resps' : False,
'ROI' : [],
'ROI_downsplfact' : 1,
'include_constant' : True,
'reg_frac' : 0.0,
'val_frac' : 0.0,
'rand_seed' : -1,
'RF_tau' : 0,
'RF_Ntau' : 7,
'STA_biases' : range(0,100001,1000),
'volt2eig_nsig' : 5,
'check_HSM_compatibility' : True,


### 4/ PLOTTING PARAMETERS ###
'ncells_per_fig' : 20,
'im_type' : '.svg',
'display_plots' : True,
'invert_corrplot' : False,
'smooth_plots' : False,
'rf_colors' : {'HSM': (0,0.8,0.8), 'STALR_NL': (1,0,1), 'volt2diag': (0.5,0,0.5), 'volt2sig': (0.8,0,0.4)},
'stim_colors' : {'DN': (0,0,1), 'natmov': (0,0.6,0), 'natmovRF': (0,1,0), 'vanhateren': (1,0,0), 'vanhaterenRP':(0.8,0.8,0), 'vanhateren6':(1,0.5,0), 'all':(0,0,0)}