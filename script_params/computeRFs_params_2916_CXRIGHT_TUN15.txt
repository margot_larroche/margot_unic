### DATA PATHS ###
'data_file' : '/DATA/Margot/ownCloud/MargotUNIC/Data/Extracellular_elphy_data/2916_CXRIGHT_TUN15_croppedstim.mat',
'HSM_file' : '/DATA/Margot/ownCloud/MargotUNIC/Data/LSCSM_data/FittingResults/2916_CXRIGHT_TUN15_croppedstim_standardparams_metaparams',
'rf_path' : '/DATA/Margot/ownCloud/MargotUNIC/Data/ReceptiveFields/2916_CXRIGHT_TUN15',


### PLOTTING PARAMETERS ###
'ncells_per_fig' : 50,
'im_type' : '.svg',
'display_plots' : True,


### RF COMPUTING PARAMETERS ###
'normalize_stim' : True,
'normalize_resps' : True,
'ROI' : [],
'ROI_downsplfact' : 1,
'include_constant' : True,
'reg_frac' : 0.,
'val_frac' : 0.2,
'rand_seed' : 0,
'RF_tau' : 3,
'RF_Ntau' : 1,
'STA_biases' : [10000],
'volt2eig_nsig' : 5,


### STEPS TO EXECUTE ###
'load_data' : True,
'model_types' : ['volt1', 'volt2diag', 'volt2', 'volt2sig', 'STALR', 'HSM'],
'to_compute' : ['RFs', 'corr'],
'load_existing_RFs' : True,
'to_plot' : ['RFs', 'corr_barplot_cells'],
'to_save' : ['RFs', 'plots']
