### 1/ TO RUN ###
'load_metadata' : True,
'load_resps' : True,
'load_stim' : True,
'to_plot' : [],
'split_by' : {'stim_type': ['natmov','natmov_randphase','natmov_randframe','natmov_randspatpix','natmov_randallpix','natmov_randframe3']},


### 2/ DATA PATHS ###
'epinfo_file' : None,
'python_data_file' : None,
'python_stim_file' : None,
'elphy_file' : '/media/jan/LabStorageML/MARGOT_PHD/NON_SYNCED_FILES/DATA/raw_elphy_files/main_mixedstims_files/1718_CXRIGHT_TUN2.DAT',
'elphy_structure_file': 'elphy_fileinfo_structure_2018.txt',
'spikesorting_file' : '1718_CXRIGHT_TUN2_spksort180523.kwik',
'stimbank_file' : 'mixedstims_tex_20pix_std60_090218/mixedtypes_fr20_0001.tex',
'stimbank_info_file': 'stimbankinfo_mixedtypes_20_sd60_withgratings.txt',
'frameorder_file' : None,


### 3/ PROTOCOL INFOS ###
'protocol' : 'movies',


### 4/ DATA PREPROCESSING PARAMETERS ###
'stim_numtype' : 'float32',
'crop_range' : [],
'stim_downsplfact' : 1,
'normalize_stim' : True,
'discard_wrong_t0' : True,
'blank_val' : 0.0,


### 5/ ANALYSIS PARAMETERS ###
'eps_tokeep' : [],
'errors_to_discard': ['wrong_init_blanks','wrong_dton','missing_samples'],
'cells_tokeep' : [],
'unit_quality_tokeep': ['isolated','sorted_multiunit'],
'corr_threshold' : -1.0,
'rate_threshold' : 0.0,
'RF_valfrac' : 0.2,
'data_rand_seed' : None,
'ROI' : [[4,16],[3,15]],
'ROI_downsplfact' : 1,
'RF_tau' : 0,
'RF_Ntau' : 9,
'STA_bias' : 10000,
'fourier_space' : 'false',
'spike_tol' : 6,
'ncells_per_fig' : 16,
'onset_dur' : 0.8,
'offset_dur' : 0.5,
'crosscorr_bin' : 0.0166666666667,
'crosscorr_max' : 0.4,
'stim_colors' : {'all': 'black', ('natmov',): 'dark_green', ('natmov_randframe3',): 'light_green', ('natmov_randframe',): 'light_red', ('natmov_randallpix',): 'dark_blue', ('natmov_randspatpix',):'orange', ('natmov_randphase',):'yellow'}
