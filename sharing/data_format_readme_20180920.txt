the *.mat file contains stimuli and responses organized in episodes :

- stimulus : list of n_episodes items.
stimulus[i] : n_bins x n_pixels x n_pixels array corresponding to episode i

- responses : list of n_episodes items.
responses[i] : n_bins x n_units array corresponding to episode i

- bin_width : width of the time bin for stimulus and responses, in seconds.

- init_blank_nbins : duration of blank at the beginning of each episod (number of bins)

- final_blank_nbins : duration of blank at the end of each episod (number of bins)


the *_epinfo.txt file contains metadata about episodes. when loaded with load_info_table function, gives a dictionnary of 1d arrays
- epinfo['stim_type'][i]: which stimulus type was shown at episode i. for RF analysis, discard episodes of grating_pair type
- epinfo['stim_id'][i]: ID of the stimulus shown at episode i. Allows to match episodes between data files
- epinfo['data_splitting'] : a proposition of repartition of episodes between training set ('train'), validation set ('val') and regularization set ('reg'). Note that the validation set is composed of repeated sequences BUT repetitions havent been averaged together yet : each episode is a single trial


To merge data between files :
- check that bin_width is the same between files (there might be a negligible difference)
- check whether init_blank_nbins and final_blank_nbins are the same between files, otherwise truncate episodes appropriately
- use epinfo['stim_id'] to match episodes with identical stimuli
- stimulus arrays might be slightly different between files even if stimulus ID is the same, due to correction of small errors in the display. The best strategy is probably to take the average of all versions of the same stimulus
