Each .mat file correspond to a given recording session (~ 30 min to 4h duration) in a given cortical location. Files who differ only by their final number have been recorded at the same cortical location, so potentially with the same cells (but stability of the population and of the animal's physiological state is not guaranteed, and spike sorting has been done independently).

In each file, the data is organized in 12s episodes (10s stimulus + 2x1s blanks). During each episode, a given 10s sequence of a given stimulus type (natural movie, dense noise, etc) has been played. Stimulus types were randomly interleaved.

The stimulus types are the following :
- dense_noise_6pix : Ternary dense noise composed of 6 pixel - wide squares. Probabilities to be respectively black, white or gray for each square on each frame are equal (1/3)
- dense_noise_halfdensity_6pix : Same as previous but the probabilities to be black, white or gray are respectively 1/6, 1/6 and 2/3
- dense_noise_densercenter_6pix : Same as previous but the 4 most central squares have probabilities 1/3,1/3,1/3 while the others have probabilities 1/6,1/6,2/3
- natural_movies : Movies of natural scenes (temporally correlated)
- natural_movies_randtime : Same movies as previous except that the order of the frames has been randomized
- natural_images : Flashed natural images (Van Hateren database)
- natural_images_6pix : Pixellated version (6 time bigger pixels for the same image size) of previous images
- natural_images_randphase : Same as natural_images but with Fourier phase randomized


Description of a .mat file content :

#### data ####
- responses (n_episodes x n_time_bins x n_units) : number of spikes per unit per time bin
- stimulus (n_episodes x n_time_bins x n_pixels x n_pixels) : normalized pixel intensities
-> to use for RF computation, reshape : responses[episodes_you_want_to_keep].reshape(-1,n_units), stimulus[episodes_you_want_to_keep].reshape(-1,n_pixels**2)

#### data labels ####
- unit_types_LABELS : List of possible unit types (isolated : spike sorted and well isolated, sorted_multiunit : spike sorted and poorly isolated, raw_multiunit : raw spike times on a given electrode channel, no spike sorting)
- unit_types : Unit type for each unit (coded by numbers 0, 1, ... , the meaning being given by unit_types_LABELS)
- stim_types_LABELS : List of possible stimulus types (dense_noise_6pix = ternary dense noise composed of 6x6 pixels squares, natural_movies, etc)
- stim_types : Stim type for each episode (coded by numbers 0, 1, ... , the meaning being given by stim_types_LABELS)
- stim_ids : movie identifier for each episode (the identifiers are the same across data files of the same protocol_id, allowing for data merging)
- num_repetitions : for each episode, number of repeated trials that have been averaged to yield the provided data
- data_subsets_LABELS : List of possible subsets in which to split the data (training, regularization, validation)
- data_subsets : A proposed split of the data into the different subsets : ie for each episode, a code (0, 1, ...) indicating to which data subset it should belong to. The proposed validation subset corresponds to movies which have been repeated several times and the respective responses averaged to yield the provided data. Training and regularization subsets correspond to single-trial data. The split between training and regularization itself is totally random.

#### experimental/technical details ####
- stimulation_protocol_id : a string identifying the stimulation protocol (displayed stimulus set + contrast) for this data file. Only data files corresponding to the same protocol can be merged for model fitting
- frame_duration : Duration of a stimulus frame = of a time bin, in seconds
- init_blank_nbins : Duration (number of time bins) of the blank that precedes each ten second movie in the provided stimulus (1s = 60 time bins in theory, but can be less due to post-hoc realignment of movie onsets)
- final_blank_nbins : Duration (number of time bins) of the blank that follows each ten second movie in the provided stimulus (1s = 60 time bins in theory, but can be less due to post-hoc realignment of movie onsets)
- original_stim_shape : shape (n_pixel x pixels) of the displayed stimulus (the provided stimulus being a cropped and spatially downsampled version of it)
- original_stim_pixel_size : size of the displayed stimulus pixels, in degrees of visual angle
- stim_ROI : spatial region of the orignal stimulus (in pixel coordinates) that has been cropped to yield the provided stimulus
- stim_downsampling_factor : by how much the provided stimulus has been spatially downsampled compared to the original one

#### preliminary analysis ####
- theo_max_correlation (n_stimulus_types+1 x n_units): for each stimulus type (last one = all stimulus types merged) and each unit, theoretical maximal correlation coefficient between validation data and model prediction that could be reached by a perfect model, given noise in mean response estimation (trial noise, limited number of repeated trials)
- theo_max_correlation_corrected (n_stimulus_types+1 x n_units): same as previous but after removing mean, onset and offset responses for the computation


