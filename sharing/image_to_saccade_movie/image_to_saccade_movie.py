import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
from scipy.interpolate import interp2d
import matplotlib.animation as animation


image_file='image_naturelle_HIGHC.bmp'
saccade_file='saccade_positions_elphycoords.txt' # successive saccade positions, as given by elphy, in a text file (1 line per position)
crop_size=[500,500] # size of the cropping window to apply to the image at each saccade position, in pixels
im_height_in_elphy=58.477  # in degrees
blank_value=128 # values with which to pad frames if cropping window exceeds image size



def saccade_elphy2python(saccade_array,degree_to_pixel):
    """
    Converts elphy coordinates to python coordinates
    """    
    # (Field of view displacement on image) = -1 x (image displacement on screen)
    #new_sacc=-saccade_array 
    new_sacc=saccade_array # In fact no, the right sign for NBR is this one... Who knows why ...
    # Screen: x goes from left to right, y goes from down to up
    # Image: x goes from up to down, y goes from left to right
    new_sacc=np.array([-new_sacc[:,1],new_sacc[:,0]]).T  
    # From degrees to number of image pixels
    new_sacc=degree_to_pixel*new_sacc
    return new_sacc


def add_saccades_to_movie(movie_array,saccade_array,final_shape,nsaccades_per_frame=np.inf,degree_to_pixel=24,blank_val=0):
    """
    movie_array : initial_nframes x initial_npix1 x initial_npix2, the original movie
    saccade_array : n_saccades * 2 (position1,position2), successive saccade positions as given by elphy (they will be converted into python array coordinates, but one has to provide the magnification factor degree_to_pixel)
    final_shape : 2 (final_npix1,final_npix2), frame shape of the returned movie
    degree_to_pixel : magnification factor to convert elphy coordinates to python array coordinations
    blank_val : value with which to pad generated frames if they exceed image borders
    n_saccade_per_frame : how many saccades you do on each frame before the next frame is displayed. If inf: all saccades are done on the first frame (following frames are discarded)
    """
    if len(movie_array.shape)==2:
        movie_array=movie_array.reshape((1,)+movie_array.shape)
    n_saccades=saccade_array.shape[0]
    final_shape=np.array(final_shape)
    saccade_array=saccade_elphy2python(saccade_array,degree_to_pixel=degree_to_pixel)+0.5*np.array(movie_array.shape[1:])
    crop_ranges=np.rollaxis(np.array([saccade_array-final_shape*0.5,saccade_array+final_shape*0.5]),0,3)
    extensions=np.array([np.ceil(np.array([-np.minimum(crop_ranges[i_sacc,:,0],0),np.maximum(crop_ranges[i_sacc,:,1]-np.array(movie_array.shape[1:]),0)]).T).astype(int) for i_sacc in range(n_saccades)]).max(axis=0)
    if np.any(extensions>0):
        print "Warning : crop range out of bounds for some frames, padding with blank value ("+str(blank_val)+")"
    saccade_movie=np.zeros([n_saccades]+list(final_shape))
    
    frame_num=0
    for i_sacc in range(n_saccades): 
        if i_sacc%nsaccades_per_frame==0:            
            base_frame=np.zeros(np.array(movie_array.shape[1:])+extensions.sum(axis=1))+blank_val  
            base_frame[extensions[0,0]:movie_array.shape[1]+extensions[0,0],extensions[1,0]:movie_array.shape[2]+extensions[1,0]]=movie_array[frame_num]
            interp_func=interp2d(np.arange(base_frame.shape[1])-extensions[1,0],np.arange(base_frame.shape[0])-extensions[0,0],base_frame)  
            frame_num+=1
        saccade_movie[i_sacc]=interp_func(np.arange(final_shape[1])+crop_ranges[i_sacc,1,0],np.arange(final_shape[0])+crop_ranges[i_sacc,0,0])
       
    return saccade_movie               



def playMovieFromMat(mat,frame_dur=1./60,indicators=None,vmin=None,vmax=None):
    """
    Play a movie, provided as a nframes x npix1 x npix2 array   
    """ 
    fig = plt.figure()
    ax = fig.add_subplot(111)
    if vmin is None:
        vmin=0
    else:
        vmin=(vmin-mat.min())/(mat.max()-mat.min())
    if vmax is None:
        vmax=1
    else:
        vmax=(vmax-mat.min())/(mat.max()-mat.min())    
    mat=(mat[:]-mat.min())/(mat.max()-mat.min())


    if indicators is not None:
        margin=int(np.round(mat.shape[1]*0.2))
        indicators=(indicators[:]-indicators.min())/(indicators.max()-indicators.min())
        indicators=indicators.repeat(int(mat.shape[2]/indicators.shape[1])).reshape(indicators.shape[0],-1)
        frame=np.zeros((mat.shape[1]+margin,mat.shape[2]))+0.5
        frame[-margin:,:indicators.shape[1]]=indicators[0,:]
        frame[:-margin,:]=mat[0,:,:]
    else:
        frame=mat[0,:,:]
                
    im = ax.imshow(frame,cmap='gray',interpolation='none',vmin=vmin,vmax=vmax)

    def update_frame(iframe):
        if indicators is not None:
            frame[-margin:,:indicators.shape[1]]=indicators[iframe,:]
        frame[:mat.shape[1],:]=mat[iframe,:,:]
        im.set_data(frame)
        return im

    ani = animation.FuncAnimation(fig,update_frame,mat.shape[0],interval=frame_dur*1000,repeat_delay=2)
    return fig, ani


if __name__=='__main__':
    image_array=np.asarray(Image.open(image_file).convert('L'))
    
    with open(saccade_file,'r') as f:
        lines=f.readlines()
    saccade_array=np.array([l.split(',') for l in lines]).astype(float) 
    
    saccade_movie=add_saccades_to_movie(image_array,saccade_array,crop_size,nsaccades_per_frame=np.inf,degree_to_pixel=image_array.shape[0]*1./im_height_in_elphy,blank_val=blank_value)
    playMovieFromMat(saccade_movie) # /!\ You may have to rerun this separately for it to work
    
