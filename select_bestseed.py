import glob
import os
from lscsm.checkpointing import loadErrCorr
import numpy as np
from shutil import copyfile

in_dir='/home/margot/Cluster/HSM/Explore_params/1718_CXRIGHT_TUN2_pix9x9_ceiling/running_slurm'
out_dir='/home/margot/Bureau/temp/bestfiles'


hsm_files=sorted(glob.glob(os.path.join(in_dir,'*_BEST_metaparams')))
remaining_inds=range(len(hsm_files))
matched_inds=[]
best_inds=[]

# Make groups differing only by seed value
while len(remaining_inds)>0:
    ind1=remaining_inds[0]
    matched_inds.append([ind1])
    for ind2 in remaining_inds:
        if np.all([(pattern1==pattern2) or (('seed' in pattern1) and ('seed' in pattern2)) for pattern1,pattern2 in zip(hsm_files[ind1].split('_'),hsm_files[ind2].split('_'))]):
            matched_inds[-1].append(ind2)
    remaining_inds=list(set(remaining_inds)-set(matched_inds[-1]))       
    
# Find best seed in each group    
for igroup in range(len(matched_inds)):
    corr_vals=[loadErrCorr(hsm_files[ind][:-10]+'corr')[1][-1] for ind in matched_inds[igroup]]  
    best_inds.append(matched_inds[igroup][np.argmax(corr_vals)])
    
    
# Copy files to new directory for each best seed
for ind in best_inds:    
    best_files=[hsm_files[ind][:-10]+suffix for suffix in ['error','corr','metaparams','K']]#+[hsm_files[ind][:-15]+suffix for suffix in ['error','corr','metaparams','K']]
    for fname in best_files:        
        copyfile(fname,os.path.join(out_dir,os.path.basename(fname)))
    
    