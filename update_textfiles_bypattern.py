import glob
import os

in_dir='/media/margot/DATA/Margot/HSM_exploreparams/2817_CXLEFT_TUN21/simulated_data/new_poissonbeforehist/v2_newdata_with500valrep'
out_dir='/media/margot/DATA/Margot/HSM_exploreparams/2817_CXLEFT_TUN21/simulated_data/new_poissonbeforehist/v2_newdata_with500valrep/epsize1000'
patterns=['initweightsandthreshs0','metaparams']

replace_in_name={'_epsize100_':'_epsize1000_'}
replace_in_text={"'epochSize': 100":"'epochSize': 1000",'epsize100':'epsize1000'}

files_to_convert=glob.glob(os.path.join(in_dir,'*'+'*'.join(patterns)+'*'))

for fname in files_to_convert:
    with open(fname,'r') as f:
        text=f.read()
        
    new_fname=os.path.basename(fname)   
    for key in replace_in_name.keys():
        new_fname=new_fname.replace(key,replace_in_name[key])
    new_text=text    
    for key in replace_in_text.keys():
        new_text=new_text.replace(key,replace_in_text[key])
        
    with open(os.path.join(out_dir,new_fname),'w') as f:
        f.write(new_text)
        
    print os.path.basename(fname), '>', new_fname    