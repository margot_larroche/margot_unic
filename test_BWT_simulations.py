
# coding: utf-8

# In[2]:

# Imports
from utils.simulations import *
import matplotlib.pyplot as plt
from data_analysis.extracell_funcs_NEW import loadTexStim
import glob
import numpy as np
from miscellaneous.convertVideo import playMovieFromMat
import matlab.engine
from utils.volterra_kernels import lsq_kernel, movie_to_BWT
from utils.theano_rf_models import fit_volterra_theano


# In[3]:

# Parameters
stim_size=9
stim_dir='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/mixedstims_tex_20pix_std60_090218'
n_stim=30
power_law=1
stim_fracs={'train':0.7,'reg':0.1,'val':0.2}
colors='rgbycm'
line_width=2
numEpochs=20
epochSize=100
noise_ampl=1
computed_rfs=['volt1','BWT']
compute_modes=['linsolve','theano']


# In[4]:

# Generate subunits
subunits=[onoffSubunit(stim_size,updw_margin=1,side_margin=2,phase=0,transpose=False),
          onoffSubunit(stim_size,updw_margin=1,side_margin=2,phase=1,transpose=False),
          onoffSubunit(stim_size,updw_margin=1,side_margin=3,phase=0,transpose=False),
          onoffSubunit(stim_size,updw_margin=1,side_margin=2,phase=0,transpose=True)]

subunits=np.concatenate(subunits,axis=1)
subunits=(subunits-subunits.mean(axis=0))*1./subunits.std(axis=0)
plt.figure(figsize=(2*subunits.shape[1],2))
for isub in range(subunits.shape[1]):
    plt.subplot(1,subunits.shape[1],isub+1)
    plt.imshow(subunits[:,isub].reshape(stim_size,stim_size))


# In[5]:

# Define models
model_funcs={'linear1': lambda stim_mat: np.dot(stim_mat,subunits[:,0:1]),
             'LN1': lambda stim_mat: LNLmodel_resp(stim_mat,subunits[:,0:1],subunits_exp=power_law,output_exp=power_law),
             'LN2': lambda stim_mat: LNLmodel_resp(stim_mat,subunits[:,1:2],subunits_exp=power_law,output_exp=power_law),
             #'LN3': lambda stim_mat: LNLmodel_resp(stim_mat,subunits[:,2:3],subunits_exp=power_law,output_exp=power_law),
             #'LN4': lambda stim_mat: LNLmodel_resp(stim_mat,subunits[:,3:4],subunits_exp=power_law,output_exp=power_law),
             'LNLN1': lambda stim_mat: LNLmodel_resp(stim_mat,subunits,subunits_exp=power_law,output_exp=power_law), 
             'LNLN2': lambda stim_mat: LNLmodel_resp(stim_mat,subunits,subunits_exp=power_law,output_exp=power_law,subunits_weights=[1,-1,1,-1]),
             'LNLN3': lambda stim_mat: LNLmodel_resp(stim_mat,subunits,subunits_exp=power_law,output_exp=power_law,subunits_weights=[1,1,-1,-1]),
             'LNLN4': lambda stim_mat: LNLmodel_resp(stim_mat,subunits,subunits_exp=power_law,output_exp=power_law,subunits_weights=[-0.5,-1.2,1.7,0.2])}
model_names=sorted(model_funcs.keys())


# In[6]:

# Load stimulus
stim_list=sorted(glob.glob(stim_dir+'/*.tex'))[:n_stim]
stim=loadTexStim(stim_list,crop_range=np.array([[0,stim_size],[0,stim_size]]))
stim=np.concatenate(stim,axis=0)
stim=(stim-stim.mean())*1./stim.std()
print stim.shape
playMovieFromMat(stim)
stim=stim.reshape(stim.shape[0],-1)


# In[7]:

# Compute stim BWT
#if not(vars().has_key('matlab_engine')):
#    matlab_engine=matlab.engine.start_matlab()
#stim_BWT=movie_to_BWT(stim.reshape(-1,stim_size,stim_size), matlab_engine=matlab_engine)
stim_BWT=movie_to_BWT(stim)
playMovieFromMat(stim_BWT.reshape(-1,stim_size,stim_size))


# In[8]:

# Set stim subparts (training, regularization, validation)
stim_frac_names=stim_fracs.keys()
data_indices={key: np.arange(int(np.floor(stim.shape[0]*stim_fracs[key]))) for key in stim_frac_names}
for ifrac in range(1,len(stim_frac_names)):
    data_indices[stim_frac_names[ifrac]]+=data_indices[stim_frac_names[ifrac-1]][-1]+1


# In[9]:

# Compute responses
resps={model: model_funcs[model](stim) for model in model_names}
for model in model_names: # add_noise
    resps[model+'_noisy']=resps[model].copy()
    resps[model+'_noisy'][data_indices['train']]+=np.random.normal(size=resps[model][data_indices['train']].shape,scale=resps[model][data_indices['train']].std()*noise_ampl)
model_names=sorted(resps.keys())
plt.figure(figsize=(10,3));
for model in model_names:
    plt.plot(resps[model])
plt.legend(model_names)        


# In[10]:

import utils.theano_rf_models
reload(utils.theano_rf_models)
from utils.theano_rf_models import fit_volterra_theano

# Compute rfs
rfs={model:{} for model in model_names}

for model in model_names:
    rfs[model]={rf: {cm: {} for cm in compute_modes} for rf in computed_rfs}
    for rf in computed_rfs:
        for cm in compute_modes:
            if (rf=='volt1') and (cm=='linsolve'):
                rfs[model][rf][cm][0],rfs[model][rf][cm][1]=lsq_kernel(stim[data_indices['train']],resps[model][data_indices['train']],order0=True,order2=False)
            elif (rf=='volt1') and (cm=='theano'):
                rfs[model][rf][cm][0],rfs[model][rf][cm][1],rfs[model][rf][cm]['terr'],rfs[model][rf][cm]['verr'],rfs[model][rf][cm]['tcorr'],rfs[model][rf][cm]['vcorr']=fit_volterra_theano(stim[data_indices['train']],resps[model][data_indices['train']],stim[data_indices['reg']],resps[model][data_indices['reg']],orders=['0','1'],numEpochs=numEpochs,epochSize=epochSize)
            elif (rf=='BWT') and (cm=='linsolve'):  
                rfs[model][rf][cm][0],rfs[model][rf][cm][1],rfs[model][rf][cm][-1]=lsq_kernel(stim_BWT[data_indices['train']],resps[model][data_indices['train']],order0=True,order1='onoff',order2=False)
            elif (rf=='BWT') and (cm=='theano'):      
                rfs[model][rf][cm][0],rfs[model][rf][cm][1],rfs[model][rf][cm][-1],rfs[model][rf][cm]['terr'],rfs[model][rf][cm]['verr'],rfs[model][rf][cm]['tcorr'],rfs[model][rf][cm]['vcorr']=fit_volterra_theano(stim_BWT[data_indices['train']],resps[model][data_indices['train']],stim_BWT[data_indices['reg']],resps[model][data_indices['reg']],orders=['0','onoff'],numEpochs=numEpochs,epochSize=epochSize)


# In[ ]:

# Display rfs
for rf in computed_rfs:
    for cm in compute_modes:
        plt.figure(figsize=(2*len(model_names),2))
        plt.suptitle(rf+' '+cm)
        for imod in range(len(model_names)):
            plt.subplot(1,len(model_names),imod+1)
            plt.imshow(rfs[model_names[imod]][rf][cm][1].reshape(stim_size,stim_size))
            plt.title(model_names[imod])


# In[ ]:

# Compute predictions and correlations
rf_preds={model:{} for model in model_names}
rf_corr={model:{} for model in model_names}
for model in model_names:
    rf_preds[model]={rf: {} for rf in computed_rfs}
    rf_corr[model]={rf: {cm: {} for cm in compute_modes} for rf in computed_rfs}
    for rf in computed_rfs:
        for cm in compute_modes:
            if rf=='volt1':
                rf_preds[model][rf][cm]=rfs[model][rf][cm][0]+np.dot(stim,rfs[model][rf][cm][1])
            elif rf=='BWT':    
                rf_preds[model][rf][cm]=rfs[model][rf][cm][0]+np.dot(np.maximum(stim_BWT,0),rfs[model][rf][cm][1])+np.dot(np.maximum(-stim_BWT,0),rfs[model][rf][cm][-1])
            for frac in  stim_fracs:  
                rf_corr[model][rf][cm][frac]=np.corrcoef(rf_preds[model][rf][cm][data_indices[frac]].flatten(),resps[model][data_indices[frac]].flatten())[0,1]


# In[ ]:

# Plot predictions
for model in model_names:
    plt.figure(figsize=(10,3))
    plt.plot(resps[model],'k',linewidth=line_width*2)
    count=0
    leg_handles={}
    for rf in computed_rfs:
        for cm in compute_modes:
            plt.plot(data_indices['train'],rf_preds[model][rf][cm][data_indices['train']],colors[count]+'--',linewidth=line_width)
            plt.plot(data_indices['reg'],rf_preds[model][rf][cm][data_indices['reg']],colors[count]+':',linewidth=line_width)
            leg_handles[rf+' '+cm]=plt.plot(data_indices['val'],rf_preds[model][rf][cm][data_indices['val']],colors[count],linewidth=line_width)[0]
            count+=1
    plt.legend(leg_handles.values(),leg_handles.keys()) 
    plt.title(model)


# In[ ]:

# Display correlation across fit
for imod in range(len(model_names)):
    plt.figure(figsize=(3,3))
    count=0
    leg_items=[]
    for rf in computed_rfs: 
        plt.axhline(rf_corr[model_names[imod]][rf]['linsolve']['reg'],color=colors[count],linestyle=':')
        plt.plot(rfs[model_names[imod]][rf]['theano']['tcorr'],colors[count]+'--')
        leg_items.append(plt.plot(rfs[model_names[imod]][rf]['theano']['vcorr'],colors[count])[0])
        count+=1
    plt.title(model_names[imod])
    plt.legend(leg_items,computed_rfs)


# In[ ]:



