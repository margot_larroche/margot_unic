import numpy as np
import matplotlib.pyplot as plt
from miscellaneous.convertVideo import loadTexMovie

mov_name='/media/margot/LabStorageML/STIMS/mixedstims_tex_20pix_st60_upsampled6/mixedtypes_fr120_'
frame_nums=[146,148,154,156,158]#range(146,159,1)
#frame_nums=range(0,60,3)
#frame_nums=range(0,600,30)
mov_shift=300
#n_types=1
n_types=5

for mov_num in [47]: #range(30,50): #43,47
    plt.figure(figsize=np.array([len(frame_nums),n_types])*2)
    plt.suptitle(mov_num)
    for mov_type in range(n_types):
        mov=loadTexMovie(mov_name+str(mov_num+mov_type*mov_shift).zfill(4)+'.tex')
        for i in range(len(frame_nums)):
            plt.subplot(n_types,len(frame_nums),mov_type*len(frame_nums)+i+1)
            plt.imshow(mov[frame_nums[i]],cmap='gray',interpolation='none')
            plt.axis('off')
            if mov_type==0:
                plt.title(frame_nums[i])