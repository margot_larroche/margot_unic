import os
import glob

joblist_file='/home/margot/current_jobs.txt'
main_dir='/home/margot/HSM/Explore_params'
n_digits=6

os.system('squeue -u margot -t RUNNING > '+joblist_file)
with open(joblist_file,'r') as f:
    lines=f.read().split('\n')
    
for l in lines[1:]:
    if len(l)>0:
        job_num=l[1:n_digits+1]
        matching=[y for x in os.walk(main_dir) for y in glob.glob(os.path.join(x[0],'*'+job_num+'*log.txt'))]
        if len(matching)==0:
            print job_num
            #os.system('scancel '+job_num)
    
    