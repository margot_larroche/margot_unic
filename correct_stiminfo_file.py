from utils.handlebinaryfiles import load_info_table, save_info_table
from collections import OrderedDict
import glob
from miscellaneous.convertVideo import readTexHeader
import numpy as np

dir_list = ['/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/grating_pairs_1440_std60_201801',
            '/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/grating_pairs_144_120pix_stdmax_20180225',
            '/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/bar_pairs_32_195pix_stdmax_20180225',
            '/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/grating_pairs_432_120pix_stdmax_20180302',
            '/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/grating_pairs_24_120pix_stdmax_20180416',
            '/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/bar_pairs_24_195pix_stdmax_20180416',
            '/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/grating_pairs_144_120pix_freq06_stdmax_20180423',
            '/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/gratings_108_120pix_stdmax_20180424']
#offset=0
#stim_type='grating_pairs'
#mask_size=2./3
stim_npix=[120,120,195,120,120,195,120,120]

for idir in range(len(dir_list)):
    
    info_name=glob.glob(dir_list[idir]+'/*.txt')[0]
    print info_name
    tex_name=glob.glob(dir_list[idir]+'/*.tex')[0]
    info=load_info_table(info_name)
    n_stim=len(info.values()[0])
    tex_info=readTexHeader(tex_name)
    new_info=OrderedDict()
    #new_info['stim_id']=info['id']+offset
    #new_info['stim_type']=np.array([stim_type]*n_stim)
    #new_info['pixel_size']=np.ones(n_stim,dtype=int)
    #new_info['mask_size']=np.array([tex_info['frame_shape'][0]*mask_size]*n_stim)
    for key in ['stim_id','stim_type']:
        new_info[key]=info[key]
    new_info['stim_npix']=np.array([stim_npix[idir]]*n_stim,dtype=int)
    new_info['mask_npix']=info['mask_size']
    new_info['theo_nframes']=info['theo_nframes']
    for key in ['pixel_size','ISI','period1','orientation1','phase1','dton1','color1','period2','orientation2','phase2','dton2','color2']:
        new_info[key]=info[key]
    new_info['theo_nframes']=np.array([tex_info['num_frames']]*n_stim)    
    
    save_info_table(new_info,info_name)


