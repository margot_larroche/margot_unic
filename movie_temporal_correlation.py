from miscellaneous.convertVideo import loadTexMovie, playMovieFromMat, playMovieFromTex
import numpy as np
import matplotlib.pyplot as plt
from utils.various_tools import gaussianLowpass, cross_correlation


movie_files=['/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/mixedstims_tex_20pix_std60_090218/mixedtypes_fr20_0001.tex']
rand_nums=[1,10,600]
max_tau=100
n_repeats=10
colors='rgbycm'
linestyles=['-','--',':']


custom_mov=np.zeros([600,10,10])
trajectories=gaussianLowpass(np.round((np.cumsum((np.random.rand(custom_mov.shape[0],2)-0.5)*2,axis=0)+np.array(custom_mov.shape[1:])*0.5)),15,axis=0,width_fact=6).astype(int)%custom_mov.shape[1]
for iframe in range(custom_mov.shape[0]):
    custom_mov[iframe,trajectories[iframe,0],trajectories[iframe,1]]=1
custom_mov=gaussianLowpass(gaussianLowpass(custom_mov,8,axis=1,width_fact=6),8,axis=2,width_fact=6)  
playMovieFromMat(custom_mov)

movies=[loadTexMovie(f) for f in movie_files]+[custom_mov]
movie_shapes=[mov.shape for mov in movies]
movies=[mov.reshape(mov.shape[0],-1) for mov in movies]
#autocorrs=np.zeros([max_tau+1,len(movies),len(rand_nums)])
autocorrs=np.zeros([2*max_tau-1,len(movies),len(rand_nums),n_repeats])
#tau_vals=np.arange(-max_tau,max_tau+1)
    

for imov in range(len(movies)):
    for inum in range(len(rand_nums)):
        assert movies[imov].shape[0]%rand_nums[inum]==0
        for rep in range(n_repeats):            
            rand_order=np.random.permutation(int(movies[imov].shape[0]/rand_nums[inum]))
            rand_order=(np.repeat(rand_order.reshape(-1,1),rand_nums[inum],axis=1)+np.arange(rand_nums[inum]).reshape(1,-1)).flatten()
            autocorrs[:,imov,inum,rep]=np.nanmean(np.array([cross_correlation(movies[imov][rand_order][:,ipix],movies[imov][rand_order][:,ipix],max_tau) for ipix in range(movies[imov].shape[1])]),axis=0)
           
tau_vals=np.arange(1-max_tau,max_tau)        
plt.figure()
for imov in range(len(movies)):
    for inum in range(len(rand_nums)):
        plt.errorbar(tau_vals,autocorrs[:,imov,inum].mean(axis=1),yerr=autocorrs[:,imov,inum].std(axis=1),color=colors[inum%len(colors)],linestyle=linestyles[imov%len(linestyles)])
    plt.legend(rand_nums)    
            