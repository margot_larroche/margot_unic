from os.path import getsize
import numpy as np
from elphy_utils.elphy_reader import ElphyFile
import matplotlib.pyplot as plt
from utils.handlebinaryfiles import loadVec, save_info_table
from struct import pack



def load_klusta_binary(klusta_name,eplen_name,ep_nums='all',fmt='h',endian='<'):
    with open(eplen_name,'r') as f:
        ep_lengths=np.array([int(l) for l in f.readlines() if len(l)>0])
    n_ep=len(ep_lengths)    
    if ep_nums=='all':
        ep_nums=range(n_ep)
    item_size=len(pack(endian+'1'+fmt,1.))
    file_size=getsize(klusta_name)
    assert file_size%(ep_lengths.sum()*item_size)==0  
    n_elec=int(file_size/(ep_lengths.sum()*item_size))
    return [loadVec(klusta_name,fmt=fmt,endian=endian,start=ep_lengths[:iep].sum()*n_elec,stop=ep_lengths[:iep+1].sum()*n_elec,keep_fmt=True).reshape(ep_lengths[iep],-1) for iep in ep_nums]



def elphy_visualize(elphy_object,ep_num=0,elec_nums=range(64)):
    ep_vecs=elphy_object.episodes[ep_num].get_data()      
    fig=plt.figure();
    for ielec in range(len(elec_nums)):
        #ax=plt.subplot(len(elec_nums),1,ielec+1)
        dx=elphy_object.episodes[ep_num].channels[elec_nums[ielec]].dxu
        x0=elphy_object.episodes[ep_num].channels[elec_nums[ielec]].x0u
        signal=-(ep_vecs[elec_nums[ielec]]-ep_vecs[elec_nums[ielec]].mean())*0.2/ep_vecs[elec_nums[ielec]].std()
        plt.plot(np.arange(len(ep_vecs[elec_nums[ielec]]))*dx+x0,signal-signal[0]+ielec) 
#        plt.ylabel(elec_nums[ielec],fontsize=8)
#        ax.set_yticks([])
#        if ielec<len(elec_nums)-1:
#            ax.set_xticks([])
#        plt.axis([x0,len(ep_vecs[elec_nums[ielec]])*dx]+list(plt.axis()[2:]))
    plt.gca().invert_yaxis()    
    return fig  
    


def elphy_epinfo(elphy_name):
    ef=ElphyFile(elphy_name,readdata=False)
    info_dict={}
    info_dict['n_samples']=[ep.channels[0].nsamp for ep in ef.episodes]
    info_dict['n_missing']=[ep.channels[0].nmissing for ep in ef.episodes]
    save_info_table(info_dict,elphy_name.split('.')[0]+'_epinfo.txt')
    
    

def elphy_moviestim_list(elphy_object,elphy_struct_file):
    with open(elphy_struct_file,'r') as f:
        exec(f.read())
    elphy_object.decode_file_info(elphy_fileinfo_struct)
    corresp_list=elphy_object.file_info['TabFixvalueMov'][2].astype(int)
    raw_list=[]
    for iep in range(elphy_object.n_episodes):
        elphy_object.episodes[iep].decode_episode_info(elphy_epinfo_struct)
        raw_list.append(elphy_object.episodes[iep].episode_info['stimCode'])
    return corresp_list[np.array(raw_list)-1]



def elphy_moviestim_infotable(elphy_object,stimbank_table,elphy_struct_file): 
    stim_list=elphy_moviestim_list(elphy_object,elphy_struct_file)
    indices=np.array([list(stimbank_table['stim_id']).index(sid) for sid in stim_list])
    out_table={key: stimbank_table[key][indices] for key in stimbank_table.keys()}
    out_table['stim_num']=indices
    return out_table
    
    
    
def elphy_vtags(elphy_object,n_vtags=2,discarded_length=200):
    # Extract vtag array from elphy file and convert it into list of impulse times
    # /!\ There is often a first spurious impulse ~35 samples (~1 ms) after start, therefore we neglect the first discarded_length samples
    vtags=[[] for itag in range(n_vtags)]
    for iep in range(elphy_object.n_episodes):
        vtag_mat=elphy_object.episodes[iep].get_cyber_tags()[discarded_length:,:n_vtags]
        for itag in range(n_vtags):
            vtags[itag].append(np.where(np.diff(vtag_mat[:,itag].astype(int))==1)[0]+1+discarded_length)
    return vtags
    
    
    
def detect_repeated_samples(elphy_episode,channel_nums=range(64)):
    signal=elphy_episode.get_data()
    ch_lengths=np.array([len(ch) for ch in signal])
    assert np.all(ch_lengths[channel_nums]==ch_lengths[channel_nums[0]])
    signal=np.array([signal[ich] for ich in channel_nums]).T
    return np.where(np.all(np.diff(signal,axis=0)==0,axis=1))[0]+1    
    
    
    
def elphy_rawspikes(elphy_object):
    ep_len=np.array([ep.channels[0].nsamp for ep in elphy_object.episodes])
    spikelist={}
    for iep in range(elphy_object.n_episodes):
        ep_times,ep_units=elphy_object.episodes[iep].spikes.get_data(do_waves=False)
        for i_ch in range(len(ep_units)):
            for u_num in set(ep_units[i_ch]):
                if (i_ch,u_num) not in spikelist.keys():
                    spikelist[(i_ch,u_num)]=[]
                spikelist[(i_ch,u_num)]+=list(ep_times[i_ch][ep_units[i_ch]==u_num]*1./elphy_object.episodes[iep].dxu+ep_len[:iep].sum())
                
    return {u: np.array(spikelist[u]) for u in spikelist.keys()}    