#import os
#os.chdir(workingdir)
from lscsm.checkpointing import loadErrCorr, loadParams
import matplotlib.pyplot as plt
import numpy as np

resultspath='/DATA/Margot/ownCloud/MargotUNIC/LSCSM_data/LSCSM_performance_data/'
#plottype='SGDparams'
#plottype='epochSize'
plottype='modelseed'
prefix='testSmooth'
#prefix='testSmooth2'
#nbins=30001
#val_frac=0.2
#ntraining=np.int(np.round((1-val_frac)*nbins)) 
modelseed=0
fitseed=1
epochsize=1000
#expl_values=[500,1000,5000,10000,20000]
#expl_values=[500,1000,5000,10000,50000,100000,200000]
#expl_values=[50,100,500,1000,5000,10000,50000,100000]
#expl_values=[50,100,500,1000,5000,10000,50000,100000]
#expl_values=range(5)+range(10,20)
expl_values=range(15,20)
repnames=range(5,10)
#expl_values=[500000,1000000,2000000]
#expl_values=(np.array([0.05,0.1,0.5,1])*ntraining).astype(int)
#expl_values=(np.array([0.0001,0.0005,0.001,0.005,0.01,0.05,0.1,0.5,1])*ntraining).astype(int)
#expl_values=(np.array([0.0001,0.0005,0.001,0.005,0.01,0.05,0.1,0.5,1])*ntraining).astype(int)
#expl_values=[1,10,50,100,500,725,1000]
nbins=2000
fitseeds=[1]
#batchsizes=[1,10,100,1000]
batchsizes=[1,10,100]
learning_rates=[10**-5,10**-4,10**-3]
#learning_rates=[10**-5,10**-4,10**-3]
SGDseeds=[1,2,3]
#expl_values=[1,10,50,100,500]
#expl_values=arange(1,21)
#expl_values=[1,10,50,100]
#expl_values=(np.array([0.05,0.1])*ntraining).astype(int)
#expl_values=[2,12,24,120,240,1200,2400,12000,24001]
#expl_values=[1200,2400,12000,24001]
nrep=3
#nrep=[5,5,5,5,5,1]
#stimtypes=['SN','DN']
stimtypes=['DN']
colors=['r','g','b','c','m','y','k']*3

terr={}
verr={}
tcorr={}
vcorr={}
finalval={'SN':{'error':{},'corr':{}},'DN':{'error':{},'corr':{}}}
totparams={'epochSize': {'SN':[], 'DN':[]}, 'numEpochs': {'SN':[], 'DN':[]}}

if plottype=='datalen':
    fileext=''
    legtitle='Data size'
elif plottype=='nOut':
    fileext='nOut'
    legtitle='N output cells'    
elif plottype=='fitseed':
    legtitle='Fit seed'  
elif plottype=='epochSize':
    legtitle='Epoch size'  
elif plottype=='modelseed':
    legtitle='Model seed'    

expl_values=np.array(expl_values)
expl_values.sort()

if plottype!='SGDparams':
    for stimtype in stimtypes:
        for iexpl in range(len(expl_values)):
            for irep in range(nrep):
    #            if iexpl!=6:
    #                if plottype in ['datalen','nOut']:
    #                    #filename=resultspath+prefix+stimtype+'_'+fileext+str(expl_values[iexpl])+'_m'+str(modelseed)+'f'+str(fitseed)+'_'+'datalen100000_'+str(irep+1)
    #                    filename=resultspath+prefix+stimtype+'_'+fileext+str(expl_values[iexpl])+'_'+str(irep+1)
    #                elif plottype=='fitseed': 
    #                    filename=resultspath+prefix+stimtype+'_m'+str(modelseed)+'f'+str(expl_values[iexpl])
    #            else:
    #                filename=resultspath+prefix+stimtype+'_'+fileext+str(expl_values[iexpl])+'_'+str(1)
                if plottype in ['datalen','nOut']:
                    filename=resultspath+prefix+stimtype+'_'+fileext+str(expl_values[iexpl])+'_m'+str(modelseed)+'f'+str(fitseed)+'_'+'datalen100000_'+str(irep+1)
                    #filename=resultspath+prefix+stimtype+'_'+fileext+str(expl_values[iexpl])+'_'+str(irep+1)
                    #filename=resultspath+prefix+stimtype+'_m'+str(modelseed)+'f'+str(fitseed)+'_noise1_'+fileext+str(expl_values[iexpl])+'_'+str(irep+1)
                elif plottype=='fitseed': 
                    filename=resultspath+prefix+stimtype+'_m'+str(modelseed)+'f'+str(expl_values[iexpl])
                elif plottype=='modelseed': 
                    filename=resultspath+prefix+stimtype+'_m'+str(expl_values[iexpl])+'f'+str(repnames[irep])+'_datalen'+str(nbins)+'_epochsize'+str(epochsize)    
                elif plottype=='epochSize':
                    #filename=resultspath+prefix+stimtype+'_m'+str(modelseed)+'f'+str(fitseed)+'_epochsize'+str(expl_values[iexpl])
                    filename=resultspath+prefix+stimtype+'_m'+str(modelseed)+'f'+str(fitseed)+'_nbins'+str(nbins)+'_epochsize'+str(e)+'_thrchanged'#+'_equalized'
                metaparams,supplparams=loadParams(filename+'_metaparams')
                if (iexpl==0) & (irep==0):
                    if plottype!='epochSize':
                        terr[stimtype]=np.zeros((len(expl_values),nrep,supplparams['numEpochs']))  
                        verr[stimtype]=np.zeros((len(expl_values),nrep,supplparams['numEpochs']))
                        tcorr[stimtype]=np.zeros((len(expl_values),nrep,supplparams['numEpochs']))
                        vcorr[stimtype]=np.zeros((len(expl_values),nrep,supplparams['numEpochs']))
                    else:
                        terr[stimtype]={}
                        verr[stimtype]={}
                        tcorr[stimtype]={}
                        vcorr[stimtype]={}
                if plottype!='epochSize':    
                    terr[stimtype][iexpl,irep,:],verr[stimtype][iexpl,irep,:]=loadErrCorr(filename+'_error')
                    tcorr[stimtype][iexpl,irep,:],vcorr[stimtype][iexpl,irep,:]=loadErrCorr(filename+'_corr')
                else:
                    terr[stimtype][expl_values[iexpl]],verr[stimtype][expl_values[iexpl]]=loadErrCorr(filename+'_error')
                    tcorr[stimtype][expl_values[iexpl]],vcorr[stimtype][expl_values[iexpl]]=loadErrCorr(filename+'_corr')
            totparams['epochSize'][stimtype].append(supplparams['epochSize'])
             #totparams['numEpochs'][stimtype].append(supplparams['numEpochs'])
            totparams['numEpochs'][stimtype].append(np.int(supplparams['n_rep']/supplparams['epochSize']))                   
                    
  
else:
    ref_terr={}   
    ref_verr={} 
    ref_tcorr={} 
    ref_vcorr={}        
    for stimtype in stimtypes:
        for ifseed in range(len(fitseeds)):
            for ibsize in range(len(batchsizes)): 
                for ilrate in range(len(learning_rates)):
                    for isgdseed in range(len(SGDseeds)):
                        filename=resultspath+prefix+stimtype+'_m'+str(modelseed)+'f'+str(fitseeds[ifseed])+'_SGDbs'+str(batchsizes[ibsize])+'lr'+str(learning_rates[ilrate])+'ss'+str(SGDseeds[isgdseed])  
                        metaparams,supplparams=loadParams(filename+'_metaparams')
                        if (ifseed==0) & (ibsize==0) & (ilrate==0) & (isgdseed==0):
                            terr[stimtype]=np.zeros((len(fitseeds),len(batchsizes),len(learning_rates),len(SGDseeds),supplparams['numEpochs']))  
                            verr[stimtype]=np.zeros((len(fitseeds),len(batchsizes),len(learning_rates),len(SGDseeds),supplparams['numEpochs']))
                            tcorr[stimtype]=np.zeros((len(fitseeds),len(batchsizes),len(learning_rates),len(SGDseeds),supplparams['numEpochs']))
                            vcorr[stimtype]=np.zeros((len(fitseeds),len(batchsizes),len(learning_rates),len(SGDseeds),supplparams['numEpochs']))
                        terr[stimtype][ifseed,ibsize,ilrate,isgdseed,:],verr[stimtype][ifseed,ibsize,ilrate,isgdseed,:]=loadErrCorr(filename+'_error')
                        tcorr[stimtype][ifseed,ibsize,ilrate,isgdseed,:],vcorr[stimtype][ifseed,ibsize,ilrate,isgdseed,:]=loadErrCorr(filename+'_corr')            
                        totparams['epochSize'][stimtype].append(supplparams['epochSize'])
                        #totparams['numEpochs'][stimtype].append(supplparams['numEpochs'])
                        totparams['numEpochs'][stimtype].append(np.int(supplparams['n_rep']/supplparams['epochSize']))
    
    for stimtype in stimtypes:
        for ifseed in range(len(fitseeds)):
            filename=resultspath+prefix+stimtype+'_m'+str(modelseed)+'f'+str(fitseeds[ifseed])+'_fmintnc'
            ref_metaparams,ref_supplparams=loadParams(filename+'_metaparams')
            if ifseed==0:
                ref_terr[stimtype]=np.zeros((len(fitseeds),ref_supplparams['numEpochs']))
                ref_verr[stimtype]=np.zeros((len(fitseeds),ref_supplparams['numEpochs']))
                ref_tcorr[stimtype]=np.zeros((len(fitseeds),ref_supplparams['numEpochs'])) 
                ref_vcorr[stimtype]=np.zeros((len(fitseeds),ref_supplparams['numEpochs']))
            ref_terr[stimtype][ifseed,:],ref_verr[stimtype][ifseed,:]=loadErrCorr(filename+'_error')
            ref_tcorr[stimtype][ifseed,:],ref_vcorr[stimtype][ifseed,:]=loadErrCorr(filename+'_corr')  
                        
            

if plottype=='SGDparams':
#    legtitle='Learning rate'
#    fitseed=0
#    batchsize=1
#    expl_values=learning_rates
#    nrep=len(SGDseeds)
#    terrbis=terr.copy()
#    verrbis=verr.copy()
#    tcorrbis=tcorr.copy()
#    vcorrbis=vcorr.copy()
#    for stimtype in stimtypes:
#        terr[stimtype]=terrbis[stimtype][fitseed,batchsize,:,:,:].copy()
#        verr[stimtype]=verrbis[stimtype][fitseed,batchsize,:,:,:].copy()
#        tcorr[stimtype]=tcorrbis[stimtype][fitseed,batchsize,:,:,:].copy()
#        vcorr[stimtype]=vcorrbis[stimtype][fitseed,batchsize,:,:,:].copy()
        
    legtitle='Batch size'
    fitseed=0
    lrate=2
    expl_values=batchsizes
    nrep=len(SGDseeds)
    terrbis=terr.copy()
    verrbis=verr.copy()
    tcorrbis=tcorr.copy()
    vcorrbis=vcorr.copy()
    for stimtype in stimtypes:
        terr[stimtype]=terrbis[stimtype][fitseed,:,lrate,:,:].copy()
        verr[stimtype]=verrbis[stimtype][fitseed,:,lrate,:,:].copy()
        tcorr[stimtype]=tcorrbis[stimtype][fitseed,:,lrate,:,:].copy()
        vcorr[stimtype]=vcorrbis[stimtype][fitseed,:,lrate,:,:].copy()        
                        

fontsize=15
linewidth=1
alpha=0.1
#marker='x'
marker=''
show_var='shade'
#show_var='plot_all'
leg=[str(ss) for ss in expl_values]


for stimtype in stimtypes:
    #t=np.arange(1,terr[stimtype].shape[2]+1)*supplparams['epochSize']
    plt.figure()
    lids=[[]]*len(expl_values)
    
    for iexpl in range(len(expl_values)):
        t=np.arange(1,totparams['numEpochs'][stimtype][iexpl]+1)*totparams['epochSize'][stimtype][iexpl]
        if plottype=='epochSize':
            tempterr=terr[stimtype][expl_values[iexpl]].reshape(1,-1)
            tempverr=verr[stimtype][expl_values[iexpl]].reshape(1,-1)
        else:
            tempterr=terr[stimtype][iexpl,:,:]
            tempverr=verr[stimtype][iexpl,:,:]          
        if show_var=='plot_all':
            for irep in range(nrep):
                plt.plot(t,tempterr[irep,:],colors[iexpl]+'--'+marker,linewidth=linewidth)
                lids[iexpl],=plt.plot(t,tempverr[irep,:],colors[iexpl]+'-'+marker,linewidth=linewidth)
        elif show_var=='shade':        
            plt.plot(t,np.nanmean(tempterr,axis=0),colors[iexpl]+'--'+marker,linewidth=linewidth)
            lids[iexpl],=plt.plot(t,np.nanmean(tempverr,axis=0),colors[iexpl]+'-'+marker,linewidth=linewidth)
            plt.fill_between(t, np.nanmean(tempterr,axis=0)-np.nanstd(tempterr,axis=0), np.nanmean(tempterr,axis=0)+np.nanstd(tempterr,axis=0),alpha=alpha,facecolor=colors[iexpl])
            plt.fill_between(t, np.nanmean(tempverr,axis=0)-np.nanstd(tempverr,axis=0), np.nanmean(tempverr,axis=0)+np.nanstd(tempverr,axis=0),alpha=alpha,facecolor=colors[iexpl])
    if plottype=='SGDparams':
#        ref_t=np.arange(1,ref_terr[stimtype].shape[1]+1)*ref_supplparams['epochSize']
#        plt.plot(ref_t,ref_terr[stimtype][fitseed,:],'k--',linewidth=linewidth*2)
#        plt.plot(ref_t,ref_verr[stimtype][fitseed,:],'k',linewidth=linewidth*2)
        plt.plot(t,np.ones(len(t))*ref_terr[stimtype][fitseed,:].min(),'k--',linewidth=linewidth*2)
        plt.plot(t,np.ones(len(t))*ref_verr[stimtype][fitseed,:].min(),'k',linewidth=linewidth*2)
        
    plt.xlabel("Nombre d'iterations",fontsize=fontsize)
    plt.ylabel('Erreur',fontsize=fontsize)
    plt.tick_params(axis='y',labelsize=fontsize)
    plt.tick_params(axis='x',labelsize=fontsize)
    if plottype in ['datalen','nOut','SGDparams','epochSize','modelseed']:
        plt.legend(lids,leg,loc='upper right',title=legtitle)
    plt.title(stimtype)
    
    
    lids=[[]]*len(expl_values)
    plt.figure()    
    for iexpl in range(len(expl_values)):
        t=np.arange(1,totparams['numEpochs'][stimtype][iexpl]+1)*totparams['epochSize'][stimtype][iexpl]
        if plottype=='epochSize':
            temptcorr=tcorr[stimtype][expl_values[iexpl]].reshape(1,-1)
            tempvcorr=vcorr[stimtype][expl_values[iexpl]].reshape(1,-1)
        else:
            temptcorr=tcorr[stimtype][iexpl,:,:]
            tempvcorr=vcorr[stimtype][iexpl,:,:]     
        if show_var=='plot_all':        
            for irep in range(nrep):
                plt.plot(t,temptcorr[irep,:],colors[iexpl]+'--'+marker,linewidth=linewidth)
                lids[iexpl],=plt.plot(t,tempvcorr[irep,:],colors[iexpl]+'-'+marker,linewidth=linewidth)        
        elif show_var=='shade':
            plt.plot(t,np.nanmean(temptcorr,axis=0),colors[iexpl]+'--'+marker,linewidth=linewidth)
            lids[iexpl],=plt.plot(t,np.nanmean(tempvcorr,axis=0),colors[iexpl]+'-'+marker,linewidth=linewidth)
            plt.fill_between(t, np.nanmean(temptcorr,axis=0)-np.nanstd(temptcorr,axis=0), np.nanmean(temptcorr,axis=0)+np.nanstd(temptcorr,axis=0),alpha=alpha,facecolor=colors[iexpl])
            plt.fill_between(t, np.nanmean(tempvcorr,axis=0)-np.nanstd(tempvcorr,axis=0), np.nanmean(tempvcorr,axis=0)+np.nanstd(tempvcorr,axis=0),alpha=alpha,facecolor=colors[iexpl])
    if plottype=='SGDparams':
#        ref_t=np.arange(1,ref_tcorr[stimtype].shape[1]+1)*ref_supplparams['epochSize']
#        plt.plot(ref_t,ref_tcorr[stimtype][fitseed,:],'k--',linewidth=linewidth*2)
#        plt.plot(ref_t,ref_vcorr[stimtype][fitseed,:],'k',linewidth=linewidth*2)
        plt.plot(t,np.ones(len(t))*ref_tcorr[stimtype][fitseed,:].max(),'k--',linewidth=linewidth*2)
        plt.plot(t,np.ones(len(t))*ref_vcorr[stimtype][fitseed,:].max(),'k',linewidth=linewidth*2)

    plt.xlabel("Nombre d'iterations",fontsize=fontsize)
    plt.ylabel('Correlation entre reponse predite et mesuree',fontsize=fontsize)
    plt.tick_params(axis='y',labelsize=fontsize)
    plt.tick_params(axis='x',labelsize=fontsize)
    if plottype in ['datalen','nOut','SGDparams','epochSize','modelseed']:
        plt.legend(lids,leg,loc='lower right',title=legtitle)
    plt.title(stimtype)
    #plt.axis([plt.axis()[0],plt.axis()[1],0.8,0.97])
 
#    plt.figure()
#    plt.hist(tcorr[stimtype][:,:,-1].mean(axis=1),30)
#    plt.hist(vcorr[stimtype][:,:,-1].mean(axis=1),30)

   
#    plt.figure()
##    plt.plot(expl_values,terr[stimtype][:,:,-1].mean(axis=1),'b--')
##    plt.plot(expl_values,verr[stimtype][:,:,-1].mean(axis=1),'b')
#    plt.errorbar(expl_values,terr[stimtype][:,:,-1].mean(axis=1),yerr=terr[stimtype][:,:,-1].std(axis=1),fmt='--b',linewidth=linewidth)
#    plt.errorbar(expl_values,verr[stimtype][:,:,-1].mean(axis=1),yerr=verr[stimtype][:,:,-1].std(axis=1),fmt='b',linewidth=linewidth)
#    plt.ylabel('Final error (negative log-likelihood)',color='b')
#    plt.twinx()
##    plt.plot(expl_values,tcorr[stimtype][:,:,-1].mean(axis=1),'r--')
##    plt.plot(expl_values,vcorr[stimtype][:,:,-1].mean(axis=1),'r')
#    plt.errorbar(expl_values,tcorr[stimtype][:,:,-1].mean(axis=1),yerr=tcorr[stimtype][:,:,-1].std(axis=1),fmt='--r',linewidth=linewidth)
#    plt.errorbar(expl_values,vcorr[stimtype][:,:,-1].mean(axis=1),yerr=vcorr[stimtype][:,:,-1].std(axis=1),fmt='r',linewidth=linewidth)    
#    plt.ylabel('Final correlation between predicted and actual response',color='r')
#    plt.xlabel('Data size (number of stimulus frames)')
##    lims=plt.axis()
##    plt.axis([-200,expl_values[-1],lims[2],lims[3]])
#    plt.title(stimtype)