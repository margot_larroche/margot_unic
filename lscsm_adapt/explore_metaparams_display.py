from explore_metaparams_funcs import *
import param
from utils.param_gui_tools import *
from os.path import basename, dirname
from os.path import join as pathjoin
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from lscsm.checkpointing import loadErrCorr

main_path='/media/margot/DATA/Margot/ownCloud/'

class display_expl_params(param.Parameterized):
    
    data_file=param.Filename(default=None,allow_None=True,search_paths=['/home/margot/HSM/Data',main_path+'DATA/extracell_data/python_data',main_path+'DATA/extracell_data/python_data/excluded_from_owncloud'],doc='Data file on which the exploration has been done')
    exploration_dirs=param.List(default=[main_path+'ANALYSIS/HSM_analysis/HSM_exploration_files'])
    perf_measures=param.ListSelector(default=['correlation'], objects=['correlation','log-likelihood'], doc="Which performance measure(s) to use")
    collapse_func=param.ObjectSelector(default='max', objects=['mean','min','max'])
    to_plot=param.ListSelector(default=['best_params','partial_mats','num_units','perf_curve'], objects=['best_params','partial_mats','num_units','perf_curve'])
    n_best=param.Integer(default=10,doc="Number of best param sets to display")
   
display_order={'PARAMETERS': ['data_file','exploration_dirs','perf_measures','collapse_func','to_plot','n_best']}
 
 
try: pm
except NameError: pm=display_expl_params()
if (len(sys.argv)<2):    
    pm,validated=param_dialog(pm,display_order)
else:
    if sys.argv[1]!='previous_params':
        pm=display_expl_params(**correct_inputs(retrieveParams(sys.argv[1],raise_error=True),pm))
    validated=True    
if validated:
    print 'PARAMETERS : \n\n'+param_list_string(pm,display_order)+'\n\n'

    collapse_func=eval('np.nan'+pm.collapse_func)
    for var_name in ['perf_mat','best_params','best_perf']:
        try: vars()[var_name]
        except KeyError: vars()[var_name] = {}               
    expl_names=[pathjoin(expl_dir,basename(pm.data_file).split('.')[0])+'_' for expl_dir in pm.exploration_dirs]
    pranges,name_mat=reconstruct_pranges(expl_names)
    print '\nAvailable results :', (name_mat!='').sum(), '/', np.prod(name_mat.shape), '\n'
    
    
    for perf_m in pm.perf_measures:        
        print perf_m+'\n'        
        perf_mat[perf_m]=compute_perfmat(pranges,name_mat,data_path=dirname(pm.data_file),measure=perf_m)
    
    for perf_m in perf_mat.keys():
        
        if 'best_params' in pm.to_plot:
            if perf_m=='correlation':
                sort_order=-1
            else:
                sort_order=1
            best_params[perf_m],best_perf[perf_m]=bestParamSets(createCombinations(pranges,''),perf_mat[perf_m],order=sort_order)
            #print '5 best parameters sets:', '\n'+'\n'.join([str(bp) for bp in best_params[perf_m][:5]])
            #print 'Corresponding values:', ' '.join([str(bp) for bp in best_perf[perf_m][:5]])
            param_str='\n'+' '.join(best_params[perf_m][0].keys()+[perf_m])+'\n'
            for iset in range(min(pm.n_best,len(best_params[perf_m]))):
                param_str+='\n'+'\t'.join([str(val) for val in best_params[perf_m][iset].values()]+[str(best_perf[perf_m][iset])])
            print param_str        

        if 'partial_mats' in pm.to_plot:
            redmats,red_pranges=compute_partial_perfs(perf_mat[perf_m],pranges,to_ignore=['seed'],collapse_func=collapse_func)
            partialperf_colormap(redmats,red_pranges,forcescale=None)
            
        if 'num_units' in pm.to_plot:
            to_ignore=list(set(pranges.keys())-set(['num_lgn','num_hidden']))
            assert len(pranges)==len(to_ignore)+2
            redmats,red_pranges=compute_partial_perfs(perf_mat[perf_m],pranges,to_ignore=to_ignore,collapse_func=collapse_func)
            #partialperf_colormap(redmats,red_pranges,forcescale=None)
            ny,nx=redmats[0].shape
            fig=plt.figure();
            ax=fig.gca(projection='3d')
            ax.plot(range(nx)*ny,np.arange(ny).reshape(ny,1).repeat(nx,axis=1).flatten(),redmats[0].flatten(),'o')
            plt.yticks(range(ny),red_pranges.values()[0])
            plt.xticks(range(nx),red_pranges.values()[1])
            plt.ylabel(red_pranges.keys()[0])
            plt.xlabel(red_pranges.keys()[1])
            ax.set_zlabel(perf_m)
            
        if 'perf_curve' in pm.to_plot:
            n_rep=5
            if perf_m=='correlation':
                suffix='corr'
            else:
                suffix='error'
            time_fig=plt.figure()
            files=name_mat[name_mat!=''].flatten()
            short_curves=np.zeros([n_rep+1,len(files)])+np.nan
            for i_file in range(len(files)):
                _,full_curve=loadErrCorr(files[i_file][:-15]+suffix)
                short_length=min(n_rep,len(full_curve))
                short_curves[:short_length,i_file]=full_curve[:short_length]
                short_curves[-1,i_file]=full_curve[-1]
                plt.plot(full_curve)
            
            rep_fig=plt.figure()
            for i_rep in range(n_rep):
                plt.plot(short_curves[i_rep],short_curves[-1],'o')
                
                
            
        