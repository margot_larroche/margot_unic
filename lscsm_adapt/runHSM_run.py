from runHSM_funcs import runHSM, HSM_suppl_params as sp_updating
import param
from utils.param_gui_tools import *
from lscsm.checkpointing import loadParams
from os.path import isfile, dirname, basename
from lscsm.LSCSM import LSCSM
from time import time
from datetime import timedelta
from sys import setrecursionlimit
from glob import glob
from traceback import format_exc
setrecursionlimit(2000)


class runHSM_params(param.Parameterized):
    param_file=param.Filename(search_paths=['/media/margot/DATA/Margot/ownCloud/ANALYSIS/HSM_analysis/HSM_fitting_files','/home/margot/HSM/Fitting_results'],doc='Parameter file of the fit')    
    whole_dir=param.Boolean(default=False)
    file_number_list=param.List(default=[], doc='List of param files (numbers) to consider in the directory')
    reset_K=param.Boolean(default=True)
    update_params=param.Boolean(default=False)    
    compute_corr=param.Boolean(default=True)
    dialog_maxnrows=param.Integer(default=30)
        
display_order={'SCRIPT PARAMETERS': ['param_file','whole_dir','file_number_list','reset_K','update_params','compute_corr','dialog_maxnrows']}

class mp_updating(LSCSM):
    name=param.String(default='', doc="Tag to add to the files that will be created by the optimization function")
    def __init__(self,**params):
        param.Parameterized.__init__(self,**params)
    
#class sp_updating(param.Parameterized):
#    datafilename=param.Filename(default='2916_CXRIGHT_TUN15_croppedstim.mat',search_paths=['/home/margot/HSM/Data','/media/margot/DATA/Margot/ownCloud/Data/Extracellular_elphy_data','/media/margot/DATA/Margot/Extracell_data/HSM_format','/Users/margot/ownCloud/MargotUNIC/Data/Extracellular_elphy_data'])
#    epochSize=param.Integer(default=1000)
#    norm_stim=param.Boolean(default=True)
#    numEpochs=param.Integer(default=1)
#    seed=param.Integer(default=0)
#    stim_seed=param.Integer(default=0,allow_None=True)
#    tau=param.Integer(default=0)
#    reg_frac=param.Number(default=0.2)
#    val_frac=param.Number(default=0)
#    cell_nums=param.List(default=[], doc="List of cells to include (default: all)")   
#    pruned=param.Boolean(default=False)
#    K_mask=param.Filename(default=None,allow_None=True,search_paths=['/media/margot/DATA/Margot/ownCloud/Data/LSCSM_data/FittingResults','/home/margot/HSM/Fitting_results','/Users/margot/ownCloud/MargotUNIC/Data/LSCSM_data/FittingResults'])
#    n_stuck_iter=param.Integer(default=None, allow_None=True)
#    n_rep=param.Integer(default=0,doc="Number of already runned iterations (for cases were K is not reset)")

try: pm
except NameError: pm=runHSM_params()
if (len(sys.argv)<2):    
    pm,validated=param_dialog(pm,display_order,max_nrows=pm.dialog_maxnrows)
else:
    if sys.argv[1]!='previous_params':
        if '=' in sys.argv[1]:
            pm=runHSM_params(**correct_inputs(dict([inpt.split('=') for inpt in sys.argv[1:]]),pm))
        else:    
            pm=runHSM_params(**correct_inputs(retrieve_params(sys.argv[1],raise_error=True),pm))
    validated=True  
    
if validated:
    print '\nPARAMETERS : \n\n'+param_list_string(pm,display_order)+'\n\n'

    if pm.whole_dir:
        already_run=glob(dirname(pm.param_file)+'/*_BEST_metaparams')
        to_ignore=[name[:-15]+'metaparams' for name in already_run]
        parameter_files=sorted(list(set(glob(dirname(pm.param_file)+'/*_metaparams'))-set(already_run+to_ignore)))
        if len(pm.file_number_list)>0:
            parameter_files=[parameter_files[i] for i in pm.file_number_list if i<len(parameter_files)]
    else:
        parameter_files=[pm.param_file]
        
    for pfile in parameter_files:   
        print 'Running', pfile, '\n'
        
        mp_vals,sp_vals=loadParams(pfile)
        mp=mp_updating()
        sp=sp_updating()
        mp=mp_updating(**correct_inputs({key: mp_vals[key] for key in (set(mp.params().keys()) & set(mp_vals.keys()))},mp))
        sp=sp_updating(**correct_inputs({key: sp_vals[key] for key in (set(sp.params().keys()) & set(sp_vals.keys()))},sp))
        if pm.update_params:        
            mp=param_dialog(mp,max_nrows=pm.dialog_maxnrows)[0]
            sp=param_dialog(sp,max_nrows=pm.dialog_maxnrows)[0]
        
        # Issue warning if program is going to overwrite previous data 
        expected_prefix=dirname(pfile)+'/'+basename(sp.datafilename[:-4])+'_'+mp.name
        if isfile(expected_prefix+'_BEST_metaparams'):    
            print 'WARNING: name ' + mp.name + ' has already been used. You can type a new one or press ENTER to proceed.'
            new_name=raw_input('NEW NAME :')
            if len(new_name)>0:
                mp.name=new_name
            
        print 'HSM META-PARAMETERS :\n'+param_list_string(mp)+'\n\n'
        print 'HSM SUPPLEMENTARY PARAMETERS :\n'+param_list_string(sp)+'\n\n'
        
        t0=time()
        try:
            K,lscsm,terr,verr,tcorr,vcorr,error_msg=runHSM(prevrunfile=pfile[:-11],resetK=pm.reset_K,meta_params=dict(mp.get_param_values()),suppl_params=dict(sp.get_param_values()),compCorr=pm.compute_corr)
        except:
            print format_exc()# If there was some error, print its description            
        print '\nTime elapsed : '+str(timedelta(seconds=int(time()-t0)))