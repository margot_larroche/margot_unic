import numpy as np
import matplotlib.pyplot as plt

tau=20
ampl=5

model_variables={'stim':np.zeros(300)}
model_variables['stim'][100:200]=1
init_bias=0
weight=1
history_location='output'
bias_shape='multiplicative'
plot_range=[0,300]#[100,104]

def bias_iteration(prev_bias,prev_signal,history_loc='input',shape='divisive'):
    if history_loc=='input':
        return np.exp(-1./tau)*prev_bias + ampl*prev_signal
    elif history_loc=='output':
        return np.exp(-1./tau)*prev_bias + ampl*adapted_weight(weight,prev_bias,shape)*prev_signal

def adapted_weight(baseline_w,w_bias,shape='divisive'):
    if shape=='divisive':
        return baseline_w*1./(1+w_bias)
    elif shape=='multiplicative':
        return baseline_w*np.maximum(1-w_bias,0)

model_variables['bias']=np.zeros(len(model_variables['stim']))
model_variables['bias'][0]=init_bias
for t in range(len(model_variables['stim'])-1):
    model_variables['bias'][t+1]=bias_iteration(model_variables['bias'][t],model_variables['stim'][t],history_location,bias_shape)
model_variables['adapted_weight']=adapted_weight(weight,model_variables['bias'],bias_shape)    
model_variables['output']=model_variables['stim']*model_variables['adapted_weight'] 

plt.figure()
var_names=model_variables.keys()
count=0
for mv in var_names:
    count+=1
    plt.subplot(len(var_names),1,count)
    plt.plot(model_variables[mv][plot_range[0]:plot_range[1]])
    plt.title(mv)